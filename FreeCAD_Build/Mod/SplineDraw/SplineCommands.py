# -*- coding: utf-8 -*-
# ***************************************************************************
# *                                                                         *
# *   Copyright (c) 2016 sliptonic <shopinthewoods@gmail.com>               *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************
import os.path
import FreeCAD
import Path
import PathScripts
import PathScripts.PathLog as PathLog
import traceback
import FreeCAD
import Draft
import Show

#import sys
#sys.path.append("D:\\SoftwareInstalled\\Python37\\Lib\\site-packages")
# request, urllib3, chardet, certifi, idna

import base64
import io
import requests
import argparse
import json
import os

from PathScripts.PathUtils import loopdetect
from PathScripts.PathUtils import horizontalEdgeLoop
from PathScripts.PathUtils import horizontalFaceLoop
from PathScripts.PathUtils import addToJob
from PathScripts.PathUtils import findParentJob

if FreeCAD.GuiUp:
    import FreeCADGui
    from PySide import QtCore
    from PySide import QtGui
else:
    def translate(ctxt, txt):
        return txt

__title__ = "FreeCAD Path Commands"
__author__ = "sliptonic"
__url__ = "http://www.freecadweb.org"
url = "http://13.58.166.91:3000/"
splineEndPoint = "process_denture"
holeEndpoint = "find_holes"
file_extension = 'csv'

class _SplineCommand:
    "the Path command to complete loop selection definition"

    def __init__(self):
        self.obj = None
        self.sub = []
        self.active = False

    def GetResources(self):
        return {'Pixmap': 'Spline_Draw',
                'MenuText': QtCore.QT_TRANSLATE_NOOP("Spline_Draw", "Spline Draw"),
                'Accel': "P, L",
                'ToolTip': QtCore.QT_TRANSLATE_NOOP("Spline_Draw", "Complete loop selection from two edges"),
                'CmdType': "ForEdit"}

    def IsActive(self):
        return True
        # if bool(FreeCADGui.Selection.getSelection()) is False:
        #     return False
        # try:
        #     obj = FreeCADGui.Selection.getSelectionEx()[0].Object
        #     return isinstance(obj.Proxy, PathScripts.PathOp.ObjectOp)
        # except(IndexError, AttributeError):
        #     return False

    def to_csv(self, data, file_path):
        decrypted = base64.b64decode(data)
        response_file = file_path.split('.')[0] + '.' + file_extension
        with open(response_file,"wb") as f:
            f.write(decrypted)
            print("Response details written in file : " + f.name)
            arr = decrypted.decode("utf-8")
            pointsArr = arr.split("\n")
            return pointsArr


    def get_stl_file(self):
        parser = argparse.ArgumentParser(description='STL curvature.')
        parser.add_argument('-i', '--input', nargs='?', required=True,
                        help='Input STL file | Directory')
        args = parser.parse_args()
        return os.path.abspath(args.input)


    def send_file(self, encoded_data, file_extension, endPoint):
        headers = {
            'Content-Type':  'application/json'
        }

        body = {
            'fileContent':encoded_data
        }

        response = requests.post(url+endPoint, headers=headers, json=body)
        print(response.status_code)
        FreeCAD.Console.PrintWarning(response.status_code)
        return response.text

    def start_test(self, file_path, endPoint):
        print('File Path : ')
        print(file_path)
        df = open(file_path, "rb")
        encoded = base64.b64encode(df.read())
        base64_string = encoded.decode('utf-8')
        res = self.send_file(base64_string, file_extension, endPoint)
        return res

    def Activated(self):
        #Msg('Start Spline draw... done\n')
        #FreeCAD.Console.PrintWarning(FreeCAD.ActiveDocument.Objects)
        objToRotate1 = FreeCAD.ActiveDocument.Objects[0]
        label = FreeCAD.ActiveDocument.Objects[1].Label
        label2 = FreeCAD.ActiveDocument.Objects[1].Label2

        self.pointsArr = []
        self.holePoints = {}
        self.splinePoints = []
        input_arg = label2
        #"D:\\Mastercam\\SPLineAutomation\\server\\test\\models_for_AI_2\\19-0002 U0.stl"
        if os.path.isdir(input_arg) :
            for filename in os.listdir(input_arg):
                abs_path = os.path.abspath(input_arg + os.sep + filename)
                if filename.endswith(".stl"):
                    res = self.start_test(abs_path, splineEndPoint)                    
                    self.pointsArr = self.to_csv(res, abs_path)
                    self.holePoints = self.start_test(abs_path, holeEndpoint)
                    continue
                else:
                    continue
        else:
            res = self.start_test(input_arg, splineEndPoint)                    
            self.pointsArr = self.to_csv(res, input_arg)
            self.holePoints = self.start_test(input_arg, holeEndpoint)

        for value in self.pointsArr:
            points = value.split(",")
            if len(points) == 3 :
                self.splinePoints.append(FreeCAD.Vector(float(points[0]),float(points[1]),float(points[2])))

        BSpline1 = Draft.makeWire(self.splinePoints, closed=True)
        print(self.holePoints)
        """ o = FreeCAD.ActiveDocument.addObject("Path::Feature","myPath2")
        o.Path = Path.fromShape(BSpline1)
        FreeCAD.ActiveDocument.recompute()
        p =  o.Path
        FreeCAD.Console.PrintWarning("Gcode " + p.toGCode()) """

        """objToRotate2 = FreeCAD.ActiveDocument.Objects[2]
        FreeCAD.Console.PrintWarning(FreeCAD.ActiveDocument.Objects[2].Label)

        pl = objToRotate1.Placement
        pl.rotate((0,0,0),(1,0,0),90)
        
        pl = objToRotate2.Placement
        pl.rotate((0,0,0),(1,0,0),90)
        
        # Denture *605 U12 : Start
        Draft.move(objToRotate1, FreeCAD.Vector(-1.3599653244018555, 1.308645248413086, 0.0), copy=False)
        Draft.move(objToRotate2, FreeCAD.Vector(-1.3599653244018555, 1.308645248413086, 0.0), copy=False)
        objToRotate1.Placement.move(FreeCAD.Vector(0.0,0,-24.34))
        objToRotate2.Placement.move(FreeCAD.Vector(0.0,0,-24.34))
        # Denture *605 U12 : End     """
        
        
        #obj.Active = not(obj.Active)
        FreeCAD.ActiveDocument.recompute()

if FreeCAD.GuiUp:
    FreeCADGui.addCommand('Spline_Draw', _SplineCommand())
