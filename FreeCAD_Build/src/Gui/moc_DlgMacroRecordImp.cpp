/****************************************************************************
** Meta object code from reading C++ file 'DlgMacroRecordImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/DlgMacroRecordImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgMacroRecordImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__DlgMacroRecordImp_t {
    QByteArrayData data[7];
    char stringdata0[165];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DlgMacroRecordImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DlgMacroRecordImp_t qt_meta_stringdata_Gui__Dialog__DlgMacroRecordImp = {
    {
QT_MOC_LITERAL(0, 0, 30), // "Gui::Dialog::DlgMacroRecordImp"
QT_MOC_LITERAL(1, 31, 22), // "on_buttonStart_clicked"
QT_MOC_LITERAL(2, 54, 0), // ""
QT_MOC_LITERAL(3, 55, 21), // "on_buttonStop_clicked"
QT_MOC_LITERAL(4, 77, 23), // "on_buttonCancel_clicked"
QT_MOC_LITERAL(5, 101, 30), // "on_pushButtonChooseDir_clicked"
QT_MOC_LITERAL(6, 132, 32) // "on_lineEditMacroPath_textChanged"

    },
    "Gui::Dialog::DlgMacroRecordImp\0"
    "on_buttonStart_clicked\0\0on_buttonStop_clicked\0"
    "on_buttonCancel_clicked\0"
    "on_pushButtonChooseDir_clicked\0"
    "on_lineEditMacroPath_textChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DlgMacroRecordImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x09 /* Protected */,
       3,    0,   40,    2, 0x09 /* Protected */,
       4,    0,   41,    2, 0x09 /* Protected */,
       5,    0,   42,    2, 0x09 /* Protected */,
       6,    1,   43,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,

       0        // eod
};

void Gui::Dialog::DlgMacroRecordImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgMacroRecordImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_buttonStart_clicked(); break;
        case 1: _t->on_buttonStop_clicked(); break;
        case 2: _t->on_buttonCancel_clicked(); break;
        case 3: _t->on_pushButtonChooseDir_clicked(); break;
        case 4: _t->on_lineEditMacroPath_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DlgMacroRecordImp::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DlgMacroRecordImp.data,
    qt_meta_data_Gui__Dialog__DlgMacroRecordImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DlgMacroRecordImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DlgMacroRecordImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DlgMacroRecordImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui_DlgMacroRecord"))
        return static_cast< Ui_DlgMacroRecord*>(this);
    if (!strcmp(_clname, "Gui::WindowParameter"))
        return static_cast< Gui::WindowParameter*>(this);
    return QDialog::qt_metacast(_clname);
}

int Gui::Dialog::DlgMacroRecordImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
