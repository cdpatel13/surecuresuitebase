/********************************************************************************
** Form generated from reading UI file 'DlgGeneral.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGGENERAL_H
#define UI_DLGGENERAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

namespace Gui {
namespace Dialog {

class Ui_DlgGeneral
{
public:
    QGridLayout *gridLayout;
    QGroupBox *GroupBox5;
    QVBoxLayout *verticalLayout;
    QGroupBox *GroupBox7;
    QGridLayout *gridLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *TextLabel1_4;
    QComboBox *Languages;
    QGroupBox *GroupBox3;
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout1;
    QLabel *recentFileListLabel;
    QSpacerItem *spacerItem;
    Gui::PrefSpinBox *RecentFiles;
    QHBoxLayout *hboxLayout2;
    QCheckBox *tiledBackground;
    QHBoxLayout *hboxLayout3;
    QLabel *styleSheetLabel;
    QComboBox *StyleSheets;
    QHBoxLayout *hboxLayout4;
    QLabel *iconSizeLabel;
    QComboBox *toolbarIconSize;
    QHBoxLayout *hboxLayout5;
    QLabel *treeModeLabel;
    QComboBox *treeMode;
    QGroupBox *GroupBox10;
    QGridLayout *gridLayout2;
    Gui::PrefCheckBox *SplashScreen;
    QGridLayout *gridLayout3;
    QLabel *autoModuleLabel;
    QComboBox *AutoloadModuleCombo;
    QGroupBox *GroupBox11;
    QGridLayout *_4;
    Gui::PrefCheckBox *PythonWordWrap;
    QSpacerItem *spacerItem1;

    void setupUi(QWidget *Gui__Dialog__DlgGeneral)
    {
        if (Gui__Dialog__DlgGeneral->objectName().isEmpty())
            Gui__Dialog__DlgGeneral->setObjectName(QString::fromUtf8("Gui__Dialog__DlgGeneral"));
        Gui__Dialog__DlgGeneral->resize(425, 578);
        gridLayout = new QGridLayout(Gui__Dialog__DlgGeneral);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(9, 9, 9, 9);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        GroupBox5 = new QGroupBox(Gui__Dialog__DlgGeneral);
        GroupBox5->setObjectName(QString::fromUtf8("GroupBox5"));
        verticalLayout = new QVBoxLayout(GroupBox5);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        GroupBox7 = new QGroupBox(GroupBox5);
        GroupBox7->setObjectName(QString::fromUtf8("GroupBox7"));
        gridLayout1 = new QGridLayout(GroupBox7);
        gridLayout1->setSpacing(6);
        gridLayout1->setContentsMargins(11, 11, 11, 11);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        TextLabel1_4 = new QLabel(GroupBox7);
        TextLabel1_4->setObjectName(QString::fromUtf8("TextLabel1_4"));

        hboxLayout->addWidget(TextLabel1_4);

        Languages = new QComboBox(GroupBox7);
        Languages->setObjectName(QString::fromUtf8("Languages"));

        hboxLayout->addWidget(Languages);


        gridLayout1->addLayout(hboxLayout, 0, 0, 1, 1);


        verticalLayout->addWidget(GroupBox7);

        GroupBox3 = new QGroupBox(GroupBox5);
        GroupBox3->setObjectName(QString::fromUtf8("GroupBox3"));
        vboxLayout = new QVBoxLayout(GroupBox3);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        recentFileListLabel = new QLabel(GroupBox3);
        recentFileListLabel->setObjectName(QString::fromUtf8("recentFileListLabel"));

        hboxLayout1->addWidget(recentFileListLabel);

        spacerItem = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem);

        RecentFiles = new Gui::PrefSpinBox(GroupBox3);
        RecentFiles->setObjectName(QString::fromUtf8("RecentFiles"));
        RecentFiles->setValue(4);
        RecentFiles->setProperty("prefEntry", QVariant(QByteArray("RecentFiles")));
        RecentFiles->setProperty("prefPath", QVariant(QByteArray("RecentFiles")));

        hboxLayout1->addWidget(RecentFiles);


        vboxLayout->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        tiledBackground = new QCheckBox(GroupBox3);
        tiledBackground->setObjectName(QString::fromUtf8("tiledBackground"));

        hboxLayout2->addWidget(tiledBackground);


        vboxLayout->addLayout(hboxLayout2);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        styleSheetLabel = new QLabel(GroupBox3);
        styleSheetLabel->setObjectName(QString::fromUtf8("styleSheetLabel"));

        hboxLayout3->addWidget(styleSheetLabel);

        StyleSheets = new QComboBox(GroupBox3);
        StyleSheets->setObjectName(QString::fromUtf8("StyleSheets"));

        hboxLayout3->addWidget(StyleSheets);


        vboxLayout->addLayout(hboxLayout3);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setSpacing(6);
        hboxLayout4->setContentsMargins(0, 0, 0, 0);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        iconSizeLabel = new QLabel(GroupBox3);
        iconSizeLabel->setObjectName(QString::fromUtf8("iconSizeLabel"));

        hboxLayout4->addWidget(iconSizeLabel);

        toolbarIconSize = new QComboBox(GroupBox3);
        toolbarIconSize->setObjectName(QString::fromUtf8("toolbarIconSize"));

        hboxLayout4->addWidget(toolbarIconSize);


        vboxLayout->addLayout(hboxLayout4);

        hboxLayout5 = new QHBoxLayout();
        hboxLayout5->setSpacing(6);
        hboxLayout5->setContentsMargins(0, 0, 0, 0);
        hboxLayout5->setObjectName(QString::fromUtf8("hboxLayout5"));
        treeModeLabel = new QLabel(GroupBox3);
        treeModeLabel->setObjectName(QString::fromUtf8("treeModeLabel"));

        hboxLayout5->addWidget(treeModeLabel);

        treeMode = new QComboBox(GroupBox3);
        treeMode->setObjectName(QString::fromUtf8("treeMode"));

        hboxLayout5->addWidget(treeMode);


        vboxLayout->addLayout(hboxLayout5);


        verticalLayout->addWidget(GroupBox3);

        GroupBox10 = new QGroupBox(GroupBox5);
        GroupBox10->setObjectName(QString::fromUtf8("GroupBox10"));
        gridLayout2 = new QGridLayout(GroupBox10);
        gridLayout2->setSpacing(6);
        gridLayout2->setContentsMargins(11, 11, 11, 11);
        gridLayout2->setObjectName(QString::fromUtf8("gridLayout2"));
        SplashScreen = new Gui::PrefCheckBox(GroupBox10);
        SplashScreen->setObjectName(QString::fromUtf8("SplashScreen"));
        SplashScreen->setChecked(true);
        SplashScreen->setProperty("prefEntry", QVariant(QByteArray("ShowSplasher")));
        SplashScreen->setProperty("prefPath", QVariant(QByteArray("General")));

        gridLayout2->addWidget(SplashScreen, 2, 0, 1, 1);

        gridLayout3 = new QGridLayout();
        gridLayout3->setSpacing(6);
        gridLayout3->setContentsMargins(0, 0, 0, 0);
        gridLayout3->setObjectName(QString::fromUtf8("gridLayout3"));
        autoModuleLabel = new QLabel(GroupBox10);
        autoModuleLabel->setObjectName(QString::fromUtf8("autoModuleLabel"));

        gridLayout3->addWidget(autoModuleLabel, 0, 0, 1, 1);

        AutoloadModuleCombo = new QComboBox(GroupBox10);
        AutoloadModuleCombo->setObjectName(QString::fromUtf8("AutoloadModuleCombo"));

        gridLayout3->addWidget(AutoloadModuleCombo, 0, 1, 1, 1);


        gridLayout2->addLayout(gridLayout3, 0, 0, 1, 1);


        verticalLayout->addWidget(GroupBox10);

        GroupBox11 = new QGroupBox(GroupBox5);
        GroupBox11->setObjectName(QString::fromUtf8("GroupBox11"));
        _4 = new QGridLayout(GroupBox11);
        _4->setSpacing(6);
        _4->setContentsMargins(11, 11, 11, 11);
        _4->setObjectName(QString::fromUtf8("_4"));
        PythonWordWrap = new Gui::PrefCheckBox(GroupBox11);
        PythonWordWrap->setObjectName(QString::fromUtf8("PythonWordWrap"));
        PythonWordWrap->setChecked(true);
        PythonWordWrap->setProperty("prefEntry", QVariant(QByteArray("PythonWordWrap")));
        PythonWordWrap->setProperty("prefPath", QVariant(QByteArray("General")));

        _4->addWidget(PythonWordWrap, 0, 0, 1, 1);


        verticalLayout->addWidget(GroupBox11);

        spacerItem1 = new QSpacerItem(352, 221, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(spacerItem1);


        gridLayout->addWidget(GroupBox5, 0, 0, 1, 1);

        QWidget::setTabOrder(Languages, RecentFiles);
        QWidget::setTabOrder(RecentFiles, SplashScreen);
        QWidget::setTabOrder(SplashScreen, PythonWordWrap);

        retranslateUi(Gui__Dialog__DlgGeneral);

        QMetaObject::connectSlotsByName(Gui__Dialog__DlgGeneral);
    } // setupUi

    void retranslateUi(QWidget *Gui__Dialog__DlgGeneral)
    {
        Gui__Dialog__DlgGeneral->setWindowTitle(QApplication::translate("Gui::Dialog::DlgGeneral", "General", nullptr));
        GroupBox5->setTitle(QApplication::translate("Gui::Dialog::DlgGeneral", "General", nullptr));
        GroupBox7->setTitle(QApplication::translate("Gui::Dialog::DlgGeneral", "Language", nullptr));
        TextLabel1_4->setText(QApplication::translate("Gui::Dialog::DlgGeneral", "Change language:", nullptr));
#ifndef QT_NO_TOOLTIP
        Languages->setToolTip(QApplication::translate("Gui::Dialog::DlgGeneral", "Language of the application's user interface", nullptr));
#endif // QT_NO_TOOLTIP
        GroupBox3->setTitle(QApplication::translate("Gui::Dialog::DlgGeneral", "Main window", nullptr));
        recentFileListLabel->setText(QApplication::translate("Gui::Dialog::DlgGeneral", "Size of recent file list", nullptr));
#ifndef QT_NO_TOOLTIP
        RecentFiles->setToolTip(QApplication::translate("Gui::Dialog::DlgGeneral", "How many files should be listed in recent files list", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        tiledBackground->setToolTip(QApplication::translate("Gui::Dialog::DlgGeneral", "Background of the main window will consist of tiles of a special image.\n"
"See the FreeCAD Wiki for details about the image.", nullptr));
#endif // QT_NO_TOOLTIP
        tiledBackground->setText(QApplication::translate("Gui::Dialog::DlgGeneral", "Enable tiled background", nullptr));
        styleSheetLabel->setText(QApplication::translate("Gui::Dialog::DlgGeneral", "Style sheet:", nullptr));
#ifndef QT_NO_TOOLTIP
        StyleSheets->setToolTip(QApplication::translate("Gui::Dialog::DlgGeneral", "Style sheet how user interface will look like", nullptr));
#endif // QT_NO_TOOLTIP
        iconSizeLabel->setText(QApplication::translate("Gui::Dialog::DlgGeneral", "Size of toolbar icons:", nullptr));
#ifndef QT_NO_TOOLTIP
        toolbarIconSize->setToolTip(QApplication::translate("Gui::Dialog::DlgGeneral", "Choose your preference for toolbar icon size. You can adjust\n"
"this according to your screen size or personal taste", nullptr));
#endif // QT_NO_TOOLTIP
        treeModeLabel->setText(QApplication::translate("Gui::Dialog::DlgGeneral", "Tree view mode:", nullptr));
#ifndef QT_NO_TOOLTIP
        treeMode->setToolTip(QApplication::translate("Gui::Dialog::DlgGeneral", "Tree View Mode provides the option the option to\n"
"customize the properties panel. The options are:\n"
"1. 'Combiview'\n"
"2. 'TreeView + PropertyView\n"
"3. 'Both'", nullptr));
#endif // QT_NO_TOOLTIP
        GroupBox10->setTitle(QApplication::translate("Gui::Dialog::DlgGeneral", "Start up", nullptr));
#ifndef QT_NO_TOOLTIP
        SplashScreen->setToolTip(QApplication::translate("Gui::Dialog::DlgGeneral", "A Splash screen is a small loading window that is shown\n"
"when FreeCAD is launching. If this option is checked, FreeCAD will\n"
"display the splash screen", nullptr));
#endif // QT_NO_TOOLTIP
        SplashScreen->setText(QApplication::translate("Gui::Dialog::DlgGeneral", "Enable splash screen at start up", nullptr));
        autoModuleLabel->setText(QApplication::translate("Gui::Dialog::DlgGeneral", "Auto load module after start up:", nullptr));
#ifndef QT_NO_TOOLTIP
        AutoloadModuleCombo->setToolTip(QApplication::translate("Gui::Dialog::DlgGeneral", "Choose which workbench will be activated and shown\n"
"after FreeCAD launches", nullptr));
#endif // QT_NO_TOOLTIP
        GroupBox11->setTitle(QApplication::translate("Gui::Dialog::DlgGeneral", "Python console", nullptr));
#ifndef QT_NO_TOOLTIP
        PythonWordWrap->setToolTip(QApplication::translate("Gui::Dialog::DlgGeneral", "Words will be wrapped when they exceed available\n"
"horizontal space in Python console", nullptr));
#endif // QT_NO_TOOLTIP
        PythonWordWrap->setText(QApplication::translate("Gui::Dialog::DlgGeneral", "Enable word wrap", nullptr));
    } // retranslateUi

};

} // namespace Dialog
} // namespace Gui

namespace Gui {
namespace Dialog {
namespace Ui {
    class DlgGeneral: public Ui_DlgGeneral {};
} // namespace Ui
} // namespace Dialog
} // namespace Gui

#endif // UI_DLGGENERAL_H
