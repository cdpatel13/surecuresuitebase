/********************************************************************************
** Form generated from reading UI file 'DlgSettingsUnits.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGSUNITS_H
#define UI_DLGSETTINGSUNITS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace Gui {
namespace Dialog {

class Ui_DlgSettingsUnits
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *GroupBox6;
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QSpinBox *spinBoxDecimals;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *comboBox_ViewSystem;
    QTableWidget *tableWidget;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QComboBox *comboBox_FracInch;

    void setupUi(QWidget *Gui__Dialog__DlgSettingsUnits)
    {
        if (Gui__Dialog__DlgSettingsUnits->objectName().isEmpty())
            Gui__Dialog__DlgSettingsUnits->setObjectName(QString::fromUtf8("Gui__Dialog__DlgSettingsUnits"));
        Gui__Dialog__DlgSettingsUnits->resize(484, 388);
        verticalLayout_2 = new QVBoxLayout(Gui__Dialog__DlgSettingsUnits);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        GroupBox6 = new QGroupBox(Gui__Dialog__DlgSettingsUnits);
        GroupBox6->setObjectName(QString::fromUtf8("GroupBox6"));
        gridLayout = new QGridLayout(GroupBox6);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalSpacer = new QSpacerItem(20, 79, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 4, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(GroupBox6);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        spinBoxDecimals = new QSpinBox(GroupBox6);
        spinBoxDecimals->setObjectName(QString::fromUtf8("spinBoxDecimals"));
        spinBoxDecimals->setMinimum(1);
        spinBoxDecimals->setMaximum(12);

        horizontalLayout_2->addWidget(spinBoxDecimals);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(GroupBox6);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        comboBox_ViewSystem = new QComboBox(GroupBox6);
        comboBox_ViewSystem->addItem(QString());
        comboBox_ViewSystem->addItem(QString());
        comboBox_ViewSystem->addItem(QString());
        comboBox_ViewSystem->addItem(QString());
        comboBox_ViewSystem->addItem(QString());
        comboBox_ViewSystem->addItem(QString());
        comboBox_ViewSystem->addItem(QString());
        comboBox_ViewSystem->addItem(QString());
        comboBox_ViewSystem->setObjectName(QString::fromUtf8("comboBox_ViewSystem"));

        horizontalLayout->addWidget(comboBox_ViewSystem);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        tableWidget = new QTableWidget(GroupBox6);
        if (tableWidget->columnCount() < 2)
            tableWidget->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tableWidget->sizePolicy().hasHeightForWidth());
        tableWidget->setSizePolicy(sizePolicy);
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

        gridLayout->addWidget(tableWidget, 3, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(GroupBox6);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_3->addWidget(label_3);

        comboBox_FracInch = new QComboBox(GroupBox6);
        comboBox_FracInch->addItem(QString());
        comboBox_FracInch->addItem(QString());
        comboBox_FracInch->addItem(QString());
        comboBox_FracInch->addItem(QString());
        comboBox_FracInch->addItem(QString());
        comboBox_FracInch->addItem(QString());
        comboBox_FracInch->addItem(QString());
        comboBox_FracInch->setObjectName(QString::fromUtf8("comboBox_FracInch"));

        horizontalLayout_3->addWidget(comboBox_FracInch);


        gridLayout->addLayout(horizontalLayout_3, 2, 0, 1, 1);


        verticalLayout_2->addWidget(GroupBox6);


        retranslateUi(Gui__Dialog__DlgSettingsUnits);

        QMetaObject::connectSlotsByName(Gui__Dialog__DlgSettingsUnits);
    } // setupUi

    void retranslateUi(QWidget *Gui__Dialog__DlgSettingsUnits)
    {
        Gui__Dialog__DlgSettingsUnits->setWindowTitle(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Units", nullptr));
        GroupBox6->setTitle(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Units settings", nullptr));
        label_2->setText(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Number of decimals:", nullptr));
#ifndef QT_NO_TOOLTIP
        spinBoxDecimals->setToolTip(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Number of decimals that should be shown for numbers and dimensions", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Unit system:", nullptr));
        comboBox_ViewSystem->setItemText(0, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Standard (mm/kg/s/degree)", nullptr));
        comboBox_ViewSystem->setItemText(1, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "MKS (m/kg/s/degree)", nullptr));
        comboBox_ViewSystem->setItemText(2, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "US customary (in/lb)", nullptr));
        comboBox_ViewSystem->setItemText(3, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Imperial decimal (in/lb)", nullptr));
        comboBox_ViewSystem->setItemText(4, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Building Euro (cm/m\302\262/m\302\263)", nullptr));
        comboBox_ViewSystem->setItemText(5, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Building US (ft-in/sqft/cuft)", nullptr));
        comboBox_ViewSystem->setItemText(6, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Metric small parts & CNC(mm, mm/min)", nullptr));
        comboBox_ViewSystem->setItemText(7, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Imperial Civil (ft/ft^2/ft^3)", nullptr));

#ifndef QT_NO_TOOLTIP
        comboBox_ViewSystem->setToolTip(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Unit system that should be used for all parts the application", nullptr));
#endif // QT_NO_TOOLTIP
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Magnitude", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Unit", nullptr));
        label_3->setText(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Minimum fractional inch:", nullptr));
        comboBox_FracInch->setItemText(0, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "1/2\"", nullptr));
        comboBox_FracInch->setItemText(1, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "1/4\"", nullptr));
        comboBox_FracInch->setItemText(2, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "1/8\"", nullptr));
        comboBox_FracInch->setItemText(3, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "1/16\"", nullptr));
        comboBox_FracInch->setItemText(4, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "1/32\"", nullptr));
        comboBox_FracInch->setItemText(5, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "1/64\"", nullptr));
        comboBox_FracInch->setItemText(6, QApplication::translate("Gui::Dialog::DlgSettingsUnits", "1/128\"", nullptr));

#ifndef QT_NO_TOOLTIP
        comboBox_FracInch->setToolTip(QApplication::translate("Gui::Dialog::DlgSettingsUnits", "Minimum fractional inch to be displayed", nullptr));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

} // namespace Dialog
} // namespace Gui

namespace Gui {
namespace Dialog {
namespace Ui {
    class DlgSettingsUnits: public Ui_DlgSettingsUnits {};
} // namespace Ui
} // namespace Dialog
} // namespace Gui

#endif // UI_DLGSETTINGSUNITS_H
