/********************************************************************************
** Form generated from reading UI file 'DlgAddProperty.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGADDPROPERTY_H
#define UI_DLGADDPROPERTY_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>

namespace Gui {
namespace Dialog {

class Ui_DlgAddProperty
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QComboBox *comboType;
    QLabel *label_2;
    QLineEdit *edtGroup;
    QLabel *label_3;
    QLineEdit *edtName;
    QLabel *label_4;
    QPlainTextEdit *edtDoc;
    QCheckBox *chkAppend;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *Gui__Dialog__DlgAddProperty)
    {
        if (Gui__Dialog__DlgAddProperty->objectName().isEmpty())
            Gui__Dialog__DlgAddProperty->setObjectName(QString::fromUtf8("Gui__Dialog__DlgAddProperty"));
        Gui__Dialog__DlgAddProperty->resize(354, 258);
        formLayout = new QFormLayout(Gui__Dialog__DlgAddProperty);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label = new QLabel(Gui__Dialog__DlgAddProperty);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        comboType = new QComboBox(Gui__Dialog__DlgAddProperty);
        comboType->setObjectName(QString::fromUtf8("comboType"));

        formLayout->setWidget(0, QFormLayout::FieldRole, comboType);

        label_2 = new QLabel(Gui__Dialog__DlgAddProperty);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        edtGroup = new QLineEdit(Gui__Dialog__DlgAddProperty);
        edtGroup->setObjectName(QString::fromUtf8("edtGroup"));

        formLayout->setWidget(1, QFormLayout::FieldRole, edtGroup);

        label_3 = new QLabel(Gui__Dialog__DlgAddProperty);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        edtName = new QLineEdit(Gui__Dialog__DlgAddProperty);
        edtName->setObjectName(QString::fromUtf8("edtName"));

        formLayout->setWidget(2, QFormLayout::FieldRole, edtName);

        label_4 = new QLabel(Gui__Dialog__DlgAddProperty);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        edtDoc = new QPlainTextEdit(Gui__Dialog__DlgAddProperty);
        edtDoc->setObjectName(QString::fromUtf8("edtDoc"));

        formLayout->setWidget(3, QFormLayout::FieldRole, edtDoc);

        chkAppend = new QCheckBox(Gui__Dialog__DlgAddProperty);
        chkAppend->setObjectName(QString::fromUtf8("chkAppend"));

        formLayout->setWidget(4, QFormLayout::FieldRole, chkAppend);

        buttonBox = new QDialogButtonBox(Gui__Dialog__DlgAddProperty);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout->setWidget(5, QFormLayout::SpanningRole, buttonBox);

        QWidget::setTabOrder(comboType, edtGroup);
        QWidget::setTabOrder(edtGroup, edtName);

        retranslateUi(Gui__Dialog__DlgAddProperty);
        QObject::connect(buttonBox, SIGNAL(accepted()), Gui__Dialog__DlgAddProperty, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Gui__Dialog__DlgAddProperty, SLOT(reject()));

        QMetaObject::connectSlotsByName(Gui__Dialog__DlgAddProperty);
    } // setupUi

    void retranslateUi(QDialog *Gui__Dialog__DlgAddProperty)
    {
        Gui__Dialog__DlgAddProperty->setWindowTitle(QApplication::translate("Gui::Dialog::DlgAddProperty", "Add property", nullptr));
        label->setText(QApplication::translate("Gui::Dialog::DlgAddProperty", "Type", nullptr));
        label_2->setText(QApplication::translate("Gui::Dialog::DlgAddProperty", "Group", nullptr));
        label_3->setText(QApplication::translate("Gui::Dialog::DlgAddProperty", "Name", nullptr));
        label_4->setText(QApplication::translate("Gui::Dialog::DlgAddProperty", "Document", nullptr));
#ifndef QT_NO_TOOLTIP
        chkAppend->setToolTip(QApplication::translate("Gui::Dialog::DlgAddProperty", "Append the group name in front of the property name in the form of 'group'_'name' to avoid conflict with existing property. The prefixed group name will be auto trimmed when shown in the property editor.", nullptr));
#endif // QT_NO_TOOLTIP
        chkAppend->setText(QApplication::translate("Gui::Dialog::DlgAddProperty", "Append group name", nullptr));
    } // retranslateUi

};

} // namespace Dialog
} // namespace Gui

namespace Gui {
namespace Dialog {
namespace Ui {
    class DlgAddProperty: public Ui_DlgAddProperty {};
} // namespace Ui
} // namespace Dialog
} // namespace Gui

#endif // UI_DLGADDPROPERTY_H
