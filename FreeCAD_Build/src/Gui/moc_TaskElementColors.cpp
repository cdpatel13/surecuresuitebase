/****************************************************************************
** Meta object code from reading C++ file 'TaskElementColors.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/TaskElementColors.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskElementColors.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__ElementColors_t {
    QByteArrayData data[15];
    char stringdata0[302];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__ElementColors_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__ElementColors_t qt_meta_stringdata_Gui__ElementColors = {
    {
QT_MOC_LITERAL(0, 0, 18), // "Gui::ElementColors"
QT_MOC_LITERAL(1, 19, 26), // "on_removeSelection_clicked"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 23), // "on_addSelection_clicked"
QT_MOC_LITERAL(4, 71, 20), // "on_removeAll_clicked"
QT_MOC_LITERAL(5, 92, 32), // "on_elementList_itemDoubleClicked"
QT_MOC_LITERAL(6, 125, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(7, 142, 4), // "item"
QT_MOC_LITERAL(8, 147, 35), // "on_elementList_itemSelectionC..."
QT_MOC_LITERAL(9, 183, 26), // "on_elementList_itemEntered"
QT_MOC_LITERAL(10, 210, 20), // "on_recompute_clicked"
QT_MOC_LITERAL(11, 231, 7), // "checked"
QT_MOC_LITERAL(12, 239, 16), // "on_onTop_clicked"
QT_MOC_LITERAL(13, 256, 24), // "on_hideSelection_clicked"
QT_MOC_LITERAL(14, 281, 20) // "on_boxSelect_clicked"

    },
    "Gui::ElementColors\0on_removeSelection_clicked\0"
    "\0on_addSelection_clicked\0on_removeAll_clicked\0"
    "on_elementList_itemDoubleClicked\0"
    "QListWidgetItem*\0item\0"
    "on_elementList_itemSelectionChanged\0"
    "on_elementList_itemEntered\0"
    "on_recompute_clicked\0checked\0"
    "on_onTop_clicked\0on_hideSelection_clicked\0"
    "on_boxSelect_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__ElementColors[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x08 /* Private */,
       3,    0,   65,    2, 0x08 /* Private */,
       4,    0,   66,    2, 0x08 /* Private */,
       5,    1,   67,    2, 0x08 /* Private */,
       8,    0,   70,    2, 0x08 /* Private */,
       9,    1,   71,    2, 0x08 /* Private */,
      10,    1,   74,    2, 0x08 /* Private */,
      12,    1,   77,    2, 0x08 /* Private */,
      13,    0,   80,    2, 0x08 /* Private */,
      14,    0,   81,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Bool,   11,
    QMetaType::Void, QMetaType::Bool,   11,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Gui::ElementColors::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ElementColors *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_removeSelection_clicked(); break;
        case 1: _t->on_addSelection_clicked(); break;
        case 2: _t->on_removeAll_clicked(); break;
        case 3: _t->on_elementList_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 4: _t->on_elementList_itemSelectionChanged(); break;
        case 5: _t->on_elementList_itemEntered((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 6: _t->on_recompute_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_onTop_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_hideSelection_clicked(); break;
        case 9: _t->on_boxSelect_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::ElementColors::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_Gui__ElementColors.data,
    qt_meta_data_Gui__ElementColors,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::ElementColors::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::ElementColors::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__ElementColors.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "SelectionObserver"))
        return static_cast< SelectionObserver*>(this);
    return QWidget::qt_metacast(_clname);
}

int Gui::ElementColors::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
struct qt_meta_stringdata_Gui__TaskElementColors_t {
    QByteArrayData data[1];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__TaskElementColors_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__TaskElementColors_t qt_meta_stringdata_Gui__TaskElementColors = {
    {
QT_MOC_LITERAL(0, 0, 22) // "Gui::TaskElementColors"

    },
    "Gui::TaskElementColors"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__TaskElementColors[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::TaskElementColors::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::TaskElementColors::staticMetaObject = { {
    &TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_Gui__TaskElementColors.data,
    qt_meta_data_Gui__TaskElementColors,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::TaskElementColors::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::TaskElementColors::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__TaskElementColors.stringdata0))
        return static_cast<void*>(this);
    return TaskView::TaskDialog::qt_metacast(_clname);
}

int Gui::TaskElementColors::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
