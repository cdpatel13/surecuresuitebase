/****************************************************************************
** Meta object code from reading C++ file 'TaskAppearance.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../FreeCAD/src/Gui/TaskView/TaskAppearance.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskAppearance.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__TaskView__TaskAppearance_t {
    QByteArrayData data[7];
    char stringdata0[172];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__TaskView__TaskAppearance_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__TaskView__TaskAppearance_t qt_meta_stringdata_Gui__TaskView__TaskAppearance = {
    {
QT_MOC_LITERAL(0, 0, 29), // "Gui::TaskView::TaskAppearance"
QT_MOC_LITERAL(1, 30, 23), // "on_changeMode_activated"
QT_MOC_LITERAL(2, 54, 0), // ""
QT_MOC_LITERAL(3, 55, 23), // "on_changePlot_activated"
QT_MOC_LITERAL(4, 79, 32), // "on_spinTransparency_valueChanged"
QT_MOC_LITERAL(5, 112, 29), // "on_spinPointSize_valueChanged"
QT_MOC_LITERAL(6, 142, 29) // "on_spinLineWidth_valueChanged"

    },
    "Gui::TaskView::TaskAppearance\0"
    "on_changeMode_activated\0\0"
    "on_changePlot_activated\0"
    "on_spinTransparency_valueChanged\0"
    "on_spinPointSize_valueChanged\0"
    "on_spinLineWidth_valueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__TaskView__TaskAppearance[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x08 /* Private */,
       3,    1,   42,    2, 0x08 /* Private */,
       4,    1,   45,    2, 0x08 /* Private */,
       5,    1,   48,    2, 0x08 /* Private */,
       6,    1,   51,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void Gui::TaskView::TaskAppearance::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskAppearance *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_changeMode_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_changePlot_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->on_spinTransparency_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_spinPointSize_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_spinLineWidth_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::TaskView::TaskAppearance::staticMetaObject = { {
    &TaskBox::staticMetaObject,
    qt_meta_stringdata_Gui__TaskView__TaskAppearance.data,
    qt_meta_data_Gui__TaskView__TaskAppearance,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::TaskView::TaskAppearance::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::TaskView::TaskAppearance::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__TaskView__TaskAppearance.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionSingleton::ObserverType"))
        return static_cast< Gui::SelectionSingleton::ObserverType*>(this);
    return TaskBox::qt_metacast(_clname);
}

int Gui::TaskView::TaskAppearance::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
