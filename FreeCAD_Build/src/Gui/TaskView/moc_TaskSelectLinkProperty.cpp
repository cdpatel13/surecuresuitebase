/****************************************************************************
** Meta object code from reading C++ file 'TaskSelectLinkProperty.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../FreeCAD/src/Gui/TaskView/TaskSelectLinkProperty.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskSelectLinkProperty.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__TaskView__TaskSelectLinkProperty_t {
    QByteArrayData data[8];
    char stringdata0[143];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__TaskView__TaskSelectLinkProperty_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__TaskView__TaskSelectLinkProperty_t qt_meta_stringdata_Gui__TaskView__TaskSelectLinkProperty = {
    {
QT_MOC_LITERAL(0, 0, 37), // "Gui::TaskView::TaskSelectLink..."
QT_MOC_LITERAL(1, 38, 16), // "emitSelectionFit"
QT_MOC_LITERAL(2, 55, 0), // ""
QT_MOC_LITERAL(3, 56, 19), // "emitSelectionMisfit"
QT_MOC_LITERAL(4, 76, 17), // "on_Remove_clicked"
QT_MOC_LITERAL(5, 94, 14), // "on_Add_clicked"
QT_MOC_LITERAL(6, 109, 17), // "on_Invert_clicked"
QT_MOC_LITERAL(7, 127, 15) // "on_Help_clicked"

    },
    "Gui::TaskView::TaskSelectLinkProperty\0"
    "emitSelectionFit\0\0emitSelectionMisfit\0"
    "on_Remove_clicked\0on_Add_clicked\0"
    "on_Invert_clicked\0on_Help_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__TaskView__TaskSelectLinkProperty[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    0,   45,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   46,    2, 0x08 /* Private */,
       5,    1,   49,    2, 0x08 /* Private */,
       6,    1,   52,    2, 0x08 /* Private */,
       7,    1,   55,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void Gui::TaskView::TaskSelectLinkProperty::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskSelectLinkProperty *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->emitSelectionFit(); break;
        case 1: _t->emitSelectionMisfit(); break;
        case 2: _t->on_Remove_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_Add_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_Invert_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_Help_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TaskSelectLinkProperty::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TaskSelectLinkProperty::emitSelectionFit)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TaskSelectLinkProperty::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TaskSelectLinkProperty::emitSelectionMisfit)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::TaskView::TaskSelectLinkProperty::staticMetaObject = { {
    &TaskBox::staticMetaObject,
    qt_meta_stringdata_Gui__TaskView__TaskSelectLinkProperty.data,
    qt_meta_data_Gui__TaskView__TaskSelectLinkProperty,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::TaskView::TaskSelectLinkProperty::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::TaskView::TaskSelectLinkProperty::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__TaskView__TaskSelectLinkProperty.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionSingleton::ObserverType"))
        return static_cast< Gui::SelectionSingleton::ObserverType*>(this);
    return TaskBox::qt_metacast(_clname);
}

int Gui::TaskView::TaskSelectLinkProperty::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void Gui::TaskView::TaskSelectLinkProperty::emitSelectionFit()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Gui::TaskView::TaskSelectLinkProperty::emitSelectionMisfit()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
