/********************************************************************************
** Form generated from reading UI file 'DlgObjectSelection.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGOBJECTSELECTION_H
#define UI_DLGOBJECTSELECTION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>

namespace Gui {

class Ui_DlgObjectSelection
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QSplitter *splitter;
    QTreeWidget *treeWidget;
    QTreeWidget *depList;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *Gui__DlgObjectSelection)
    {
        if (Gui__DlgObjectSelection->objectName().isEmpty())
            Gui__DlgObjectSelection->setObjectName(QString::fromUtf8("Gui__DlgObjectSelection"));
        Gui__DlgObjectSelection->resize(621, 383);
        Gui__DlgObjectSelection->setSizeGripEnabled(true);
        Gui__DlgObjectSelection->setModal(true);
        verticalLayout = new QVBoxLayout(Gui__DlgObjectSelection);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(Gui__DlgObjectSelection);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setWordWrap(true);

        verticalLayout->addWidget(label);

        splitter = new QSplitter(Gui__DlgObjectSelection);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(splitter->sizePolicy().hasHeightForWidth());
        splitter->setSizePolicy(sizePolicy1);
        splitter->setOrientation(Qt::Horizontal);
        treeWidget = new QTreeWidget(splitter);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        treeWidget->setHeaderItem(__qtreewidgetitem);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        sizePolicy1.setHeightForWidth(treeWidget->sizePolicy().hasHeightForWidth());
        treeWidget->setSizePolicy(sizePolicy1);
        treeWidget->setBaseSize(QSize(0, 0));
        treeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
        treeWidget->setRootIsDecorated(true);
        treeWidget->setSortingEnabled(true);
        treeWidget->setHeaderHidden(false);
        treeWidget->setColumnCount(1);
        splitter->addWidget(treeWidget);
        treeWidget->header()->setVisible(true);
        treeWidget->header()->setCascadingSectionResizes(false);
        treeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        treeWidget->header()->setStretchLastSection(false);
        depList = new QTreeWidget(splitter);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem();
        __qtreewidgetitem1->setText(2, QString::fromUtf8("3"));
        __qtreewidgetitem1->setText(1, QString::fromUtf8("2"));
        __qtreewidgetitem1->setText(0, QString::fromUtf8("1"));
        depList->setHeaderItem(__qtreewidgetitem1);
        depList->setObjectName(QString::fromUtf8("depList"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(1);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(depList->sizePolicy().hasHeightForWidth());
        depList->setSizePolicy(sizePolicy2);
        depList->setSelectionMode(QAbstractItemView::ExtendedSelection);
        depList->setRootIsDecorated(false);
        depList->setSortingEnabled(true);
        depList->setColumnCount(4);
        splitter->addWidget(depList);
        depList->header()->setProperty("showSortIndicator", QVariant(true));

        verticalLayout->addWidget(splitter);

        buttonBox = new QDialogButtonBox(Gui__DlgObjectSelection);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(1);
        sizePolicy3.setHeightForWidth(buttonBox->sizePolicy().hasHeightForWidth());
        buttonBox->setSizePolicy(sizePolicy3);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(Gui__DlgObjectSelection);

        QMetaObject::connectSlotsByName(Gui__DlgObjectSelection);
    } // setupUi

    void retranslateUi(QDialog *Gui__DlgObjectSelection)
    {
        Gui__DlgObjectSelection->setWindowTitle(QApplication::translate("Gui::DlgObjectSelection", "Object selection", nullptr));
        label->setText(QApplication::translate("Gui::DlgObjectSelection", "The selected objects contain other dependencies. Please select which objects to export. All dependencies are auto selected by default.", nullptr));
    } // retranslateUi

};

} // namespace Gui

namespace Gui {
namespace Ui {
    class DlgObjectSelection: public Ui_DlgObjectSelection {};
} // namespace Ui
} // namespace Gui

#endif // UI_DLGOBJECTSELECTION_H
