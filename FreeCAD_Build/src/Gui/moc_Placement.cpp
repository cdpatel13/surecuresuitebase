/****************************************************************************
** Meta object code from reading C++ file 'Placement.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/Placement.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Placement.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__Placement_t {
    QByteArrayData data[12];
    char stringdata0[248];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__Placement_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__Placement_t qt_meta_stringdata_Gui__Dialog__Placement = {
    {
QT_MOC_LITERAL(0, 0, 22), // "Gui::Dialog::Placement"
QT_MOC_LITERAL(1, 23, 16), // "placementChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 16), // "directionChanged"
QT_MOC_LITERAL(4, 58, 15), // "openTransaction"
QT_MOC_LITERAL(5, 74, 22), // "on_applyButton_clicked"
QT_MOC_LITERAL(6, 97, 36), // "on_applyIncrementalPlacement_..."
QT_MOC_LITERAL(7, 134, 18), // "onPlacementChanged"
QT_MOC_LITERAL(8, 153, 22), // "on_resetButton_clicked"
QT_MOC_LITERAL(9, 176, 23), // "on_centerOfMass_toggled"
QT_MOC_LITERAL(10, 200, 25), // "on_selectedVertex_clicked"
QT_MOC_LITERAL(11, 226, 21) // "on_applyAxial_clicked"

    },
    "Gui::Dialog::Placement\0placementChanged\0"
    "\0directionChanged\0openTransaction\0"
    "on_applyButton_clicked\0"
    "on_applyIncrementalPlacement_toggled\0"
    "onPlacementChanged\0on_resetButton_clicked\0"
    "on_centerOfMass_toggled\0"
    "on_selectedVertex_clicked\0"
    "on_applyAxial_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__Placement[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   64,    2, 0x06 /* Public */,
       3,    0,   71,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   72,    2, 0x08 /* Private */,
       5,    0,   73,    2, 0x08 /* Private */,
       6,    1,   74,    2, 0x08 /* Private */,
       7,    1,   77,    2, 0x08 /* Private */,
       8,    0,   80,    2, 0x08 /* Private */,
       9,    1,   81,    2, 0x08 /* Private */,
      10,    0,   84,    2, 0x08 /* Private */,
      11,    0,   85,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QVariant, QMetaType::Bool, QMetaType::Bool,    2,    2,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Gui::Dialog::Placement::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Placement *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->placementChanged((*reinterpret_cast< const QVariant(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 1: _t->directionChanged(); break;
        case 2: _t->openTransaction(); break;
        case 3: _t->on_applyButton_clicked(); break;
        case 4: _t->on_applyIncrementalPlacement_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->onPlacementChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_resetButton_clicked(); break;
        case 7: _t->on_centerOfMass_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_selectedVertex_clicked(); break;
        case 9: _t->on_applyAxial_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Placement::*)(const QVariant & , bool , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Placement::placementChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Placement::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Placement::directionChanged)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::Placement::staticMetaObject = { {
    &Gui::LocationDialog::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__Placement.data,
    qt_meta_data_Gui__Dialog__Placement,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::Placement::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::Placement::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__Placement.stringdata0))
        return static_cast<void*>(this);
    return Gui::LocationDialog::qt_metacast(_clname);
}

int Gui::Dialog::Placement::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::LocationDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void Gui::Dialog::Placement::placementChanged(const QVariant & _t1, bool _t2, bool _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Gui::Dialog::Placement::directionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
struct qt_meta_stringdata_Gui__Dialog__DockablePlacement_t {
    QByteArrayData data[1];
    char stringdata0[31];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DockablePlacement_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DockablePlacement_t qt_meta_stringdata_Gui__Dialog__DockablePlacement = {
    {
QT_MOC_LITERAL(0, 0, 30) // "Gui::Dialog::DockablePlacement"

    },
    "Gui::Dialog::DockablePlacement"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DockablePlacement[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::Dialog::DockablePlacement::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DockablePlacement::staticMetaObject = { {
    &Placement::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DockablePlacement.data,
    qt_meta_data_Gui__Dialog__DockablePlacement,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DockablePlacement::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DockablePlacement::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DockablePlacement.stringdata0))
        return static_cast<void*>(this);
    return Placement::qt_metacast(_clname);
}

int Gui::Dialog::DockablePlacement::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Placement::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__Dialog__TaskPlacement_t {
    QByteArrayData data[4];
    char stringdata0[66];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__TaskPlacement_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__TaskPlacement_t qt_meta_stringdata_Gui__Dialog__TaskPlacement = {
    {
QT_MOC_LITERAL(0, 0, 26), // "Gui::Dialog::TaskPlacement"
QT_MOC_LITERAL(1, 27, 16), // "placementChanged"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 20) // "slotPlacementChanged"

    },
    "Gui::Dialog::TaskPlacement\0placementChanged\0"
    "\0slotPlacementChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__TaskPlacement[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   24,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    3,   31,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QVariant, QMetaType::Bool, QMetaType::Bool,    2,    2,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::QVariant, QMetaType::Bool, QMetaType::Bool,    2,    2,    2,

       0        // eod
};

void Gui::Dialog::TaskPlacement::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPlacement *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->placementChanged((*reinterpret_cast< const QVariant(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 1: _t->slotPlacementChanged((*reinterpret_cast< const QVariant(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TaskPlacement::*)(const QVariant & , bool , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TaskPlacement::placementChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::TaskPlacement::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__TaskPlacement.data,
    qt_meta_data_Gui__Dialog__TaskPlacement,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::TaskPlacement::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::TaskPlacement::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__TaskPlacement.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int Gui::Dialog::TaskPlacement::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void Gui::Dialog::TaskPlacement::placementChanged(const QVariant & _t1, bool _t2, bool _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
