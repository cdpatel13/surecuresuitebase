/****************************************************************************
** Meta object code from reading C++ file 'DlgEditorImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/DlgEditorImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgEditorImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__DlgSettingsEditorImp_t {
    QByteArrayData data[8];
    char stringdata0[161];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DlgSettingsEditorImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DlgSettingsEditorImp_t qt_meta_stringdata_Gui__Dialog__DlgSettingsEditorImp = {
    {
QT_MOC_LITERAL(0, 0, 33), // "Gui::Dialog::DlgSettingsEdito..."
QT_MOC_LITERAL(1, 34, 34), // "on_displayItems_currentItemCh..."
QT_MOC_LITERAL(2, 69, 0), // ""
QT_MOC_LITERAL(3, 70, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(4, 87, 1), // "i"
QT_MOC_LITERAL(5, 89, 22), // "on_colorButton_changed"
QT_MOC_LITERAL(6, 112, 23), // "on_fontFamily_activated"
QT_MOC_LITERAL(7, 136, 24) // "on_fontSize_valueChanged"

    },
    "Gui::Dialog::DlgSettingsEditorImp\0"
    "on_displayItems_currentItemChanged\0\0"
    "QTreeWidgetItem*\0i\0on_colorButton_changed\0"
    "on_fontFamily_activated\0"
    "on_fontSize_valueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DlgSettingsEditorImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x09 /* Protected */,
       5,    0,   37,    2, 0x09 /* Protected */,
       6,    1,   38,    2, 0x09 /* Protected */,
       7,    1,   41,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,

       0        // eod
};

void Gui::Dialog::DlgSettingsEditorImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgSettingsEditorImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_displayItems_currentItemChanged((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 1: _t->on_colorButton_changed(); break;
        case 2: _t->on_fontFamily_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->on_fontSize_valueChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DlgSettingsEditorImp::staticMetaObject = { {
    &PreferencePage::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DlgSettingsEditorImp.data,
    qt_meta_data_Gui__Dialog__DlgSettingsEditorImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DlgSettingsEditorImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DlgSettingsEditorImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DlgSettingsEditorImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui_DlgEditorSettings"))
        return static_cast< Ui_DlgEditorSettings*>(this);
    return PreferencePage::qt_metacast(_clname);
}

int Gui::Dialog::DlgSettingsEditorImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PreferencePage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
