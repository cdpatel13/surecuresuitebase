/****************************************************************************
** Meta object code from reading C++ file 'DlgSettingsImageImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/DlgSettingsImageImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgSettingsImageImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__DlgSettingsImageImp_t {
    QByteArrayData data[8];
    char stringdata0[188];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DlgSettingsImageImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DlgSettingsImageImp_t qt_meta_stringdata_Gui__Dialog__DlgSettingsImageImp = {
    {
QT_MOC_LITERAL(0, 0, 32), // "Gui::Dialog::DlgSettingsImageImp"
QT_MOC_LITERAL(1, 33, 16), // "onSelectedFilter"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 28), // "on_buttonRatioScreen_clicked"
QT_MOC_LITERAL(4, 80, 25), // "on_buttonRatio4x3_clicked"
QT_MOC_LITERAL(5, 106, 26), // "on_buttonRatio16x9_clicked"
QT_MOC_LITERAL(6, 133, 25), // "on_buttonRatio1x1_clicked"
QT_MOC_LITERAL(7, 159, 28) // "on_standardSizeBox_activated"

    },
    "Gui::Dialog::DlgSettingsImageImp\0"
    "onSelectedFilter\0\0on_buttonRatioScreen_clicked\0"
    "on_buttonRatio4x3_clicked\0"
    "on_buttonRatio16x9_clicked\0"
    "on_buttonRatio1x1_clicked\0"
    "on_standardSizeBox_activated"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DlgSettingsImageImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x0a /* Public */,
       3,    0,   47,    2, 0x09 /* Protected */,
       4,    0,   48,    2, 0x09 /* Protected */,
       5,    0,   49,    2, 0x09 /* Protected */,
       6,    0,   50,    2, 0x09 /* Protected */,
       7,    1,   51,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void Gui::Dialog::DlgSettingsImageImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgSettingsImageImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onSelectedFilter((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_buttonRatioScreen_clicked(); break;
        case 2: _t->on_buttonRatio4x3_clicked(); break;
        case 3: _t->on_buttonRatio16x9_clicked(); break;
        case 4: _t->on_buttonRatio1x1_clicked(); break;
        case 5: _t->on_standardSizeBox_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DlgSettingsImageImp::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DlgSettingsImageImp.data,
    qt_meta_data_Gui__Dialog__DlgSettingsImageImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DlgSettingsImageImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DlgSettingsImageImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DlgSettingsImageImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::DlgSettingsImage"))
        return static_cast< Ui::DlgSettingsImage*>(this);
    return QWidget::qt_metacast(_clname);
}

int Gui::Dialog::DlgSettingsImageImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
