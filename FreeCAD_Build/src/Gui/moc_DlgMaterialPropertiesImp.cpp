/****************************************************************************
** Meta object code from reading C++ file 'DlgMaterialPropertiesImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/DlgMaterialPropertiesImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgMaterialPropertiesImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__DlgMaterialPropertiesImp_t {
    QByteArrayData data[7];
    char stringdata0[163];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DlgMaterialPropertiesImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DlgMaterialPropertiesImp_t qt_meta_stringdata_Gui__Dialog__DlgMaterialPropertiesImp = {
    {
QT_MOC_LITERAL(0, 0, 37), // "Gui::Dialog::DlgMaterialPrope..."
QT_MOC_LITERAL(1, 38, 23), // "on_ambientColor_changed"
QT_MOC_LITERAL(2, 62, 0), // ""
QT_MOC_LITERAL(3, 63, 23), // "on_diffuseColor_changed"
QT_MOC_LITERAL(4, 87, 24), // "on_emissiveColor_changed"
QT_MOC_LITERAL(5, 112, 24), // "on_specularColor_changed"
QT_MOC_LITERAL(6, 137, 25) // "on_shininess_valueChanged"

    },
    "Gui::Dialog::DlgMaterialPropertiesImp\0"
    "on_ambientColor_changed\0\0"
    "on_diffuseColor_changed\0"
    "on_emissiveColor_changed\0"
    "on_specularColor_changed\0"
    "on_shininess_valueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DlgMaterialPropertiesImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x0a /* Public */,
       3,    0,   40,    2, 0x0a /* Public */,
       4,    0,   41,    2, 0x0a /* Public */,
       5,    0,   42,    2, 0x0a /* Public */,
       6,    1,   43,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void Gui::Dialog::DlgMaterialPropertiesImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgMaterialPropertiesImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_ambientColor_changed(); break;
        case 1: _t->on_diffuseColor_changed(); break;
        case 2: _t->on_emissiveColor_changed(); break;
        case 3: _t->on_specularColor_changed(); break;
        case 4: _t->on_shininess_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DlgMaterialPropertiesImp::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DlgMaterialPropertiesImp.data,
    qt_meta_data_Gui__Dialog__DlgMaterialPropertiesImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DlgMaterialPropertiesImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DlgMaterialPropertiesImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DlgMaterialPropertiesImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui_DlgMaterialProperties"))
        return static_cast< Ui_DlgMaterialProperties*>(this);
    return QDialog::qt_metacast(_clname);
}

int Gui::Dialog::DlgMaterialPropertiesImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
