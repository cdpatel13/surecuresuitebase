/****************************************************************************
** Meta object code from reading C++ file 'Clipping.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/Clipping.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Clipping.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__Clipping_t {
    QByteArrayData data[18];
    char stringdata0[378];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__Clipping_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__Clipping_t qt_meta_stringdata_Gui__Dialog__Clipping = {
    {
QT_MOC_LITERAL(0, 0, 21), // "Gui::Dialog::Clipping"
QT_MOC_LITERAL(1, 22, 20), // "on_groupBoxX_toggled"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 20), // "on_groupBoxY_toggled"
QT_MOC_LITERAL(4, 65, 20), // "on_groupBoxZ_toggled"
QT_MOC_LITERAL(5, 86, 21), // "on_clipX_valueChanged"
QT_MOC_LITERAL(6, 108, 21), // "on_clipY_valueChanged"
QT_MOC_LITERAL(7, 130, 21), // "on_clipZ_valueChanged"
QT_MOC_LITERAL(8, 152, 20), // "on_flipClipX_clicked"
QT_MOC_LITERAL(9, 173, 20), // "on_flipClipY_clicked"
QT_MOC_LITERAL(10, 194, 20), // "on_flipClipZ_clicked"
QT_MOC_LITERAL(11, 215, 23), // "on_groupBoxView_toggled"
QT_MOC_LITERAL(12, 239, 24), // "on_clipView_valueChanged"
QT_MOC_LITERAL(13, 264, 19), // "on_fromView_clicked"
QT_MOC_LITERAL(14, 284, 30), // "on_adjustViewdirection_toggled"
QT_MOC_LITERAL(15, 315, 20), // "on_dirX_valueChanged"
QT_MOC_LITERAL(16, 336, 20), // "on_dirY_valueChanged"
QT_MOC_LITERAL(17, 357, 20) // "on_dirZ_valueChanged"

    },
    "Gui::Dialog::Clipping\0on_groupBoxX_toggled\0"
    "\0on_groupBoxY_toggled\0on_groupBoxZ_toggled\0"
    "on_clipX_valueChanged\0on_clipY_valueChanged\0"
    "on_clipZ_valueChanged\0on_flipClipX_clicked\0"
    "on_flipClipY_clicked\0on_flipClipZ_clicked\0"
    "on_groupBoxView_toggled\0"
    "on_clipView_valueChanged\0on_fromView_clicked\0"
    "on_adjustViewdirection_toggled\0"
    "on_dirX_valueChanged\0on_dirY_valueChanged\0"
    "on_dirZ_valueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__Clipping[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x09 /* Protected */,
       3,    1,   97,    2, 0x09 /* Protected */,
       4,    1,  100,    2, 0x09 /* Protected */,
       5,    1,  103,    2, 0x09 /* Protected */,
       6,    1,  106,    2, 0x09 /* Protected */,
       7,    1,  109,    2, 0x09 /* Protected */,
       8,    0,  112,    2, 0x09 /* Protected */,
       9,    0,  113,    2, 0x09 /* Protected */,
      10,    0,  114,    2, 0x09 /* Protected */,
      11,    1,  115,    2, 0x09 /* Protected */,
      12,    1,  118,    2, 0x09 /* Protected */,
      13,    0,  121,    2, 0x09 /* Protected */,
      14,    1,  122,    2, 0x09 /* Protected */,
      15,    1,  125,    2, 0x09 /* Protected */,
      16,    1,  128,    2, 0x09 /* Protected */,
      17,    1,  131,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,

       0        // eod
};

void Gui::Dialog::Clipping::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Clipping *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_groupBoxX_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->on_groupBoxY_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->on_groupBoxZ_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_clipX_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->on_clipY_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->on_clipZ_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->on_flipClipX_clicked(); break;
        case 7: _t->on_flipClipY_clicked(); break;
        case 8: _t->on_flipClipZ_clicked(); break;
        case 9: _t->on_groupBoxView_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->on_clipView_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->on_fromView_clicked(); break;
        case 12: _t->on_adjustViewdirection_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->on_dirX_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->on_dirY_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: _t->on_dirZ_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::Clipping::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__Clipping.data,
    qt_meta_data_Gui__Dialog__Clipping,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::Clipping::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::Clipping::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__Clipping.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int Gui::Dialog::Clipping::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
