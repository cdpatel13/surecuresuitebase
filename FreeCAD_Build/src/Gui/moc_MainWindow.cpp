/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/MainWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__UrlHandler_t {
    QByteArrayData data[1];
    char stringdata0[16];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__UrlHandler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__UrlHandler_t qt_meta_stringdata_Gui__UrlHandler = {
    {
QT_MOC_LITERAL(0, 0, 15) // "Gui::UrlHandler"

    },
    "Gui::UrlHandler"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__UrlHandler[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::UrlHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::UrlHandler::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_Gui__UrlHandler.data,
    qt_meta_data_Gui__UrlHandler,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::UrlHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::UrlHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__UrlHandler.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Gui::UrlHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__MainWindow_t {
    QByteArrayData data[46];
    char stringdata0[633];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__MainWindow_t qt_meta_stringdata_Gui__MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 15), // "Gui::MainWindow"
QT_MOC_LITERAL(1, 16, 9), // "timeEvent"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 18), // "windowStateChanged"
QT_MOC_LITERAL(4, 46, 8), // "MDIView*"
QT_MOC_LITERAL(5, 55, 18), // "workbenchActivated"
QT_MOC_LITERAL(6, 74, 16), // "mainWindowClosed"
QT_MOC_LITERAL(7, 91, 11), // "setPaneText"
QT_MOC_LITERAL(8, 103, 1), // "i"
QT_MOC_LITERAL(9, 105, 4), // "text"
QT_MOC_LITERAL(10, 110, 12), // "arrangeIcons"
QT_MOC_LITERAL(11, 123, 4), // "tile"
QT_MOC_LITERAL(12, 128, 7), // "cascade"
QT_MOC_LITERAL(13, 136, 17), // "closeActiveWindow"
QT_MOC_LITERAL(14, 154, 17), // "closeAllDocuments"
QT_MOC_LITERAL(15, 172, 5), // "close"
QT_MOC_LITERAL(16, 178, 11), // "confirmSave"
QT_MOC_LITERAL(17, 190, 11), // "const char*"
QT_MOC_LITERAL(18, 202, 7), // "docName"
QT_MOC_LITERAL(19, 210, 8), // "QWidget*"
QT_MOC_LITERAL(20, 219, 6), // "parent"
QT_MOC_LITERAL(21, 226, 11), // "addCheckBox"
QT_MOC_LITERAL(22, 238, 18), // "activateNextWindow"
QT_MOC_LITERAL(23, 257, 22), // "activatePreviousWindow"
QT_MOC_LITERAL(24, 280, 17), // "activateWorkbench"
QT_MOC_LITERAL(25, 298, 9), // "whatsThis"
QT_MOC_LITERAL(26, 308, 20), // "switchToTopLevelMode"
QT_MOC_LITERAL(27, 329, 18), // "switchToDockedMode"
QT_MOC_LITERAL(28, 348, 20), // "statusMessageChanged"
QT_MOC_LITERAL(29, 369, 11), // "showMessage"
QT_MOC_LITERAL(30, 381, 7), // "message"
QT_MOC_LITERAL(31, 389, 7), // "timeout"
QT_MOC_LITERAL(32, 397, 20), // "onSetActiveSubWindow"
QT_MOC_LITERAL(33, 418, 6), // "window"
QT_MOC_LITERAL(34, 425, 17), // "onWindowActivated"
QT_MOC_LITERAL(35, 443, 14), // "QMdiSubWindow*"
QT_MOC_LITERAL(36, 458, 17), // "tabCloseRequested"
QT_MOC_LITERAL(37, 476, 5), // "index"
QT_MOC_LITERAL(38, 482, 24), // "onWindowsMenuAboutToShow"
QT_MOC_LITERAL(39, 507, 24), // "onToolBarMenuAboutToShow"
QT_MOC_LITERAL(40, 532, 27), // "onDockWindowMenuAboutToShow"
QT_MOC_LITERAL(41, 560, 14), // "_updateActions"
QT_MOC_LITERAL(42, 575, 14), // "showMainWindow"
QT_MOC_LITERAL(43, 590, 14), // "delayedStartup"
QT_MOC_LITERAL(44, 605, 15), // "processMessages"
QT_MOC_LITERAL(45, 621, 11) // "clearStatus"

    },
    "Gui::MainWindow\0timeEvent\0\0"
    "windowStateChanged\0MDIView*\0"
    "workbenchActivated\0mainWindowClosed\0"
    "setPaneText\0i\0text\0arrangeIcons\0tile\0"
    "cascade\0closeActiveWindow\0closeAllDocuments\0"
    "close\0confirmSave\0const char*\0docName\0"
    "QWidget*\0parent\0addCheckBox\0"
    "activateNextWindow\0activatePreviousWindow\0"
    "activateWorkbench\0whatsThis\0"
    "switchToTopLevelMode\0switchToDockedMode\0"
    "statusMessageChanged\0showMessage\0"
    "message\0timeout\0onSetActiveSubWindow\0"
    "window\0onWindowActivated\0QMdiSubWindow*\0"
    "tabCloseRequested\0index\0"
    "onWindowsMenuAboutToShow\0"
    "onToolBarMenuAboutToShow\0"
    "onDockWindowMenuAboutToShow\0_updateActions\0"
    "showMainWindow\0delayedStartup\0"
    "processMessages\0clearStatus"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      34,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  184,    2, 0x06 /* Public */,
       3,    1,  185,    2, 0x06 /* Public */,
       5,    1,  188,    2, 0x06 /* Public */,
       6,    0,  191,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    2,  192,    2, 0x0a /* Public */,
      10,    0,  197,    2, 0x0a /* Public */,
      11,    0,  198,    2, 0x0a /* Public */,
      12,    0,  199,    2, 0x0a /* Public */,
      13,    0,  200,    2, 0x0a /* Public */,
      14,    1,  201,    2, 0x0a /* Public */,
      14,    0,  204,    2, 0x2a /* Public | MethodCloned */,
      16,    3,  205,    2, 0x0a /* Public */,
      16,    2,  212,    2, 0x2a /* Public | MethodCloned */,
      16,    1,  217,    2, 0x2a /* Public | MethodCloned */,
      22,    0,  220,    2, 0x0a /* Public */,
      23,    0,  221,    2, 0x0a /* Public */,
      24,    1,  222,    2, 0x0a /* Public */,
      25,    0,  225,    2, 0x0a /* Public */,
      26,    0,  226,    2, 0x0a /* Public */,
      27,    0,  227,    2, 0x0a /* Public */,
      28,    0,  228,    2, 0x0a /* Public */,
      29,    2,  229,    2, 0x0a /* Public */,
      29,    1,  234,    2, 0x2a /* Public | MethodCloned */,
      32,    1,  237,    2, 0x08 /* Private */,
      34,    1,  240,    2, 0x08 /* Private */,
      36,    1,  243,    2, 0x08 /* Private */,
      38,    0,  246,    2, 0x08 /* Private */,
      39,    0,  247,    2, 0x08 /* Private */,
      40,    0,  248,    2, 0x08 /* Private */,
      41,    0,  249,    2, 0x08 /* Private */,
      42,    0,  250,    2, 0x08 /* Private */,
      43,    0,  251,    2, 0x08 /* Private */,
      44,    1,  252,    2, 0x08 /* Private */,
      45,    0,  255,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::QString,    8,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::Bool,   15,
    QMetaType::Bool,
    QMetaType::Int, 0x80000000 | 17, 0x80000000 | 19, QMetaType::Bool,   18,   20,   21,
    QMetaType::Int, 0x80000000 | 17, 0x80000000 | 19,   18,   20,
    QMetaType::Int, 0x80000000 | 17,   18,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   30,   31,
    QMetaType::Void, QMetaType::QString,   30,
    QMetaType::Void, 0x80000000 | 19,   33,
    QMetaType::Void, 0x80000000 | 35,    2,
    QMetaType::Void, QMetaType::Int,   37,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArrayList,    2,
    QMetaType::Void,

       0        // eod
};

void Gui::MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->timeEvent(); break;
        case 1: _t->windowStateChanged((*reinterpret_cast< MDIView*(*)>(_a[1]))); break;
        case 2: _t->workbenchActivated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->mainWindowClosed(); break;
        case 4: _t->setPaneText((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 5: _t->arrangeIcons(); break;
        case 6: _t->tile(); break;
        case 7: _t->cascade(); break;
        case 8: _t->closeActiveWindow(); break;
        case 9: { bool _r = _t->closeAllDocuments((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 10: { bool _r = _t->closeAllDocuments();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 11: { int _r = _t->confirmSave((*reinterpret_cast< const char*(*)>(_a[1])),(*reinterpret_cast< QWidget*(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 12: { int _r = _t->confirmSave((*reinterpret_cast< const char*(*)>(_a[1])),(*reinterpret_cast< QWidget*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 13: { int _r = _t->confirmSave((*reinterpret_cast< const char*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 14: _t->activateNextWindow(); break;
        case 15: _t->activatePreviousWindow(); break;
        case 16: _t->activateWorkbench((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 17: _t->whatsThis(); break;
        case 18: _t->switchToTopLevelMode(); break;
        case 19: _t->switchToDockedMode(); break;
        case 20: _t->statusMessageChanged(); break;
        case 21: _t->showMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 22: _t->showMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 23: _t->onSetActiveSubWindow((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 24: _t->onWindowActivated((*reinterpret_cast< QMdiSubWindow*(*)>(_a[1]))); break;
        case 25: _t->tabCloseRequested((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->onWindowsMenuAboutToShow(); break;
        case 27: _t->onToolBarMenuAboutToShow(); break;
        case 28: _t->onDockWindowMenuAboutToShow(); break;
        case 29: _t->_updateActions(); break;
        case 30: _t->showMainWindow(); break;
        case 31: _t->delayedStartup(); break;
        case 32: _t->processMessages((*reinterpret_cast< const QList<QByteArray>(*)>(_a[1]))); break;
        case 33: _t->clearStatus(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QWidget* >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QWidget* >(); break;
            }
            break;
        case 23:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QWidget* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::timeEvent)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (MainWindow::*)(MDIView * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::windowStateChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (MainWindow::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::workbenchActivated)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (MainWindow::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::mainWindowClosed)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_Gui__MainWindow.data,
    qt_meta_data_Gui__MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Gui::MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 34)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 34;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 34)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 34;
    }
    return _id;
}

// SIGNAL 0
void Gui::MainWindow::timeEvent()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Gui::MainWindow::windowStateChanged(MDIView * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Gui::MainWindow::workbenchActivated(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Gui::MainWindow::mainWindowClosed()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
