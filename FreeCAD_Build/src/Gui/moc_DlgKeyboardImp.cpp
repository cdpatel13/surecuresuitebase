/****************************************************************************
** Meta object code from reading C++ file 'DlgKeyboardImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/DlgKeyboardImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgKeyboardImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__DlgCustomKeyboardImp_t {
    QByteArrayData data[14];
    char stringdata0[304];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DlgCustomKeyboardImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DlgCustomKeyboardImp_t qt_meta_stringdata_Gui__Dialog__DlgCustomKeyboardImp = {
    {
QT_MOC_LITERAL(0, 0, 33), // "Gui::Dialog::DlgCustomKeyboar..."
QT_MOC_LITERAL(1, 34, 24), // "on_categoryBox_activated"
QT_MOC_LITERAL(2, 59, 0), // ""
QT_MOC_LITERAL(3, 60, 5), // "index"
QT_MOC_LITERAL(4, 66, 39), // "on_commandTreeWidget_currentI..."
QT_MOC_LITERAL(5, 106, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(6, 123, 23), // "on_buttonAssign_clicked"
QT_MOC_LITERAL(7, 147, 22), // "on_buttonClear_clicked"
QT_MOC_LITERAL(8, 170, 22), // "on_buttonReset_clicked"
QT_MOC_LITERAL(9, 193, 25), // "on_buttonResetAll_clicked"
QT_MOC_LITERAL(10, 219, 27), // "on_editShortcut_textChanged"
QT_MOC_LITERAL(11, 247, 16), // "onAddMacroAction"
QT_MOC_LITERAL(12, 264, 19), // "onRemoveMacroAction"
QT_MOC_LITERAL(13, 284, 19) // "onModifyMacroAction"

    },
    "Gui::Dialog::DlgCustomKeyboardImp\0"
    "on_categoryBox_activated\0\0index\0"
    "on_commandTreeWidget_currentItemChanged\0"
    "QTreeWidgetItem*\0on_buttonAssign_clicked\0"
    "on_buttonClear_clicked\0on_buttonReset_clicked\0"
    "on_buttonResetAll_clicked\0"
    "on_editShortcut_textChanged\0"
    "onAddMacroAction\0onRemoveMacroAction\0"
    "onModifyMacroAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DlgCustomKeyboardImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x09 /* Protected */,
       4,    1,   67,    2, 0x09 /* Protected */,
       6,    0,   70,    2, 0x09 /* Protected */,
       7,    0,   71,    2, 0x09 /* Protected */,
       8,    0,   72,    2, 0x09 /* Protected */,
       9,    0,   73,    2, 0x09 /* Protected */,
      10,    1,   74,    2, 0x09 /* Protected */,
      11,    1,   77,    2, 0x09 /* Protected */,
      12,    1,   80,    2, 0x09 /* Protected */,
      13,    1,   83,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, 0x80000000 | 5,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,

       0        // eod
};

void Gui::Dialog::DlgCustomKeyboardImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgCustomKeyboardImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_categoryBox_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_commandTreeWidget_currentItemChanged((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 2: _t->on_buttonAssign_clicked(); break;
        case 3: _t->on_buttonClear_clicked(); break;
        case 4: _t->on_buttonReset_clicked(); break;
        case 5: _t->on_buttonResetAll_clicked(); break;
        case 6: _t->on_editShortcut_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->onAddMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 8: _t->onRemoveMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 9: _t->onModifyMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DlgCustomKeyboardImp::staticMetaObject = { {
    &CustomizeActionPage::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DlgCustomKeyboardImp.data,
    qt_meta_data_Gui__Dialog__DlgCustomKeyboardImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DlgCustomKeyboardImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DlgCustomKeyboardImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DlgCustomKeyboardImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui_DlgCustomKeyboard"))
        return static_cast< Ui_DlgCustomKeyboard*>(this);
    return CustomizeActionPage::qt_metacast(_clname);
}

int Gui::Dialog::DlgCustomKeyboardImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CustomizeActionPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
