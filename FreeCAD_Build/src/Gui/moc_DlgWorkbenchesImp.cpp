/****************************************************************************
** Meta object code from reading C++ file 'DlgWorkbenchesImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/DlgWorkbenchesImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgWorkbenchesImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__DlgWorkbenchesImp_t {
    QByteArrayData data[11];
    char stringdata0[334];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DlgWorkbenchesImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DlgWorkbenchesImp_t qt_meta_stringdata_Gui__Dialog__DlgWorkbenchesImp = {
    {
QT_MOC_LITERAL(0, 0, 30), // "Gui::Dialog::DlgWorkbenchesImp"
QT_MOC_LITERAL(1, 31, 16), // "onAddMacroAction"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 19), // "onRemoveMacroAction"
QT_MOC_LITERAL(4, 69, 19), // "onModifyMacroAction"
QT_MOC_LITERAL(5, 89, 41), // "on_add_to_enabled_workbenches..."
QT_MOC_LITERAL(6, 131, 46), // "on_remove_from_enabled_workbe..."
QT_MOC_LITERAL(7, 178, 33), // "on_shift_workbench_up_btn_cli..."
QT_MOC_LITERAL(8, 212, 35), // "on_shift_workbench_down_btn_c..."
QT_MOC_LITERAL(9, 248, 39), // "on_sort_enabled_workbenches_b..."
QT_MOC_LITERAL(10, 288, 45) // "on_add_all_to_enabled_workben..."

    },
    "Gui::Dialog::DlgWorkbenchesImp\0"
    "onAddMacroAction\0\0onRemoveMacroAction\0"
    "onModifyMacroAction\0"
    "on_add_to_enabled_workbenches_btn_clicked\0"
    "on_remove_from_enabled_workbenches_btn_clicked\0"
    "on_shift_workbench_up_btn_clicked\0"
    "on_shift_workbench_down_btn_clicked\0"
    "on_sort_enabled_workbenches_btn_clicked\0"
    "on_add_all_to_enabled_workbenches_btn_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DlgWorkbenchesImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x09 /* Protected */,
       3,    1,   62,    2, 0x09 /* Protected */,
       4,    1,   65,    2, 0x09 /* Protected */,
       5,    0,   68,    2, 0x09 /* Protected */,
       6,    0,   69,    2, 0x09 /* Protected */,
       7,    0,   70,    2, 0x09 /* Protected */,
       8,    0,   71,    2, 0x09 /* Protected */,
       9,    0,   72,    2, 0x09 /* Protected */,
      10,    0,   73,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Gui::Dialog::DlgWorkbenchesImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgWorkbenchesImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onAddMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 1: _t->onRemoveMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 2: _t->onModifyMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->on_add_to_enabled_workbenches_btn_clicked(); break;
        case 4: _t->on_remove_from_enabled_workbenches_btn_clicked(); break;
        case 5: _t->on_shift_workbench_up_btn_clicked(); break;
        case 6: _t->on_shift_workbench_down_btn_clicked(); break;
        case 7: _t->on_sort_enabled_workbenches_btn_clicked(); break;
        case 8: _t->on_add_all_to_enabled_workbenches_btn_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DlgWorkbenchesImp::staticMetaObject = { {
    &CustomizeActionPage::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DlgWorkbenchesImp.data,
    qt_meta_data_Gui__Dialog__DlgWorkbenchesImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DlgWorkbenchesImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DlgWorkbenchesImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DlgWorkbenchesImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui_DlgWorkbenches"))
        return static_cast< Ui_DlgWorkbenches*>(this);
    return CustomizeActionPage::qt_metacast(_clname);
}

int Gui::Dialog::DlgWorkbenchesImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CustomizeActionPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
