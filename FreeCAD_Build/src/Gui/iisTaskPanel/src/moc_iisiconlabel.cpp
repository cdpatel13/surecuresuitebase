/****************************************************************************
** Meta object code from reading C++ file 'iisiconlabel.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Gui/iisTaskPanel/src/iisiconlabel.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'iisiconlabel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_iisIconLabel_t {
    QByteArrayData data[7];
    char stringdata0[61];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_iisIconLabel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_iisIconLabel_t qt_meta_stringdata_iisIconLabel = {
    {
QT_MOC_LITERAL(0, 0, 12), // "iisIconLabel"
QT_MOC_LITERAL(1, 13, 7), // "pressed"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 8), // "released"
QT_MOC_LITERAL(4, 31, 7), // "clicked"
QT_MOC_LITERAL(5, 39, 9), // "activated"
QT_MOC_LITERAL(6, 49, 11) // "contextMenu"

    },
    "iisIconLabel\0pressed\0\0released\0clicked\0"
    "activated\0contextMenu"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_iisIconLabel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x06 /* Public */,
       3,    0,   40,    2, 0x06 /* Public */,
       4,    0,   41,    2, 0x06 /* Public */,
       5,    0,   42,    2, 0x06 /* Public */,
       6,    0,   43,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void iisIconLabel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<iisIconLabel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pressed(); break;
        case 1: _t->released(); break;
        case 2: _t->clicked(); break;
        case 3: _t->activated(); break;
        case 4: _t->contextMenu(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (iisIconLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&iisIconLabel::pressed)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (iisIconLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&iisIconLabel::released)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (iisIconLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&iisIconLabel::clicked)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (iisIconLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&iisIconLabel::activated)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (iisIconLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&iisIconLabel::contextMenu)) {
                *result = 4;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject iisIconLabel::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_iisIconLabel.data,
    qt_meta_data_iisIconLabel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *iisIconLabel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *iisIconLabel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_iisIconLabel.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int iisIconLabel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void iisIconLabel::pressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void iisIconLabel::released()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void iisIconLabel::clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void iisIconLabel::activated()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void iisIconLabel::contextMenu()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
