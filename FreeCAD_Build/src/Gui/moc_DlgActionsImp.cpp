/****************************************************************************
** Meta object code from reading C++ file 'DlgActionsImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/DlgActionsImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgActionsImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__DlgCustomActionsImp_t {
    QByteArrayData data[15];
    char stringdata0[313];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DlgCustomActionsImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DlgCustomActionsImp_t qt_meta_stringdata_Gui__Dialog__DlgCustomActionsImp = {
    {
QT_MOC_LITERAL(0, 0, 32), // "Gui::Dialog::DlgCustomActionsImp"
QT_MOC_LITERAL(1, 33, 14), // "addMacroAction"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 17), // "removeMacroAction"
QT_MOC_LITERAL(4, 67, 17), // "modifyMacroAction"
QT_MOC_LITERAL(5, 85, 33), // "on_actionListWidget_itemActiv..."
QT_MOC_LITERAL(6, 119, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(7, 136, 1), // "i"
QT_MOC_LITERAL(8, 138, 29), // "on_buttonChoosePixmap_clicked"
QT_MOC_LITERAL(9, 168, 26), // "on_buttonAddAction_clicked"
QT_MOC_LITERAL(10, 195, 29), // "on_buttonRemoveAction_clicked"
QT_MOC_LITERAL(11, 225, 30), // "on_buttonReplaceAction_clicked"
QT_MOC_LITERAL(12, 256, 16), // "onAddMacroAction"
QT_MOC_LITERAL(13, 273, 19), // "onRemoveMacroAction"
QT_MOC_LITERAL(14, 293, 19) // "onModifyMacroAction"

    },
    "Gui::Dialog::DlgCustomActionsImp\0"
    "addMacroAction\0\0removeMacroAction\0"
    "modifyMacroAction\0on_actionListWidget_itemActivated\0"
    "QTreeWidgetItem*\0i\0on_buttonChoosePixmap_clicked\0"
    "on_buttonAddAction_clicked\0"
    "on_buttonRemoveAction_clicked\0"
    "on_buttonReplaceAction_clicked\0"
    "onAddMacroAction\0onRemoveMacroAction\0"
    "onModifyMacroAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DlgCustomActionsImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x06 /* Public */,
       3,    1,   72,    2, 0x06 /* Public */,
       4,    1,   75,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   78,    2, 0x09 /* Protected */,
       8,    0,   81,    2, 0x09 /* Protected */,
       9,    0,   82,    2, 0x09 /* Protected */,
      10,    0,   83,    2, 0x09 /* Protected */,
      11,    0,   84,    2, 0x09 /* Protected */,
      12,    1,   85,    2, 0x09 /* Protected */,
      13,    1,   88,    2, 0x09 /* Protected */,
      14,    1,   91,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void, QMetaType::QByteArray,    2,

       0        // eod
};

void Gui::Dialog::DlgCustomActionsImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgCustomActionsImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->addMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 1: _t->removeMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 2: _t->modifyMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->on_actionListWidget_itemActivated((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 4: _t->on_buttonChoosePixmap_clicked(); break;
        case 5: _t->on_buttonAddAction_clicked(); break;
        case 6: _t->on_buttonRemoveAction_clicked(); break;
        case 7: _t->on_buttonReplaceAction_clicked(); break;
        case 8: _t->onAddMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 9: _t->onRemoveMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 10: _t->onModifyMacroAction((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (DlgCustomActionsImp::*)(const QByteArray & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DlgCustomActionsImp::addMacroAction)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (DlgCustomActionsImp::*)(const QByteArray & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DlgCustomActionsImp::removeMacroAction)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (DlgCustomActionsImp::*)(const QByteArray & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DlgCustomActionsImp::modifyMacroAction)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DlgCustomActionsImp::staticMetaObject = { {
    &CustomizeActionPage::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DlgCustomActionsImp.data,
    qt_meta_data_Gui__Dialog__DlgCustomActionsImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DlgCustomActionsImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DlgCustomActionsImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DlgCustomActionsImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui_DlgCustomActions"))
        return static_cast< Ui_DlgCustomActions*>(this);
    return CustomizeActionPage::qt_metacast(_clname);
}

int Gui::Dialog::DlgCustomActionsImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CustomizeActionPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void Gui::Dialog::DlgCustomActionsImp::addMacroAction(const QByteArray & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Gui::Dialog::DlgCustomActionsImp::removeMacroAction(const QByteArray & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Gui::Dialog::DlgCustomActionsImp::modifyMacroAction(const QByteArray & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
struct qt_meta_stringdata_Gui__Dialog__IconDialog_t {
    QByteArrayData data[3];
    char stringdata0[39];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__IconDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__IconDialog_t qt_meta_stringdata_Gui__Dialog__IconDialog = {
    {
QT_MOC_LITERAL(0, 0, 23), // "Gui::Dialog::IconDialog"
QT_MOC_LITERAL(1, 24, 13), // "onAddIconPath"
QT_MOC_LITERAL(2, 38, 0) // ""

    },
    "Gui::Dialog::IconDialog\0onAddIconPath\0"
    ""
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__IconDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void Gui::Dialog::IconDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<IconDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onAddIconPath(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::IconDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__IconDialog.data,
    qt_meta_data_Gui__Dialog__IconDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::IconDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::IconDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__IconDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int Gui::Dialog::IconDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_Gui__Dialog__IconFolders_t {
    QByteArrayData data[4];
    char stringdata0[49];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__IconFolders_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__IconFolders_t qt_meta_stringdata_Gui__Dialog__IconFolders = {
    {
QT_MOC_LITERAL(0, 0, 24), // "Gui::Dialog::IconFolders"
QT_MOC_LITERAL(1, 25, 9), // "addFolder"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 12) // "removeFolder"

    },
    "Gui::Dialog::IconFolders\0addFolder\0\0"
    "removeFolder"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__IconFolders[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x08 /* Private */,
       3,    0,   25,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Gui::Dialog::IconFolders::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<IconFolders *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->addFolder(); break;
        case 1: _t->removeFolder(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::IconFolders::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__IconFolders.data,
    qt_meta_data_Gui__Dialog__IconFolders,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::IconFolders::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::IconFolders::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__IconFolders.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int Gui::Dialog::IconFolders::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
