/********************************************************************************
** Form generated from reading UI file 'DlgPropertyLink.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGPROPERTYLINK_H
#define UI_DLGPROPERTYLINK_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTreeWidget>

namespace Gui {
namespace Dialog {

class Ui_DlgPropertyLink
{
public:
    QGridLayout *gridLayout;
    QCheckBox *checkObjectType;
    QTreeWidget *treeWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *searchBox;
    QDialogButtonBox *buttonBox;
    QComboBox *comboBox;
    QTreeWidget *typeTree;

    void setupUi(QDialog *Gui__Dialog__DlgPropertyLink)
    {
        if (Gui__Dialog__DlgPropertyLink->objectName().isEmpty())
            Gui__Dialog__DlgPropertyLink->setObjectName(QString::fromUtf8("Gui__Dialog__DlgPropertyLink"));
        Gui__Dialog__DlgPropertyLink->resize(436, 438);
        gridLayout = new QGridLayout(Gui__Dialog__DlgPropertyLink);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        checkObjectType = new QCheckBox(Gui__Dialog__DlgPropertyLink);
        checkObjectType->setObjectName(QString::fromUtf8("checkObjectType"));

        gridLayout->addWidget(checkObjectType, 6, 0, 1, 1);

        treeWidget = new QTreeWidget(Gui__Dialog__DlgPropertyLink);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        treeWidget->setHeaderItem(__qtreewidgetitem);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        treeWidget->setHeaderHidden(true);
        treeWidget->setExpandsOnDoubleClick(false);

        gridLayout->addWidget(treeWidget, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(Gui__Dialog__DlgPropertyLink);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        searchBox = new QLineEdit(Gui__Dialog__DlgPropertyLink);
        searchBox->setObjectName(QString::fromUtf8("searchBox"));

        horizontalLayout->addWidget(searchBox);


        gridLayout->addLayout(horizontalLayout, 3, 0, 1, 1);

        buttonBox = new QDialogButtonBox(Gui__Dialog__DlgPropertyLink);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 7, 0, 1, 1);

        comboBox = new QComboBox(Gui__Dialog__DlgPropertyLink);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 0, 0, 1, 1);

        typeTree = new QTreeWidget(Gui__Dialog__DlgPropertyLink);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem();
        __qtreewidgetitem1->setText(0, QString::fromUtf8("1"));
        typeTree->setHeaderItem(__qtreewidgetitem1);
        typeTree->setObjectName(QString::fromUtf8("typeTree"));
        typeTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
        typeTree->setRootIsDecorated(false);
        typeTree->header()->setVisible(false);

        gridLayout->addWidget(typeTree, 2, 0, 1, 1);


        retranslateUi(Gui__Dialog__DlgPropertyLink);
        QObject::connect(buttonBox, SIGNAL(accepted()), Gui__Dialog__DlgPropertyLink, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Gui__Dialog__DlgPropertyLink, SLOT(reject()));

        QMetaObject::connectSlotsByName(Gui__Dialog__DlgPropertyLink);
    } // setupUi

    void retranslateUi(QDialog *Gui__Dialog__DlgPropertyLink)
    {
        Gui__Dialog__DlgPropertyLink->setWindowTitle(QApplication::translate("Gui::Dialog::DlgPropertyLink", "Link", nullptr));
        checkObjectType->setText(QApplication::translate("Gui::Dialog::DlgPropertyLink", "Filter by type", nullptr));
        label->setText(QApplication::translate("Gui::Dialog::DlgPropertyLink", "Search", nullptr));
#ifndef QT_NO_TOOLTIP
        searchBox->setToolTip(QApplication::translate("Gui::Dialog::DlgPropertyLink", "A search pattern to filter the results above", nullptr));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

} // namespace Dialog
} // namespace Gui

namespace Gui {
namespace Dialog {
namespace Ui {
    class DlgPropertyLink: public Ui_DlgPropertyLink {};
} // namespace Ui
} // namespace Dialog
} // namespace Gui

#endif // UI_DLGPROPERTYLINK_H
