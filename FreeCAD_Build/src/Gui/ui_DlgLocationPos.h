/********************************************************************************
** Form generated from reading UI file 'DlgLocationPos.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGLOCATIONPOS_H
#define UI_DLGLOCATIONPOS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Position
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QDoubleSpinBox *vectorX;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QDoubleSpinBox *vectorY;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QDoubleSpinBox *vectorZ;
    QCheckBox *checkBox;
    QComboBox *comboBoxGridSize;

    void setupUi(QWidget *Position)
    {
        if (Position->objectName().isEmpty())
            Position->setObjectName(QString::fromUtf8("Position"));
        Position->resize(171, 178);
        verticalLayout = new QVBoxLayout(Position);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(Position);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        vectorX = new QDoubleSpinBox(Position);
        vectorX->setObjectName(QString::fromUtf8("vectorX"));
        vectorX->setMinimum(-2147480000.000000000000000);
        vectorX->setMaximum(2147480000.000000000000000);

        horizontalLayout->addWidget(vectorX);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(Position);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        vectorY = new QDoubleSpinBox(Position);
        vectorY->setObjectName(QString::fromUtf8("vectorY"));
        vectorY->setMinimum(-2147480000.000000000000000);
        vectorY->setMaximum(2147480000.000000000000000);

        horizontalLayout_2->addWidget(vectorY);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(Position);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_3->addWidget(label_3);

        vectorZ = new QDoubleSpinBox(Position);
        vectorZ->setObjectName(QString::fromUtf8("vectorZ"));
        vectorZ->setMinimum(-2147480000.000000000000000);
        vectorZ->setMaximum(2147480000.000000000000000);
        vectorZ->setValue(1.000000000000000);

        horizontalLayout_3->addWidget(vectorZ);


        verticalLayout->addLayout(horizontalLayout_3);

        checkBox = new QCheckBox(Position);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        verticalLayout->addWidget(checkBox);

        comboBoxGridSize = new QComboBox(Position);
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->addItem(QString());
        comboBoxGridSize->setObjectName(QString::fromUtf8("comboBoxGridSize"));
        comboBoxGridSize->setEditable(true);

        verticalLayout->addWidget(comboBoxGridSize);


        retranslateUi(Position);

        QMetaObject::connectSlotsByName(Position);
    } // setupUi

    void retranslateUi(QWidget *Position)
    {
        Position->setWindowTitle(QApplication::translate("Position", "Form", nullptr));
        label->setText(QApplication::translate("Position", "X:", nullptr));
        label_2->setText(QApplication::translate("Position", "Y:", nullptr));
        label_3->setText(QApplication::translate("Position", "Z:", nullptr));
        checkBox->setText(QApplication::translate("Position", "Grid Snap in", nullptr));
        comboBoxGridSize->setItemText(0, QApplication::translate("Position", "0.1 mm", nullptr));
        comboBoxGridSize->setItemText(1, QApplication::translate("Position", "0.5 mm", nullptr));
        comboBoxGridSize->setItemText(2, QApplication::translate("Position", "1 mm", nullptr));
        comboBoxGridSize->setItemText(3, QApplication::translate("Position", "2 mm", nullptr));
        comboBoxGridSize->setItemText(4, QApplication::translate("Position", "5 mm", nullptr));
        comboBoxGridSize->setItemText(5, QApplication::translate("Position", "10 mm", nullptr));
        comboBoxGridSize->setItemText(6, QApplication::translate("Position", "20 mm", nullptr));
        comboBoxGridSize->setItemText(7, QApplication::translate("Position", "50 mm", nullptr));
        comboBoxGridSize->setItemText(8, QApplication::translate("Position", "100 mm", nullptr));
        comboBoxGridSize->setItemText(9, QApplication::translate("Position", "200 mm", nullptr));
        comboBoxGridSize->setItemText(10, QApplication::translate("Position", "500 mm", nullptr));
        comboBoxGridSize->setItemText(11, QApplication::translate("Position", "1 m", nullptr));
        comboBoxGridSize->setItemText(12, QApplication::translate("Position", "2 m", nullptr));
        comboBoxGridSize->setItemText(13, QApplication::translate("Position", "5 m ", nullptr));

    } // retranslateUi

};

namespace Ui {
    class Position: public Ui_Position {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLGLOCATIONPOS_H
