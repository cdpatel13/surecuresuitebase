/****************************************************************************
** Meta object code from reading C++ file 'PropertyItem.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../FreeCAD/src/Gui/propertyeditor/PropertyItem.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PropertyItem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyItem_t {
    QByteArrayData data[1];
    char stringdata0[34];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyItem = {
    {
QT_MOC_LITERAL(0, 0, 33) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyItem::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyItem.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "ExpressionBinding"))
        return static_cast< ExpressionBinding*>(this);
    return QObject::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyStringItem_t {
    QByteArrayData data[1];
    char stringdata0[40];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyStringItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyStringItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyStringItem = {
    {
QT_MOC_LITERAL(0, 0, 39) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyStringItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyStringItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyStringItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyStringItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyStringItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyStringItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyStringItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyStringItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyStringItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyStringItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyFontItem_t {
    QByteArrayData data[1];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyFontItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyFontItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyFontItem = {
    {
QT_MOC_LITERAL(0, 0, 37) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyFontItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyFontItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyFontItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyFontItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyFontItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyFontItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyFontItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyFontItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyFontItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyFontItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertySeparatorItem_t {
    QByteArrayData data[1];
    char stringdata0[43];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertySeparatorItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertySeparatorItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertySeparatorItem = {
    {
QT_MOC_LITERAL(0, 0, 42) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertySeparatorItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertySeparatorItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertySeparatorItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertySeparatorItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertySeparatorItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertySeparatorItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertySeparatorItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertySeparatorItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertySeparatorItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertySeparatorItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerItem_t {
    QByteArrayData data[1];
    char stringdata0[41];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerItem = {
    {
QT_MOC_LITERAL(0, 0, 40) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyIntegerItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyIntegerItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyIntegerItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyIntegerItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyIntegerItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyIntegerItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyIntegerItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyIntegerItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerConstraintItem_t {
    QByteArrayData data[1];
    char stringdata0[51];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerConstraintItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerConstraintItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerConstraintItem = {
    {
QT_MOC_LITERAL(0, 0, 50) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyIntegerConstraintItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyIntegerConstraintItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyIntegerConstraintItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyIntegerConstraintItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerConstraintItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyIntegerConstraintItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyIntegerConstraintItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyIntegerConstraintItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerConstraintItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyIntegerConstraintItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatItem_t {
    QByteArrayData data[1];
    char stringdata0[39];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatItem = {
    {
QT_MOC_LITERAL(0, 0, 38) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyFloatItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyFloatItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyFloatItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyFloatItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyFloatItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyFloatItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyFloatItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyFloatItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitItem_t {
    QByteArrayData data[1];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitItem = {
    {
QT_MOC_LITERAL(0, 0, 37) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyUnitItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyUnitItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyUnitItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyUnitItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyUnitItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyUnitItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyUnitItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyUnitItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitConstraintItem_t {
    QByteArrayData data[1];
    char stringdata0[48];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitConstraintItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitConstraintItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitConstraintItem = {
    {
QT_MOC_LITERAL(0, 0, 47) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyUnitConstraintItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyUnitConstraintItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyUnitConstraintItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyUnitConstraintItem::staticMetaObject = { {
    &PropertyUnitItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitConstraintItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyUnitConstraintItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyUnitConstraintItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyUnitConstraintItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyUnitConstraintItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyUnitItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyUnitConstraintItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyUnitItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatConstraintItem_t {
    QByteArrayData data[1];
    char stringdata0[49];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatConstraintItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatConstraintItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatConstraintItem = {
    {
QT_MOC_LITERAL(0, 0, 48) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyFloatConstraintItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyFloatConstraintItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyFloatConstraintItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyFloatConstraintItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatConstraintItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyFloatConstraintItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyFloatConstraintItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyFloatConstraintItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatConstraintItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyFloatConstraintItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyPrecisionItem_t {
    QByteArrayData data[1];
    char stringdata0[43];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyPrecisionItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyPrecisionItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyPrecisionItem = {
    {
QT_MOC_LITERAL(0, 0, 42) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyPrecisionItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyPrecisionItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyPrecisionItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyPrecisionItem::staticMetaObject = { {
    &PropertyFloatConstraintItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyPrecisionItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyPrecisionItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyPrecisionItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyPrecisionItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyPrecisionItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyFloatConstraintItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyPrecisionItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyFloatConstraintItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyAngleItem_t {
    QByteArrayData data[1];
    char stringdata0[39];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyAngleItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyAngleItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyAngleItem = {
    {
QT_MOC_LITERAL(0, 0, 38) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyAngleItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyAngleItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyAngleItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyAngleItem::staticMetaObject = { {
    &PropertyUnitConstraintItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyAngleItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyAngleItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyAngleItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyAngleItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyAngleItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyUnitConstraintItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyAngleItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyUnitConstraintItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyBoolItem_t {
    QByteArrayData data[1];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyBoolItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyBoolItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyBoolItem = {
    {
QT_MOC_LITERAL(0, 0, 37) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyBoolItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyBoolItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyBoolItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyBoolItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyBoolItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyBoolItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyBoolItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyBoolItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyBoolItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyBoolItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorItem_t {
    QByteArrayData data[4];
    char stringdata0[46];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorItem = {
    {
QT_MOC_LITERAL(0, 0, 39), // "Gui::PropertyEditor::Property..."
QT_MOC_LITERAL(1, 40, 1), // "x"
QT_MOC_LITERAL(2, 42, 1), // "y"
QT_MOC_LITERAL(3, 44, 1) // "z"

    },
    "Gui::PropertyEditor::PropertyVectorItem\0"
    "x\0y\0z"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyVectorItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       3,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::Double, 0x00195103,
       2, QMetaType::Double, 0x00195103,
       3, QMetaType::Double, 0x00195103,

       0        // eod
};

void Gui::PropertyEditor::PropertyVectorItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<PropertyVectorItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< double*>(_v) = _t->x(); break;
        case 1: *reinterpret_cast< double*>(_v) = _t->y(); break;
        case 2: *reinterpret_cast< double*>(_v) = _t->z(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<PropertyVectorItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setX(*reinterpret_cast< double*>(_v)); break;
        case 1: _t->setY(*reinterpret_cast< double*>(_v)); break;
        case 2: _t->setZ(*reinterpret_cast< double*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyVectorItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyVectorItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyVectorItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyVectorItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyVectorItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorDistanceItem_t {
    QByteArrayData data[5];
    char stringdata0[69];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorDistanceItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorDistanceItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorDistanceItem = {
    {
QT_MOC_LITERAL(0, 0, 47), // "Gui::PropertyEditor::Property..."
QT_MOC_LITERAL(1, 48, 1), // "x"
QT_MOC_LITERAL(2, 50, 14), // "Base::Quantity"
QT_MOC_LITERAL(3, 65, 1), // "y"
QT_MOC_LITERAL(4, 67, 1) // "z"

    },
    "Gui::PropertyEditor::PropertyVectorDistanceItem\0"
    "x\0Base::Quantity\0y\0z"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyVectorDistanceItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       3,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, 0x80000000 | 2, 0x0019510b,
       3, 0x80000000 | 2, 0x0019510b,
       4, 0x80000000 | 2, 0x0019510b,

       0        // eod
};

void Gui::PropertyEditor::PropertyVectorDistanceItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
        case 1:
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Base::Quantity >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<PropertyVectorDistanceItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Base::Quantity*>(_v) = _t->x(); break;
        case 1: *reinterpret_cast< Base::Quantity*>(_v) = _t->y(); break;
        case 2: *reinterpret_cast< Base::Quantity*>(_v) = _t->z(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<PropertyVectorDistanceItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setX(*reinterpret_cast< Base::Quantity*>(_v)); break;
        case 1: _t->setY(*reinterpret_cast< Base::Quantity*>(_v)); break;
        case 2: _t->setZ(*reinterpret_cast< Base::Quantity*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyVectorDistanceItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorDistanceItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyVectorDistanceItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyVectorDistanceItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyVectorDistanceItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyVectorDistanceItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyVectorDistanceItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyPositionItem_t {
    QByteArrayData data[1];
    char stringdata0[42];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyPositionItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyPositionItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyPositionItem = {
    {
QT_MOC_LITERAL(0, 0, 41) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyPositionItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyPositionItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyPositionItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyPositionItem::staticMetaObject = { {
    &PropertyVectorDistanceItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyPositionItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyPositionItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyPositionItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyPositionItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyPositionItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyVectorDistanceItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyPositionItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyVectorDistanceItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyDirectionItem_t {
    QByteArrayData data[1];
    char stringdata0[43];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyDirectionItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyDirectionItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyDirectionItem = {
    {
QT_MOC_LITERAL(0, 0, 42) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyDirectionItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyDirectionItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyDirectionItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyDirectionItem::staticMetaObject = { {
    &PropertyVectorDistanceItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyDirectionItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyDirectionItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyDirectionItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyDirectionItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyDirectionItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyVectorDistanceItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyDirectionItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyVectorDistanceItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyMatrixItem_t {
    QByteArrayData data[17];
    char stringdata0[104];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyMatrixItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyMatrixItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyMatrixItem = {
    {
QT_MOC_LITERAL(0, 0, 39), // "Gui::PropertyEditor::Property..."
QT_MOC_LITERAL(1, 40, 3), // "A11"
QT_MOC_LITERAL(2, 44, 3), // "A12"
QT_MOC_LITERAL(3, 48, 3), // "A13"
QT_MOC_LITERAL(4, 52, 3), // "A14"
QT_MOC_LITERAL(5, 56, 3), // "A21"
QT_MOC_LITERAL(6, 60, 3), // "A22"
QT_MOC_LITERAL(7, 64, 3), // "A23"
QT_MOC_LITERAL(8, 68, 3), // "A24"
QT_MOC_LITERAL(9, 72, 3), // "A31"
QT_MOC_LITERAL(10, 76, 3), // "A32"
QT_MOC_LITERAL(11, 80, 3), // "A33"
QT_MOC_LITERAL(12, 84, 3), // "A34"
QT_MOC_LITERAL(13, 88, 3), // "A41"
QT_MOC_LITERAL(14, 92, 3), // "A42"
QT_MOC_LITERAL(15, 96, 3), // "A43"
QT_MOC_LITERAL(16, 100, 3) // "A44"

    },
    "Gui::PropertyEditor::PropertyMatrixItem\0"
    "A11\0A12\0A13\0A14\0A21\0A22\0A23\0A24\0A31\0"
    "A32\0A33\0A34\0A41\0A42\0A43\0A44"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyMatrixItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
      16,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::Double, 0x00195103,
       2, QMetaType::Double, 0x00195103,
       3, QMetaType::Double, 0x00195103,
       4, QMetaType::Double, 0x00195103,
       5, QMetaType::Double, 0x00195103,
       6, QMetaType::Double, 0x00195103,
       7, QMetaType::Double, 0x00195103,
       8, QMetaType::Double, 0x00195103,
       9, QMetaType::Double, 0x00195103,
      10, QMetaType::Double, 0x00195103,
      11, QMetaType::Double, 0x00195103,
      12, QMetaType::Double, 0x00195103,
      13, QMetaType::Double, 0x00195103,
      14, QMetaType::Double, 0x00195103,
      15, QMetaType::Double, 0x00195103,
      16, QMetaType::Double, 0x00195103,

       0        // eod
};

void Gui::PropertyEditor::PropertyMatrixItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<PropertyMatrixItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< double*>(_v) = _t->getA11(); break;
        case 1: *reinterpret_cast< double*>(_v) = _t->getA12(); break;
        case 2: *reinterpret_cast< double*>(_v) = _t->getA13(); break;
        case 3: *reinterpret_cast< double*>(_v) = _t->getA14(); break;
        case 4: *reinterpret_cast< double*>(_v) = _t->getA21(); break;
        case 5: *reinterpret_cast< double*>(_v) = _t->getA22(); break;
        case 6: *reinterpret_cast< double*>(_v) = _t->getA23(); break;
        case 7: *reinterpret_cast< double*>(_v) = _t->getA24(); break;
        case 8: *reinterpret_cast< double*>(_v) = _t->getA31(); break;
        case 9: *reinterpret_cast< double*>(_v) = _t->getA32(); break;
        case 10: *reinterpret_cast< double*>(_v) = _t->getA33(); break;
        case 11: *reinterpret_cast< double*>(_v) = _t->getA34(); break;
        case 12: *reinterpret_cast< double*>(_v) = _t->getA41(); break;
        case 13: *reinterpret_cast< double*>(_v) = _t->getA42(); break;
        case 14: *reinterpret_cast< double*>(_v) = _t->getA43(); break;
        case 15: *reinterpret_cast< double*>(_v) = _t->getA44(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<PropertyMatrixItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setA11(*reinterpret_cast< double*>(_v)); break;
        case 1: _t->setA12(*reinterpret_cast< double*>(_v)); break;
        case 2: _t->setA13(*reinterpret_cast< double*>(_v)); break;
        case 3: _t->setA14(*reinterpret_cast< double*>(_v)); break;
        case 4: _t->setA21(*reinterpret_cast< double*>(_v)); break;
        case 5: _t->setA22(*reinterpret_cast< double*>(_v)); break;
        case 6: _t->setA23(*reinterpret_cast< double*>(_v)); break;
        case 7: _t->setA24(*reinterpret_cast< double*>(_v)); break;
        case 8: _t->setA31(*reinterpret_cast< double*>(_v)); break;
        case 9: _t->setA32(*reinterpret_cast< double*>(_v)); break;
        case 10: _t->setA33(*reinterpret_cast< double*>(_v)); break;
        case 11: _t->setA34(*reinterpret_cast< double*>(_v)); break;
        case 12: _t->setA41(*reinterpret_cast< double*>(_v)); break;
        case 13: _t->setA42(*reinterpret_cast< double*>(_v)); break;
        case 14: _t->setA43(*reinterpret_cast< double*>(_v)); break;
        case 15: _t->setA44(*reinterpret_cast< double*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyMatrixItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyMatrixItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyMatrixItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyMatrixItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyMatrixItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyMatrixItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyMatrixItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 16;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PlacementEditor_t {
    QByteArrayData data[4];
    char stringdata0[52];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PlacementEditor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PlacementEditor_t qt_meta_stringdata_Gui__PropertyEditor__PlacementEditor = {
    {
QT_MOC_LITERAL(0, 0, 36), // "Gui::PropertyEditor::Placemen..."
QT_MOC_LITERAL(1, 37, 11), // "updateValue"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 1) // "v"

    },
    "Gui::PropertyEditor::PlacementEditor\0"
    "updateValue\0\0v"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PlacementEditor[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    3,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QVariant, QMetaType::Bool, QMetaType::Bool,    3,    2,    2,

       0        // eod
};

void Gui::PropertyEditor::PlacementEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PlacementEditor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updateValue((*reinterpret_cast< const QVariant(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PlacementEditor::staticMetaObject = { {
    &Gui::LabelButton::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PlacementEditor.data,
    qt_meta_data_Gui__PropertyEditor__PlacementEditor,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PlacementEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PlacementEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PlacementEditor.stringdata0))
        return static_cast<void*>(this);
    return Gui::LabelButton::qt_metacast(_clname);
}

int Gui::PropertyEditor::PlacementEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::LabelButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyPlacementItem_t {
    QByteArrayData data[6];
    char stringdata0[93];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyPlacementItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyPlacementItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyPlacementItem = {
    {
QT_MOC_LITERAL(0, 0, 42), // "Gui::PropertyEditor::Property..."
QT_MOC_LITERAL(1, 43, 5), // "Angle"
QT_MOC_LITERAL(2, 49, 14), // "Base::Quantity"
QT_MOC_LITERAL(3, 64, 4), // "Axis"
QT_MOC_LITERAL(4, 69, 14), // "Base::Vector3d"
QT_MOC_LITERAL(5, 84, 8) // "Position"

    },
    "Gui::PropertyEditor::PropertyPlacementItem\0"
    "Angle\0Base::Quantity\0Axis\0Base::Vector3d\0"
    "Position"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyPlacementItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       3,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, 0x80000000 | 2, 0x0019510b,
       3, 0x80000000 | 4, 0x0019510b,
       5, 0x80000000 | 4, 0x0019510b,

       0        // eod
};

void Gui::PropertyEditor::PropertyPlacementItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Base::Quantity >(); break;
        case 2:
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Base::Vector3d >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<PropertyPlacementItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Base::Quantity*>(_v) = _t->getAngle(); break;
        case 1: *reinterpret_cast< Base::Vector3d*>(_v) = _t->getAxis(); break;
        case 2: *reinterpret_cast< Base::Vector3d*>(_v) = _t->getPosition(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<PropertyPlacementItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setAngle(*reinterpret_cast< Base::Quantity*>(_v)); break;
        case 1: _t->setAxis(*reinterpret_cast< Base::Vector3d*>(_v)); break;
        case 2: _t->setPosition(*reinterpret_cast< Base::Vector3d*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyPlacementItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyPlacementItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyPlacementItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyPlacementItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyPlacementItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyPlacementItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyPlacementItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyEnumItem_t {
    QByteArrayData data[1];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyEnumItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyEnumItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyEnumItem = {
    {
QT_MOC_LITERAL(0, 0, 37) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyEnumItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyEnumItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyEnumItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyEnumItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyEnumItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyEnumItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyEnumItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyEnumItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyEnumItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyEnumItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyStringListItem_t {
    QByteArrayData data[1];
    char stringdata0[44];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyStringListItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyStringListItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyStringListItem = {
    {
QT_MOC_LITERAL(0, 0, 43) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyStringListItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyStringListItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyStringListItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyStringListItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyStringListItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyStringListItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyStringListItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyStringListItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyStringListItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyStringListItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatListItem_t {
    QByteArrayData data[1];
    char stringdata0[43];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatListItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatListItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatListItem = {
    {
QT_MOC_LITERAL(0, 0, 42) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyFloatListItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyFloatListItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyFloatListItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyFloatListItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatListItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyFloatListItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyFloatListItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyFloatListItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyFloatListItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyFloatListItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerListItem_t {
    QByteArrayData data[1];
    char stringdata0[45];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerListItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerListItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerListItem = {
    {
QT_MOC_LITERAL(0, 0, 44) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyIntegerListItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyIntegerListItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyIntegerListItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyIntegerListItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerListItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyIntegerListItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyIntegerListItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyIntegerListItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyIntegerListItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyIntegerListItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyColorItem_t {
    QByteArrayData data[1];
    char stringdata0[39];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyColorItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyColorItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyColorItem = {
    {
QT_MOC_LITERAL(0, 0, 38) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyColorItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyColorItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyColorItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyColorItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyColorItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyColorItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyColorItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyColorItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyColorItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyColorItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialItem_t {
    QByteArrayData data[7];
    char stringdata0[119];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialItem = {
    {
QT_MOC_LITERAL(0, 0, 41), // "Gui::PropertyEditor::Property..."
QT_MOC_LITERAL(1, 42, 12), // "AmbientColor"
QT_MOC_LITERAL(2, 55, 12), // "DiffuseColor"
QT_MOC_LITERAL(3, 68, 13), // "SpecularColor"
QT_MOC_LITERAL(4, 82, 13), // "EmissiveColor"
QT_MOC_LITERAL(5, 96, 9), // "Shininess"
QT_MOC_LITERAL(6, 106, 12) // "Transparency"

    },
    "Gui::PropertyEditor::PropertyMaterialItem\0"
    "AmbientColor\0DiffuseColor\0SpecularColor\0"
    "EmissiveColor\0Shininess\0Transparency"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyMaterialItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       6,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::QColor, 0x00195103,
       2, QMetaType::QColor, 0x00195103,
       3, QMetaType::QColor, 0x00195103,
       4, QMetaType::QColor, 0x00195103,
       5, QMetaType::Float, 0x00195103,
       6, QMetaType::Float, 0x00195103,

       0        // eod
};

void Gui::PropertyEditor::PropertyMaterialItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<PropertyMaterialItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QColor*>(_v) = _t->getAmbientColor(); break;
        case 1: *reinterpret_cast< QColor*>(_v) = _t->getDiffuseColor(); break;
        case 2: *reinterpret_cast< QColor*>(_v) = _t->getSpecularColor(); break;
        case 3: *reinterpret_cast< QColor*>(_v) = _t->getEmissiveColor(); break;
        case 4: *reinterpret_cast< float*>(_v) = _t->getShininess(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->getTransparency(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<PropertyMaterialItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setAmbientColor(*reinterpret_cast< QColor*>(_v)); break;
        case 1: _t->setDiffuseColor(*reinterpret_cast< QColor*>(_v)); break;
        case 2: _t->setSpecularColor(*reinterpret_cast< QColor*>(_v)); break;
        case 3: _t->setEmissiveColor(*reinterpret_cast< QColor*>(_v)); break;
        case 4: _t->setShininess(*reinterpret_cast< float*>(_v)); break;
        case 5: _t->setTransparency(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyMaterialItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyMaterialItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyMaterialItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyMaterialItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyMaterialItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialListItem_t {
    QByteArrayData data[7];
    char stringdata0[123];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialListItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialListItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialListItem = {
    {
QT_MOC_LITERAL(0, 0, 45), // "Gui::PropertyEditor::Property..."
QT_MOC_LITERAL(1, 46, 12), // "AmbientColor"
QT_MOC_LITERAL(2, 59, 12), // "DiffuseColor"
QT_MOC_LITERAL(3, 72, 13), // "SpecularColor"
QT_MOC_LITERAL(4, 86, 13), // "EmissiveColor"
QT_MOC_LITERAL(5, 100, 9), // "Shininess"
QT_MOC_LITERAL(6, 110, 12) // "Transparency"

    },
    "Gui::PropertyEditor::PropertyMaterialListItem\0"
    "AmbientColor\0DiffuseColor\0SpecularColor\0"
    "EmissiveColor\0Shininess\0Transparency"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyMaterialListItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       6,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::QColor, 0x00195103,
       2, QMetaType::QColor, 0x00195103,
       3, QMetaType::QColor, 0x00195103,
       4, QMetaType::QColor, 0x00195103,
       5, QMetaType::Float, 0x00195103,
       6, QMetaType::Float, 0x00195103,

       0        // eod
};

void Gui::PropertyEditor::PropertyMaterialListItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<PropertyMaterialListItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QColor*>(_v) = _t->getAmbientColor(); break;
        case 1: *reinterpret_cast< QColor*>(_v) = _t->getDiffuseColor(); break;
        case 2: *reinterpret_cast< QColor*>(_v) = _t->getSpecularColor(); break;
        case 3: *reinterpret_cast< QColor*>(_v) = _t->getEmissiveColor(); break;
        case 4: *reinterpret_cast< float*>(_v) = _t->getShininess(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->getTransparency(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<PropertyMaterialListItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setAmbientColor(*reinterpret_cast< QColor*>(_v)); break;
        case 1: _t->setDiffuseColor(*reinterpret_cast< QColor*>(_v)); break;
        case 2: _t->setSpecularColor(*reinterpret_cast< QColor*>(_v)); break;
        case 3: _t->setEmissiveColor(*reinterpret_cast< QColor*>(_v)); break;
        case 4: _t->setShininess(*reinterpret_cast< float*>(_v)); break;
        case 5: _t->setTransparency(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyMaterialListItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialListItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyMaterialListItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyMaterialListItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyMaterialListItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyMaterialListItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyMaterialListItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
   if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyFileItem_t {
    QByteArrayData data[1];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyFileItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyFileItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyFileItem = {
    {
QT_MOC_LITERAL(0, 0, 37) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyFileItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyFileItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyFileItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyFileItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyFileItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyFileItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyFileItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyFileItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyFileItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyFileItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyPathItem_t {
    QByteArrayData data[1];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyPathItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyPathItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyPathItem = {
    {
QT_MOC_LITERAL(0, 0, 37) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyPathItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyPathItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyPathItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyPathItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyPathItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyPathItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyPathItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyPathItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyPathItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyPathItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyTransientFileItem_t {
    QByteArrayData data[1];
    char stringdata0[47];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyTransientFileItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyTransientFileItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyTransientFileItem = {
    {
QT_MOC_LITERAL(0, 0, 46) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyTransientFileItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyTransientFileItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyTransientFileItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyTransientFileItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyTransientFileItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyTransientFileItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyTransientFileItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyTransientFileItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyTransientFileItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyTransientFileItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__LinkSelection_t {
    QByteArrayData data[3];
    char stringdata0[43];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__LinkSelection_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__LinkSelection_t qt_meta_stringdata_Gui__PropertyEditor__LinkSelection = {
    {
QT_MOC_LITERAL(0, 0, 34), // "Gui::PropertyEditor::LinkSele..."
QT_MOC_LITERAL(1, 35, 6), // "select"
QT_MOC_LITERAL(2, 42, 0) // ""

    },
    "Gui::PropertyEditor::LinkSelection\0"
    "select\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__LinkSelection[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void Gui::PropertyEditor::LinkSelection::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LinkSelection *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->select(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::LinkSelection::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__LinkSelection.data,
    qt_meta_data_Gui__PropertyEditor__LinkSelection,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::LinkSelection::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::LinkSelection::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__LinkSelection.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Gui::PropertyEditor::LinkSelection::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__LinkLabel_t {
    QByteArrayData data[5];
    char stringdata0[74];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__LinkLabel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__LinkLabel_t qt_meta_stringdata_Gui__PropertyEditor__LinkLabel = {
    {
QT_MOC_LITERAL(0, 0, 30), // "Gui::PropertyEditor::LinkLabel"
QT_MOC_LITERAL(1, 31, 11), // "linkChanged"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 15), // "onLinkActivated"
QT_MOC_LITERAL(4, 60, 13) // "onEditClicked"

    },
    "Gui::PropertyEditor::LinkLabel\0"
    "linkChanged\0\0onLinkActivated\0onEditClicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__LinkLabel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,   32,    2, 0x09 /* Protected */,
       4,    0,   35,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, QMetaType::QStringList,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,

       0        // eod
};

void Gui::PropertyEditor::LinkLabel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LinkLabel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->linkChanged((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 1: _t->onLinkActivated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->onEditClicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (LinkLabel::*)(const QStringList & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LinkLabel::linkChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::LinkLabel::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__LinkLabel.data,
    qt_meta_data_Gui__PropertyEditor__LinkLabel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::LinkLabel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::LinkLabel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__LinkLabel.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int Gui::PropertyEditor::LinkLabel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void Gui::PropertyEditor::LinkLabel::linkChanged(const QStringList & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkItem_t {
    QByteArrayData data[1];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkItem = {
    {
QT_MOC_LITERAL(0, 0, 37) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyLinkItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyLinkItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyLinkItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyLinkItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyLinkItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyLinkItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyLinkItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyLinkItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__PropertyEditor__LinkListLabel_t {
    QByteArrayData data[4];
    char stringdata0[62];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__LinkListLabel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__LinkListLabel_t qt_meta_stringdata_Gui__PropertyEditor__LinkListLabel = {
    {
QT_MOC_LITERAL(0, 0, 34), // "Gui::PropertyEditor::LinkList..."
QT_MOC_LITERAL(1, 35, 11), // "linkChanged"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 13) // "onEditClicked"

    },
    "Gui::PropertyEditor::LinkListLabel\0"
    "linkChanged\0\0onEditClicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__LinkListLabel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   27,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, QMetaType::QVariantList,    2,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void Gui::PropertyEditor::LinkListLabel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LinkListLabel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->linkChanged((*reinterpret_cast< const QVariantList(*)>(_a[1]))); break;
        case 1: _t->onEditClicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (LinkListLabel::*)(const QVariantList & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LinkListLabel::linkChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::LinkListLabel::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__LinkListLabel.data,
    qt_meta_data_Gui__PropertyEditor__LinkListLabel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::LinkListLabel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::LinkListLabel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__LinkListLabel.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int Gui::PropertyEditor::LinkListLabel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void Gui::PropertyEditor::LinkListLabel::linkChanged(const QVariantList & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
struct qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkListItem_t {
    QByteArrayData data[1];
    char stringdata0[42];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkListItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkListItem_t qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkListItem = {
    {
QT_MOC_LITERAL(0, 0, 41) // "Gui::PropertyEditor::Property..."

    },
    "Gui::PropertyEditor::PropertyLinkListItem"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__PropertyEditor__PropertyLinkListItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::PropertyEditor::PropertyLinkListItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::PropertyEditor::PropertyLinkListItem::staticMetaObject = { {
    &PropertyItem::staticMetaObject,
    qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkListItem.data,
    qt_meta_data_Gui__PropertyEditor__PropertyLinkListItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::PropertyEditor::PropertyLinkListItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::PropertyEditor::PropertyLinkListItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__PropertyEditor__PropertyLinkListItem.stringdata0))
        return static_cast<void*>(this);
    return PropertyItem::qt_metacast(_clname);
}

int Gui::PropertyEditor::PropertyLinkListItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PropertyItem::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
