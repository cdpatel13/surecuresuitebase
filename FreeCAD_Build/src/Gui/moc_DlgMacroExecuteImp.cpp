/****************************************************************************
** Meta object code from reading C++ file 'DlgMacroExecuteImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/DlgMacroExecuteImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgMacroExecuteImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__DlgMacroExecuteImp_t {
    QByteArrayData data[15];
    char stringdata0[370];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DlgMacroExecuteImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DlgMacroExecuteImp_t qt_meta_stringdata_Gui__Dialog__DlgMacroExecuteImp = {
    {
QT_MOC_LITERAL(0, 0, 31), // "Gui::Dialog::DlgMacroExecuteImp"
QT_MOC_LITERAL(1, 32, 30), // "on_fileChooser_fileNameChanged"
QT_MOC_LITERAL(2, 63, 0), // ""
QT_MOC_LITERAL(3, 64, 23), // "on_createButton_clicked"
QT_MOC_LITERAL(4, 88, 23), // "on_deleteButton_clicked"
QT_MOC_LITERAL(5, 112, 21), // "on_editButton_clicked"
QT_MOC_LITERAL(6, 134, 23), // "on_renameButton_clicked"
QT_MOC_LITERAL(7, 158, 26), // "on_duplicateButton_clicked"
QT_MOC_LITERAL(8, 185, 24), // "on_toolbarButton_clicked"
QT_MOC_LITERAL(9, 210, 23), // "on_addonsButton_clicked"
QT_MOC_LITERAL(10, 234, 38), // "on_userMacroListBox_currentIt..."
QT_MOC_LITERAL(11, 273, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(12, 290, 40), // "on_systemMacroListBox_current..."
QT_MOC_LITERAL(13, 331, 32), // "on_tabMacroWidget_currentChanged"
QT_MOC_LITERAL(14, 364, 5) // "index"

    },
    "Gui::Dialog::DlgMacroExecuteImp\0"
    "on_fileChooser_fileNameChanged\0\0"
    "on_createButton_clicked\0on_deleteButton_clicked\0"
    "on_editButton_clicked\0on_renameButton_clicked\0"
    "on_duplicateButton_clicked\0"
    "on_toolbarButton_clicked\0"
    "on_addonsButton_clicked\0"
    "on_userMacroListBox_currentItemChanged\0"
    "QTreeWidgetItem*\0"
    "on_systemMacroListBox_currentItemChanged\0"
    "on_tabMacroWidget_currentChanged\0index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DlgMacroExecuteImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x0a /* Public */,
       3,    0,   72,    2, 0x0a /* Public */,
       4,    0,   73,    2, 0x0a /* Public */,
       5,    0,   74,    2, 0x0a /* Public */,
       6,    0,   75,    2, 0x0a /* Public */,
       7,    0,   76,    2, 0x0a /* Public */,
       8,    0,   77,    2, 0x0a /* Public */,
       9,    0,   78,    2, 0x0a /* Public */,
      10,    1,   79,    2, 0x09 /* Protected */,
      12,    1,   82,    2, 0x09 /* Protected */,
      13,    1,   85,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,    2,
    QMetaType::Void, 0x80000000 | 11,    2,
    QMetaType::Void, QMetaType::Int,   14,

       0        // eod
};

void Gui::Dialog::DlgMacroExecuteImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgMacroExecuteImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_fileChooser_fileNameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_createButton_clicked(); break;
        case 2: _t->on_deleteButton_clicked(); break;
        case 3: _t->on_editButton_clicked(); break;
        case 4: _t->on_renameButton_clicked(); break;
        case 5: _t->on_duplicateButton_clicked(); break;
        case 6: _t->on_toolbarButton_clicked(); break;
        case 7: _t->on_addonsButton_clicked(); break;
        case 8: _t->on_userMacroListBox_currentItemChanged((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 9: _t->on_systemMacroListBox_currentItemChanged((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 10: _t->on_tabMacroWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DlgMacroExecuteImp::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DlgMacroExecuteImp.data,
    qt_meta_data_Gui__Dialog__DlgMacroExecuteImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DlgMacroExecuteImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DlgMacroExecuteImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DlgMacroExecuteImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui_DlgMacroExecute"))
        return static_cast< Ui_DlgMacroExecute*>(this);
    if (!strcmp(_clname, "Gui::WindowParameter"))
        return static_cast< Gui::WindowParameter*>(this);
    return QDialog::qt_metacast(_clname);
}

int Gui::Dialog::DlgMacroExecuteImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
