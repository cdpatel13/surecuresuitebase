/****************************************************************************
** Meta object code from reading C++ file 'DlgDisplayPropertiesImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/DlgDisplayPropertiesImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgDisplayPropertiesImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__Dialog__DlgDisplayPropertiesImp_t {
    QByteArrayData data[13];
    char stringdata0[358];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__Dialog__DlgDisplayPropertiesImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__Dialog__DlgDisplayPropertiesImp_t qt_meta_stringdata_Gui__Dialog__DlgDisplayPropertiesImp = {
    {
QT_MOC_LITERAL(0, 0, 36), // "Gui::Dialog::DlgDisplayProper..."
QT_MOC_LITERAL(1, 37, 27), // "on_changeMaterial_activated"
QT_MOC_LITERAL(2, 65, 0), // ""
QT_MOC_LITERAL(3, 66, 23), // "on_changeMode_activated"
QT_MOC_LITERAL(4, 90, 23), // "on_changePlot_activated"
QT_MOC_LITERAL(5, 114, 22), // "on_buttonColor_changed"
QT_MOC_LITERAL(6, 137, 32), // "on_spinTransparency_valueChanged"
QT_MOC_LITERAL(7, 170, 29), // "on_spinPointSize_valueChanged"
QT_MOC_LITERAL(8, 200, 26), // "on_buttonLineColor_changed"
QT_MOC_LITERAL(9, 227, 29), // "on_spinLineWidth_valueChanged"
QT_MOC_LITERAL(10, 257, 36), // "on_spinLineTransparency_value..."
QT_MOC_LITERAL(11, 294, 36), // "on_buttonUserDefinedMaterial_..."
QT_MOC_LITERAL(12, 331, 26) // "on_buttonColorPlot_clicked"

    },
    "Gui::Dialog::DlgDisplayPropertiesImp\0"
    "on_changeMaterial_activated\0\0"
    "on_changeMode_activated\0on_changePlot_activated\0"
    "on_buttonColor_changed\0"
    "on_spinTransparency_valueChanged\0"
    "on_spinPointSize_valueChanged\0"
    "on_buttonLineColor_changed\0"
    "on_spinLineWidth_valueChanged\0"
    "on_spinLineTransparency_valueChanged\0"
    "on_buttonUserDefinedMaterial_clicked\0"
    "on_buttonColorPlot_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__Dialog__DlgDisplayPropertiesImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x08 /* Private */,
       3,    1,   72,    2, 0x08 /* Private */,
       4,    1,   75,    2, 0x08 /* Private */,
       5,    0,   78,    2, 0x08 /* Private */,
       6,    1,   79,    2, 0x08 /* Private */,
       7,    1,   82,    2, 0x08 /* Private */,
       8,    0,   85,    2, 0x08 /* Private */,
       9,    1,   86,    2, 0x08 /* Private */,
      10,    1,   89,    2, 0x08 /* Private */,
      11,    0,   92,    2, 0x08 /* Private */,
      12,    0,   93,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Gui::Dialog::DlgDisplayPropertiesImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgDisplayPropertiesImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_changeMaterial_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_changeMode_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->on_changePlot_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->on_buttonColor_changed(); break;
        case 4: _t->on_spinTransparency_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->on_spinPointSize_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_buttonLineColor_changed(); break;
        case 7: _t->on_spinLineWidth_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_spinLineTransparency_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_buttonUserDefinedMaterial_clicked(); break;
        case 10: _t->on_buttonColorPlot_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::Dialog::DlgDisplayPropertiesImp::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_Gui__Dialog__DlgDisplayPropertiesImp.data,
    qt_meta_data_Gui__Dialog__DlgDisplayPropertiesImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::Dialog::DlgDisplayPropertiesImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::Dialog::DlgDisplayPropertiesImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__Dialog__DlgDisplayPropertiesImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui_DlgDisplayProperties"))
        return static_cast< Ui_DlgDisplayProperties*>(this);
    if (!strcmp(_clname, "Gui::SelectionSingleton::ObserverType"))
        return static_cast< Gui::SelectionSingleton::ObserverType*>(this);
    return QDialog::qt_metacast(_clname);
}

int Gui::Dialog::DlgDisplayPropertiesImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
