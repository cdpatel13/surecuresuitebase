/****************************************************************************
** Meta object code from reading C++ file 'Tree.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../FreeCAD/src/Gui/Tree.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Tree.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Gui__TreeWidget_t {
    QByteArrayData data[28];
    char stringdata0[402];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__TreeWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__TreeWidget_t qt_meta_stringdata_Gui__TreeWidget = {
    {
QT_MOC_LITERAL(0, 0, 15), // "Gui::TreeWidget"
QT_MOC_LITERAL(1, 16, 17), // "emitSearchObjects"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 13), // "onCreateGroup"
QT_MOC_LITERAL(4, 49, 15), // "onRelabelObject"
QT_MOC_LITERAL(5, 65, 18), // "onActivateDocument"
QT_MOC_LITERAL(6, 84, 8), // "QAction*"
QT_MOC_LITERAL(7, 93, 14), // "onStartEditing"
QT_MOC_LITERAL(8, 108, 15), // "onFinishEditing"
QT_MOC_LITERAL(9, 124, 15), // "onSkipRecompute"
QT_MOC_LITERAL(10, 140, 2), // "on"
QT_MOC_LITERAL(11, 143, 23), // "onAllowPartialRecompute"
QT_MOC_LITERAL(12, 167, 11), // "onReloadDoc"
QT_MOC_LITERAL(13, 179, 10), // "onCloseDoc"
QT_MOC_LITERAL(14, 190, 15), // "onMarkRecompute"
QT_MOC_LITERAL(15, 206, 17), // "onRecomputeObject"
QT_MOC_LITERAL(16, 224, 16), // "onPreSelectTimer"
QT_MOC_LITERAL(17, 241, 13), // "onSelectTimer"
QT_MOC_LITERAL(18, 255, 12), // "onShowHidden"
QT_MOC_LITERAL(19, 268, 12), // "onHideInTree"
QT_MOC_LITERAL(20, 281, 15), // "onSearchObjects"
QT_MOC_LITERAL(21, 297, 22), // "onItemSelectionChanged"
QT_MOC_LITERAL(22, 320, 13), // "onItemEntered"
QT_MOC_LITERAL(23, 334, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(24, 351, 4), // "item"
QT_MOC_LITERAL(25, 356, 15), // "onItemCollapsed"
QT_MOC_LITERAL(26, 372, 14), // "onItemExpanded"
QT_MOC_LITERAL(27, 387, 14) // "onUpdateStatus"

    },
    "Gui::TreeWidget\0emitSearchObjects\0\0"
    "onCreateGroup\0onRelabelObject\0"
    "onActivateDocument\0QAction*\0onStartEditing\0"
    "onFinishEditing\0onSkipRecompute\0on\0"
    "onAllowPartialRecompute\0onReloadDoc\0"
    "onCloseDoc\0onMarkRecompute\0onRecomputeObject\0"
    "onPreSelectTimer\0onSelectTimer\0"
    "onShowHidden\0onHideInTree\0onSearchObjects\0"
    "onItemSelectionChanged\0onItemEntered\0"
    "QTreeWidgetItem*\0item\0onItemCollapsed\0"
    "onItemExpanded\0onUpdateStatus"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__TreeWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,  125,    2, 0x09 /* Protected */,
       4,    0,  126,    2, 0x09 /* Protected */,
       5,    1,  127,    2, 0x09 /* Protected */,
       7,    0,  130,    2, 0x09 /* Protected */,
       8,    0,  131,    2, 0x09 /* Protected */,
       9,    1,  132,    2, 0x09 /* Protected */,
      11,    1,  135,    2, 0x09 /* Protected */,
      12,    0,  138,    2, 0x09 /* Protected */,
      13,    0,  139,    2, 0x09 /* Protected */,
      14,    0,  140,    2, 0x09 /* Protected */,
      15,    0,  141,    2, 0x09 /* Protected */,
      16,    0,  142,    2, 0x09 /* Protected */,
      17,    0,  143,    2, 0x09 /* Protected */,
      18,    0,  144,    2, 0x09 /* Protected */,
      19,    0,  145,    2, 0x09 /* Protected */,
      20,    0,  146,    2, 0x09 /* Protected */,
      21,    0,  147,    2, 0x08 /* Private */,
      22,    1,  148,    2, 0x08 /* Private */,
      25,    1,  151,    2, 0x08 /* Private */,
      26,    1,  154,    2, 0x08 /* Private */,
      27,    0,  157,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void,

       0        // eod
};

void Gui::TreeWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TreeWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->emitSearchObjects(); break;
        case 1: _t->onCreateGroup(); break;
        case 2: _t->onRelabelObject(); break;
        case 3: _t->onActivateDocument((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 4: _t->onStartEditing(); break;
        case 5: _t->onFinishEditing(); break;
        case 6: _t->onSkipRecompute((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->onAllowPartialRecompute((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->onReloadDoc(); break;
        case 9: _t->onCloseDoc(); break;
        case 10: _t->onMarkRecompute(); break;
        case 11: _t->onRecomputeObject(); break;
        case 12: _t->onPreSelectTimer(); break;
        case 13: _t->onSelectTimer(); break;
        case 14: _t->onShowHidden(); break;
        case 15: _t->onHideInTree(); break;
        case 16: _t->onSearchObjects(); break;
        case 17: _t->onItemSelectionChanged(); break;
        case 18: _t->onItemEntered((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 19: _t->onItemCollapsed((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 20: _t->onItemExpanded((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 21: _t->onUpdateStatus(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TreeWidget::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TreeWidget::emitSearchObjects)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::TreeWidget::staticMetaObject = { {
    &QTreeWidget::staticMetaObject,
    qt_meta_stringdata_Gui__TreeWidget.data,
    qt_meta_data_Gui__TreeWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::TreeWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::TreeWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__TreeWidget.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "SelectionObserver"))
        return static_cast< SelectionObserver*>(this);
    return QTreeWidget::qt_metacast(_clname);
}

int Gui::TreeWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTreeWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void Gui::TreeWidget::emitSearchObjects()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_Gui__TreePanel_t {
    QByteArrayData data[7];
    char stringdata0[61];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__TreePanel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__TreePanel_t qt_meta_stringdata_Gui__TreePanel = {
    {
QT_MOC_LITERAL(0, 0, 14), // "Gui::TreePanel"
QT_MOC_LITERAL(1, 15, 6), // "accept"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 10), // "showEditor"
QT_MOC_LITERAL(4, 34, 10), // "hideEditor"
QT_MOC_LITERAL(5, 45, 10), // "itemSearch"
QT_MOC_LITERAL(6, 56, 4) // "text"

    },
    "Gui::TreePanel\0accept\0\0showEditor\0"
    "hideEditor\0itemSearch\0text"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__TreePanel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x08 /* Private */,
       3,    0,   35,    2, 0x08 /* Private */,
       4,    0,   36,    2, 0x08 /* Private */,
       5,    1,   37,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    6,

       0        // eod
};

void Gui::TreePanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TreePanel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->accept(); break;
        case 1: _t->showEditor(); break;
        case 2: _t->hideEditor(); break;
        case 3: _t->itemSearch((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Gui::TreePanel::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_Gui__TreePanel.data,
    qt_meta_data_Gui__TreePanel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::TreePanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::TreePanel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__TreePanel.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int Gui::TreePanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_Gui__TreeDockWidget_t {
    QByteArrayData data[1];
    char stringdata0[20];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__TreeDockWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__TreeDockWidget_t qt_meta_stringdata_Gui__TreeDockWidget = {
    {
QT_MOC_LITERAL(0, 0, 19) // "Gui::TreeDockWidget"

    },
    "Gui::TreeDockWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__TreeDockWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::TreeDockWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::TreeDockWidget::staticMetaObject = { {
    &Gui::DockWindow::staticMetaObject,
    qt_meta_stringdata_Gui__TreeDockWidget.data,
    qt_meta_data_Gui__TreeDockWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::TreeDockWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::TreeDockWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__TreeDockWidget.stringdata0))
        return static_cast<void*>(this);
    return Gui::DockWindow::qt_metacast(_clname);
}

int Gui::TreeDockWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::DockWindow::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_Gui__TreeWidgetEditDelegate_t {
    QByteArrayData data[1];
    char stringdata0[28];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Gui__TreeWidgetEditDelegate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Gui__TreeWidgetEditDelegate_t qt_meta_stringdata_Gui__TreeWidgetEditDelegate = {
    {
QT_MOC_LITERAL(0, 0, 27) // "Gui::TreeWidgetEditDelegate"

    },
    "Gui::TreeWidgetEditDelegate"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Gui__TreeWidgetEditDelegate[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void Gui::TreeWidgetEditDelegate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Gui::TreeWidgetEditDelegate::staticMetaObject = { {
    &QStyledItemDelegate::staticMetaObject,
    qt_meta_stringdata_Gui__TreeWidgetEditDelegate.data,
    qt_meta_data_Gui__TreeWidgetEditDelegate,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Gui::TreeWidgetEditDelegate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Gui::TreeWidgetEditDelegate::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Gui__TreeWidgetEditDelegate.stringdata0))
        return static_cast<void*>(this);
    return QStyledItemDelegate::qt_metacast(_clname);
}

int Gui::TreeWidgetEditDelegate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QStyledItemDelegate::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
