/********************************************************************************
** Form generated from reading UI file 'DlgSettings3DView.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGS3DVIEW_H
#define UI_DLGSETTINGS3DVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"
#include "Gui/Widgets.h"

namespace Gui {
namespace Dialog {

class Ui_DlgSettings3DView
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *GroupBox12;
    QVBoxLayout *verticalLayout;
    Gui::PrefCheckBox *CheckBox_CornerCoordSystem;
    Gui::PrefCheckBox *CheckBox_ShowFPS;
    QHBoxLayout *hboxLayout;
    Gui::PrefCheckBox *CheckBox_NaviCube;
    QSpacerItem *spacerItem;
    QLabel *cornerLabel;
    QComboBox *naviCubeCorner;
    Gui::PrefCheckBox *CheckBox_useVBO;
    QHBoxLayout *hboxLayout1;
    QLabel *renderCacheLabel;
    QComboBox *renderCache;
    Gui::PrefCheckBox *CheckBox_UseAutoRotation;
    QGridLayout *gridLayout2;
    QPushButton *mouseButton;
    QLabel *aliasingLAbel;
    QComboBox *comboOrbitStyle;
    Gui::PrefComboBox *comboAliasing;
    QLabel *newDocViewLabel;
    QComboBox *comboNavigationStyle;
    QLabel *orbitLabel;
    QLabel *navigationLabel;
    QComboBox *comboNewDocView;
    QLabel *label_2;
    PrefUnitSpinBox *qspinNewDocScale;
    QHBoxLayout *horizontalLayout;
    Gui::PrefCheckBox *checkBoxZoomAtCursor;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    Gui::PrefDoubleSpinBox *spinBoxZoomStep;
    Gui::PrefCheckBox *checkBoxInvertZoom;
    Gui::PrefCheckBox *checkBoxDisableTilt;
    Gui::PrefCheckBox *checkBoxDragAtCursor;
    QHBoxLayout *hboxLayout2;
    QLabel *markerSizeLabel;
    QComboBox *boxMarkerSize;
    QFrame *line1;
    QHBoxLayout *eyedistanceLayout;
    QLabel *textLabel1;
    Gui::PrefDoubleSpinBox *FloatSpinBox_EyeDistance;
    QFrame *frame;
    QGridLayout *backlightLayout;
    Gui::PrefSlider *sliderIntensity;
    Gui::PrefColorButton *backlightColor;
    QLabel *backlightLabel;
    Gui::PrefCheckBox *checkBoxBacklight;
    QGroupBox *groupBoxCamera;
    QGridLayout *gridLayout1;
    Gui::PrefRadioButton *radioOrthographic;
    Gui::PrefRadioButton *radioPerspective;
    QSpacerItem *spacerItem1;

    void setupUi(QWidget *Gui__Dialog__DlgSettings3DView)
    {
        if (Gui__Dialog__DlgSettings3DView->objectName().isEmpty())
            Gui__Dialog__DlgSettings3DView->setObjectName(QString::fromUtf8("Gui__Dialog__DlgSettings3DView"));
        Gui__Dialog__DlgSettings3DView->resize(477, 763);
        verticalLayout_2 = new QVBoxLayout(Gui__Dialog__DlgSettings3DView);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        GroupBox12 = new QGroupBox(Gui__Dialog__DlgSettings3DView);
        GroupBox12->setObjectName(QString::fromUtf8("GroupBox12"));
        verticalLayout = new QVBoxLayout(GroupBox12);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        CheckBox_CornerCoordSystem = new Gui::PrefCheckBox(GroupBox12);
        CheckBox_CornerCoordSystem->setObjectName(QString::fromUtf8("CheckBox_CornerCoordSystem"));
        CheckBox_CornerCoordSystem->setChecked(true);
        CheckBox_CornerCoordSystem->setProperty("prefEntry", QVariant(QByteArray("CornerCoordSystem")));
        CheckBox_CornerCoordSystem->setProperty("prefPath", QVariant(QByteArray("View")));

        verticalLayout->addWidget(CheckBox_CornerCoordSystem);

        CheckBox_ShowFPS = new Gui::PrefCheckBox(GroupBox12);
        CheckBox_ShowFPS->setObjectName(QString::fromUtf8("CheckBox_ShowFPS"));
        CheckBox_ShowFPS->setProperty("prefEntry", QVariant(QByteArray("ShowFPS")));
        CheckBox_ShowFPS->setProperty("prefPath", QVariant(QByteArray("View")));

        verticalLayout->addWidget(CheckBox_ShowFPS);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        CheckBox_NaviCube = new Gui::PrefCheckBox(GroupBox12);
        CheckBox_NaviCube->setObjectName(QString::fromUtf8("CheckBox_NaviCube"));
        CheckBox_NaviCube->setChecked(true);
        CheckBox_NaviCube->setProperty("prefEntry", QVariant(QByteArray("ShowNaviCube")));
        CheckBox_NaviCube->setProperty("prefPath", QVariant(QByteArray("View")));

        hboxLayout->addWidget(CheckBox_NaviCube);

        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        cornerLabel = new QLabel(GroupBox12);
        cornerLabel->setObjectName(QString::fromUtf8("cornerLabel"));

        hboxLayout->addWidget(cornerLabel);

        naviCubeCorner = new QComboBox(GroupBox12);
        naviCubeCorner->addItem(QString());
        naviCubeCorner->addItem(QString());
        naviCubeCorner->addItem(QString());
        naviCubeCorner->addItem(QString());
        naviCubeCorner->setObjectName(QString::fromUtf8("naviCubeCorner"));

        hboxLayout->addWidget(naviCubeCorner);


        verticalLayout->addLayout(hboxLayout);

        CheckBox_useVBO = new Gui::PrefCheckBox(GroupBox12);
        CheckBox_useVBO->setObjectName(QString::fromUtf8("CheckBox_useVBO"));
        CheckBox_useVBO->setProperty("prefEntry", QVariant(QByteArray("UseVBO")));
        CheckBox_useVBO->setProperty("prefPath", QVariant(QByteArray("View")));

        verticalLayout->addWidget(CheckBox_useVBO);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        renderCacheLabel = new QLabel(GroupBox12);
        renderCacheLabel->setObjectName(QString::fromUtf8("renderCacheLabel"));

        hboxLayout1->addWidget(renderCacheLabel);

        renderCache = new QComboBox(GroupBox12);
        renderCache->addItem(QString());
        renderCache->addItem(QString());
        renderCache->addItem(QString());
        renderCache->setObjectName(QString::fromUtf8("renderCache"));

        hboxLayout1->addWidget(renderCache);


        verticalLayout->addLayout(hboxLayout1);

        CheckBox_UseAutoRotation = new Gui::PrefCheckBox(GroupBox12);
        CheckBox_UseAutoRotation->setObjectName(QString::fromUtf8("CheckBox_UseAutoRotation"));
        CheckBox_UseAutoRotation->setEnabled(true);
        CheckBox_UseAutoRotation->setChecked(false);
        CheckBox_UseAutoRotation->setProperty("prefEntry", QVariant(QByteArray("UseAutoRotation")));
        CheckBox_UseAutoRotation->setProperty("prefPath", QVariant(QByteArray("View")));

        verticalLayout->addWidget(CheckBox_UseAutoRotation);

        gridLayout2 = new QGridLayout();
        gridLayout2->setSpacing(6);
        gridLayout2->setObjectName(QString::fromUtf8("gridLayout2"));
        mouseButton = new QPushButton(GroupBox12);
        mouseButton->setObjectName(QString::fromUtf8("mouseButton"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(mouseButton->sizePolicy().hasHeightForWidth());
        mouseButton->setSizePolicy(sizePolicy);

        gridLayout2->addWidget(mouseButton, 0, 2, 1, 1);

        aliasingLAbel = new QLabel(GroupBox12);
        aliasingLAbel->setObjectName(QString::fromUtf8("aliasingLAbel"));

        gridLayout2->addWidget(aliasingLAbel, 2, 0, 1, 1);

        comboOrbitStyle = new QComboBox(GroupBox12);
        comboOrbitStyle->addItem(QString());
        comboOrbitStyle->addItem(QString());
        comboOrbitStyle->setObjectName(QString::fromUtf8("comboOrbitStyle"));

        gridLayout2->addWidget(comboOrbitStyle, 1, 1, 1, 1);

        comboAliasing = new Gui::PrefComboBox(GroupBox12);
        comboAliasing->addItem(QString());
        comboAliasing->addItem(QString());
        comboAliasing->addItem(QString());
        comboAliasing->addItem(QString());
        comboAliasing->addItem(QString());
        comboAliasing->setObjectName(QString::fromUtf8("comboAliasing"));
        comboAliasing->setProperty("prefEntry", QVariant(QByteArray("AntiAliasing")));
        comboAliasing->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout2->addWidget(comboAliasing, 2, 1, 1, 1);

        newDocViewLabel = new QLabel(GroupBox12);
        newDocViewLabel->setObjectName(QString::fromUtf8("newDocViewLabel"));

        gridLayout2->addWidget(newDocViewLabel, 3, 0, 1, 1);

        comboNavigationStyle = new QComboBox(GroupBox12);
        comboNavigationStyle->setObjectName(QString::fromUtf8("comboNavigationStyle"));

        gridLayout2->addWidget(comboNavigationStyle, 0, 1, 1, 1);

        orbitLabel = new QLabel(GroupBox12);
        orbitLabel->setObjectName(QString::fromUtf8("orbitLabel"));

        gridLayout2->addWidget(orbitLabel, 1, 0, 1, 1);

        navigationLabel = new QLabel(GroupBox12);
        navigationLabel->setObjectName(QString::fromUtf8("navigationLabel"));

        gridLayout2->addWidget(navigationLabel, 0, 0, 1, 1);

        comboNewDocView = new QComboBox(GroupBox12);
        comboNewDocView->setObjectName(QString::fromUtf8("comboNewDocView"));

        gridLayout2->addWidget(comboNewDocView, 3, 1, 1, 1);

        label_2 = new QLabel(GroupBox12);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout2->addWidget(label_2, 4, 0, 1, 1);

        qspinNewDocScale = new PrefUnitSpinBox(GroupBox12);
        qspinNewDocScale->setObjectName(QString::fromUtf8("qspinNewDocScale"));
        qspinNewDocScale->setProperty("minimum", QVariant(0.000010000000000));
        qspinNewDocScale->setProperty("maximum", QVariant(10000000.000000000000000));
        qspinNewDocScale->setProperty("rawValue", QVariant(100.000000000000000));

        gridLayout2->addWidget(qspinNewDocScale, 4, 1, 1, 1);


        verticalLayout->addLayout(gridLayout2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        checkBoxZoomAtCursor = new Gui::PrefCheckBox(GroupBox12);
        checkBoxZoomAtCursor->setObjectName(QString::fromUtf8("checkBoxZoomAtCursor"));
        checkBoxZoomAtCursor->setChecked(true);
        checkBoxZoomAtCursor->setProperty("prefEntry", QVariant(QByteArray("ZoomAtCursor")));
        checkBoxZoomAtCursor->setProperty("prefPath", QVariant(QByteArray("View")));

        horizontalLayout->addWidget(checkBoxZoomAtCursor);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(GroupBox12);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        spinBoxZoomStep = new Gui::PrefDoubleSpinBox(GroupBox12);
        spinBoxZoomStep->setObjectName(QString::fromUtf8("spinBoxZoomStep"));
        spinBoxZoomStep->setMinimum(0.010000000000000);
        spinBoxZoomStep->setMaximum(1.000000000000000);
        spinBoxZoomStep->setSingleStep(0.050000000000000);
        spinBoxZoomStep->setValue(0.200000000000000);
        spinBoxZoomStep->setProperty("prefEntry", QVariant(QByteArray("ZoomStep")));
        spinBoxZoomStep->setProperty("prefPath", QVariant(QByteArray("View")));

        horizontalLayout->addWidget(spinBoxZoomStep);


        verticalLayout->addLayout(horizontalLayout);

        checkBoxInvertZoom = new Gui::PrefCheckBox(GroupBox12);
        checkBoxInvertZoom->setObjectName(QString::fromUtf8("checkBoxInvertZoom"));
        checkBoxInvertZoom->setChecked(true);
        checkBoxInvertZoom->setProperty("prefEntry", QVariant(QByteArray("InvertZoom")));
        checkBoxInvertZoom->setProperty("prefPath", QVariant(QByteArray("View")));

        verticalLayout->addWidget(checkBoxInvertZoom);

        checkBoxDisableTilt = new Gui::PrefCheckBox(GroupBox12);
        checkBoxDisableTilt->setObjectName(QString::fromUtf8("checkBoxDisableTilt"));
        checkBoxDisableTilt->setChecked(true);
        checkBoxDisableTilt->setProperty("prefEntry", QVariant(QByteArray("DisableTouchTilt")));
        checkBoxDisableTilt->setProperty("prefPath", QVariant(QByteArray("View")));

        verticalLayout->addWidget(checkBoxDisableTilt);

        checkBoxDragAtCursor = new Gui::PrefCheckBox(GroupBox12);
        checkBoxDragAtCursor->setObjectName(QString::fromUtf8("checkBoxDragAtCursor"));
        checkBoxDragAtCursor->setChecked(false);
        checkBoxDragAtCursor->setProperty("prefEntry", QVariant(QByteArray("DragAtCursor")));
        checkBoxDragAtCursor->setProperty("prefPath", QVariant(QByteArray("View")));

        verticalLayout->addWidget(checkBoxDragAtCursor);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        markerSizeLabel = new QLabel(GroupBox12);
        markerSizeLabel->setObjectName(QString::fromUtf8("markerSizeLabel"));

        hboxLayout2->addWidget(markerSizeLabel);

        boxMarkerSize = new QComboBox(GroupBox12);
        boxMarkerSize->setObjectName(QString::fromUtf8("boxMarkerSize"));

        hboxLayout2->addWidget(boxMarkerSize);


        verticalLayout->addLayout(hboxLayout2);

        line1 = new QFrame(GroupBox12);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);
        line1->setFrameShape(QFrame::HLine);

        verticalLayout->addWidget(line1);

        eyedistanceLayout = new QHBoxLayout();
        eyedistanceLayout->setSpacing(6);
        eyedistanceLayout->setObjectName(QString::fromUtf8("eyedistanceLayout"));
        eyedistanceLayout->setContentsMargins(11, 11, 11, 11);
        textLabel1 = new QLabel(GroupBox12);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));

        eyedistanceLayout->addWidget(textLabel1);

        FloatSpinBox_EyeDistance = new Gui::PrefDoubleSpinBox(GroupBox12);
        FloatSpinBox_EyeDistance->setObjectName(QString::fromUtf8("FloatSpinBox_EyeDistance"));
        FloatSpinBox_EyeDistance->setDecimals(1);
        FloatSpinBox_EyeDistance->setMinimum(0.100000000000000);
        FloatSpinBox_EyeDistance->setMaximum(1000.000000000000000);
        FloatSpinBox_EyeDistance->setSingleStep(2.000000000000000);
        FloatSpinBox_EyeDistance->setValue(5.000000000000000);
        FloatSpinBox_EyeDistance->setProperty("prefEntry", QVariant(QByteArray("EyeDistance")));
        FloatSpinBox_EyeDistance->setProperty("prefPath", QVariant(QByteArray("View")));

        eyedistanceLayout->addWidget(FloatSpinBox_EyeDistance);


        verticalLayout->addLayout(eyedistanceLayout);

        frame = new QFrame(GroupBox12);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);

        verticalLayout->addWidget(frame);

        backlightLayout = new QGridLayout();
        backlightLayout->setSpacing(6);
        backlightLayout->setObjectName(QString::fromUtf8("backlightLayout"));
        backlightLayout->setContentsMargins(11, 11, 11, 11);
        sliderIntensity = new Gui::PrefSlider(GroupBox12);
        sliderIntensity->setObjectName(QString::fromUtf8("sliderIntensity"));
        sliderIntensity->setEnabled(false);
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(sliderIntensity->sizePolicy().hasHeightForWidth());
        sliderIntensity->setSizePolicy(sizePolicy1);
        sliderIntensity->setMaximum(100);
        sliderIntensity->setSingleStep(1);
        sliderIntensity->setValue(100);
        sliderIntensity->setOrientation(Qt::Horizontal);
        sliderIntensity->setTickPosition(QSlider::TicksBelow);
        sliderIntensity->setTickInterval(10);
        sliderIntensity->setProperty("prefEntry", QVariant(QByteArray("BacklightIntensity")));
        sliderIntensity->setProperty("prefPath", QVariant(QByteArray("View")));

        backlightLayout->addWidget(sliderIntensity, 1, 1, 1, 1);

        backlightColor = new Gui::PrefColorButton(GroupBox12);
        backlightColor->setObjectName(QString::fromUtf8("backlightColor"));
        backlightColor->setEnabled(false);
        backlightColor->setProperty("color", QVariant(QColor(255, 255, 255)));
        backlightColor->setProperty("prefEntry", QVariant(QByteArray("BacklightColor")));
        backlightColor->setProperty("prefPath", QVariant(QByteArray("View")));

        backlightLayout->addWidget(backlightColor, 0, 1, 1, 1);

        backlightLabel = new QLabel(GroupBox12);
        backlightLabel->setObjectName(QString::fromUtf8("backlightLabel"));

        backlightLayout->addWidget(backlightLabel, 1, 0, 1, 1);

        checkBoxBacklight = new Gui::PrefCheckBox(GroupBox12);
        checkBoxBacklight->setObjectName(QString::fromUtf8("checkBoxBacklight"));
        checkBoxBacklight->setProperty("prefEntry", QVariant(QByteArray("EnableBacklight")));
        checkBoxBacklight->setProperty("prefPath", QVariant(QByteArray("View")));

        backlightLayout->addWidget(checkBoxBacklight, 0, 0, 1, 1);


        verticalLayout->addLayout(backlightLayout);


        verticalLayout_2->addWidget(GroupBox12);

        groupBoxCamera = new QGroupBox(Gui__Dialog__DlgSettings3DView);
        groupBoxCamera->setObjectName(QString::fromUtf8("groupBoxCamera"));
        gridLayout1 = new QGridLayout(groupBoxCamera);
        gridLayout1->setSpacing(6);
        gridLayout1->setContentsMargins(11, 11, 11, 11);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        gridLayout1->setContentsMargins(11, 11, 11, 11);
        radioOrthographic = new Gui::PrefRadioButton(groupBoxCamera);
        radioOrthographic->setObjectName(QString::fromUtf8("radioOrthographic"));
        radioOrthographic->setChecked(true);
        radioOrthographic->setProperty("prefEntry", QVariant(QByteArray("Orthographic")));
        radioOrthographic->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout1->addWidget(radioOrthographic, 0, 1, 1, 1);

        radioPerspective = new Gui::PrefRadioButton(groupBoxCamera);
        radioPerspective->setObjectName(QString::fromUtf8("radioPerspective"));
        radioPerspective->setProperty("prefEntry", QVariant(QByteArray("Perspective")));
        radioPerspective->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout1->addWidget(radioPerspective, 0, 0, 1, 1);


        verticalLayout_2->addWidget(groupBoxCamera);

        spacerItem1 = new QSpacerItem(455, 16, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(spacerItem1);

        QWidget::setTabOrder(CheckBox_CornerCoordSystem, CheckBox_ShowFPS);
        QWidget::setTabOrder(CheckBox_ShowFPS, CheckBox_NaviCube);
        QWidget::setTabOrder(CheckBox_NaviCube, CheckBox_useVBO);
        QWidget::setTabOrder(CheckBox_useVBO, CheckBox_UseAutoRotation);
        QWidget::setTabOrder(CheckBox_UseAutoRotation, comboNavigationStyle);
        QWidget::setTabOrder(comboNavigationStyle, mouseButton);
        QWidget::setTabOrder(mouseButton, comboOrbitStyle);
        QWidget::setTabOrder(comboOrbitStyle, comboNewDocView);
        QWidget::setTabOrder(comboNewDocView, checkBoxZoomAtCursor);
        QWidget::setTabOrder(checkBoxZoomAtCursor, spinBoxZoomStep);
        QWidget::setTabOrder(spinBoxZoomStep, checkBoxInvertZoom);
        QWidget::setTabOrder(checkBoxInvertZoom, checkBoxDragAtCursor);
        QWidget::setTabOrder(checkBoxDragAtCursor, FloatSpinBox_EyeDistance);
        QWidget::setTabOrder(FloatSpinBox_EyeDistance, checkBoxBacklight);
        QWidget::setTabOrder(checkBoxBacklight, backlightColor);
        QWidget::setTabOrder(backlightColor, sliderIntensity);
        QWidget::setTabOrder(sliderIntensity, radioPerspective);
        QWidget::setTabOrder(radioPerspective, radioOrthographic);

        retranslateUi(Gui__Dialog__DlgSettings3DView);
        QObject::connect(checkBoxBacklight, SIGNAL(toggled(bool)), backlightColor, SLOT(setEnabled(bool)));
        QObject::connect(checkBoxBacklight, SIGNAL(toggled(bool)), sliderIntensity, SLOT(setEnabled(bool)));

        naviCubeCorner->setCurrentIndex(1);
        renderCache->setCurrentIndex(0);
        comboOrbitStyle->setCurrentIndex(1);
        comboNavigationStyle->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(Gui__Dialog__DlgSettings3DView);
    } // setupUi

    void retranslateUi(QWidget *Gui__Dialog__DlgSettings3DView)
    {
        Gui__Dialog__DlgSettings3DView->setWindowTitle(QApplication::translate("Gui::Dialog::DlgSettings3DView", "3D View", nullptr));
        GroupBox12->setTitle(QApplication::translate("Gui::Dialog::DlgSettings3DView", "3D View settings", nullptr));
#ifndef QT_NO_TOOLTIP
        CheckBox_CornerCoordSystem->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Main coordinate system will always be shown at lower right in opened files", nullptr));
#endif // QT_NO_TOOLTIP
        CheckBox_CornerCoordSystem->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Show coordinate system in the corner", nullptr));
#ifndef QT_NO_TOOLTIP
        CheckBox_ShowFPS->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Time needed for last operation and resulting frame rate\n"
"will be shown at the lower left in opened files", nullptr));
#endif // QT_NO_TOOLTIP
        CheckBox_ShowFPS->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Show counter of frames per second", nullptr));
#ifndef QT_NO_TOOLTIP
        CheckBox_NaviCube->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Navigation cube will always be shown in opened files ", nullptr));
#endif // QT_NO_TOOLTIP
        CheckBox_NaviCube->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Show navigation cube", nullptr));
        cornerLabel->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Corner", nullptr));
        naviCubeCorner->setItemText(0, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Top left", nullptr));
        naviCubeCorner->setItemText(1, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Top right", nullptr));
        naviCubeCorner->setItemText(2, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Bottom left", nullptr));
        naviCubeCorner->setItemText(3, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Bottom right", nullptr));

#ifndef QT_NO_TOOLTIP
        naviCubeCorner->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Corner where navigation cube is shown", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        CheckBox_useVBO->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "If checked, Vertex Buffer Objects (VBO) will be used.\n"
"A VBO is an OpenGL feature that provides methods for uploading\n"
"vertex data (position, normal vector, color, etc.) to the graphics card.\n"
"VBOs offer substantial performance gains because the data resides\n"
"in the graphics memory rather than the system memory and so it\n"
"can be rendered directly by GPU. \n"
"\n"
"Note: Sometimes this feature may lead to a host of different issues ranging\n"
"from graphical anomalies to GPU crash bugs. Remember to report this setting\n"
"is enabled when seeking support on the FreeCAD forums.", nullptr));
#endif // QT_NO_TOOLTIP
        CheckBox_useVBO->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Use OpenGL VBO (Vertex Buffer Object)", nullptr));
        renderCacheLabel->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Render cache", nullptr));
        renderCache->setItemText(0, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Auto", nullptr));
        renderCache->setItemText(1, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Distributed", nullptr));
        renderCache->setItemText(2, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Centralized", nullptr));

#ifndef QT_NO_TOOLTIP
        renderCache->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "'Render Caching' is another way to say 'Rendering Acceleration'.\n"
"There are 3 options available to achieve this: \n"
"1) 'Auto' (default), let Coin3D decide where to cache.\n"
"2) 'Distributed', manually turn on cache for all view provider root node.\n"
"3) 'Centralized', manually turn off cache in all nodes of all view provider, and\n"
"only cache at the scene graph root node. This offers the fastest rendering speed\n"
"but slower response to any scene changes.", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        CheckBox_UseAutoRotation->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Enable animated rotations", nullptr));
#endif // QT_NO_TOOLTIP
        CheckBox_UseAutoRotation->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Enable animation", nullptr));
#ifndef QT_NO_TOOLTIP
        mouseButton->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "List the mouse button configs for each chosen navigation setting.\n"
"Select a set and then press the button to view said configurations.", nullptr));
#endif // QT_NO_TOOLTIP
        mouseButton->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Mouse...", nullptr));
        aliasingLAbel->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Anti-Aliasing", nullptr));
        comboOrbitStyle->setItemText(0, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Turntable", nullptr));
        comboOrbitStyle->setItemText(1, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Trackball", nullptr));

#ifndef QT_NO_TOOLTIP
        comboOrbitStyle->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Rotation orbit style.\n"
"Trackball: moving the mouse horizontally will rotate the part around the y-axis\n"
"Turntable: the part will be rotated around the z-axis.", nullptr));
#endif // QT_NO_TOOLTIP
        comboAliasing->setItemText(0, QApplication::translate("Gui::Dialog::DlgSettings3DView", "None", nullptr));
        comboAliasing->setItemText(1, QApplication::translate("Gui::Dialog::DlgSettings3DView", "Line Smoothing", nullptr));
        comboAliasing->setItemText(2, QApplication::translate("Gui::Dialog::DlgSettings3DView", "MSAA 2x", nullptr));
        comboAliasing->setItemText(3, QApplication::translate("Gui::Dialog::DlgSettings3DView", "MSAA 4x", nullptr));
        comboAliasing->setItemText(4, QApplication::translate("Gui::Dialog::DlgSettings3DView", "MSAA 8x", nullptr));

#ifndef QT_NO_TOOLTIP
        comboAliasing->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "What kind of multisample anti-aliasing is used ", nullptr));
#endif // QT_NO_TOOLTIP
        newDocViewLabel->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "New Document Camera Orientation", nullptr));
#ifndef QT_NO_TOOLTIP
        comboNavigationStyle->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Navigation settings set", nullptr));
#endif // QT_NO_TOOLTIP
        orbitLabel->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Orbit style", nullptr));
        navigationLabel->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "3D Navigation", nullptr));
#ifndef QT_NO_TOOLTIP
        comboNewDocView->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Camera orientation for new documents", nullptr));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "New document scale", nullptr));
#ifndef QT_NO_TOOLTIP
        qspinNewDocScale->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Sets camera zoom for new documents.\n"
"The value is the diameter of the sphere to fit on the screen.", nullptr));
#endif // QT_NO_TOOLTIP
        qspinNewDocScale->setProperty("unit", QVariant(QApplication::translate("Gui::Dialog::DlgSettings3DView", "mm", nullptr)));
        qspinNewDocScale->setProperty("prefEntry", QVariant(QApplication::translate("Gui::Dialog::DlgSettings3DView", "NewDocumentCameraScale", nullptr)));
        qspinNewDocScale->setProperty("prefPath", QVariant(QApplication::translate("Gui::Dialog::DlgSettings3DView", "View", nullptr)));
#ifndef QT_NO_TOOLTIP
        checkBoxZoomAtCursor->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Zoom operations will be performed at position of mouse pointer", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxZoomAtCursor->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Zoom at cursor", nullptr));
        label->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Zoom step", nullptr));
#ifndef QT_NO_TOOLTIP
        spinBoxZoomStep->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "How much will be zoomed.\n"
"Zoom step of '1' means a factor of 7.5 for every zoom step. ", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        checkBoxInvertZoom->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Direction of zoom operations will be inverted", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxInvertZoom->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Invert zoom", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxDisableTilt->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Prevents view tilting when pinch-zooming.\n"
"Affects only gesture navigation style.\n"
"Mouse tilting is not disabled by this setting.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxDisableTilt->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Disable touchscreen tilt gesture", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxDragAtCursor->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Rotations in 3D will use current cursor position as center for rotation", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxDragAtCursor->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Rotate at cursor", nullptr));
#ifndef QT_NO_TOOLTIP
        markerSizeLabel->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        markerSizeLabel->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Marker size:", nullptr));
#ifndef QT_NO_TOOLTIP
        boxMarkerSize->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Size of vertices in the Sketcher workbench", nullptr));
#endif // QT_NO_TOOLTIP
        textLabel1->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Eye to eye distance for stereo modes:", nullptr));
#ifndef QT_NO_TOOLTIP
        FloatSpinBox_EyeDistance->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Eye-to-eye distance used for stereo projections.\n"
"The specified value is a factor that will be multiplied with the\n"
"bounding box size of the 3D object that is currently displayed. ", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        sliderIntensity->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Intensity of the backlight", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        backlightColor->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Backlight color", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        backlightLabel->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        backlightLabel->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Intensity of backlight", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxBacklight->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Backlight is enabled with the defined color", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxBacklight->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Enable backlight color", nullptr));
        groupBoxCamera->setTitle(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Camera type", nullptr));
#ifndef QT_NO_TOOLTIP
        radioOrthographic->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Objects will be projected in orthographic projection", nullptr));
#endif // QT_NO_TOOLTIP
        radioOrthographic->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Or&thographic rendering", nullptr));
#ifndef QT_NO_TOOLTIP
        radioPerspective->setToolTip(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Objects will appear in a perspective projection", nullptr));
#endif // QT_NO_TOOLTIP
        radioPerspective->setText(QApplication::translate("Gui::Dialog::DlgSettings3DView", "Perspective renderin&g", nullptr));
    } // retranslateUi

};

} // namespace Dialog
} // namespace Gui

namespace Gui {
namespace Dialog {
namespace Ui {
    class DlgSettings3DView: public Ui_DlgSettings3DView {};
} // namespace Ui
} // namespace Dialog
} // namespace Gui

#endif // UI_DLGSETTINGS3DVIEW_H
