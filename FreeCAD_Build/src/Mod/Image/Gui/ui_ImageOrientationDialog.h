/********************************************************************************
** Form generated from reading UI file 'ImageOrientationDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGEORIENTATIONDIALOG_H
#define UI_IMAGEORIENTATIONDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include "Gui/QuantitySpinBox.h"

namespace ImageGui {

class Ui_ImageOrientationDialog
{
public:
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QRadioButton *XY_radioButton;
    QRadioButton *XZ_radioButton;
    QRadioButton *YZ_radioButton;
    QLabel *previewLabel;
    QCheckBox *Reverse_checkBox;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    Gui::QuantitySpinBox *Offset_doubleSpinBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ImageGui__ImageOrientationDialog)
    {
        if (ImageGui__ImageOrientationDialog->objectName().isEmpty())
            ImageGui__ImageOrientationDialog->setObjectName(QString::fromUtf8("ImageGui__ImageOrientationDialog"));
        ImageGui__ImageOrientationDialog->resize(178, 201);
        gridLayout = new QGridLayout(ImageGui__ImageOrientationDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox = new QGroupBox(ImageGui__ImageOrientationDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        XY_radioButton = new QRadioButton(groupBox);
        XY_radioButton->setObjectName(QString::fromUtf8("XY_radioButton"));
        XY_radioButton->setChecked(true);

        verticalLayout->addWidget(XY_radioButton);

        XZ_radioButton = new QRadioButton(groupBox);
        XZ_radioButton->setObjectName(QString::fromUtf8("XZ_radioButton"));

        verticalLayout->addWidget(XZ_radioButton);

        YZ_radioButton = new QRadioButton(groupBox);
        YZ_radioButton->setObjectName(QString::fromUtf8("YZ_radioButton"));

        verticalLayout->addWidget(YZ_radioButton);


        gridLayout->addWidget(groupBox, 0, 0, 1, 1);

        previewLabel = new QLabel(ImageGui__ImageOrientationDialog);
        previewLabel->setObjectName(QString::fromUtf8("previewLabel"));
        previewLabel->setMinimumSize(QSize(48, 48));
        previewLabel->setMaximumSize(QSize(48, 48));
        previewLabel->setText(QString::fromUtf8("Preview"));

        gridLayout->addWidget(previewLabel, 0, 1, 1, 1);

        Reverse_checkBox = new QCheckBox(ImageGui__ImageOrientationDialog);
        Reverse_checkBox->setObjectName(QString::fromUtf8("Reverse_checkBox"));

        gridLayout->addWidget(Reverse_checkBox, 1, 0, 1, 2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(ImageGui__ImageOrientationDialog);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        Offset_doubleSpinBox = new Gui::QuantitySpinBox(ImageGui__ImageOrientationDialog);
        Offset_doubleSpinBox->setObjectName(QString::fromUtf8("Offset_doubleSpinBox"));
        Offset_doubleSpinBox->setProperty("unit", QVariant(QString::fromUtf8("mm")));
        Offset_doubleSpinBox->setMinimum(-999999999.000000000000000);
        Offset_doubleSpinBox->setMaximum(999999999.000000000000000);
        Offset_doubleSpinBox->setSingleStep(10.000000000000000);

        horizontalLayout->addWidget(Offset_doubleSpinBox);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 2);

        buttonBox = new QDialogButtonBox(ImageGui__ImageOrientationDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 3, 0, 1, 2);


        retranslateUi(ImageGui__ImageOrientationDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ImageGui__ImageOrientationDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ImageGui__ImageOrientationDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ImageGui__ImageOrientationDialog);
    } // setupUi

    void retranslateUi(QDialog *ImageGui__ImageOrientationDialog)
    {
        ImageGui__ImageOrientationDialog->setWindowTitle(QApplication::translate("ImageGui::ImageOrientationDialog", "Choose orientation", nullptr));
        groupBox->setTitle(QApplication::translate("ImageGui::ImageOrientationDialog", "Image plane", nullptr));
        XY_radioButton->setText(QApplication::translate("ImageGui::ImageOrientationDialog", "XY-Plane", nullptr));
        XZ_radioButton->setText(QApplication::translate("ImageGui::ImageOrientationDialog", "XZ-Plane", nullptr));
        YZ_radioButton->setText(QApplication::translate("ImageGui::ImageOrientationDialog", "YZ-Plane", nullptr));
        Reverse_checkBox->setText(QApplication::translate("ImageGui::ImageOrientationDialog", "Reverse direction", nullptr));
        label->setText(QApplication::translate("ImageGui::ImageOrientationDialog", "Offset:", nullptr));
    } // retranslateUi

};

} // namespace ImageGui

namespace ImageGui {
namespace Ui {
    class ImageOrientationDialog: public Ui_ImageOrientationDialog {};
} // namespace Ui
} // namespace ImageGui

#endif // UI_IMAGEORIENTATIONDIALOG_H
