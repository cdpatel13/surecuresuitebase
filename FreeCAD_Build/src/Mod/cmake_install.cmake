# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Points/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Complete/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Test/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Image/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Mesh/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Part/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Material/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/PartDesign/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Raytracing/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Drawing/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Sketcher/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Robot/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/ReverseEngineering/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/MeshPart/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Draft/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Web/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Surface/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Start/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Idf/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Import/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Inspection/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Arch/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Fem/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/OpenSCAD/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Spreadsheet/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Path/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Show/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Measure/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/TechDraw/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/AddonManager/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Tux/cmake_install.cmake")

endif()

