/****************************************************************************
** Meta object code from reading C++ file 'SegmentationBestFit.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Mesh/Gui/SegmentationBestFit.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SegmentationBestFit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MeshGui__ParametersDialog_t {
    QByteArrayData data[6];
    char stringdata0[99];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MeshGui__ParametersDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MeshGui__ParametersDialog_t qt_meta_stringdata_MeshGui__ParametersDialog = {
    {
QT_MOC_LITERAL(0, 0, 25), // "MeshGui::ParametersDialog"
QT_MOC_LITERAL(1, 26, 17), // "on_region_clicked"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 17), // "on_single_clicked"
QT_MOC_LITERAL(4, 63, 16), // "on_clear_clicked"
QT_MOC_LITERAL(5, 80, 18) // "on_compute_clicked"

    },
    "MeshGui::ParametersDialog\0on_region_clicked\0"
    "\0on_single_clicked\0on_clear_clicked\0"
    "on_compute_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MeshGui__ParametersDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x08 /* Private */,
       3,    0,   35,    2, 0x08 /* Private */,
       4,    0,   36,    2, 0x08 /* Private */,
       5,    0,   37,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MeshGui::ParametersDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ParametersDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_region_clicked(); break;
        case 1: _t->on_single_clicked(); break;
        case 2: _t->on_clear_clicked(); break;
        case 3: _t->on_compute_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject MeshGui::ParametersDialog::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_MeshGui__ParametersDialog.data,
    qt_meta_data_MeshGui__ParametersDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MeshGui::ParametersDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MeshGui::ParametersDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MeshGui__ParametersDialog.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int MeshGui::ParametersDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_MeshGui__SegmentationBestFit_t {
    QByteArrayData data[5];
    char stringdata0[115];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MeshGui__SegmentationBestFit_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MeshGui__SegmentationBestFit_t qt_meta_stringdata_MeshGui__SegmentationBestFit = {
    {
QT_MOC_LITERAL(0, 0, 28), // "MeshGui::SegmentationBestFit"
QT_MOC_LITERAL(1, 29, 26), // "on_planeParameters_clicked"
QT_MOC_LITERAL(2, 56, 0), // ""
QT_MOC_LITERAL(3, 57, 29), // "on_cylinderParameters_clicked"
QT_MOC_LITERAL(4, 87, 27) // "on_sphereParameters_clicked"

    },
    "MeshGui::SegmentationBestFit\0"
    "on_planeParameters_clicked\0\0"
    "on_cylinderParameters_clicked\0"
    "on_sphereParameters_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MeshGui__SegmentationBestFit[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x08 /* Private */,
       3,    0,   30,    2, 0x08 /* Private */,
       4,    0,   31,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MeshGui::SegmentationBestFit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SegmentationBestFit *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_planeParameters_clicked(); break;
        case 1: _t->on_cylinderParameters_clicked(); break;
        case 2: _t->on_sphereParameters_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject MeshGui::SegmentationBestFit::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_MeshGui__SegmentationBestFit.data,
    qt_meta_data_MeshGui__SegmentationBestFit,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MeshGui::SegmentationBestFit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MeshGui::SegmentationBestFit::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MeshGui__SegmentationBestFit.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int MeshGui::SegmentationBestFit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
