
// This file is generated by src/Tools/generateTemaplates/templateClassPyExport.py out of the .XML file
// Every change you make here gets lost in the next full rebuild!
// This File is normally built as an include in ViewProviderMeshPyImp.cpp! It's not intended to be in a project!

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/exception.hpp>
#include <Base/PyObjectBase.h>
#include <Base/Console.h>
#include <Base/Exception.h>
#include <CXX/Objects.hxx>

using Base::streq;
using namespace MeshGui;

/// Type structure of ViewProviderMeshPy
PyTypeObject ViewProviderMeshPy::Type = {
    PyVarObject_HEAD_INIT(&PyType_Type,0)
    "MeshGui.ViewProviderMesh",     /*tp_name*/
    sizeof(ViewProviderMeshPy),                       /*tp_basicsize*/
    0,                                                /*tp_itemsize*/
    /* methods */
    PyDestructor,                                     /*tp_dealloc*/
    0,                                                /*tp_print*/
    0,                                                /*tp_getattr*/
    0,                                                /*tp_setattr*/
    0,                                                /*tp_compare*/
    __repr,                                           /*tp_repr*/
    0,                                                /*tp_as_number*/
    0,                                                /*tp_as_sequence*/
    0,                                                /*tp_as_mapping*/
    0,                                                /*tp_hash*/
    0,                                                /*tp_call */
    0,                                                /*tp_str  */
    __getattro,                                       /*tp_getattro*/
    __setattro,                                       /*tp_setattro*/
    /* --- Functions to access object as input/output buffer ---------*/
    0,                                                /* tp_as_buffer */
    /* --- Flags to define presence of optional/expanded features */
#if PY_MAJOR_VERSION >= 3
    Py_TPFLAGS_BASETYPE|Py_TPFLAGS_DEFAULT,        /*tp_flags */
#else
    Py_TPFLAGS_DEFAULT,        /*tp_flags */
#endif
    "This is the ViewProvider base class",           /*tp_doc */
    0,                                                /*tp_traverse */
    0,                                                /*tp_clear */
    0,                                                /*tp_richcompare */
    0,                                                /*tp_weaklistoffset */
    0,                                                /*tp_iter */
    0,                                                /*tp_iternext */
    MeshGui::ViewProviderMeshPy::Methods,                     /*tp_methods */
    0,                                                /*tp_members */
    MeshGui::ViewProviderMeshPy::GetterSetter,                     /*tp_getset */
    &Gui::ViewProviderDocumentObjectPy::Type,                        /*tp_base */
    0,                                                /*tp_dict */
    0,                                                /*tp_descr_get */
    0,                                                /*tp_descr_set */
    0,                                                /*tp_dictoffset */
    __PyInit,                                         /*tp_init */
    0,                                                /*tp_alloc */
    MeshGui::ViewProviderMeshPy::PyMake,/*tp_new */
    0,                                                /*tp_free   Low-level free-memory routine */
    0,                                                /*tp_is_gc  For PyObject_IS_GC */
    0,                                                /*tp_bases */
    0,                                                /*tp_mro    method resolution order */
    0,                                                /*tp_cache */
    0,                                                /*tp_subclasses */
    0,                                                /*tp_weaklist */
    0,                                                /*tp_del */
    0                                                 /*tp_version_tag */
#if PY_MAJOR_VERSION >= 3
    ,0                                                /*tp_finalize */
#endif
};

/// Methods structure of ViewProviderMeshPy
PyMethodDef ViewProviderMeshPy::Methods[] = {
    {"setSelection",
        reinterpret_cast<PyCFunction>( staticCallback_setSelection ),
        METH_VARARGS,
        "Select list of facets"
    },
    {"addSelection",
        reinterpret_cast<PyCFunction>( staticCallback_addSelection ),
        METH_VARARGS,
        "Add list of facets to selection"
    },
    {"removeSelection",
        reinterpret_cast<PyCFunction>( staticCallback_removeSelection ),
        METH_VARARGS,
        "Remove list of facets from selection"
    },
    {"invertSelection",
        reinterpret_cast<PyCFunction>( staticCallback_invertSelection ),
        METH_VARARGS,
        "Invert the selection"
    },
    {"clearSelection",
        reinterpret_cast<PyCFunction>( staticCallback_clearSelection ),
        METH_VARARGS,
        "Clear the selection"
    },
    {NULL, NULL, 0, NULL}		/* Sentinel */
};



/// Attribute structure of ViewProviderMeshPy
PyGetSetDef ViewProviderMeshPy::GetterSetter[] = {
    {NULL, NULL, NULL, NULL, NULL}		/* Sentinel */
};

// setSelection() callback and implementer
// PyObject*  ViewProviderMeshPy::setSelection(PyObject *args){};
// has to be implemented in ViewProviderMeshPyImp.cpp
PyObject * ViewProviderMeshPy::staticCallback_setSelection (PyObject *self, PyObject *args)
{
    // make sure that not a null pointer is passed
    if (!self) {
        PyErr_SetString(PyExc_TypeError, "descriptor 'setSelection' of 'MeshGui.ViewProviderMesh' object needs an argument");
        return NULL;
    }

    // test if twin object isn't already deleted
    if (!static_cast<PyObjectBase*>(self)->isValid()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }

    // test if object is set Const
    if (static_cast<PyObjectBase*>(self)->isConst()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is immutable, you can not set any attribute or call a non const method");
        return NULL;
    }

    try { // catches all exceptions coming up from c++ and generate a python exception
        PyObject* ret = static_cast<ViewProviderMeshPy*>(self)->setSelection(args);
        if (ret != 0)
            static_cast<ViewProviderMeshPy*>(self)->startNotify();
        return ret;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif
}

// addSelection() callback and implementer
// PyObject*  ViewProviderMeshPy::addSelection(PyObject *args){};
// has to be implemented in ViewProviderMeshPyImp.cpp
PyObject * ViewProviderMeshPy::staticCallback_addSelection (PyObject *self, PyObject *args)
{
    // make sure that not a null pointer is passed
    if (!self) {
        PyErr_SetString(PyExc_TypeError, "descriptor 'addSelection' of 'MeshGui.ViewProviderMesh' object needs an argument");
        return NULL;
    }

    // test if twin object isn't already deleted
    if (!static_cast<PyObjectBase*>(self)->isValid()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }

    // test if object is set Const
    if (static_cast<PyObjectBase*>(self)->isConst()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is immutable, you can not set any attribute or call a non const method");
        return NULL;
    }

    try { // catches all exceptions coming up from c++ and generate a python exception
        PyObject* ret = static_cast<ViewProviderMeshPy*>(self)->addSelection(args);
        if (ret != 0)
            static_cast<ViewProviderMeshPy*>(self)->startNotify();
        return ret;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif
}

// removeSelection() callback and implementer
// PyObject*  ViewProviderMeshPy::removeSelection(PyObject *args){};
// has to be implemented in ViewProviderMeshPyImp.cpp
PyObject * ViewProviderMeshPy::staticCallback_removeSelection (PyObject *self, PyObject *args)
{
    // make sure that not a null pointer is passed
    if (!self) {
        PyErr_SetString(PyExc_TypeError, "descriptor 'removeSelection' of 'MeshGui.ViewProviderMesh' object needs an argument");
        return NULL;
    }

    // test if twin object isn't already deleted
    if (!static_cast<PyObjectBase*>(self)->isValid()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }

    // test if object is set Const
    if (static_cast<PyObjectBase*>(self)->isConst()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is immutable, you can not set any attribute or call a non const method");
        return NULL;
    }

    try { // catches all exceptions coming up from c++ and generate a python exception
        PyObject* ret = static_cast<ViewProviderMeshPy*>(self)->removeSelection(args);
        if (ret != 0)
            static_cast<ViewProviderMeshPy*>(self)->startNotify();
        return ret;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif
}

// invertSelection() callback and implementer
// PyObject*  ViewProviderMeshPy::invertSelection(PyObject *args){};
// has to be implemented in ViewProviderMeshPyImp.cpp
PyObject * ViewProviderMeshPy::staticCallback_invertSelection (PyObject *self, PyObject *args)
{
    // make sure that not a null pointer is passed
    if (!self) {
        PyErr_SetString(PyExc_TypeError, "descriptor 'invertSelection' of 'MeshGui.ViewProviderMesh' object needs an argument");
        return NULL;
    }

    // test if twin object isn't already deleted
    if (!static_cast<PyObjectBase*>(self)->isValid()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }

    // test if object is set Const
    if (static_cast<PyObjectBase*>(self)->isConst()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is immutable, you can not set any attribute or call a non const method");
        return NULL;
    }

    try { // catches all exceptions coming up from c++ and generate a python exception
        PyObject* ret = static_cast<ViewProviderMeshPy*>(self)->invertSelection(args);
        if (ret != 0)
            static_cast<ViewProviderMeshPy*>(self)->startNotify();
        return ret;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif
}

// clearSelection() callback and implementer
// PyObject*  ViewProviderMeshPy::clearSelection(PyObject *args){};
// has to be implemented in ViewProviderMeshPyImp.cpp
PyObject * ViewProviderMeshPy::staticCallback_clearSelection (PyObject *self, PyObject *args)
{
    // make sure that not a null pointer is passed
    if (!self) {
        PyErr_SetString(PyExc_TypeError, "descriptor 'clearSelection' of 'MeshGui.ViewProviderMesh' object needs an argument");
        return NULL;
    }

    // test if twin object isn't already deleted
    if (!static_cast<PyObjectBase*>(self)->isValid()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }

    // test if object is set Const
    if (static_cast<PyObjectBase*>(self)->isConst()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is immutable, you can not set any attribute or call a non const method");
        return NULL;
    }

    try { // catches all exceptions coming up from c++ and generate a python exception
        PyObject* ret = static_cast<ViewProviderMeshPy*>(self)->clearSelection(args);
        if (ret != 0)
            static_cast<ViewProviderMeshPy*>(self)->startNotify();
        return ret;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif
}




//--------------------------------------------------------------------------
// Constructor
//--------------------------------------------------------------------------
ViewProviderMeshPy::ViewProviderMeshPy(ViewProviderMesh *pcObject, PyTypeObject *T)
    : ViewProviderDocumentObjectPy(static_cast<ViewProviderDocumentObjectPy::PointerType>(pcObject), T)
{
}

PyObject *ViewProviderMeshPy::PyMake(struct _typeobject *, PyObject *, PyObject *)  // Python wrapper
{
    // never create such objects with the constructor
    PyErr_SetString(PyExc_RuntimeError, "You cannot create directly an instance of 'ViewProviderMeshPy'.");
 
    return 0;
}

int ViewProviderMeshPy::PyInit(PyObject* /*args*/, PyObject* /*kwd*/)
{
    return 0;
}

//--------------------------------------------------------------------------
// destructor
//--------------------------------------------------------------------------
ViewProviderMeshPy::~ViewProviderMeshPy()                                // Everything handled in parent
{
}

//--------------------------------------------------------------------------
// ViewProviderMeshPy representation
//--------------------------------------------------------------------------
PyObject *ViewProviderMeshPy::_repr(void)
{
    return Py_BuildValue("s", representation().c_str());
}

//--------------------------------------------------------------------------
// ViewProviderMeshPy Attributes
//--------------------------------------------------------------------------
PyObject *ViewProviderMeshPy::_getattr(const char *attr)			// __getattr__ function: note only need to handle new state
{
    try {
        // getter method for special Attributes (e.g. dynamic ones)
        PyObject *r = getCustomAttributes(attr);
        if(r) return r;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif

    PyMethodDef *ml = Methods;
    for (; ml->ml_name != NULL; ml++) {
        if (attr[0] == ml->ml_name[0] &&
            strcmp(attr+1, ml->ml_name+1) == 0)
            return PyCFunction_New(ml, this);
    }

    PyErr_Clear();
    return ViewProviderDocumentObjectPy::_getattr(attr);
}

int ViewProviderMeshPy::_setattr(const char *attr, PyObject *value) // __setattr__ function: note only need to handle new state
{
    try {
        // setter for special Attributes (e.g. dynamic ones)
        int r = setCustomAttributes(attr, value);
        // r = 1: handled
        // r = -1: error
        // r = 0: ignore
        if (r == 1)
            return 0;
        else if (r == -1)
            return -1;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return -1;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return -1;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return -1;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return -1;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return -1;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return -1;
    }
#endif

    return ViewProviderDocumentObjectPy::_setattr(attr, value);
}

ViewProviderMesh *ViewProviderMeshPy::getViewProviderMeshPtr(void) const
{
    return static_cast<ViewProviderMesh *>(_pcTwinPointer);
}

#if 0
/* From here on come the methods you have to implement, but NOT in this module. Implement in ViewProviderMeshPyImp.cpp! This prototypes 
 * are just for convenience when you add a new method.
 */



// returns a string which represents the object e.g. when printed in python
std::string ViewProviderMeshPy::representation(void) const
{
    return std::string("<ViewProviderMesh object>");
}

PyObject* ViewProviderMeshPy::setSelection(PyObject *args)
{
    PyErr_SetString(PyExc_NotImplementedError, "Not yet implemented");
    return 0;
}

PyObject* ViewProviderMeshPy::addSelection(PyObject *args)
{
    PyErr_SetString(PyExc_NotImplementedError, "Not yet implemented");
    return 0;
}

PyObject* ViewProviderMeshPy::removeSelection(PyObject *args)
{
    PyErr_SetString(PyExc_NotImplementedError, "Not yet implemented");
    return 0;
}

PyObject* ViewProviderMeshPy::invertSelection(PyObject *args)
{
    PyErr_SetString(PyExc_NotImplementedError, "Not yet implemented");
    return 0;
}

PyObject* ViewProviderMeshPy::clearSelection(PyObject *args)
{
    PyErr_SetString(PyExc_NotImplementedError, "Not yet implemented");
    return 0;
}



PyObject *ViewProviderMeshPy::getCustomAttributes(const char* /*attr*/) const
{
    return 0;
}

int ViewProviderMeshPy::setCustomAttributes(const char* /*attr*/, PyObject* /*obj*/)
{
    return 0; 
}
#endif



