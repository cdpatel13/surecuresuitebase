/********************************************************************************
** Form generated from reading UI file 'DlgSettingsImportExport.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGSIMPORTEXPORT_H
#define UI_DLGSETTINGSIMPORTEXPORT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"
#include "Gui/QuantitySpinBox.h"

namespace MeshGui {

class Ui_DlgSettingsImportExport
{
public:
    QGridLayout *gridLayout;
    QGroupBox *GroupBox12;
    QGridLayout *gridLayout1;
    QGridLayout *gridLayout2;
    Gui::QuantitySpinBox *maxDeviationExport;
    QLabel *textLabel1;
    Gui::PrefCheckBox *exportAmfCompressed;
    QSpacerItem *spacerItem;

    void setupUi(QWidget *MeshGui__DlgSettingsImportExport)
    {
        if (MeshGui__DlgSettingsImportExport->objectName().isEmpty())
            MeshGui__DlgSettingsImportExport->setObjectName(QString::fromUtf8("MeshGui__DlgSettingsImportExport"));
        MeshGui__DlgSettingsImportExport->resize(539, 339);
        gridLayout = new QGridLayout(MeshGui__DlgSettingsImportExport);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        GroupBox12 = new QGroupBox(MeshGui__DlgSettingsImportExport);
        GroupBox12->setObjectName(QString::fromUtf8("GroupBox12"));
        gridLayout1 = new QGridLayout(GroupBox12);
        gridLayout1->setSpacing(6);
        gridLayout1->setContentsMargins(11, 11, 11, 11);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        gridLayout2 = new QGridLayout();
        gridLayout2->setSpacing(6);
        gridLayout2->setObjectName(QString::fromUtf8("gridLayout2"));
        maxDeviationExport = new Gui::QuantitySpinBox(GroupBox12);
        maxDeviationExport->setObjectName(QString::fromUtf8("maxDeviationExport"));
        maxDeviationExport->setProperty("unit", QVariant(QString::fromUtf8("mm")));
        maxDeviationExport->setMinimum(0.000000000000000);
        maxDeviationExport->setMaximum(100000000.000000000000000);
        maxDeviationExport->setSingleStep(0.010000000000000);
        maxDeviationExport->setValue(0.100000000000000);

        gridLayout2->addWidget(maxDeviationExport, 0, 1, 1, 1);

        textLabel1 = new QLabel(GroupBox12);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));

        gridLayout2->addWidget(textLabel1, 0, 0, 1, 1);

        exportAmfCompressed = new Gui::PrefCheckBox(GroupBox12);
        exportAmfCompressed->setObjectName(QString::fromUtf8("exportAmfCompressed"));
        exportAmfCompressed->setChecked(true);
        exportAmfCompressed->setProperty("prefEntry", QVariant(QByteArray("ExportAmfCompressed")));
        exportAmfCompressed->setProperty("prefPath", QVariant(QByteArray("Mod/Mesh")));

        gridLayout2->addWidget(exportAmfCompressed, 1, 0, 1, 1);


        gridLayout1->addLayout(gridLayout2, 0, 0, 1, 1);


        gridLayout->addWidget(GroupBox12, 0, 0, 1, 1);

        spacerItem = new QSpacerItem(20, 61, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(spacerItem, 1, 0, 1, 1);


        retranslateUi(MeshGui__DlgSettingsImportExport);

        QMetaObject::connectSlotsByName(MeshGui__DlgSettingsImportExport);
    } // setupUi

    void retranslateUi(QWidget *MeshGui__DlgSettingsImportExport)
    {
        MeshGui__DlgSettingsImportExport->setWindowTitle(QApplication::translate("MeshGui::DlgSettingsImportExport", "Mesh Formats", nullptr));
        GroupBox12->setTitle(QApplication::translate("MeshGui::DlgSettingsImportExport", "Export", nullptr));
#ifndef QT_NO_TOOLTIP
        maxDeviationExport->setToolTip(QApplication::translate("MeshGui::DlgSettingsImportExport", "Maximal deviation between mesh and object", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        textLabel1->setToolTip(QApplication::translate("MeshGui::DlgSettingsImportExport", "Deviation of tessellation to the actual surface", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_WHATSTHIS
        textLabel1->setWhatsThis(QApplication::translate("MeshGui::DlgSettingsImportExport", "<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:7.8pt; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Tessellation</span></p><p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;\"></p><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;\"><span style=\" font-weight:400;\">Defines the maximum deviation of the tessellated mesh to the surface. The smaller the value is the slower the render speed which results in increased detail/resolution.</span></p></body></html>", nullptr));
#endif // QT_NO_WHATSTHIS
        textLabel1->setText(QApplication::translate("MeshGui::DlgSettingsImportExport", "Maximum mesh deviation", nullptr));
#ifndef QT_NO_TOOLTIP
        exportAmfCompressed->setToolTip(QApplication::translate("MeshGui::DlgSettingsImportExport", "ZIP compression is used when writing a mesh file in AMF format", nullptr));
#endif // QT_NO_TOOLTIP
        exportAmfCompressed->setText(QApplication::translate("MeshGui::DlgSettingsImportExport", "Export AMF files using compression", nullptr));
    } // retranslateUi

};

} // namespace MeshGui

namespace MeshGui {
namespace Ui {
    class DlgSettingsImportExport: public Ui_DlgSettingsImportExport {};
} // namespace Ui
} // namespace MeshGui

#endif // UI_DLGSETTINGSIMPORTEXPORT_H
