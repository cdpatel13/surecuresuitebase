/****************************************************************************
** Meta object code from reading C++ file 'DlgEvaluateMeshImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Mesh/Gui/DlgEvaluateMeshImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgEvaluateMeshImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MeshGui__CleanupHandler_t {
    QByteArrayData data[3];
    char stringdata0[33];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MeshGui__CleanupHandler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MeshGui__CleanupHandler_t qt_meta_stringdata_MeshGui__CleanupHandler = {
    {
QT_MOC_LITERAL(0, 0, 23), // "MeshGui::CleanupHandler"
QT_MOC_LITERAL(1, 24, 7), // "cleanup"
QT_MOC_LITERAL(2, 32, 0) // ""

    },
    "MeshGui::CleanupHandler\0cleanup\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MeshGui__CleanupHandler[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void MeshGui::CleanupHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CleanupHandler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->cleanup(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject MeshGui::CleanupHandler::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_MeshGui__CleanupHandler.data,
    qt_meta_data_MeshGui__CleanupHandler,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MeshGui::CleanupHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MeshGui::CleanupHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MeshGui__CleanupHandler.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int MeshGui::CleanupHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_MeshGui__DlgEvaluateMeshImp_t {
    QByteArrayData data[32];
    char stringdata0[1035];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MeshGui__DlgEvaluateMeshImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MeshGui__DlgEvaluateMeshImp_t qt_meta_stringdata_MeshGui__DlgEvaluateMeshImp = {
    {
QT_MOC_LITERAL(0, 0, 27), // "MeshGui::DlgEvaluateMeshImp"
QT_MOC_LITERAL(1, 28, 33), // "on_checkOrientationButton_cli..."
QT_MOC_LITERAL(2, 62, 0), // ""
QT_MOC_LITERAL(3, 63, 35), // "on_analyzeOrientationButton_c..."
QT_MOC_LITERAL(4, 99, 34), // "on_repairOrientationButton_cl..."
QT_MOC_LITERAL(5, 134, 37), // "on_checkDuplicatedFacesButton..."
QT_MOC_LITERAL(6, 172, 39), // "on_analyzeDuplicatedFacesButt..."
QT_MOC_LITERAL(7, 212, 38), // "on_repairDuplicatedFacesButto..."
QT_MOC_LITERAL(8, 251, 38), // "on_checkDuplicatedPointsButto..."
QT_MOC_LITERAL(9, 290, 40), // "on_analyzeDuplicatedPointsBut..."
QT_MOC_LITERAL(10, 331, 39), // "on_repairDuplicatedPointsButt..."
QT_MOC_LITERAL(11, 371, 34), // "on_checkNonmanifoldsButton_cl..."
QT_MOC_LITERAL(12, 406, 36), // "on_analyzeNonmanifoldsButton_..."
QT_MOC_LITERAL(13, 443, 35), // "on_repairNonmanifoldsButton_c..."
QT_MOC_LITERAL(14, 479, 34), // "on_checkDegenerationButton_cl..."
QT_MOC_LITERAL(15, 514, 35), // "on_analyzeDegeneratedButton_c..."
QT_MOC_LITERAL(16, 550, 34), // "on_repairDegeneratedButton_cl..."
QT_MOC_LITERAL(17, 585, 29), // "on_checkIndicesButton_clicked"
QT_MOC_LITERAL(18, 615, 31), // "on_analyzeIndicesButton_clicked"
QT_MOC_LITERAL(19, 647, 30), // "on_repairIndicesButton_clicked"
QT_MOC_LITERAL(20, 678, 38), // "on_checkSelfIntersectionButto..."
QT_MOC_LITERAL(21, 717, 40), // "on_analyzeSelfIntersectionBut..."
QT_MOC_LITERAL(22, 758, 39), // "on_repairSelfIntersectionButt..."
QT_MOC_LITERAL(23, 798, 27), // "on_checkFoldsButton_clicked"
QT_MOC_LITERAL(24, 826, 29), // "on_analyzeFoldsButton_clicked"
QT_MOC_LITERAL(25, 856, 28), // "on_repairFoldsButton_clicked"
QT_MOC_LITERAL(26, 885, 29), // "on_analyzeAllTogether_clicked"
QT_MOC_LITERAL(27, 915, 28), // "on_repairAllTogether_clicked"
QT_MOC_LITERAL(28, 944, 24), // "on_refreshButton_clicked"
QT_MOC_LITERAL(29, 969, 27), // "on_meshNameButton_activated"
QT_MOC_LITERAL(30, 997, 20), // "on_buttonBox_clicked"
QT_MOC_LITERAL(31, 1018, 16) // "QAbstractButton*"

    },
    "MeshGui::DlgEvaluateMeshImp\0"
    "on_checkOrientationButton_clicked\0\0"
    "on_analyzeOrientationButton_clicked\0"
    "on_repairOrientationButton_clicked\0"
    "on_checkDuplicatedFacesButton_clicked\0"
    "on_analyzeDuplicatedFacesButton_clicked\0"
    "on_repairDuplicatedFacesButton_clicked\0"
    "on_checkDuplicatedPointsButton_clicked\0"
    "on_analyzeDuplicatedPointsButton_clicked\0"
    "on_repairDuplicatedPointsButton_clicked\0"
    "on_checkNonmanifoldsButton_clicked\0"
    "on_analyzeNonmanifoldsButton_clicked\0"
    "on_repairNonmanifoldsButton_clicked\0"
    "on_checkDegenerationButton_clicked\0"
    "on_analyzeDegeneratedButton_clicked\0"
    "on_repairDegeneratedButton_clicked\0"
    "on_checkIndicesButton_clicked\0"
    "on_analyzeIndicesButton_clicked\0"
    "on_repairIndicesButton_clicked\0"
    "on_checkSelfIntersectionButton_clicked\0"
    "on_analyzeSelfIntersectionButton_clicked\0"
    "on_repairSelfIntersectionButton_clicked\0"
    "on_checkFoldsButton_clicked\0"
    "on_analyzeFoldsButton_clicked\0"
    "on_repairFoldsButton_clicked\0"
    "on_analyzeAllTogether_clicked\0"
    "on_repairAllTogether_clicked\0"
    "on_refreshButton_clicked\0"
    "on_meshNameButton_activated\0"
    "on_buttonBox_clicked\0QAbstractButton*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MeshGui__DlgEvaluateMeshImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      29,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  159,    2, 0x09 /* Protected */,
       3,    0,  160,    2, 0x09 /* Protected */,
       4,    0,  161,    2, 0x09 /* Protected */,
       5,    0,  162,    2, 0x09 /* Protected */,
       6,    0,  163,    2, 0x09 /* Protected */,
       7,    0,  164,    2, 0x09 /* Protected */,
       8,    0,  165,    2, 0x09 /* Protected */,
       9,    0,  166,    2, 0x09 /* Protected */,
      10,    0,  167,    2, 0x09 /* Protected */,
      11,    0,  168,    2, 0x09 /* Protected */,
      12,    0,  169,    2, 0x09 /* Protected */,
      13,    0,  170,    2, 0x09 /* Protected */,
      14,    0,  171,    2, 0x09 /* Protected */,
      15,    0,  172,    2, 0x09 /* Protected */,
      16,    0,  173,    2, 0x09 /* Protected */,
      17,    0,  174,    2, 0x09 /* Protected */,
      18,    0,  175,    2, 0x09 /* Protected */,
      19,    0,  176,    2, 0x09 /* Protected */,
      20,    0,  177,    2, 0x09 /* Protected */,
      21,    0,  178,    2, 0x09 /* Protected */,
      22,    0,  179,    2, 0x09 /* Protected */,
      23,    0,  180,    2, 0x09 /* Protected */,
      24,    0,  181,    2, 0x09 /* Protected */,
      25,    0,  182,    2, 0x09 /* Protected */,
      26,    0,  183,    2, 0x09 /* Protected */,
      27,    0,  184,    2, 0x09 /* Protected */,
      28,    0,  185,    2, 0x09 /* Protected */,
      29,    1,  186,    2, 0x09 /* Protected */,
      30,    1,  189,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, 0x80000000 | 31,    2,

       0        // eod
};

void MeshGui::DlgEvaluateMeshImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgEvaluateMeshImp *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_checkOrientationButton_clicked(); break;
        case 1: _t->on_analyzeOrientationButton_clicked(); break;
        case 2: _t->on_repairOrientationButton_clicked(); break;
        case 3: _t->on_checkDuplicatedFacesButton_clicked(); break;
        case 4: _t->on_analyzeDuplicatedFacesButton_clicked(); break;
        case 5: _t->on_repairDuplicatedFacesButton_clicked(); break;
        case 6: _t->on_checkDuplicatedPointsButton_clicked(); break;
        case 7: _t->on_analyzeDuplicatedPointsButton_clicked(); break;
        case 8: _t->on_repairDuplicatedPointsButton_clicked(); break;
        case 9: _t->on_checkNonmanifoldsButton_clicked(); break;
        case 10: _t->on_analyzeNonmanifoldsButton_clicked(); break;
        case 11: _t->on_repairNonmanifoldsButton_clicked(); break;
        case 12: _t->on_checkDegenerationButton_clicked(); break;
        case 13: _t->on_analyzeDegeneratedButton_clicked(); break;
        case 14: _t->on_repairDegeneratedButton_clicked(); break;
        case 15: _t->on_checkIndicesButton_clicked(); break;
        case 16: _t->on_analyzeIndicesButton_clicked(); break;
        case 17: _t->on_repairIndicesButton_clicked(); break;
        case 18: _t->on_checkSelfIntersectionButton_clicked(); break;
        case 19: _t->on_analyzeSelfIntersectionButton_clicked(); break;
        case 20: _t->on_repairSelfIntersectionButton_clicked(); break;
        case 21: _t->on_checkFoldsButton_clicked(); break;
        case 22: _t->on_analyzeFoldsButton_clicked(); break;
        case 23: _t->on_repairFoldsButton_clicked(); break;
        case 24: _t->on_analyzeAllTogether_clicked(); break;
        case 25: _t->on_repairAllTogether_clicked(); break;
        case 26: _t->on_refreshButton_clicked(); break;
        case 27: _t->on_meshNameButton_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: _t->on_buttonBox_clicked((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MeshGui::DlgEvaluateMeshImp::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_MeshGui__DlgEvaluateMeshImp.data,
    qt_meta_data_MeshGui__DlgEvaluateMeshImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MeshGui::DlgEvaluateMeshImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MeshGui::DlgEvaluateMeshImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MeshGui__DlgEvaluateMeshImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "App::DocumentObserver"))
        return static_cast< App::DocumentObserver*>(this);
    return QDialog::qt_metacast(_clname);
}

int MeshGui::DlgEvaluateMeshImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 29)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 29;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 29)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 29;
    }
    return _id;
}
struct qt_meta_stringdata_MeshGui__DockEvaluateMeshImp_t {
    QByteArrayData data[1];
    char stringdata0[29];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MeshGui__DockEvaluateMeshImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MeshGui__DockEvaluateMeshImp_t qt_meta_stringdata_MeshGui__DockEvaluateMeshImp = {
    {
QT_MOC_LITERAL(0, 0, 28) // "MeshGui::DockEvaluateMeshImp"

    },
    "MeshGui::DockEvaluateMeshImp"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MeshGui__DockEvaluateMeshImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void MeshGui::DockEvaluateMeshImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject MeshGui::DockEvaluateMeshImp::staticMetaObject = { {
    &DlgEvaluateMeshImp::staticMetaObject,
    qt_meta_stringdata_MeshGui__DockEvaluateMeshImp.data,
    qt_meta_data_MeshGui__DockEvaluateMeshImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MeshGui::DockEvaluateMeshImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MeshGui::DockEvaluateMeshImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MeshGui__DockEvaluateMeshImp.stringdata0))
        return static_cast<void*>(this);
    return DlgEvaluateMeshImp::qt_metacast(_clname);
}

int MeshGui::DockEvaluateMeshImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DlgEvaluateMeshImp::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
