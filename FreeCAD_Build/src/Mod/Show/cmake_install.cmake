# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Show" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/Containers.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/DepGraphTools.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/SceneDetail.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/mTempoVis.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/TVObserver.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/TVStack.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/ShowUtils.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Show/SceneDetails" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/SceneDetails/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/SceneDetails/Camera.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/SceneDetails/ClipPlane.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/SceneDetails/ObjectClipPlane.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/SceneDetails/Pickability.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/SceneDetails/VProperty.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Show/SceneDetails/Workbench.py"
    )
endif()

