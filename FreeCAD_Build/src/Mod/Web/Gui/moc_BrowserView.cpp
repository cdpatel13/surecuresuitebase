/****************************************************************************
** Meta object code from reading C++ file 'BrowserView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Web/Gui/BrowserView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'BrowserView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_WebGui__WebView_t {
    QByteArrayData data[6];
    char stringdata0[99];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WebGui__WebView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WebGui__WebView_t qt_meta_stringdata_WebGui__WebView = {
    {
QT_MOC_LITERAL(0, 0, 15), // "WebGui::WebView"
QT_MOC_LITERAL(1, 16, 25), // "openLinkInExternalBrowser"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 19), // "openLinkInNewWindow"
QT_MOC_LITERAL(4, 63, 10), // "viewSource"
QT_MOC_LITERAL(5, 74, 24) // "triggerContextMenuAction"

    },
    "WebGui::WebView\0openLinkInExternalBrowser\0"
    "\0openLinkInNewWindow\0viewSource\0"
    "triggerContextMenuAction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WebGui__WebView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x06 /* Public */,
       3,    1,   37,    2, 0x06 /* Public */,
       4,    1,   40,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   43,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QUrl,    2,
    QMetaType::Void, QMetaType::QUrl,    2,
    QMetaType::Void, QMetaType::QUrl,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void WebGui::WebView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<WebView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->openLinkInExternalBrowser((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 1: _t->openLinkInNewWindow((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 2: _t->viewSource((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 3: _t->triggerContextMenuAction((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (WebView::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&WebView::openLinkInExternalBrowser)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (WebView::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&WebView::openLinkInNewWindow)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (WebView::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&WebView::viewSource)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject WebGui::WebView::staticMetaObject = { {
    &QWebEngineView::staticMetaObject,
    qt_meta_stringdata_WebGui__WebView.data,
    qt_meta_data_WebGui__WebView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *WebGui::WebView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WebGui::WebView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_WebGui__WebView.stringdata0))
        return static_cast<void*>(this);
    return QWebEngineView::qt_metacast(_clname);
}

int WebGui::WebView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWebEngineView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void WebGui::WebView::openLinkInExternalBrowser(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void WebGui::WebView::openLinkInNewWindow(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void WebGui::WebView::viewSource(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
struct qt_meta_stringdata_WebGui__BrowserView_t {
    QByteArrayData data[17];
    char stringdata0[234];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WebGui__BrowserView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WebGui__BrowserView_t qt_meta_stringdata_WebGui__BrowserView = {
    {
QT_MOC_LITERAL(0, 0, 19), // "WebGui::BrowserView"
QT_MOC_LITERAL(1, 20, 13), // "onLoadStarted"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 14), // "onLoadProgress"
QT_MOC_LITERAL(4, 50, 14), // "onLoadFinished"
QT_MOC_LITERAL(5, 65, 15), // "chckHostAllowed"
QT_MOC_LITERAL(6, 81, 4), // "host"
QT_MOC_LITERAL(7, 86, 19), // "onDownloadRequested"
QT_MOC_LITERAL(8, 106, 23), // "QWebEngineDownloadItem*"
QT_MOC_LITERAL(9, 130, 7), // "request"
QT_MOC_LITERAL(10, 138, 13), // "setWindowIcon"
QT_MOC_LITERAL(11, 152, 4), // "icon"
QT_MOC_LITERAL(12, 157, 9), // "urlFilter"
QT_MOC_LITERAL(13, 167, 3), // "url"
QT_MOC_LITERAL(14, 171, 12), // "onViewSource"
QT_MOC_LITERAL(15, 184, 27), // "onOpenLinkInExternalBrowser"
QT_MOC_LITERAL(16, 212, 21) // "onOpenLinkInNewWindow"

    },
    "WebGui::BrowserView\0onLoadStarted\0\0"
    "onLoadProgress\0onLoadFinished\0"
    "chckHostAllowed\0host\0onDownloadRequested\0"
    "QWebEngineDownloadItem*\0request\0"
    "setWindowIcon\0icon\0urlFilter\0url\0"
    "onViewSource\0onOpenLinkInExternalBrowser\0"
    "onOpenLinkInNewWindow"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WebGui__BrowserView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x09 /* Protected */,
       3,    1,   65,    2, 0x09 /* Protected */,
       4,    1,   68,    2, 0x09 /* Protected */,
       5,    1,   71,    2, 0x09 /* Protected */,
       7,    1,   74,    2, 0x09 /* Protected */,
      10,    1,   77,    2, 0x09 /* Protected */,
      12,    1,   80,    2, 0x09 /* Protected */,
      14,    1,   83,    2, 0x09 /* Protected */,
      15,    1,   86,    2, 0x09 /* Protected */,
      16,    1,   89,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Bool, QMetaType::QString,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, QMetaType::QIcon,   11,
    QMetaType::Void, QMetaType::QUrl,   13,
    QMetaType::Void, QMetaType::QUrl,   13,
    QMetaType::Void, QMetaType::QUrl,   13,
    QMetaType::Void, QMetaType::QUrl,    2,

       0        // eod
};

void WebGui::BrowserView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<BrowserView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onLoadStarted(); break;
        case 1: _t->onLoadProgress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->onLoadFinished((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: { bool _r = _t->chckHostAllowed((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: _t->onDownloadRequested((*reinterpret_cast< QWebEngineDownloadItem*(*)>(_a[1]))); break;
        case 5: _t->setWindowIcon((*reinterpret_cast< const QIcon(*)>(_a[1]))); break;
        case 6: _t->urlFilter((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 7: _t->onViewSource((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 8: _t->onOpenLinkInExternalBrowser((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 9: _t->onOpenLinkInNewWindow((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QWebEngineDownloadItem* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject WebGui::BrowserView::staticMetaObject = { {
    &Gui::MDIView::staticMetaObject,
    qt_meta_stringdata_WebGui__BrowserView.data,
    qt_meta_data_WebGui__BrowserView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *WebGui::BrowserView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WebGui::BrowserView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_WebGui__BrowserView.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::WindowParameter"))
        return static_cast< Gui::WindowParameter*>(this);
    return Gui::MDIView::qt_metacast(_clname);
}

int WebGui::BrowserView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::MDIView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
struct qt_meta_stringdata_WebGui__UrlWidget_t {
    QByteArrayData data[1];
    char stringdata0[18];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WebGui__UrlWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WebGui__UrlWidget_t qt_meta_stringdata_WebGui__UrlWidget = {
    {
QT_MOC_LITERAL(0, 0, 17) // "WebGui::UrlWidget"

    },
    "WebGui::UrlWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WebGui__UrlWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void WebGui::UrlWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject WebGui::UrlWidget::staticMetaObject = { {
    &QLineEdit::staticMetaObject,
    qt_meta_stringdata_WebGui__UrlWidget.data,
    qt_meta_data_WebGui__UrlWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *WebGui::UrlWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WebGui::UrlWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_WebGui__UrlWidget.stringdata0))
        return static_cast<void*>(this);
    return QLineEdit::qt_metacast(_clname);
}

int WebGui::UrlWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLineEdit::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
