/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintInitialTemperature.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTINITIALTEMPERATURE_H
#define UI_TASKFEMCONSTRAINTINITIALTEMPERATURE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/InputField.h"

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintInitialTemperature
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    Gui::InputField *if_temperature;

    void setupUi(QWidget *TaskFemConstraintInitialTemperature)
    {
        if (TaskFemConstraintInitialTemperature->objectName().isEmpty())
            TaskFemConstraintInitialTemperature->setObjectName(QString::fromUtf8("TaskFemConstraintInitialTemperature"));
        TaskFemConstraintInitialTemperature->resize(307, 118);
        verticalLayout = new QVBoxLayout(TaskFemConstraintInitialTemperature);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(TaskFemConstraintInitialTemperature);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        if_temperature = new Gui::InputField(TaskFemConstraintInitialTemperature);
        if_temperature->setObjectName(QString::fromUtf8("if_temperature"));
        if_temperature->setProperty("unit", QVariant(QString::fromUtf8("")));

        verticalLayout->addWidget(if_temperature);


        retranslateUi(TaskFemConstraintInitialTemperature);

        QMetaObject::connectSlotsByName(TaskFemConstraintInitialTemperature);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintInitialTemperature)
    {
        TaskFemConstraintInitialTemperature->setWindowTitle(QApplication::translate("TaskFemConstraintInitialTemperature", "Dialog", nullptr));
        label->setText(QApplication::translate("TaskFemConstraintInitialTemperature", "Insert component's initial temperature:", nullptr));
        if_temperature->setText(QApplication::translate("TaskFemConstraintInitialTemperature", "300 K", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintInitialTemperature: public Ui_TaskFemConstraintInitialTemperature {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTINITIALTEMPERATURE_H
