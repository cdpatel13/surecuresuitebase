/****************************************************************************
** Meta object code from reading C++ file 'TaskPostBoxes.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Fem/Gui/TaskPostBoxes.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskPostBoxes.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_FemGui__PointMarker_t {
    QByteArrayData data[9];
    char stringdata0[53];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__PointMarker_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__PointMarker_t qt_meta_stringdata_FemGui__PointMarker = {
    {
QT_MOC_LITERAL(0, 0, 19), // "FemGui::PointMarker"
QT_MOC_LITERAL(1, 20, 13), // "PointsChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 2), // "x1"
QT_MOC_LITERAL(4, 38, 2), // "y1"
QT_MOC_LITERAL(5, 41, 2), // "z1"
QT_MOC_LITERAL(6, 44, 2), // "x2"
QT_MOC_LITERAL(7, 47, 2), // "y2"
QT_MOC_LITERAL(8, 50, 2) // "z2"

    },
    "FemGui::PointMarker\0PointsChanged\0\0"
    "x1\0y1\0z1\0x2\0y2\0z2"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__PointMarker[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    6,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double,    3,    4,    5,    6,    7,    8,

       0        // eod
};

void FemGui::PointMarker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PointMarker *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->PointsChanged((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< double(*)>(_a[5])),(*reinterpret_cast< double(*)>(_a[6]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (PointMarker::*)(double , double , double , double , double , double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PointMarker::PointsChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::PointMarker::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_FemGui__PointMarker.data,
    qt_meta_data_FemGui__PointMarker,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::PointMarker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::PointMarker::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__PointMarker.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int FemGui::PointMarker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void FemGui::PointMarker::PointsChanged(double _t1, double _t2, double _t3, double _t4, double _t5, double _t6)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
struct qt_meta_stringdata_FemGui__DataMarker_t {
    QByteArrayData data[6];
    char stringdata0[40];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__DataMarker_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__DataMarker_t qt_meta_stringdata_FemGui__DataMarker = {
    {
QT_MOC_LITERAL(0, 0, 18), // "FemGui::DataMarker"
QT_MOC_LITERAL(1, 19, 13), // "PointsChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 1), // "x"
QT_MOC_LITERAL(4, 36, 1), // "y"
QT_MOC_LITERAL(5, 38, 1) // "z"

    },
    "FemGui::DataMarker\0PointsChanged\0\0x\0"
    "y\0z"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__DataMarker[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Double, QMetaType::Double, QMetaType::Double,    3,    4,    5,

       0        // eod
};

void FemGui::DataMarker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DataMarker *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->PointsChanged((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (DataMarker::*)(double , double , double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DataMarker::PointsChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::DataMarker::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_FemGui__DataMarker.data,
    qt_meta_data_FemGui__DataMarker,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::DataMarker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::DataMarker::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__DataMarker.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int FemGui::DataMarker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void FemGui::DataMarker::PointsChanged(double _t1, double _t2, double _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
struct qt_meta_stringdata_FemGui__TaskPostBox_t {
    QByteArrayData data[1];
    char stringdata0[20];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskPostBox_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskPostBox_t qt_meta_stringdata_FemGui__TaskPostBox = {
    {
QT_MOC_LITERAL(0, 0, 19) // "FemGui::TaskPostBox"

    },
    "FemGui::TaskPostBox"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskPostBox[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void FemGui::TaskPostBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskPostBox::staticMetaObject = { {
    &Gui::TaskView::TaskBox::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskPostBox.data,
    qt_meta_data_FemGui__TaskPostBox,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskPostBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskPostBox::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskPostBox.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskBox::qt_metacast(_clname);
}

int FemGui::TaskPostBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskBox::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskDlgPost_t {
    QByteArrayData data[1];
    char stringdata0[20];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskDlgPost_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskDlgPost_t qt_meta_stringdata_FemGui__TaskDlgPost = {
    {
QT_MOC_LITERAL(0, 0, 19) // "FemGui::TaskDlgPost"

    },
    "FemGui::TaskDlgPost"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskDlgPost[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void FemGui::TaskDlgPost::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskDlgPost::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskDlgPost.data,
    qt_meta_data_FemGui__TaskDlgPost,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskDlgPost::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskDlgPost::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskDlgPost.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int FemGui::TaskDlgPost::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskPostDisplay_t {
    QByteArrayData data[7];
    char stringdata0[127];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskPostDisplay_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskPostDisplay_t qt_meta_stringdata_FemGui__TaskPostDisplay = {
    {
QT_MOC_LITERAL(0, 0, 23), // "FemGui::TaskPostDisplay"
QT_MOC_LITERAL(1, 24, 27), // "on_Representation_activated"
QT_MOC_LITERAL(2, 52, 0), // ""
QT_MOC_LITERAL(3, 53, 1), // "i"
QT_MOC_LITERAL(4, 55, 18), // "on_Field_activated"
QT_MOC_LITERAL(5, 74, 23), // "on_VectorMode_activated"
QT_MOC_LITERAL(6, 98, 28) // "on_Transparency_valueChanged"

    },
    "FemGui::TaskPostDisplay\0"
    "on_Representation_activated\0\0i\0"
    "on_Field_activated\0on_VectorMode_activated\0"
    "on_Transparency_valueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskPostDisplay[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x08 /* Private */,
       4,    1,   37,    2, 0x08 /* Private */,
       5,    1,   40,    2, 0x08 /* Private */,
       6,    1,   43,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,

       0        // eod
};

void FemGui::TaskPostDisplay::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPostDisplay *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_Representation_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_Field_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_VectorMode_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_Transparency_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskPostDisplay::staticMetaObject = { {
    &TaskPostBox::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskPostDisplay.data,
    qt_meta_data_FemGui__TaskPostDisplay,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskPostDisplay::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskPostDisplay::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskPostDisplay.stringdata0))
        return static_cast<void*>(this);
    return TaskPostBox::qt_metacast(_clname);
}

int FemGui::TaskPostDisplay::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskPostBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskPostFunction_t {
    QByteArrayData data[1];
    char stringdata0[25];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskPostFunction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskPostFunction_t qt_meta_stringdata_FemGui__TaskPostFunction = {
    {
QT_MOC_LITERAL(0, 0, 24) // "FemGui::TaskPostFunction"

    },
    "FemGui::TaskPostFunction"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskPostFunction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void FemGui::TaskPostFunction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskPostFunction::staticMetaObject = { {
    &TaskPostBox::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskPostFunction.data,
    qt_meta_data_FemGui__TaskPostFunction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskPostFunction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskPostFunction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskPostFunction.stringdata0))
        return static_cast<void*>(this);
    return TaskPostBox::qt_metacast(_clname);
}

int FemGui::TaskPostFunction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskPostBox::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskPostClip_t {
    QByteArrayData data[9];
    char stringdata0[141];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskPostClip_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskPostClip_t qt_meta_stringdata_FemGui__TaskPostClip = {
    {
QT_MOC_LITERAL(0, 0, 20), // "FemGui::TaskPostClip"
QT_MOC_LITERAL(1, 21, 25), // "on_CreateButton_triggered"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 8), // "QAction*"
QT_MOC_LITERAL(4, 57, 34), // "on_FunctionBox_currentIndexCh..."
QT_MOC_LITERAL(5, 92, 3), // "idx"
QT_MOC_LITERAL(6, 96, 20), // "on_InsideOut_toggled"
QT_MOC_LITERAL(7, 117, 3), // "val"
QT_MOC_LITERAL(8, 121, 19) // "on_CutCells_toggled"

    },
    "FemGui::TaskPostClip\0on_CreateButton_triggered\0"
    "\0QAction*\0on_FunctionBox_currentIndexChanged\0"
    "idx\0on_InsideOut_toggled\0val\0"
    "on_CutCells_toggled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskPostClip[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x08 /* Private */,
       4,    1,   37,    2, 0x08 /* Private */,
       6,    1,   40,    2, 0x08 /* Private */,
       8,    1,   43,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Bool,    7,

       0        // eod
};

void FemGui::TaskPostClip::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPostClip *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_CreateButton_triggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 1: _t->on_FunctionBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_InsideOut_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_CutCells_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskPostClip::staticMetaObject = { {
    &TaskPostBox::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskPostClip.data,
    qt_meta_data_FemGui__TaskPostClip,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskPostClip::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskPostClip::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskPostClip.stringdata0))
        return static_cast<void*>(this);
    return TaskPostBox::qt_metacast(_clname);
}

int FemGui::TaskPostClip::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskPostBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskPostDataAlongLine_t {
    QByteArrayData data[19];
    char stringdata0[227];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskPostDataAlongLine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskPostDataAlongLine_t qt_meta_stringdata_FemGui__TaskPostDataAlongLine = {
    {
QT_MOC_LITERAL(0, 0, 29), // "FemGui::TaskPostDataAlongLine"
QT_MOC_LITERAL(1, 30, 23), // "on_SelectPoints_clicked"
QT_MOC_LITERAL(2, 54, 0), // ""
QT_MOC_LITERAL(3, 55, 21), // "on_CreatePlot_clicked"
QT_MOC_LITERAL(4, 77, 27), // "on_Representation_activated"
QT_MOC_LITERAL(5, 105, 1), // "i"
QT_MOC_LITERAL(6, 107, 18), // "on_Field_activated"
QT_MOC_LITERAL(7, 126, 23), // "on_VectorMode_activated"
QT_MOC_LITERAL(8, 150, 13), // "point2Changed"
QT_MOC_LITERAL(9, 164, 13), // "point1Changed"
QT_MOC_LITERAL(10, 178, 17), // "resolutionChanged"
QT_MOC_LITERAL(11, 196, 3), // "val"
QT_MOC_LITERAL(12, 200, 8), // "onChange"
QT_MOC_LITERAL(13, 209, 2), // "x1"
QT_MOC_LITERAL(14, 212, 2), // "y1"
QT_MOC_LITERAL(15, 215, 2), // "z1"
QT_MOC_LITERAL(16, 218, 2), // "x2"
QT_MOC_LITERAL(17, 221, 2), // "y2"
QT_MOC_LITERAL(18, 224, 2) // "z2"

    },
    "FemGui::TaskPostDataAlongLine\0"
    "on_SelectPoints_clicked\0\0on_CreatePlot_clicked\0"
    "on_Representation_activated\0i\0"
    "on_Field_activated\0on_VectorMode_activated\0"
    "point2Changed\0point1Changed\0"
    "resolutionChanged\0val\0onChange\0x1\0y1\0"
    "z1\0x2\0y2\0z2"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskPostDataAlongLine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    1,   61,    2, 0x08 /* Private */,
       6,    1,   64,    2, 0x08 /* Private */,
       7,    1,   67,    2, 0x08 /* Private */,
       8,    1,   70,    2, 0x08 /* Private */,
       9,    1,   73,    2, 0x08 /* Private */,
      10,    1,   76,    2, 0x08 /* Private */,
      12,    6,   79,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double, QMetaType::Double,   13,   14,   15,   16,   17,   18,

       0        // eod
};

void FemGui::TaskPostDataAlongLine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPostDataAlongLine *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_SelectPoints_clicked(); break;
        case 1: _t->on_CreatePlot_clicked(); break;
        case 2: _t->on_Representation_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_Field_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_VectorMode_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->point2Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->point1Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->resolutionChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->onChange((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4])),(*reinterpret_cast< double(*)>(_a[5])),(*reinterpret_cast< double(*)>(_a[6]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskPostDataAlongLine::staticMetaObject = { {
    &TaskPostBox::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskPostDataAlongLine.data,
    qt_meta_data_FemGui__TaskPostDataAlongLine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskPostDataAlongLine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskPostDataAlongLine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskPostDataAlongLine.stringdata0))
        return static_cast<void*>(this);
    return TaskPostBox::qt_metacast(_clname);
}

int FemGui::TaskPostDataAlongLine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskPostBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskPostDataAtPoint_t {
    QByteArrayData data[10];
    char stringdata0[102];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskPostDataAtPoint_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskPostDataAtPoint_t qt_meta_stringdata_FemGui__TaskPostDataAtPoint = {
    {
QT_MOC_LITERAL(0, 0, 27), // "FemGui::TaskPostDataAtPoint"
QT_MOC_LITERAL(1, 28, 22), // "on_SelectPoint_clicked"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 18), // "on_Field_activated"
QT_MOC_LITERAL(4, 71, 1), // "i"
QT_MOC_LITERAL(5, 73, 13), // "centerChanged"
QT_MOC_LITERAL(6, 87, 8), // "onChange"
QT_MOC_LITERAL(7, 96, 1), // "x"
QT_MOC_LITERAL(8, 98, 1), // "y"
QT_MOC_LITERAL(9, 100, 1) // "z"

    },
    "FemGui::TaskPostDataAtPoint\0"
    "on_SelectPoint_clicked\0\0on_Field_activated\0"
    "i\0centerChanged\0onChange\0x\0y\0z"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskPostDataAtPoint[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x08 /* Private */,
       3,    1,   35,    2, 0x08 /* Private */,
       5,    1,   38,    2, 0x08 /* Private */,
       6,    3,   41,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double, QMetaType::Double, QMetaType::Double,    7,    8,    9,

       0        // eod
};

void FemGui::TaskPostDataAtPoint::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPostDataAtPoint *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_SelectPoint_clicked(); break;
        case 1: _t->on_Field_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->centerChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->onChange((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskPostDataAtPoint::staticMetaObject = { {
    &TaskPostBox::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskPostDataAtPoint.data,
    qt_meta_data_FemGui__TaskPostDataAtPoint,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskPostDataAtPoint::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskPostDataAtPoint::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskPostDataAtPoint.stringdata0))
        return static_cast<void*>(this);
    return TaskPostBox::qt_metacast(_clname);
}

int FemGui::TaskPostDataAtPoint::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskPostBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskPostScalarClip_t {
    QByteArrayData data[9];
    char stringdata0[134];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskPostScalarClip_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskPostScalarClip_t qt_meta_stringdata_FemGui__TaskPostScalarClip = {
    {
QT_MOC_LITERAL(0, 0, 26), // "FemGui::TaskPostScalarClip"
QT_MOC_LITERAL(1, 27, 22), // "on_Slider_valueChanged"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 1), // "v"
QT_MOC_LITERAL(4, 53, 21), // "on_Value_valueChanged"
QT_MOC_LITERAL(5, 75, 29), // "on_Scalar_currentIndexChanged"
QT_MOC_LITERAL(6, 105, 3), // "idx"
QT_MOC_LITERAL(7, 109, 20), // "on_InsideOut_toggled"
QT_MOC_LITERAL(8, 130, 3) // "val"

    },
    "FemGui::TaskPostScalarClip\0"
    "on_Slider_valueChanged\0\0v\0"
    "on_Value_valueChanged\0"
    "on_Scalar_currentIndexChanged\0idx\0"
    "on_InsideOut_toggled\0val"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskPostScalarClip[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x08 /* Private */,
       4,    1,   37,    2, 0x08 /* Private */,
       5,    1,   40,    2, 0x08 /* Private */,
       7,    1,   43,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Bool,    8,

       0        // eod
};

void FemGui::TaskPostScalarClip::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPostScalarClip *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_Value_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->on_Scalar_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_InsideOut_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskPostScalarClip::staticMetaObject = { {
    &TaskPostBox::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskPostScalarClip.data,
    qt_meta_data_FemGui__TaskPostScalarClip,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskPostScalarClip::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskPostScalarClip::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskPostScalarClip.stringdata0))
        return static_cast<void*>(this);
    return TaskPostBox::qt_metacast(_clname);
}

int FemGui::TaskPostScalarClip::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskPostBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskPostWarpVector_t {
    QByteArrayData data[9];
    char stringdata0[149];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskPostWarpVector_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskPostWarpVector_t qt_meta_stringdata_FemGui__TaskPostWarpVector = {
    {
QT_MOC_LITERAL(0, 0, 26), // "FemGui::TaskPostWarpVector"
QT_MOC_LITERAL(1, 27, 22), // "on_Slider_valueChanged"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 1), // "v"
QT_MOC_LITERAL(4, 53, 21), // "on_Value_valueChanged"
QT_MOC_LITERAL(5, 75, 19), // "on_Max_valueChanged"
QT_MOC_LITERAL(6, 95, 19), // "on_Min_valueChanged"
QT_MOC_LITERAL(7, 115, 29), // "on_Vector_currentIndexChanged"
QT_MOC_LITERAL(8, 145, 3) // "idx"

    },
    "FemGui::TaskPostWarpVector\0"
    "on_Slider_valueChanged\0\0v\0"
    "on_Value_valueChanged\0on_Max_valueChanged\0"
    "on_Min_valueChanged\0on_Vector_currentIndexChanged\0"
    "idx"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskPostWarpVector[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x08 /* Private */,
       4,    1,   42,    2, 0x08 /* Private */,
       5,    1,   45,    2, 0x08 /* Private */,
       6,    1,   48,    2, 0x08 /* Private */,
       7,    1,   51,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Int,    8,

       0        // eod
};

void FemGui::TaskPostWarpVector::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPostWarpVector *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_Slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_Value_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->on_Max_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->on_Min_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->on_Vector_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskPostWarpVector::staticMetaObject = { {
    &TaskPostBox::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskPostWarpVector.data,
    qt_meta_data_FemGui__TaskPostWarpVector,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskPostWarpVector::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskPostWarpVector::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskPostWarpVector.stringdata0))
        return static_cast<void*>(this);
    return TaskPostBox::qt_metacast(_clname);
}

int FemGui::TaskPostWarpVector::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskPostBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskPostCut_t {
    QByteArrayData data[6];
    char stringdata0[95];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskPostCut_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskPostCut_t qt_meta_stringdata_FemGui__TaskPostCut = {
    {
QT_MOC_LITERAL(0, 0, 19), // "FemGui::TaskPostCut"
QT_MOC_LITERAL(1, 20, 25), // "on_CreateButton_triggered"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 8), // "QAction*"
QT_MOC_LITERAL(4, 56, 34), // "on_FunctionBox_currentIndexCh..."
QT_MOC_LITERAL(5, 91, 3) // "idx"

    },
    "FemGui::TaskPostCut\0on_CreateButton_triggered\0"
    "\0QAction*\0on_FunctionBox_currentIndexChanged\0"
    "idx"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskPostCut[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x08 /* Private */,
       4,    1,   27,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, QMetaType::Int,    5,

       0        // eod
};

void FemGui::TaskPostCut::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPostCut *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_CreateButton_triggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 1: _t->on_FunctionBox_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskPostCut::staticMetaObject = { {
    &TaskPostBox::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskPostCut.data,
    qt_meta_data_FemGui__TaskPostCut,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskPostCut::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskPostCut::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskPostCut.stringdata0))
        return static_cast<void*>(this);
    return TaskPostBox::qt_metacast(_clname);
}

int FemGui::TaskPostCut::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskPostBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
