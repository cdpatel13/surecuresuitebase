/********************************************************************************
** Form generated from reading UI file 'TaskPostDataAtPoint.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKPOSTDATAATPOINT_H
#define UI_TASKPOSTDATAATPOINT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TaskPostDataAtPoint
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *line;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QDoubleSpinBox *centerX;
    QDoubleSpinBox *centerY;
    QDoubleSpinBox *centerZ;
    QPushButton *SelectPoint;
    QFrame *line_2;
    QFormLayout *formLayout_4;
    QLabel *label_4;
    QComboBox *Field;
    QFrame *line_3;

    void setupUi(QWidget *TaskPostDataAtPoint)
    {
        if (TaskPostDataAtPoint->objectName().isEmpty())
            TaskPostDataAtPoint->setObjectName(QString::fromUtf8("TaskPostDataAtPoint"));
        TaskPostDataAtPoint->resize(482, 363);
        verticalLayout = new QVBoxLayout(TaskPostDataAtPoint);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        line = new QFrame(TaskPostDataAtPoint);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(TaskPostDataAtPoint);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label);

        centerX = new QDoubleSpinBox(TaskPostDataAtPoint);
        centerX->setObjectName(QString::fromUtf8("centerX"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(centerX->sizePolicy().hasHeightForWidth());
        centerX->setSizePolicy(sizePolicy);
        centerX->setMinimumSize(QSize(70, 0));
        centerX->setMinimum(-999999999.000000000000000);
        centerX->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(centerX);

        centerY = new QDoubleSpinBox(TaskPostDataAtPoint);
        centerY->setObjectName(QString::fromUtf8("centerY"));
        sizePolicy.setHeightForWidth(centerY->sizePolicy().hasHeightForWidth());
        centerY->setSizePolicy(sizePolicy);
        centerY->setMinimumSize(QSize(70, 0));
        centerY->setMinimum(-999999999.000000000000000);
        centerY->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(centerY);

        centerZ = new QDoubleSpinBox(TaskPostDataAtPoint);
        centerZ->setObjectName(QString::fromUtf8("centerZ"));
        sizePolicy.setHeightForWidth(centerZ->sizePolicy().hasHeightForWidth());
        centerZ->setSizePolicy(sizePolicy);
        centerZ->setMinimumSize(QSize(70, 0));
        centerZ->setMinimum(-999999999.000000000000000);
        centerZ->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(centerZ);


        verticalLayout->addLayout(horizontalLayout);

        SelectPoint = new QPushButton(TaskPostDataAtPoint);
        SelectPoint->setObjectName(QString::fromUtf8("SelectPoint"));

        verticalLayout->addWidget(SelectPoint);

        line_2 = new QFrame(TaskPostDataAtPoint);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        formLayout_4->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_4 = new QLabel(TaskPostDataAtPoint);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_4);

        Field = new QComboBox(TaskPostDataAtPoint);
        Field->setObjectName(QString::fromUtf8("Field"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, Field);


        verticalLayout->addLayout(formLayout_4);

        line_3 = new QFrame(TaskPostDataAtPoint);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_3);

        SelectPoint->raise();
        line->raise();
        line_2->raise();
        line_3->raise();

        retranslateUi(TaskPostDataAtPoint);

        QMetaObject::connectSlotsByName(TaskPostDataAtPoint);
    } // setupUi

    void retranslateUi(QWidget *TaskPostDataAtPoint)
    {
        TaskPostDataAtPoint->setWindowTitle(QApplication::translate("TaskPostDataAtPoint", "Form", nullptr));
        label->setText(QApplication::translate("TaskPostDataAtPoint", "Center", nullptr));
        SelectPoint->setText(QApplication::translate("TaskPostDataAtPoint", "Select Point", nullptr));
        label_4->setText(QApplication::translate("TaskPostDataAtPoint", "Field", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskPostDataAtPoint: public Ui_TaskPostDataAtPoint {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKPOSTDATAATPOINT_H
