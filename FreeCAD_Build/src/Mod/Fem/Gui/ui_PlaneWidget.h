/********************************************************************************
** Form generated from reading UI file 'PlaneWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLANEWIDGET_H
#define UI_PLANEWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PlaneWidget
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QDoubleSpinBox *originX;
    QDoubleSpinBox *originY;
    QDoubleSpinBox *originZ;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QDoubleSpinBox *normalX;
    QDoubleSpinBox *normalY;
    QDoubleSpinBox *normalZ;

    void setupUi(QWidget *PlaneWidget)
    {
        if (PlaneWidget->objectName().isEmpty())
            PlaneWidget->setObjectName(QString::fromUtf8("PlaneWidget"));
        PlaneWidget->resize(287, 84);
        formLayout = new QFormLayout(PlaneWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setVerticalSpacing(2);
        formLayout->setContentsMargins(6, -1, -1, 0);
        label = new QLabel(PlaneWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(2);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        originX = new QDoubleSpinBox(PlaneWidget);
        originX->setObjectName(QString::fromUtf8("originX"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(originX->sizePolicy().hasHeightForWidth());
        originX->setSizePolicy(sizePolicy);
        originX->setMinimumSize(QSize(70, 0));
        originX->setMinimum(-999999999.000000000000000);
        originX->setMaximum(999999999.000000000000000);

        horizontalLayout_2->addWidget(originX);

        originY = new QDoubleSpinBox(PlaneWidget);
        originY->setObjectName(QString::fromUtf8("originY"));
        sizePolicy.setHeightForWidth(originY->sizePolicy().hasHeightForWidth());
        originY->setSizePolicy(sizePolicy);
        originY->setMinimumSize(QSize(70, 0));
        originY->setMinimum(-999999999.000000000000000);
        originY->setValue(0.000000000000000);

        horizontalLayout_2->addWidget(originY);

        originZ = new QDoubleSpinBox(PlaneWidget);
        originZ->setObjectName(QString::fromUtf8("originZ"));
        sizePolicy.setHeightForWidth(originZ->sizePolicy().hasHeightForWidth());
        originZ->setSizePolicy(sizePolicy);
        originZ->setMinimumSize(QSize(70, 0));
        originZ->setMinimum(-999999999.000000000000000);
        originZ->setMaximum(999999999.000000000000000);

        horizontalLayout_2->addWidget(originZ);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout_2);

        label_2 = new QLabel(PlaneWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        normalX = new QDoubleSpinBox(PlaneWidget);
        normalX->setObjectName(QString::fromUtf8("normalX"));
        sizePolicy.setHeightForWidth(normalX->sizePolicy().hasHeightForWidth());
        normalX->setSizePolicy(sizePolicy);
        normalX->setMinimumSize(QSize(70, 0));
        normalX->setMinimum(-999999999.000000000000000);
        normalX->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(normalX);

        normalY = new QDoubleSpinBox(PlaneWidget);
        normalY->setObjectName(QString::fromUtf8("normalY"));
        sizePolicy.setHeightForWidth(normalY->sizePolicy().hasHeightForWidth());
        normalY->setSizePolicy(sizePolicy);
        normalY->setMinimumSize(QSize(70, 0));
        normalY->setMinimum(-999999999.000000000000000);
        normalY->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(normalY);

        normalZ = new QDoubleSpinBox(PlaneWidget);
        normalZ->setObjectName(QString::fromUtf8("normalZ"));
        sizePolicy.setHeightForWidth(normalZ->sizePolicy().hasHeightForWidth());
        normalZ->setSizePolicy(sizePolicy);
        normalZ->setMinimumSize(QSize(70, 0));
        normalZ->setMinimum(-999999999.000000000000000);
        normalZ->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(normalZ);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout);


        retranslateUi(PlaneWidget);

        QMetaObject::connectSlotsByName(PlaneWidget);
    } // setupUi

    void retranslateUi(QWidget *PlaneWidget)
    {
        PlaneWidget->setWindowTitle(QApplication::translate("PlaneWidget", "Form", nullptr));
        label->setText(QApplication::translate("PlaneWidget", "Origin", nullptr));
        label_2->setText(QApplication::translate("PlaneWidget", "Normal", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PlaneWidget: public Ui_PlaneWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLANEWIDGET_H
