/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintContact.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTCONTACT_H
#define UI_TASKFEMCONSTRAINTCONTACT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/InputField.h"

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintContact
{
public:
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *lbl_info_2;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *btnAddMaster;
    QPushButton *btnRemoveMaster;
    QListWidget *lw_referencesMaster;
    QVBoxLayout *verticalLayout_2;
    QLabel *lbl_info;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnAddSlave;
    QPushButton *btnRemoveSlave;
    QListWidget *lw_referencesSlave;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    Gui::InputField *spSlope;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QDoubleSpinBox *spFriction;

    void setupUi(QWidget *TaskFemConstraintContact)
    {
        if (TaskFemConstraintContact->objectName().isEmpty())
            TaskFemConstraintContact->setObjectName(QString::fromUtf8("TaskFemConstraintContact"));
        TaskFemConstraintContact->resize(354, 251);
        verticalLayout_3 = new QVBoxLayout(TaskFemConstraintContact);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbl_info_2 = new QLabel(TaskFemConstraintContact);
        lbl_info_2->setObjectName(QString::fromUtf8("lbl_info_2"));

        verticalLayout->addWidget(lbl_info_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        btnAddMaster = new QPushButton(TaskFemConstraintContact);
        btnAddMaster->setObjectName(QString::fromUtf8("btnAddMaster"));

        horizontalLayout_4->addWidget(btnAddMaster);

        btnRemoveMaster = new QPushButton(TaskFemConstraintContact);
        btnRemoveMaster->setObjectName(QString::fromUtf8("btnRemoveMaster"));

        horizontalLayout_4->addWidget(btnRemoveMaster);


        verticalLayout->addLayout(horizontalLayout_4);


        verticalLayout_3->addLayout(verticalLayout);

        lw_referencesMaster = new QListWidget(TaskFemConstraintContact);
        lw_referencesMaster->setObjectName(QString::fromUtf8("lw_referencesMaster"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lw_referencesMaster->sizePolicy().hasHeightForWidth());
        lw_referencesMaster->setSizePolicy(sizePolicy);
        lw_referencesMaster->setMaximumSize(QSize(16777215, 31));

        verticalLayout_3->addWidget(lw_referencesMaster);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lbl_info = new QLabel(TaskFemConstraintContact);
        lbl_info->setObjectName(QString::fromUtf8("lbl_info"));

        verticalLayout_2->addWidget(lbl_info);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btnAddSlave = new QPushButton(TaskFemConstraintContact);
        btnAddSlave->setObjectName(QString::fromUtf8("btnAddSlave"));

        horizontalLayout->addWidget(btnAddSlave);

        btnRemoveSlave = new QPushButton(TaskFemConstraintContact);
        btnRemoveSlave->setObjectName(QString::fromUtf8("btnRemoveSlave"));

        horizontalLayout->addWidget(btnRemoveSlave);


        verticalLayout_2->addLayout(horizontalLayout);


        verticalLayout_3->addLayout(verticalLayout_2);

        lw_referencesSlave = new QListWidget(TaskFemConstraintContact);
        lw_referencesSlave->setObjectName(QString::fromUtf8("lw_referencesSlave"));
        lw_referencesSlave->setEnabled(true);
        sizePolicy.setHeightForWidth(lw_referencesSlave->sizePolicy().hasHeightForWidth());
        lw_referencesSlave->setSizePolicy(sizePolicy);
        lw_referencesSlave->setMaximumSize(QSize(16777215, 31));

        verticalLayout_3->addWidget(lw_referencesSlave);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label = new QLabel(TaskFemConstraintContact);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_3->addWidget(label);

        spSlope = new Gui::InputField(TaskFemConstraintContact);
        spSlope->setObjectName(QString::fromUtf8("spSlope"));
        spSlope->setMaximum(1000000000.000000000000000);
        spSlope->setSingleStep(1.000000000000000);
        spSlope->setValue(1000000.000000000000000);
        spSlope->setProperty("unit", QVariant(QString::fromUtf8("Pa")));

        horizontalLayout_3->addWidget(spSlope);


        verticalLayout_3->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(TaskFemConstraintContact);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        spFriction = new QDoubleSpinBox(TaskFemConstraintContact);
        spFriction->setObjectName(QString::fromUtf8("spFriction"));
        spFriction->setDecimals(1);
        spFriction->setMaximum(1.000000000000000);
        spFriction->setSingleStep(0.100000000000000);

        horizontalLayout_2->addWidget(spFriction);


        verticalLayout_3->addLayout(horizontalLayout_2);

        lbl_info->raise();
        lw_referencesSlave->raise();
        lw_referencesMaster->raise();
        lbl_info_2->raise();

        retranslateUi(TaskFemConstraintContact);

        QMetaObject::connectSlotsByName(TaskFemConstraintContact);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintContact)
    {
        TaskFemConstraintContact->setWindowTitle(QApplication::translate("TaskFemConstraintContact", "Form", nullptr));
        lbl_info_2->setText(QApplication::translate("TaskFemConstraintContact", "Select master face, click Add or Remove", nullptr));
        btnAddMaster->setText(QApplication::translate("TaskFemConstraintContact", "Add", nullptr));
        btnRemoveMaster->setText(QApplication::translate("TaskFemConstraintContact", "Remove", nullptr));
        lbl_info->setText(QApplication::translate("TaskFemConstraintContact", "Select slave face, click Add or Remove", nullptr));
        btnAddSlave->setText(QApplication::translate("TaskFemConstraintContact", "Add", nullptr));
        btnRemoveSlave->setText(QApplication::translate("TaskFemConstraintContact", "Remove", nullptr));
        label->setText(QApplication::translate("TaskFemConstraintContact", "Contact Stiffness", nullptr));
        label_2->setText(QApplication::translate("TaskFemConstraintContact", "Friction coefficient", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintContact: public Ui_TaskFemConstraintContact {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTCONTACT_H
