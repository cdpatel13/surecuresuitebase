/********************************************************************************
** Form generated from reading UI file 'TaskPostDataAlongLine.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKPOSTDATAALONGLINE_H
#define UI_TASKPOSTDATAALONGLINE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TaskPostDataAlongLine
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *line;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QDoubleSpinBox *point1X;
    QDoubleSpinBox *point1Y;
    QDoubleSpinBox *point1Z;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QDoubleSpinBox *point2X;
    QDoubleSpinBox *point2Y;
    QDoubleSpinBox *point2Z;
    QPushButton *SelectPoints;
    QFormLayout *formLayout;
    QLabel *label_6;
    QSpinBox *resolution;
    QHBoxLayout *horizontalLayout_5;
    QFrame *line_2;
    QFormLayout *formLayout_3;
    QComboBox *Representation;
    QLabel *label_3;
    QFormLayout *formLayout_4;
    QLabel *label_4;
    QComboBox *Field;
    QLabel *label_5;
    QComboBox *VectorMode;
    QFrame *line_3;
    QPushButton *CreatePlot;

    void setupUi(QWidget *TaskPostDataAlongLine)
    {
        if (TaskPostDataAlongLine->objectName().isEmpty())
            TaskPostDataAlongLine->setObjectName(QString::fromUtf8("TaskPostDataAlongLine"));
        TaskPostDataAlongLine->resize(482, 363);
        verticalLayout = new QVBoxLayout(TaskPostDataAlongLine);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        line = new QFrame(TaskPostDataAlongLine);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(TaskPostDataAlongLine);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label);

        point1X = new QDoubleSpinBox(TaskPostDataAlongLine);
        point1X->setObjectName(QString::fromUtf8("point1X"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(point1X->sizePolicy().hasHeightForWidth());
        point1X->setSizePolicy(sizePolicy);
        point1X->setMinimumSize(QSize(70, 0));
        point1X->setMinimum(-999999999.000000000000000);
        point1X->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(point1X);

        point1Y = new QDoubleSpinBox(TaskPostDataAlongLine);
        point1Y->setObjectName(QString::fromUtf8("point1Y"));
        sizePolicy.setHeightForWidth(point1Y->sizePolicy().hasHeightForWidth());
        point1Y->setSizePolicy(sizePolicy);
        point1Y->setMinimumSize(QSize(70, 0));
        point1Y->setMinimum(-999999999.000000000000000);
        point1Y->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(point1Y);

        point1Z = new QDoubleSpinBox(TaskPostDataAlongLine);
        point1Z->setObjectName(QString::fromUtf8("point1Z"));
        sizePolicy.setHeightForWidth(point1Z->sizePolicy().hasHeightForWidth());
        point1Z->setSizePolicy(sizePolicy);
        point1Z->setMinimumSize(QSize(70, 0));
        point1Z->setMinimum(-999999999.000000000000000);
        point1Z->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(point1Z);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(TaskPostDataAlongLine);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_2);

        point2X = new QDoubleSpinBox(TaskPostDataAlongLine);
        point2X->setObjectName(QString::fromUtf8("point2X"));
        sizePolicy.setHeightForWidth(point2X->sizePolicy().hasHeightForWidth());
        point2X->setSizePolicy(sizePolicy);
        point2X->setMinimumSize(QSize(70, 0));
        point2X->setMinimum(-999999999.000000000000000);
        point2X->setMaximum(999999999.000000000000000);

        horizontalLayout_2->addWidget(point2X);

        point2Y = new QDoubleSpinBox(TaskPostDataAlongLine);
        point2Y->setObjectName(QString::fromUtf8("point2Y"));
        sizePolicy.setHeightForWidth(point2Y->sizePolicy().hasHeightForWidth());
        point2Y->setSizePolicy(sizePolicy);
        point2Y->setMinimumSize(QSize(70, 0));
        point2Y->setMinimum(-999999999.000000000000000);
        point2Y->setMaximum(999999999.000000000000000);

        horizontalLayout_2->addWidget(point2Y);

        point2Z = new QDoubleSpinBox(TaskPostDataAlongLine);
        point2Z->setObjectName(QString::fromUtf8("point2Z"));
        sizePolicy.setHeightForWidth(point2Z->sizePolicy().hasHeightForWidth());
        point2Z->setSizePolicy(sizePolicy);
        point2Z->setMinimumSize(QSize(70, 0));
        point2Z->setMinimum(-999999999.000000000000000);
        point2Z->setMaximum(999999999.000000000000000);

        horizontalLayout_2->addWidget(point2Z);


        verticalLayout->addLayout(horizontalLayout_2);

        SelectPoints = new QPushButton(TaskPostDataAlongLine);
        SelectPoints->setObjectName(QString::fromUtf8("SelectPoints"));

        verticalLayout->addWidget(SelectPoints);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_6 = new QLabel(TaskPostDataAlongLine);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_6);

        resolution = new QSpinBox(TaskPostDataAlongLine);
        resolution->setObjectName(QString::fromUtf8("resolution"));
        resolution->setMaximum(999999999);

        formLayout->setWidget(1, QFormLayout::FieldRole, resolution);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));

        formLayout->setLayout(0, QFormLayout::LabelRole, horizontalLayout_5);


        verticalLayout->addLayout(formLayout);

        line_2 = new QFrame(TaskPostDataAlongLine);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        Representation = new QComboBox(TaskPostDataAlongLine);
        Representation->setObjectName(QString::fromUtf8("Representation"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, Representation);

        label_3 = new QLabel(TaskPostDataAlongLine);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_3);


        verticalLayout->addLayout(formLayout_3);

        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        formLayout_4->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_4 = new QLabel(TaskPostDataAlongLine);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_4);

        Field = new QComboBox(TaskPostDataAlongLine);
        Field->setObjectName(QString::fromUtf8("Field"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, Field);

        label_5 = new QLabel(TaskPostDataAlongLine);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout_4->setWidget(1, QFormLayout::LabelRole, label_5);

        VectorMode = new QComboBox(TaskPostDataAlongLine);
        VectorMode->setObjectName(QString::fromUtf8("VectorMode"));

        formLayout_4->setWidget(1, QFormLayout::FieldRole, VectorMode);


        verticalLayout->addLayout(formLayout_4);

        line_3 = new QFrame(TaskPostDataAlongLine);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_3);

        CreatePlot = new QPushButton(TaskPostDataAlongLine);
        CreatePlot->setObjectName(QString::fromUtf8("CreatePlot"));

        verticalLayout->addWidget(CreatePlot);

        SelectPoints->raise();
        line->raise();
        line_2->raise();
        line_3->raise();
        CreatePlot->raise();

        retranslateUi(TaskPostDataAlongLine);

        QMetaObject::connectSlotsByName(TaskPostDataAlongLine);
    } // setupUi

    void retranslateUi(QWidget *TaskPostDataAlongLine)
    {
        TaskPostDataAlongLine->setWindowTitle(QApplication::translate("TaskPostDataAlongLine", "Form", nullptr));
        label->setText(QApplication::translate("TaskPostDataAlongLine", "Point1", nullptr));
        label_2->setText(QApplication::translate("TaskPostDataAlongLine", "Point2", nullptr));
        SelectPoints->setText(QApplication::translate("TaskPostDataAlongLine", "Select Points", nullptr));
        label_6->setText(QApplication::translate("TaskPostDataAlongLine", "Resolution", nullptr));
        label_3->setText(QApplication::translate("TaskPostDataAlongLine", "Mode", nullptr));
        label_4->setText(QApplication::translate("TaskPostDataAlongLine", "Field", nullptr));
        label_5->setText(QApplication::translate("TaskPostDataAlongLine", "Vector", nullptr));
        CreatePlot->setText(QApplication::translate("TaskPostDataAlongLine", "Create Plot", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskPostDataAlongLine: public Ui_TaskPostDataAlongLine {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKPOSTDATAALONGLINE_H
