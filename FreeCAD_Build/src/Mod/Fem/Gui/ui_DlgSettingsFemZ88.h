/********************************************************************************
** Form generated from reading UI file 'DlgSettingsFemZ88.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGSFEMZ88_H
#define UI_DLGSETTINGSFEMZ88_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/FileDialog.h"
#include "Gui/PrefWidgets.h"

namespace FemGui {

class Ui_DlgSettingsFemZ88Imp
{
public:
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *gb_z88_param;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gl_z88;
    Gui::PrefCheckBox *cb_z88_binary_std;
    QLabel *l_z88_binary_std;
    QLabel *l_z88_binary_path;
    Gui::PrefFileChooser *fc_z88_binary_path;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *FemGui__DlgSettingsFemZ88Imp)
    {
        if (FemGui__DlgSettingsFemZ88Imp->objectName().isEmpty())
            FemGui__DlgSettingsFemZ88Imp->setObjectName(QString::fromUtf8("FemGui__DlgSettingsFemZ88Imp"));
        FemGui__DlgSettingsFemZ88Imp->resize(369, 144);
        verticalLayout = new QVBoxLayout(FemGui__DlgSettingsFemZ88Imp);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        gb_z88_param = new QGroupBox(FemGui__DlgSettingsFemZ88Imp);
        gb_z88_param->setObjectName(QString::fromUtf8("gb_z88_param"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(gb_z88_param->sizePolicy().hasHeightForWidth());
        gb_z88_param->setSizePolicy(sizePolicy);
        gb_z88_param->setLayoutDirection(Qt::LeftToRight);
        gb_z88_param->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        horizontalLayout = new QHBoxLayout(gb_z88_param);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetNoConstraint);
        gl_z88 = new QGridLayout();
        gl_z88->setSpacing(6);
        gl_z88->setObjectName(QString::fromUtf8("gl_z88"));
        cb_z88_binary_std = new Gui::PrefCheckBox(gb_z88_param);
        cb_z88_binary_std->setObjectName(QString::fromUtf8("cb_z88_binary_std"));
        cb_z88_binary_std->setChecked(true);
        cb_z88_binary_std->setProperty("prefEntry", QVariant(QByteArray("UseStandardZ88Location")));
        cb_z88_binary_std->setProperty("prefPath", QVariant(QByteArray("Mod/Fem/Z88")));

        gl_z88->addWidget(cb_z88_binary_std, 0, 2, 1, 1);

        l_z88_binary_std = new QLabel(gb_z88_param);
        l_z88_binary_std->setObjectName(QString::fromUtf8("l_z88_binary_std"));

        gl_z88->addWidget(l_z88_binary_std, 0, 0, 1, 1);

        l_z88_binary_path = new QLabel(gb_z88_param);
        l_z88_binary_path->setObjectName(QString::fromUtf8("l_z88_binary_path"));
        l_z88_binary_path->setEnabled(false);
        l_z88_binary_path->setMinimumSize(QSize(100, 0));

        gl_z88->addWidget(l_z88_binary_path, 2, 0, 1, 1);

        fc_z88_binary_path = new Gui::PrefFileChooser(gb_z88_param);
        fc_z88_binary_path->setObjectName(QString::fromUtf8("fc_z88_binary_path"));
        fc_z88_binary_path->setEnabled(false);
        sizePolicy.setHeightForWidth(fc_z88_binary_path->sizePolicy().hasHeightForWidth());
        fc_z88_binary_path->setSizePolicy(sizePolicy);
        fc_z88_binary_path->setMinimumSize(QSize(0, 0));
        fc_z88_binary_path->setSizeIncrement(QSize(0, 0));
        fc_z88_binary_path->setBaseSize(QSize(0, 0));
        fc_z88_binary_path->setProperty("prefEntry", QVariant(QByteArray("z88BinaryPath")));
        fc_z88_binary_path->setProperty("prefPath", QVariant(QByteArray("Mod/Fem/Z88")));

        gl_z88->addWidget(fc_z88_binary_path, 2, 2, 1, 1);


        horizontalLayout->addLayout(gl_z88);


        verticalLayout_2->addWidget(gb_z88_param);


        verticalLayout->addLayout(verticalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(FemGui__DlgSettingsFemZ88Imp);
        QObject::connect(cb_z88_binary_std, SIGNAL(toggled(bool)), l_z88_binary_path, SLOT(setEnabled(bool)));
        QObject::connect(cb_z88_binary_std, SIGNAL(toggled(bool)), fc_z88_binary_path, SLOT(setEnabled(bool)));
        QObject::connect(cb_z88_binary_std, SIGNAL(toggled(bool)), l_z88_binary_path, SLOT(setDisabled(bool)));
        QObject::connect(cb_z88_binary_std, SIGNAL(toggled(bool)), fc_z88_binary_path, SLOT(setDisabled(bool)));

        QMetaObject::connectSlotsByName(FemGui__DlgSettingsFemZ88Imp);
    } // setupUi

    void retranslateUi(QWidget *FemGui__DlgSettingsFemZ88Imp)
    {
        FemGui__DlgSettingsFemZ88Imp->setWindowTitle(QApplication::translate("FemGui::DlgSettingsFemZ88Imp", "Z88", nullptr));
        gb_z88_param->setTitle(QApplication::translate("FemGui::DlgSettingsFemZ88Imp", "Z88 binary", nullptr));
        cb_z88_binary_std->setText(QApplication::translate("FemGui::DlgSettingsFemZ88Imp", "Search in known binary directories", nullptr));
        l_z88_binary_std->setText(QApplication::translate("FemGui::DlgSettingsFemZ88Imp", "z88r", nullptr));
        l_z88_binary_path->setText(QApplication::translate("FemGui::DlgSettingsFemZ88Imp", "z88r binary path", nullptr));
#ifndef QT_NO_TOOLTIP
        fc_z88_binary_path->setToolTip(QApplication::translate("FemGui::DlgSettingsFemZ88Imp", "Leave blank to use default Z88 z88r binary file", nullptr));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

} // namespace FemGui

namespace FemGui {
namespace Ui {
    class DlgSettingsFemZ88Imp: public Ui_DlgSettingsFemZ88Imp {};
} // namespace Ui
} // namespace FemGui

#endif // UI_DLGSETTINGSFEMZ88_H
