/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintTransform.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTTRANSFORM_H
#define UI_TASKFEMCONSTRAINTTRANSFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintTransform
{
public:
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QRadioButton *rb_rect;
    QRadioButton *rb_cylin;
    QLabel *lbl_info_2;
    QHBoxLayout *hLayout1_3;
    QPushButton *btnAdd;
    QPushButton *btnRemove;
    QListWidget *lw_Rect;
    QStackedWidget *sw_transform;
    QWidget *page;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_4;
    QSpinBox *sp_X;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_10;
    QSpinBox *sp_Y;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_11;
    QSpinBox *sp_Z;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QListWidget *lw_displobj_rect;
    QListWidget *lw_dis_rect;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_3;
    QListWidget *lw_displobj_cylin;
    QListWidget *lw_dis_cylin;

    void setupUi(QWidget *TaskFemConstraintTransform)
    {
        if (TaskFemConstraintTransform->objectName().isEmpty())
            TaskFemConstraintTransform->setObjectName(QString::fromUtf8("TaskFemConstraintTransform"));
        TaskFemConstraintTransform->resize(382, 491);
        verticalLayout_3 = new QVBoxLayout(TaskFemConstraintTransform);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        rb_rect = new QRadioButton(TaskFemConstraintTransform);
        rb_rect->setObjectName(QString::fromUtf8("rb_rect"));
        rb_rect->setAutoExclusive(true);

        horizontalLayout_2->addWidget(rb_rect);

        rb_cylin = new QRadioButton(TaskFemConstraintTransform);
        rb_cylin->setObjectName(QString::fromUtf8("rb_cylin"));
        rb_cylin->setAutoExclusive(true);

        horizontalLayout_2->addWidget(rb_cylin);


        verticalLayout_3->addLayout(horizontalLayout_2);

        lbl_info_2 = new QLabel(TaskFemConstraintTransform);
        lbl_info_2->setObjectName(QString::fromUtf8("lbl_info_2"));

        verticalLayout_3->addWidget(lbl_info_2);

        hLayout1_3 = new QHBoxLayout();
        hLayout1_3->setObjectName(QString::fromUtf8("hLayout1_3"));
        btnAdd = new QPushButton(TaskFemConstraintTransform);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));

        hLayout1_3->addWidget(btnAdd);

        btnRemove = new QPushButton(TaskFemConstraintTransform);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));

        hLayout1_3->addWidget(btnRemove);


        verticalLayout_3->addLayout(hLayout1_3);

        lw_Rect = new QListWidget(TaskFemConstraintTransform);
        lw_Rect->setObjectName(QString::fromUtf8("lw_Rect"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lw_Rect->sizePolicy().hasHeightForWidth());
        lw_Rect->setSizePolicy(sizePolicy);
        lw_Rect->setMaximumSize(QSize(16777215, 31));

        verticalLayout_3->addWidget(lw_Rect);

        sw_transform = new QStackedWidget(TaskFemConstraintTransform);
        sw_transform->setObjectName(QString::fromUtf8("sw_transform"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        verticalLayout = new QVBoxLayout(page);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_4 = new QLabel(page);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_7->addWidget(label_4);

        sp_X = new QSpinBox(page);
        sp_X->setObjectName(QString::fromUtf8("sp_X"));
        sp_X->setMinimum(-360);
        sp_X->setMaximum(360);

        horizontalLayout_7->addWidget(sp_X);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_10 = new QLabel(page);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_8->addWidget(label_10);

        sp_Y = new QSpinBox(page);
        sp_Y->setObjectName(QString::fromUtf8("sp_Y"));
        sp_Y->setMinimum(-360);
        sp_Y->setMaximum(360);

        horizontalLayout_8->addWidget(sp_Y);


        verticalLayout->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_11 = new QLabel(page);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_9->addWidget(label_11);

        sp_Z = new QSpinBox(page);
        sp_Z->setObjectName(QString::fromUtf8("sp_Z"));
        sp_Z->setMinimum(-360);
        sp_Z->setMaximum(360);

        horizontalLayout_9->addWidget(sp_Z);


        verticalLayout->addLayout(horizontalLayout_9);

        groupBox = new QGroupBox(page);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lw_displobj_rect = new QListWidget(groupBox);
        lw_displobj_rect->setObjectName(QString::fromUtf8("lw_displobj_rect"));

        horizontalLayout->addWidget(lw_displobj_rect);

        lw_dis_rect = new QListWidget(groupBox);
        lw_dis_rect->setObjectName(QString::fromUtf8("lw_dis_rect"));

        horizontalLayout->addWidget(lw_dis_rect);


        verticalLayout->addWidget(groupBox);

        sw_transform->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        verticalLayout_2 = new QVBoxLayout(page_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox_2 = new QGroupBox(page_2);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        horizontalLayout_3 = new QHBoxLayout(groupBox_2);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        lw_displobj_cylin = new QListWidget(groupBox_2);
        lw_displobj_cylin->setObjectName(QString::fromUtf8("lw_displobj_cylin"));

        horizontalLayout_3->addWidget(lw_displobj_cylin);

        lw_dis_cylin = new QListWidget(groupBox_2);
        lw_dis_cylin->setObjectName(QString::fromUtf8("lw_dis_cylin"));

        horizontalLayout_3->addWidget(lw_dis_cylin);


        verticalLayout_2->addWidget(groupBox_2);

        sw_transform->addWidget(page_2);

        verticalLayout_3->addWidget(sw_transform);


        retranslateUi(TaskFemConstraintTransform);

        sw_transform->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(TaskFemConstraintTransform);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintTransform)
    {
        TaskFemConstraintTransform->setWindowTitle(QApplication::translate("TaskFemConstraintTransform", "Form", nullptr));
        rb_rect->setText(QApplication::translate("TaskFemConstraintTransform", "Rectangular transform", nullptr));
        rb_cylin->setText(QApplication::translate("TaskFemConstraintTransform", "Cylindrical transform", nullptr));
        lbl_info_2->setText(QApplication::translate("TaskFemConstraintTransform", "Select a face, click Add or Remove", nullptr));
        btnAdd->setText(QApplication::translate("TaskFemConstraintTransform", "Add", nullptr));
        btnRemove->setText(QApplication::translate("TaskFemConstraintTransform", "Remove", nullptr));
        label_4->setText(QApplication::translate("TaskFemConstraintTransform", "Rotation about X-Axis", nullptr));
        label_10->setText(QApplication::translate("TaskFemConstraintTransform", "Rotation about Y-Axis", nullptr));
        label_11->setText(QApplication::translate("TaskFemConstraintTransform", "Rotation about Z-Axis", nullptr));
        groupBox->setTitle(QApplication::translate("TaskFemConstraintTransform", "Transformable surfaces", nullptr));
        groupBox_2->setTitle(QApplication::translate("TaskFemConstraintTransform", "Transformable surfaces", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintTransform: public Ui_TaskFemConstraintTransform {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTTRANSFORM_H
