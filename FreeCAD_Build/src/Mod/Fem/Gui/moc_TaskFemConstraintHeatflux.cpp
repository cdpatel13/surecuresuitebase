/****************************************************************************
** Meta object code from reading C++ file 'TaskFemConstraintHeatflux.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Fem/Gui/TaskFemConstraintHeatflux.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskFemConstraintHeatflux.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_FemGui__TaskFemConstraintHeatflux_t {
    QByteArrayData data[14];
    char stringdata0[195];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskFemConstraintHeatflux_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskFemConstraintHeatflux_t qt_meta_stringdata_FemGui__TaskFemConstraintHeatflux = {
    {
QT_MOC_LITERAL(0, 0, 33), // "FemGui::TaskFemConstraintHeat..."
QT_MOC_LITERAL(1, 34, 18), // "onReferenceDeleted"
QT_MOC_LITERAL(2, 53, 0), // ""
QT_MOC_LITERAL(3, 54, 20), // "onAmbientTempChanged"
QT_MOC_LITERAL(4, 75, 3), // "val"
QT_MOC_LITERAL(5, 79, 17), // "onFilmCoefChanged"
QT_MOC_LITERAL(6, 97, 17), // "onHeatFluxChanged"
QT_MOC_LITERAL(7, 115, 4), // "Conv"
QT_MOC_LITERAL(8, 120, 4), // "Flux"
QT_MOC_LITERAL(9, 125, 14), // "addToSelection"
QT_MOC_LITERAL(10, 140, 19), // "removeFromSelection"
QT_MOC_LITERAL(11, 160, 12), // "setSelection"
QT_MOC_LITERAL(12, 173, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(13, 190, 4) // "item"

    },
    "FemGui::TaskFemConstraintHeatflux\0"
    "onReferenceDeleted\0\0onAmbientTempChanged\0"
    "val\0onFilmCoefChanged\0onHeatFluxChanged\0"
    "Conv\0Flux\0addToSelection\0removeFromSelection\0"
    "setSelection\0QListWidgetItem*\0item"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskFemConstraintHeatflux[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    1,   60,    2, 0x08 /* Private */,
       5,    1,   63,    2, 0x08 /* Private */,
       6,    1,   66,    2, 0x08 /* Private */,
       7,    0,   69,    2, 0x08 /* Private */,
       8,    0,   70,    2, 0x08 /* Private */,
       9,    0,   71,    2, 0x08 /* Private */,
      10,    0,   72,    2, 0x08 /* Private */,
      11,    1,   73,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    4,
    QMetaType::Void, QMetaType::Double,    4,
    QMetaType::Void, QMetaType::Double,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 12,   13,

       0        // eod
};

void FemGui::TaskFemConstraintHeatflux::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskFemConstraintHeatflux *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onReferenceDeleted(); break;
        case 1: _t->onAmbientTempChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->onFilmCoefChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->onHeatFluxChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->Conv(); break;
        case 5: _t->Flux(); break;
        case 6: _t->addToSelection(); break;
        case 7: _t->removeFromSelection(); break;
        case 8: _t->setSelection((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskFemConstraintHeatflux::staticMetaObject = { {
    &TaskFemConstraint::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskFemConstraintHeatflux.data,
    qt_meta_data_FemGui__TaskFemConstraintHeatflux,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskFemConstraintHeatflux::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskFemConstraintHeatflux::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskFemConstraintHeatflux.stringdata0))
        return static_cast<void*>(this);
    return TaskFemConstraint::qt_metacast(_clname);
}

int FemGui::TaskFemConstraintHeatflux::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskFemConstraint::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskDlgFemConstraintHeatflux_t {
    QByteArrayData data[1];
    char stringdata0[37];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskDlgFemConstraintHeatflux_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskDlgFemConstraintHeatflux_t qt_meta_stringdata_FemGui__TaskDlgFemConstraintHeatflux = {
    {
QT_MOC_LITERAL(0, 0, 36) // "FemGui::TaskDlgFemConstraintH..."

    },
    "FemGui::TaskDlgFemConstraintHeatflux"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskDlgFemConstraintHeatflux[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void FemGui::TaskDlgFemConstraintHeatflux::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskDlgFemConstraintHeatflux::staticMetaObject = { {
    &TaskDlgFemConstraint::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskDlgFemConstraintHeatflux.data,
    qt_meta_data_FemGui__TaskDlgFemConstraintHeatflux,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskDlgFemConstraintHeatflux::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskDlgFemConstraintHeatflux::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskDlgFemConstraintHeatflux.stringdata0))
        return static_cast<void*>(this);
    return TaskDlgFemConstraint::qt_metacast(_clname);
}

int FemGui::TaskDlgFemConstraintHeatflux::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskDlgFemConstraint::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
