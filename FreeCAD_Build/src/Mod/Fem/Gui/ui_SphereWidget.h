/********************************************************************************
** Form generated from reading UI file 'SphereWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPHEREWIDGET_H
#define UI_SPHEREWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SphereWidget
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QDoubleSpinBox *radius;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QDoubleSpinBox *centerX;
    QDoubleSpinBox *centerY;
    QDoubleSpinBox *centerZ;

    void setupUi(QWidget *SphereWidget)
    {
        if (SphereWidget->objectName().isEmpty())
            SphereWidget->setObjectName(QString::fromUtf8("SphereWidget"));
        SphereWidget->resize(346, 84);
        formLayout = new QFormLayout(SphereWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setVerticalSpacing(2);
        formLayout->setContentsMargins(6, -1, -1, 0);
        label = new QLabel(SphereWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(2);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        radius = new QDoubleSpinBox(SphereWidget);
        radius->setObjectName(QString::fromUtf8("radius"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(radius->sizePolicy().hasHeightForWidth());
        radius->setSizePolicy(sizePolicy);
        radius->setMinimumSize(QSize(70, 0));
        radius->setKeyboardTracking(false);
        radius->setMinimum(-999999999.000000000000000);
        radius->setMaximum(999999999.000000000000000);

        horizontalLayout_2->addWidget(radius);


        formLayout->setLayout(0, QFormLayout::FieldRole, horizontalLayout_2);

        label_2 = new QLabel(SphereWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        centerX = new QDoubleSpinBox(SphereWidget);
        centerX->setObjectName(QString::fromUtf8("centerX"));
        sizePolicy.setHeightForWidth(centerX->sizePolicy().hasHeightForWidth());
        centerX->setSizePolicy(sizePolicy);
        centerX->setMinimumSize(QSize(70, 0));
        centerX->setKeyboardTracking(false);
        centerX->setMinimum(-999999999.000000000000000);
        centerX->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(centerX);

        centerY = new QDoubleSpinBox(SphereWidget);
        centerY->setObjectName(QString::fromUtf8("centerY"));
        sizePolicy.setHeightForWidth(centerY->sizePolicy().hasHeightForWidth());
        centerY->setSizePolicy(sizePolicy);
        centerY->setMinimumSize(QSize(70, 0));
        centerY->setKeyboardTracking(false);
        centerY->setMinimum(-999999999.000000000000000);
        centerY->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(centerY);

        centerZ = new QDoubleSpinBox(SphereWidget);
        centerZ->setObjectName(QString::fromUtf8("centerZ"));
        sizePolicy.setHeightForWidth(centerZ->sizePolicy().hasHeightForWidth());
        centerZ->setSizePolicy(sizePolicy);
        centerZ->setMinimumSize(QSize(70, 0));
        centerZ->setKeyboardTracking(false);
        centerZ->setMinimum(-999999999.000000000000000);
        centerZ->setMaximum(999999999.000000000000000);

        horizontalLayout->addWidget(centerZ);


        formLayout->setLayout(1, QFormLayout::FieldRole, horizontalLayout);


        retranslateUi(SphereWidget);

        QMetaObject::connectSlotsByName(SphereWidget);
    } // setupUi

    void retranslateUi(QWidget *SphereWidget)
    {
        SphereWidget->setWindowTitle(QApplication::translate("SphereWidget", "Form", nullptr));
        label->setText(QApplication::translate("SphereWidget", "Radius", nullptr));
        label_2->setText(QApplication::translate("SphereWidget", "Center", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SphereWidget: public Ui_SphereWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPHEREWIDGET_H
