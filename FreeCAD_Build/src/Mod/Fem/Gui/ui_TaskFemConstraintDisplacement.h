/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintDisplacement.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTDISPLACEMENT_H
#define UI_TASKFEMCONSTRAINTDISPLACEMENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintDisplacement
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *lbl_info;
    QHBoxLayout *hLayout1;
    QPushButton *btnAdd;
    QPushButton *btnRemove;
    QListWidget *lw_references;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_6;
    QLabel *dispx;
    QVBoxLayout *verticalLayout_8;
    QCheckBox *dispxfree;
    QCheckBox *dispxfix;
    QDoubleSpinBox *spinxDisplacement;
    QHBoxLayout *horizontalLayout_5;
    QLabel *dispy;
    QVBoxLayout *verticalLayout_7;
    QCheckBox *dispyfree;
    QCheckBox *dispyfix;
    QDoubleSpinBox *spinyDisplacement;
    QHBoxLayout *horizontalLayout_4;
    QLabel *dispz;
    QVBoxLayout *verticalLayout_6;
    QCheckBox *dispzfree;
    QCheckBox *dispzfix;
    QDoubleSpinBox *spinzDisplacement;
    QLabel *label;
    QHBoxLayout *horizontalLayout_3;
    QLabel *rotx;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *rotxfree;
    QCheckBox *rotxfix;
    QDoubleSpinBox *rotxv;
    QHBoxLayout *horizontalLayout_2;
    QLabel *roty;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *rotyfree;
    QCheckBox *rotyfix;
    QDoubleSpinBox *rotyv;
    QHBoxLayout *horizontalLayout;
    QLabel *rotz;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *rotzfree;
    QCheckBox *rotzfix;
    QDoubleSpinBox *rotzv;

    void setupUi(QWidget *TaskFemConstraintDisplacement)
    {
        if (TaskFemConstraintDisplacement->objectName().isEmpty())
            TaskFemConstraintDisplacement->setObjectName(QString::fromUtf8("TaskFemConstraintDisplacement"));
        TaskFemConstraintDisplacement->setEnabled(true);
        TaskFemConstraintDisplacement->resize(400, 800);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(3);
        sizePolicy.setVerticalStretch(80);
        sizePolicy.setHeightForWidth(TaskFemConstraintDisplacement->sizePolicy().hasHeightForWidth());
        TaskFemConstraintDisplacement->setSizePolicy(sizePolicy);
        TaskFemConstraintDisplacement->setMinimumSize(QSize(400, 800));
        TaskFemConstraintDisplacement->setBaseSize(QSize(400, 800));
        verticalLayoutWidget = new QWidget(TaskFemConstraintDisplacement);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(40, 0, 311, 743));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        lbl_info = new QLabel(verticalLayoutWidget);
        lbl_info->setObjectName(QString::fromUtf8("lbl_info"));

        verticalLayout->addWidget(lbl_info);

        hLayout1 = new QHBoxLayout();
        hLayout1->setObjectName(QString::fromUtf8("hLayout1"));
        btnAdd = new QPushButton(verticalLayoutWidget);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));

        hLayout1->addWidget(btnAdd);

        btnRemove = new QPushButton(verticalLayoutWidget);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));

        hLayout1->addWidget(btnRemove);


        verticalLayout->addLayout(hLayout1);

        lw_references = new QListWidget(verticalLayoutWidget);
        lw_references->setObjectName(QString::fromUtf8("lw_references"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lw_references->sizePolicy().hasHeightForWidth());
        lw_references->setSizePolicy(sizePolicy1);
        lw_references->setMinimumSize(QSize(0, 100));

        verticalLayout->addWidget(lw_references);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        dispx = new QLabel(verticalLayoutWidget);
        dispx->setObjectName(QString::fromUtf8("dispx"));

        horizontalLayout_6->addWidget(dispx);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        dispxfree = new QCheckBox(verticalLayoutWidget);
        dispxfree->setObjectName(QString::fromUtf8("dispxfree"));

        verticalLayout_8->addWidget(dispxfree);

        dispxfix = new QCheckBox(verticalLayoutWidget);
        dispxfix->setObjectName(QString::fromUtf8("dispxfix"));

        verticalLayout_8->addWidget(dispxfix);

        spinxDisplacement = new QDoubleSpinBox(verticalLayoutWidget);
        spinxDisplacement->setObjectName(QString::fromUtf8("spinxDisplacement"));
        spinxDisplacement->setMinimum(-99999.000000000000000);
        spinxDisplacement->setMaximum(99999.000000000000000);
        spinxDisplacement->setValue(0.000000000000000);

        verticalLayout_8->addWidget(spinxDisplacement);


        horizontalLayout_6->addLayout(verticalLayout_8);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        dispy = new QLabel(verticalLayoutWidget);
        dispy->setObjectName(QString::fromUtf8("dispy"));

        horizontalLayout_5->addWidget(dispy);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        dispyfree = new QCheckBox(verticalLayoutWidget);
        dispyfree->setObjectName(QString::fromUtf8("dispyfree"));

        verticalLayout_7->addWidget(dispyfree);

        dispyfix = new QCheckBox(verticalLayoutWidget);
        dispyfix->setObjectName(QString::fromUtf8("dispyfix"));

        verticalLayout_7->addWidget(dispyfix);

        spinyDisplacement = new QDoubleSpinBox(verticalLayoutWidget);
        spinyDisplacement->setObjectName(QString::fromUtf8("spinyDisplacement"));
        spinyDisplacement->setMinimum(-99999.000000000000000);
        spinyDisplacement->setMaximum(99999.000000000000000);
        spinyDisplacement->setValue(0.000000000000000);

        verticalLayout_7->addWidget(spinyDisplacement);


        horizontalLayout_5->addLayout(verticalLayout_7);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        dispz = new QLabel(verticalLayoutWidget);
        dispz->setObjectName(QString::fromUtf8("dispz"));

        horizontalLayout_4->addWidget(dispz);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        dispzfree = new QCheckBox(verticalLayoutWidget);
        dispzfree->setObjectName(QString::fromUtf8("dispzfree"));

        verticalLayout_6->addWidget(dispzfree);

        dispzfix = new QCheckBox(verticalLayoutWidget);
        dispzfix->setObjectName(QString::fromUtf8("dispzfix"));

        verticalLayout_6->addWidget(dispzfix);

        spinzDisplacement = new QDoubleSpinBox(verticalLayoutWidget);
        spinzDisplacement->setObjectName(QString::fromUtf8("spinzDisplacement"));
        spinzDisplacement->setMinimum(-99999.000000000000000);
        spinzDisplacement->setMaximum(99999.000000000000000);
        spinzDisplacement->setValue(0.000000000000000);

        verticalLayout_6->addWidget(spinzDisplacement);


        horizontalLayout_4->addLayout(verticalLayout_6);


        verticalLayout_2->addLayout(horizontalLayout_4);

        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMaximumSize(QSize(400, 16777215));
        label->setScaledContents(false);
        label->setWordWrap(true);

        verticalLayout_2->addWidget(label);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        rotx = new QLabel(verticalLayoutWidget);
        rotx->setObjectName(QString::fromUtf8("rotx"));

        horizontalLayout_3->addWidget(rotx);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        rotxfree = new QCheckBox(verticalLayoutWidget);
        rotxfree->setObjectName(QString::fromUtf8("rotxfree"));

        verticalLayout_5->addWidget(rotxfree);

        rotxfix = new QCheckBox(verticalLayoutWidget);
        rotxfix->setObjectName(QString::fromUtf8("rotxfix"));

        verticalLayout_5->addWidget(rotxfix);

        rotxv = new QDoubleSpinBox(verticalLayoutWidget);
        rotxv->setObjectName(QString::fromUtf8("rotxv"));
        rotxv->setMinimum(-99999.000000000000000);
        rotxv->setMaximum(99999.000000000000000);

        verticalLayout_5->addWidget(rotxv);


        horizontalLayout_3->addLayout(verticalLayout_5);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        roty = new QLabel(verticalLayoutWidget);
        roty->setObjectName(QString::fromUtf8("roty"));

        horizontalLayout_2->addWidget(roty);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        rotyfree = new QCheckBox(verticalLayoutWidget);
        rotyfree->setObjectName(QString::fromUtf8("rotyfree"));

        verticalLayout_4->addWidget(rotyfree);

        rotyfix = new QCheckBox(verticalLayoutWidget);
        rotyfix->setObjectName(QString::fromUtf8("rotyfix"));

        verticalLayout_4->addWidget(rotyfix);

        rotyv = new QDoubleSpinBox(verticalLayoutWidget);
        rotyv->setObjectName(QString::fromUtf8("rotyv"));
        rotyv->setMinimum(-99999.000000000000000);
        rotyv->setMaximum(99999.000000000000000);

        verticalLayout_4->addWidget(rotyv);


        horizontalLayout_2->addLayout(verticalLayout_4);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        rotz = new QLabel(verticalLayoutWidget);
        rotz->setObjectName(QString::fromUtf8("rotz"));

        horizontalLayout->addWidget(rotz);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        rotzfree = new QCheckBox(verticalLayoutWidget);
        rotzfree->setObjectName(QString::fromUtf8("rotzfree"));
        rotzfree->setEnabled(true);

        verticalLayout_3->addWidget(rotzfree);

        rotzfix = new QCheckBox(verticalLayoutWidget);
        rotzfix->setObjectName(QString::fromUtf8("rotzfix"));
        rotzfix->setEnabled(true);

        verticalLayout_3->addWidget(rotzfix);

        rotzv = new QDoubleSpinBox(verticalLayoutWidget);
        rotzv->setObjectName(QString::fromUtf8("rotzv"));
        rotzv->setEnabled(true);
        rotzv->setMinimum(-99999.000000000000000);
        rotzv->setMaximum(99999.000000000000000);

        verticalLayout_3->addWidget(rotzv);


        horizontalLayout->addLayout(verticalLayout_3);


        verticalLayout_2->addLayout(horizontalLayout);


        verticalLayout->addLayout(verticalLayout_2);


        retranslateUi(TaskFemConstraintDisplacement);

        QMetaObject::connectSlotsByName(TaskFemConstraintDisplacement);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintDisplacement)
    {
        TaskFemConstraintDisplacement->setWindowTitle(QApplication::translate("TaskFemConstraintDisplacement", "Prescribed Displacement", nullptr));
        lbl_info->setText(QApplication::translate("TaskFemConstraintDisplacement", "Select multiple face(s), click Add or Remove", nullptr));
        btnAdd->setText(QApplication::translate("TaskFemConstraintDisplacement", "Add", nullptr));
        btnRemove->setText(QApplication::translate("TaskFemConstraintDisplacement", "Remove", nullptr));
        dispx->setText(QApplication::translate("TaskFemConstraintDisplacement", "Displacement x", nullptr));
        dispxfree->setText(QApplication::translate("TaskFemConstraintDisplacement", "Free", nullptr));
        dispxfix->setText(QApplication::translate("TaskFemConstraintDisplacement", "Fixed", nullptr));
        dispy->setText(QApplication::translate("TaskFemConstraintDisplacement", "Displacement y", nullptr));
        dispyfree->setText(QApplication::translate("TaskFemConstraintDisplacement", "Free", nullptr));
        dispyfix->setText(QApplication::translate("TaskFemConstraintDisplacement", "Fixed", nullptr));
        dispz->setText(QApplication::translate("TaskFemConstraintDisplacement", "Displacement z", nullptr));
        dispzfree->setText(QApplication::translate("TaskFemConstraintDisplacement", "Free", nullptr));
        dispzfix->setText(QApplication::translate("TaskFemConstraintDisplacement", "Fixed", nullptr));
        label->setText(QApplication::translate("TaskFemConstraintDisplacement", "Rotations are only valid for Beam and Shell elements.", nullptr));
        rotx->setText(QApplication::translate("TaskFemConstraintDisplacement", "Rotation x", nullptr));
        rotxfree->setText(QApplication::translate("TaskFemConstraintDisplacement", "Free", nullptr));
        rotxfix->setText(QApplication::translate("TaskFemConstraintDisplacement", "Fixed", nullptr));
        roty->setText(QApplication::translate("TaskFemConstraintDisplacement", "Rotation y", nullptr));
        rotyfree->setText(QApplication::translate("TaskFemConstraintDisplacement", "Free", nullptr));
        rotyfix->setText(QApplication::translate("TaskFemConstraintDisplacement", "Fixed", nullptr));
        rotz->setText(QApplication::translate("TaskFemConstraintDisplacement", "Rotation z", nullptr));
        rotzfree->setText(QApplication::translate("TaskFemConstraintDisplacement", "Free", nullptr));
        rotzfix->setText(QApplication::translate("TaskFemConstraintDisplacement", "Fixed", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintDisplacement: public Ui_TaskFemConstraintDisplacement {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTDISPLACEMENT_H
