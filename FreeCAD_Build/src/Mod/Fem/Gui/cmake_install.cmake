# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/Fem/Resources/icons" TYPE FILE FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/icons/FemWorkbench.svg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/Resources/ui" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/ElectrostaticPotential.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/ElementFluid1D.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/ElementGeometry1D.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/ElementGeometry2D.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/ElementRotation1D.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/FlowVelocity.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/InitialFlowVelocity.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/Material.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/MaterialReinforcement.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/MeshBoundaryLayer.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/MeshGmsh.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/MeshGroup.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/MeshGroupXDMFExport.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/MeshRegion.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/ResultShow.ui"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Gui/Resources/ui/SolverCalculix.ui"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Fem/Gui/Debug/FemGui_d.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Fem/Gui/Release/FemGui.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Fem/Gui/MinSizeRel/FemGui.lib")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY OPTIONAL FILES "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Fem/Gui/RelWithDebInfo/FemGui.lib")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/Mod/Fem/FemGui_d.pyd")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/Mod/Fem/FemGui.pyd")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/Mod/Fem/MinSizeRel/FemGui.pyd")
  elseif("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/Mod/Fem/RelWithDebInfo/FemGui.pyd")
  endif()
endif()

