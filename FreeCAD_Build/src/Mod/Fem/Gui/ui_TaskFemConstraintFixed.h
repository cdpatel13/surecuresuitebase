/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintFixed.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTFIXED_H
#define UI_TASKFEMCONSTRAINTFIXED_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintFixed
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lbl_info;
    QHBoxLayout *hLayout1;
    QPushButton *btnAdd;
    QPushButton *btnRemove;
    QListWidget *lw_references;

    void setupUi(QWidget *TaskFemConstraintFixed)
    {
        if (TaskFemConstraintFixed->objectName().isEmpty())
            TaskFemConstraintFixed->setObjectName(QString::fromUtf8("TaskFemConstraintFixed"));
        TaskFemConstraintFixed->resize(309, 207);
        verticalLayout = new QVBoxLayout(TaskFemConstraintFixed);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbl_info = new QLabel(TaskFemConstraintFixed);
        lbl_info->setObjectName(QString::fromUtf8("lbl_info"));

        verticalLayout->addWidget(lbl_info);

        hLayout1 = new QHBoxLayout();
        hLayout1->setObjectName(QString::fromUtf8("hLayout1"));
        btnAdd = new QPushButton(TaskFemConstraintFixed);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));

        hLayout1->addWidget(btnAdd);

        btnRemove = new QPushButton(TaskFemConstraintFixed);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));

        hLayout1->addWidget(btnRemove);


        verticalLayout->addLayout(hLayout1);

        lw_references = new QListWidget(TaskFemConstraintFixed);
        lw_references->setObjectName(QString::fromUtf8("lw_references"));

        verticalLayout->addWidget(lw_references);


        retranslateUi(TaskFemConstraintFixed);

        QMetaObject::connectSlotsByName(TaskFemConstraintFixed);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintFixed)
    {
        TaskFemConstraintFixed->setWindowTitle(QApplication::translate("TaskFemConstraintFixed", "Form", nullptr));
        lbl_info->setText(QApplication::translate("TaskFemConstraintFixed", "Select multiple face(s), click Add or Remove", nullptr));
        btnAdd->setText(QApplication::translate("TaskFemConstraintFixed", "Add", nullptr));
        btnRemove->setText(QApplication::translate("TaskFemConstraintFixed", "Remove", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintFixed: public Ui_TaskFemConstraintFixed {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTFIXED_H
