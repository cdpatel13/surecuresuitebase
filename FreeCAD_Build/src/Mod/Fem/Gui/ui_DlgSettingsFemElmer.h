/********************************************************************************
** Form generated from reading UI file 'DlgSettingsFemElmer.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGSFEMELMER_H
#define UI_DLGSETTINGSFEMELMER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/FileDialog.h"
#include "Gui/PrefWidgets.h"

namespace FemGui {

class Ui_DlgSettingsFemElmerImp
{
public:
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *gb_gmsh_param;
    QHBoxLayout *horizontalLayout_2;
    QGridLayout *gl_01;
    Gui::PrefFileChooser *fc_grid_binary_path;
    QLabel *l_elmer_binary_path;
    QLabel *l_grid_binary_path;
    QLabel *l_grid_binary_std;
    Gui::PrefCheckBox *cb_grid_binary_std;
    QLabel *l_elmer_binary_std;
    Gui::PrefCheckBox *cb_elmer_binary_std;
    Gui::PrefFileChooser *fc_elmer_binary_path;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *FemGui__DlgSettingsFemElmerImp)
    {
        if (FemGui__DlgSettingsFemElmerImp->objectName().isEmpty())
            FemGui__DlgSettingsFemElmerImp->setObjectName(QString::fromUtf8("FemGui__DlgSettingsFemElmerImp"));
        FemGui__DlgSettingsFemElmerImp->resize(390, 451);
        verticalLayout = new QVBoxLayout(FemGui__DlgSettingsFemElmerImp);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        gb_gmsh_param = new QGroupBox(FemGui__DlgSettingsFemElmerImp);
        gb_gmsh_param->setObjectName(QString::fromUtf8("gb_gmsh_param"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(gb_gmsh_param->sizePolicy().hasHeightForWidth());
        gb_gmsh_param->setSizePolicy(sizePolicy);
        gb_gmsh_param->setLayoutDirection(Qt::LeftToRight);
        gb_gmsh_param->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        horizontalLayout_2 = new QHBoxLayout(gb_gmsh_param);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetNoConstraint);
        gl_01 = new QGridLayout();
        gl_01->setSpacing(6);
        gl_01->setObjectName(QString::fromUtf8("gl_01"));
        fc_grid_binary_path = new Gui::PrefFileChooser(gb_gmsh_param);
        fc_grid_binary_path->setObjectName(QString::fromUtf8("fc_grid_binary_path"));
        fc_grid_binary_path->setEnabled(false);
        sizePolicy.setHeightForWidth(fc_grid_binary_path->sizePolicy().hasHeightForWidth());
        fc_grid_binary_path->setSizePolicy(sizePolicy);
        fc_grid_binary_path->setMinimumSize(QSize(0, 0));
        fc_grid_binary_path->setSizeIncrement(QSize(0, 0));
        fc_grid_binary_path->setBaseSize(QSize(0, 0));
        fc_grid_binary_path->setProperty("prefEntry", QVariant(QByteArray("gridBinaryPath")));
        fc_grid_binary_path->setProperty("prefPath", QVariant(QByteArray("Mod/Fem/Elmer")));

        gl_01->addWidget(fc_grid_binary_path, 1, 1, 1, 1);

        l_elmer_binary_path = new QLabel(gb_gmsh_param);
        l_elmer_binary_path->setObjectName(QString::fromUtf8("l_elmer_binary_path"));
        l_elmer_binary_path->setEnabled(false);
        l_elmer_binary_path->setMinimumSize(QSize(100, 0));

        gl_01->addWidget(l_elmer_binary_path, 4, 0, 1, 1);

        l_grid_binary_path = new QLabel(gb_gmsh_param);
        l_grid_binary_path->setObjectName(QString::fromUtf8("l_grid_binary_path"));
        l_grid_binary_path->setEnabled(false);
        l_grid_binary_path->setMinimumSize(QSize(100, 0));

        gl_01->addWidget(l_grid_binary_path, 1, 0, 1, 1);

        l_grid_binary_std = new QLabel(gb_gmsh_param);
        l_grid_binary_std->setObjectName(QString::fromUtf8("l_grid_binary_std"));

        gl_01->addWidget(l_grid_binary_std, 0, 0, 1, 1);

        cb_grid_binary_std = new Gui::PrefCheckBox(gb_gmsh_param);
        cb_grid_binary_std->setObjectName(QString::fromUtf8("cb_grid_binary_std"));
        cb_grid_binary_std->setChecked(true);
        cb_grid_binary_std->setProperty("prefEntry", QVariant(QByteArray("UseStandardGridLocation")));
        cb_grid_binary_std->setProperty("prefPath", QVariant(QByteArray("Mod/Fem/Elmer")));

        gl_01->addWidget(cb_grid_binary_std, 0, 1, 1, 1);

        l_elmer_binary_std = new QLabel(gb_gmsh_param);
        l_elmer_binary_std->setObjectName(QString::fromUtf8("l_elmer_binary_std"));

        gl_01->addWidget(l_elmer_binary_std, 3, 0, 1, 1);

        cb_elmer_binary_std = new Gui::PrefCheckBox(gb_gmsh_param);
        cb_elmer_binary_std->setObjectName(QString::fromUtf8("cb_elmer_binary_std"));
        cb_elmer_binary_std->setChecked(true);
        cb_elmer_binary_std->setProperty("prefEntry", QVariant(QByteArray("UseStandardElmerLocation")));
        cb_elmer_binary_std->setProperty("prefPath", QVariant(QByteArray("Mod/Fem/Elmer")));

        gl_01->addWidget(cb_elmer_binary_std, 3, 1, 1, 1);

        fc_elmer_binary_path = new Gui::PrefFileChooser(gb_gmsh_param);
        fc_elmer_binary_path->setObjectName(QString::fromUtf8("fc_elmer_binary_path"));
        fc_elmer_binary_path->setEnabled(false);
        sizePolicy.setHeightForWidth(fc_elmer_binary_path->sizePolicy().hasHeightForWidth());
        fc_elmer_binary_path->setSizePolicy(sizePolicy);
        fc_elmer_binary_path->setMinimumSize(QSize(0, 0));
        fc_elmer_binary_path->setSizeIncrement(QSize(0, 0));
        fc_elmer_binary_path->setBaseSize(QSize(0, 0));
        fc_elmer_binary_path->setProperty("prefEntry", QVariant(QByteArray("elmerBinaryPath")));
        fc_elmer_binary_path->setProperty("prefPath", QVariant(QByteArray("Mod/Fem/Elmer")));

        gl_01->addWidget(fc_elmer_binary_path, 4, 1, 1, 1);


        horizontalLayout_2->addLayout(gl_01);


        verticalLayout_2->addWidget(gb_gmsh_param);


        verticalLayout->addLayout(verticalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(FemGui__DlgSettingsFemElmerImp);
        QObject::connect(cb_elmer_binary_std, SIGNAL(toggled(bool)), l_elmer_binary_path, SLOT(setEnabled(bool)));
        QObject::connect(cb_elmer_binary_std, SIGNAL(toggled(bool)), fc_elmer_binary_path, SLOT(setEnabled(bool)));
        QObject::connect(cb_grid_binary_std, SIGNAL(toggled(bool)), l_grid_binary_path, SLOT(setEnabled(bool)));
        QObject::connect(cb_grid_binary_std, SIGNAL(toggled(bool)), fc_grid_binary_path, SLOT(setEnabled(bool)));
        QObject::connect(cb_grid_binary_std, SIGNAL(toggled(bool)), l_grid_binary_path, SLOT(setDisabled(bool)));
        QObject::connect(cb_grid_binary_std, SIGNAL(toggled(bool)), fc_grid_binary_path, SLOT(setDisabled(bool)));
        QObject::connect(cb_elmer_binary_std, SIGNAL(toggled(bool)), l_elmer_binary_path, SLOT(setDisabled(bool)));
        QObject::connect(cb_elmer_binary_std, SIGNAL(toggled(bool)), fc_elmer_binary_path, SLOT(setDisabled(bool)));

        QMetaObject::connectSlotsByName(FemGui__DlgSettingsFemElmerImp);
    } // setupUi

    void retranslateUi(QWidget *FemGui__DlgSettingsFemElmerImp)
    {
        FemGui__DlgSettingsFemElmerImp->setWindowTitle(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "Elmer", nullptr));
        gb_gmsh_param->setTitle(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "Elmer binaries", nullptr));
#ifndef QT_NO_TOOLTIP
        fc_grid_binary_path->setToolTip(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "Leave blank to use default ElmerGrid binary file", nullptr));
#endif // QT_NO_TOOLTIP
        l_elmer_binary_path->setText(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "ElmerSolver binary path", nullptr));
        l_grid_binary_path->setText(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "ElmerGrid binary path", nullptr));
        l_grid_binary_std->setText(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "ElmerGrid:", nullptr));
        cb_grid_binary_std->setText(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "Search in known binary directories", nullptr));
        l_elmer_binary_std->setText(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "ElmerSolver:", nullptr));
        cb_elmer_binary_std->setText(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "Search in known binary directories", nullptr));
#ifndef QT_NO_TOOLTIP
        fc_elmer_binary_path->setToolTip(QApplication::translate("FemGui::DlgSettingsFemElmerImp", "Leave blank to use default Elmer elmer binary file", nullptr));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

} // namespace FemGui

namespace FemGui {
namespace Ui {
    class DlgSettingsFemElmerImp: public Ui_DlgSettingsFemElmerImp {};
} // namespace Ui
} // namespace FemGui

#endif // UI_DLGSETTINGSFEMELMER_H
