/****************************************************************************
** Meta object code from reading C++ file 'TaskFemConstraintDisplacement.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Fem/Gui/TaskFemConstraintDisplacement.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskFemConstraintDisplacement.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_FemGui__TaskFemConstraintDisplacement_t {
    QByteArrayData data[26];
    char stringdata0[260];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskFemConstraintDisplacement_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskFemConstraintDisplacement_t qt_meta_stringdata_FemGui__TaskFemConstraintDisplacement = {
    {
QT_MOC_LITERAL(0, 0, 37), // "FemGui::TaskFemConstraintDisp..."
QT_MOC_LITERAL(1, 38, 18), // "onReferenceDeleted"
QT_MOC_LITERAL(2, 57, 0), // ""
QT_MOC_LITERAL(3, 58, 9), // "x_changed"
QT_MOC_LITERAL(4, 68, 9), // "y_changed"
QT_MOC_LITERAL(5, 78, 9), // "z_changed"
QT_MOC_LITERAL(6, 88, 5), // "x_rot"
QT_MOC_LITERAL(7, 94, 5), // "y_rot"
QT_MOC_LITERAL(8, 100, 5), // "z_rot"
QT_MOC_LITERAL(9, 106, 4), // "fixx"
QT_MOC_LITERAL(10, 111, 5), // "freex"
QT_MOC_LITERAL(11, 117, 4), // "fixy"
QT_MOC_LITERAL(12, 122, 5), // "freey"
QT_MOC_LITERAL(13, 128, 4), // "fixz"
QT_MOC_LITERAL(14, 133, 5), // "freez"
QT_MOC_LITERAL(15, 139, 7), // "rotfixx"
QT_MOC_LITERAL(16, 147, 8), // "rotfreex"
QT_MOC_LITERAL(17, 156, 7), // "rotfixy"
QT_MOC_LITERAL(18, 164, 8), // "rotfreey"
QT_MOC_LITERAL(19, 173, 7), // "rotfixz"
QT_MOC_LITERAL(20, 181, 8), // "rotfreez"
QT_MOC_LITERAL(21, 190, 14), // "addToSelection"
QT_MOC_LITERAL(22, 205, 19), // "removeFromSelection"
QT_MOC_LITERAL(23, 225, 12), // "setSelection"
QT_MOC_LITERAL(24, 238, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(25, 255, 4) // "item"

    },
    "FemGui::TaskFemConstraintDisplacement\0"
    "onReferenceDeleted\0\0x_changed\0y_changed\0"
    "z_changed\0x_rot\0y_rot\0z_rot\0fixx\0freex\0"
    "fixy\0freey\0fixz\0freez\0rotfixx\0rotfreex\0"
    "rotfixy\0rotfreey\0rotfixz\0rotfreez\0"
    "addToSelection\0removeFromSelection\0"
    "setSelection\0QListWidgetItem*\0item"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskFemConstraintDisplacement[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x08 /* Private */,
       3,    1,  125,    2, 0x08 /* Private */,
       4,    1,  128,    2, 0x08 /* Private */,
       5,    1,  131,    2, 0x08 /* Private */,
       6,    1,  134,    2, 0x08 /* Private */,
       7,    1,  137,    2, 0x08 /* Private */,
       8,    1,  140,    2, 0x08 /* Private */,
       9,    1,  143,    2, 0x08 /* Private */,
      10,    1,  146,    2, 0x08 /* Private */,
      11,    1,  149,    2, 0x08 /* Private */,
      12,    1,  152,    2, 0x08 /* Private */,
      13,    1,  155,    2, 0x08 /* Private */,
      14,    1,  158,    2, 0x08 /* Private */,
      15,    1,  161,    2, 0x08 /* Private */,
      16,    1,  164,    2, 0x08 /* Private */,
      17,    1,  167,    2, 0x08 /* Private */,
      18,    1,  170,    2, 0x08 /* Private */,
      19,    1,  173,    2, 0x08 /* Private */,
      20,    1,  176,    2, 0x08 /* Private */,
      21,    0,  179,    2, 0x08 /* Private */,
      22,    0,  180,    2, 0x08 /* Private */,
      23,    1,  181,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 24,   25,

       0        // eod
};

void FemGui::TaskFemConstraintDisplacement::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskFemConstraintDisplacement *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onReferenceDeleted(); break;
        case 1: _t->x_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->y_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->z_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->x_rot((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->y_rot((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->z_rot((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->fixx((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->freex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->fixy((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->freey((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->fixz((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->freez((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->rotfixx((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->rotfreex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->rotfixy((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->rotfreey((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->rotfixz((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->rotfreez((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->addToSelection(); break;
        case 20: _t->removeFromSelection(); break;
        case 21: _t->setSelection((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskFemConstraintDisplacement::staticMetaObject = { {
    &TaskFemConstraint::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskFemConstraintDisplacement.data,
    qt_meta_data_FemGui__TaskFemConstraintDisplacement,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskFemConstraintDisplacement::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskFemConstraintDisplacement::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskFemConstraintDisplacement.stringdata0))
        return static_cast<void*>(this);
    return TaskFemConstraint::qt_metacast(_clname);
}

int FemGui::TaskFemConstraintDisplacement::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskFemConstraint::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}
struct qt_meta_stringdata_FemGui__TaskDlgFemConstraintDisplacement_t {
    QByteArrayData data[1];
    char stringdata0[41];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FemGui__TaskDlgFemConstraintDisplacement_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FemGui__TaskDlgFemConstraintDisplacement_t qt_meta_stringdata_FemGui__TaskDlgFemConstraintDisplacement = {
    {
QT_MOC_LITERAL(0, 0, 40) // "FemGui::TaskDlgFemConstraintD..."

    },
    "FemGui::TaskDlgFemConstraintDisplacement"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FemGui__TaskDlgFemConstraintDisplacement[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void FemGui::TaskDlgFemConstraintDisplacement::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject FemGui::TaskDlgFemConstraintDisplacement::staticMetaObject = { {
    &TaskDlgFemConstraint::staticMetaObject,
    qt_meta_stringdata_FemGui__TaskDlgFemConstraintDisplacement.data,
    qt_meta_data_FemGui__TaskDlgFemConstraintDisplacement,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FemGui::TaskDlgFemConstraintDisplacement::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FemGui::TaskDlgFemConstraintDisplacement::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FemGui__TaskDlgFemConstraintDisplacement.stringdata0))
        return static_cast<void*>(this);
    return TaskDlgFemConstraint::qt_metacast(_clname);
}

int FemGui::TaskDlgFemConstraintDisplacement::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskDlgFemConstraint::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
