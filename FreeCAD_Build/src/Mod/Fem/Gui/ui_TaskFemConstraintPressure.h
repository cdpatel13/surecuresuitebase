/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintPressure.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTPRESSURE_H
#define UI_TASKFEMCONSTRAINTPRESSURE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/InputField.h"

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintPressure
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lbl_info;
    QHBoxLayout *hLayout1;
    QPushButton *btnAdd;
    QPushButton *btnRemove;
    QListWidget *lw_references;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelParameter1;
    Gui::InputField *if_pressure;
    QCheckBox *checkBoxReverse;

    void setupUi(QWidget *TaskFemConstraintPressure)
    {
        if (TaskFemConstraintPressure->objectName().isEmpty())
            TaskFemConstraintPressure->setObjectName(QString::fromUtf8("TaskFemConstraintPressure"));
        TaskFemConstraintPressure->resize(226, 253);
        verticalLayout = new QVBoxLayout(TaskFemConstraintPressure);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbl_info = new QLabel(TaskFemConstraintPressure);
        lbl_info->setObjectName(QString::fromUtf8("lbl_info"));

        verticalLayout->addWidget(lbl_info);

        hLayout1 = new QHBoxLayout();
        hLayout1->setObjectName(QString::fromUtf8("hLayout1"));
        btnAdd = new QPushButton(TaskFemConstraintPressure);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));

        hLayout1->addWidget(btnAdd);

        btnRemove = new QPushButton(TaskFemConstraintPressure);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));

        hLayout1->addWidget(btnRemove);


        verticalLayout->addLayout(hLayout1);

        lw_references = new QListWidget(TaskFemConstraintPressure);
        lw_references->setObjectName(QString::fromUtf8("lw_references"));

        verticalLayout->addWidget(lw_references);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        labelParameter1 = new QLabel(TaskFemConstraintPressure);
        labelParameter1->setObjectName(QString::fromUtf8("labelParameter1"));

        horizontalLayout_2->addWidget(labelParameter1);

        if_pressure = new Gui::InputField(TaskFemConstraintPressure);
        if_pressure->setObjectName(QString::fromUtf8("if_pressure"));
        if_pressure->setProperty("unit", QVariant(QString::fromUtf8("")));

        horizontalLayout_2->addWidget(if_pressure);


        verticalLayout->addLayout(horizontalLayout_2);

        checkBoxReverse = new QCheckBox(TaskFemConstraintPressure);
        checkBoxReverse->setObjectName(QString::fromUtf8("checkBoxReverse"));

        verticalLayout->addWidget(checkBoxReverse);


        retranslateUi(TaskFemConstraintPressure);

        QMetaObject::connectSlotsByName(TaskFemConstraintPressure);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintPressure)
    {
        TaskFemConstraintPressure->setWindowTitle(QApplication::translate("TaskFemConstraintPressure", "Form", nullptr));
        lbl_info->setText(QApplication::translate("TaskFemConstraintPressure", "Select multiple face(s), click Add or Remove", nullptr));
        btnAdd->setText(QApplication::translate("TaskFemConstraintPressure", "Add", nullptr));
        btnRemove->setText(QApplication::translate("TaskFemConstraintPressure", "Remove", nullptr));
        labelParameter1->setText(QApplication::translate("TaskFemConstraintPressure", "Pressure", nullptr));
        if_pressure->setText(QApplication::translate("TaskFemConstraintPressure", "0 MPa", nullptr));
        checkBoxReverse->setText(QApplication::translate("TaskFemConstraintPressure", "Reverse Direction", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintPressure: public Ui_TaskFemConstraintPressure {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTPRESSURE_H
