/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintForce.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTFORCE_H
#define UI_TASKFEMCONSTRAINTFORCE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintForce
{
public:
    QVBoxLayout *verticalLayout;
    QPushButton *buttonReference;
    QListWidget *listReferences;
    QHBoxLayout *layoutForce;
    QLabel *labelForce;
    Gui::QuantitySpinBox *spinForce;
    QHBoxLayout *layoutDirection;
    QPushButton *buttonDirection;
    QLineEdit *lineDirection;
    QCheckBox *checkReverse;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TaskFemConstraintForce)
    {
        if (TaskFemConstraintForce->objectName().isEmpty())
            TaskFemConstraintForce->setObjectName(QString::fromUtf8("TaskFemConstraintForce"));
        TaskFemConstraintForce->resize(257, 233);
        verticalLayout = new QVBoxLayout(TaskFemConstraintForce);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        buttonReference = new QPushButton(TaskFemConstraintForce);
        buttonReference->setObjectName(QString::fromUtf8("buttonReference"));

        verticalLayout->addWidget(buttonReference);

        listReferences = new QListWidget(TaskFemConstraintForce);
        listReferences->setObjectName(QString::fromUtf8("listReferences"));

        verticalLayout->addWidget(listReferences);

        layoutForce = new QHBoxLayout();
        layoutForce->setObjectName(QString::fromUtf8("layoutForce"));
        labelForce = new QLabel(TaskFemConstraintForce);
        labelForce->setObjectName(QString::fromUtf8("labelForce"));

        layoutForce->addWidget(labelForce);

        spinForce = new Gui::QuantitySpinBox(TaskFemConstraintForce);
        spinForce->setObjectName(QString::fromUtf8("spinForce"));
        spinForce->setValue(500.000000000000000);

        layoutForce->addWidget(spinForce);


        verticalLayout->addLayout(layoutForce);

        layoutDirection = new QHBoxLayout();
        layoutDirection->setObjectName(QString::fromUtf8("layoutDirection"));
        buttonDirection = new QPushButton(TaskFemConstraintForce);
        buttonDirection->setObjectName(QString::fromUtf8("buttonDirection"));

        layoutDirection->addWidget(buttonDirection);

        lineDirection = new QLineEdit(TaskFemConstraintForce);
        lineDirection->setObjectName(QString::fromUtf8("lineDirection"));

        layoutDirection->addWidget(lineDirection);


        verticalLayout->addLayout(layoutDirection);

        checkReverse = new QCheckBox(TaskFemConstraintForce);
        checkReverse->setObjectName(QString::fromUtf8("checkReverse"));

        verticalLayout->addWidget(checkReverse);

        verticalSpacer = new QSpacerItem(17, 56, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(TaskFemConstraintForce);

        QMetaObject::connectSlotsByName(TaskFemConstraintForce);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintForce)
    {
        TaskFemConstraintForce->setWindowTitle(QApplication::translate("TaskFemConstraintForce", "Form", nullptr));
        buttonReference->setText(QApplication::translate("TaskFemConstraintForce", "Add reference", nullptr));
        labelForce->setText(QApplication::translate("TaskFemConstraintForce", "Load [N]", nullptr));
        buttonDirection->setText(QApplication::translate("TaskFemConstraintForce", "Direction", nullptr));
        checkReverse->setText(QApplication::translate("TaskFemConstraintForce", "Reverse direction", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintForce: public Ui_TaskFemConstraintForce {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTFORCE_H
