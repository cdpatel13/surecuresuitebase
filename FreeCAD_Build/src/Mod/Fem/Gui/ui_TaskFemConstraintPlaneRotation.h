/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintPlaneRotation.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTPLANEROTATION_H
#define UI_TASKFEMCONSTRAINTPLANEROTATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintPlaneRotation
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lbl_info;
    QHBoxLayout *hLayout1;
    QPushButton *btnAdd;
    QPushButton *btnRemove;
    QListWidget *lw_references;

    void setupUi(QWidget *TaskFemConstraintPlaneRotation)
    {
        if (TaskFemConstraintPlaneRotation->objectName().isEmpty())
            TaskFemConstraintPlaneRotation->setObjectName(QString::fromUtf8("TaskFemConstraintPlaneRotation"));
        TaskFemConstraintPlaneRotation->resize(309, 207);
        verticalLayout = new QVBoxLayout(TaskFemConstraintPlaneRotation);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbl_info = new QLabel(TaskFemConstraintPlaneRotation);
        lbl_info->setObjectName(QString::fromUtf8("lbl_info"));

        verticalLayout->addWidget(lbl_info);

        hLayout1 = new QHBoxLayout();
        hLayout1->setObjectName(QString::fromUtf8("hLayout1"));
        btnAdd = new QPushButton(TaskFemConstraintPlaneRotation);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));

        hLayout1->addWidget(btnAdd);

        btnRemove = new QPushButton(TaskFemConstraintPlaneRotation);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));

        hLayout1->addWidget(btnRemove);


        verticalLayout->addLayout(hLayout1);

        lw_references = new QListWidget(TaskFemConstraintPlaneRotation);
        lw_references->setObjectName(QString::fromUtf8("lw_references"));

        verticalLayout->addWidget(lw_references);


        retranslateUi(TaskFemConstraintPlaneRotation);

        QMetaObject::connectSlotsByName(TaskFemConstraintPlaneRotation);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintPlaneRotation)
    {
        TaskFemConstraintPlaneRotation->setWindowTitle(QApplication::translate("TaskFemConstraintPlaneRotation", "Form", nullptr));
        lbl_info->setText(QApplication::translate("TaskFemConstraintPlaneRotation", "Select a single face, click Add or Remove", nullptr));
        btnAdd->setText(QApplication::translate("TaskFemConstraintPlaneRotation", "Add", nullptr));
        btnRemove->setText(QApplication::translate("TaskFemConstraintPlaneRotation", "Remove", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintPlaneRotation: public Ui_TaskFemConstraintPlaneRotation {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTPLANEROTATION_H
