/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintTemperature.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTTEMPERATURE_H
#define UI_TASKFEMCONSTRAINTTEMPERATURE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/InputField.h"

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintTemperature
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lbl_info;
    QHBoxLayout *hLayout1;
    QPushButton *btnAdd;
    QPushButton *btnRemove;
    QListWidget *lw_references;
    QHBoxLayout *horizontalLayout;
    QRadioButton *rb_temperature;
    QRadioButton *rb_cflux;
    QHBoxLayout *layoutTemperature;
    QLabel *lbl_type;
    Gui::InputField *if_temperature;

    void setupUi(QWidget *TaskFemConstraintTemperature)
    {
        if (TaskFemConstraintTemperature->objectName().isEmpty())
            TaskFemConstraintTemperature->setObjectName(QString::fromUtf8("TaskFemConstraintTemperature"));
        TaskFemConstraintTemperature->resize(503, 340);
        verticalLayout = new QVBoxLayout(TaskFemConstraintTemperature);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lbl_info = new QLabel(TaskFemConstraintTemperature);
        lbl_info->setObjectName(QString::fromUtf8("lbl_info"));

        verticalLayout->addWidget(lbl_info);

        hLayout1 = new QHBoxLayout();
        hLayout1->setObjectName(QString::fromUtf8("hLayout1"));
        btnAdd = new QPushButton(TaskFemConstraintTemperature);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));

        hLayout1->addWidget(btnAdd);

        btnRemove = new QPushButton(TaskFemConstraintTemperature);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));

        hLayout1->addWidget(btnRemove);


        verticalLayout->addLayout(hLayout1);

        lw_references = new QListWidget(TaskFemConstraintTemperature);
        lw_references->setObjectName(QString::fromUtf8("lw_references"));

        verticalLayout->addWidget(lw_references);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        rb_temperature = new QRadioButton(TaskFemConstraintTemperature);
        rb_temperature->setObjectName(QString::fromUtf8("rb_temperature"));
        rb_temperature->setCheckable(true);
        rb_temperature->setChecked(false);

        horizontalLayout->addWidget(rb_temperature);

        rb_cflux = new QRadioButton(TaskFemConstraintTemperature);
        rb_cflux->setObjectName(QString::fromUtf8("rb_cflux"));

        horizontalLayout->addWidget(rb_cflux);


        verticalLayout->addLayout(horizontalLayout);

        layoutTemperature = new QHBoxLayout();
        layoutTemperature->setObjectName(QString::fromUtf8("layoutTemperature"));
        lbl_type = new QLabel(TaskFemConstraintTemperature);
        lbl_type->setObjectName(QString::fromUtf8("lbl_type"));

        layoutTemperature->addWidget(lbl_type);

        if_temperature = new Gui::InputField(TaskFemConstraintTemperature);
        if_temperature->setObjectName(QString::fromUtf8("if_temperature"));
        if_temperature->setProperty("unit", QVariant(QString::fromUtf8("")));

        layoutTemperature->addWidget(if_temperature);


        verticalLayout->addLayout(layoutTemperature);


        retranslateUi(TaskFemConstraintTemperature);

        QMetaObject::connectSlotsByName(TaskFemConstraintTemperature);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintTemperature)
    {
        TaskFemConstraintTemperature->setWindowTitle(QApplication::translate("TaskFemConstraintTemperature", "Form", nullptr));
        lbl_info->setText(QApplication::translate("TaskFemConstraintTemperature", "Select multiple face(s), click Add or Remove", nullptr));
        btnAdd->setText(QApplication::translate("TaskFemConstraintTemperature", "Add", nullptr));
        btnRemove->setText(QApplication::translate("TaskFemConstraintTemperature", "Remove", nullptr));
        rb_temperature->setText(QApplication::translate("TaskFemConstraintTemperature", "Temperature", nullptr));
        rb_cflux->setText(QApplication::translate("TaskFemConstraintTemperature", "Concentrated heat flux", nullptr));
        lbl_type->setText(QApplication::translate("TaskFemConstraintTemperature", "Temperature", nullptr));
        if_temperature->setText(QApplication::translate("TaskFemConstraintTemperature", "300 K", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintTemperature: public Ui_TaskFemConstraintTemperature {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTTEMPERATURE_H
