/********************************************************************************
** Form generated from reading UI file 'DlgSettingsFemMaterial.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGSFEMMATERIAL_H
#define UI_DLGSETTINGSFEMMATERIAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/FileDialog.h"
#include "Gui/PrefWidgets.h"

namespace FemGui {

class Ui_DlgSettingsFemMaterialImp
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *gb_4_materials;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_6;
    Gui::PrefCheckBox *cb_use_built_in_materials;
    Gui::PrefCheckBox *cb_use_mat_from_config_dir;
    Gui::PrefCheckBox *cb_use_mat_from_custom_dir;
    QGridLayout *gridLayout;
    Gui::PrefFileChooser *fc_custom_mat_dir;
    QLabel *l_custom_mat_dir;
    QGroupBox *gb_4_materials_2;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_7;
    Gui::PrefCheckBox *cb_delete_duplicates;
    Gui::PrefCheckBox *cb_sort_by_resources;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *FemGui__DlgSettingsFemMaterialImp)
    {
        if (FemGui__DlgSettingsFemMaterialImp->objectName().isEmpty())
            FemGui__DlgSettingsFemMaterialImp->setObjectName(QString::fromUtf8("FemGui__DlgSettingsFemMaterialImp"));
        FemGui__DlgSettingsFemMaterialImp->resize(519, 451);
        verticalLayout = new QVBoxLayout(FemGui__DlgSettingsFemMaterialImp);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gb_4_materials = new QGroupBox(FemGui__DlgSettingsFemMaterialImp);
        gb_4_materials->setObjectName(QString::fromUtf8("gb_4_materials"));
        horizontalLayout_3 = new QHBoxLayout(gb_4_materials);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        cb_use_built_in_materials = new Gui::PrefCheckBox(gb_4_materials);
        cb_use_built_in_materials->setObjectName(QString::fromUtf8("cb_use_built_in_materials"));
        cb_use_built_in_materials->setChecked(true);
        cb_use_built_in_materials->setProperty("prefEntry", QVariant(QByteArray("UseBuiltInMaterials")));
        cb_use_built_in_materials->setProperty("prefPath", QVariant(QByteArray("Mod/Material/Resources")));

        verticalLayout_6->addWidget(cb_use_built_in_materials);

        cb_use_mat_from_config_dir = new Gui::PrefCheckBox(gb_4_materials);
        cb_use_mat_from_config_dir->setObjectName(QString::fromUtf8("cb_use_mat_from_config_dir"));
        cb_use_mat_from_config_dir->setChecked(true);
        cb_use_mat_from_config_dir->setProperty("prefEntry", QVariant(QByteArray("UseMaterialsFromConfigDir")));
        cb_use_mat_from_config_dir->setProperty("prefPath", QVariant(QByteArray("Mod/Material/Resources")));

        verticalLayout_6->addWidget(cb_use_mat_from_config_dir);

        cb_use_mat_from_custom_dir = new Gui::PrefCheckBox(gb_4_materials);
        cb_use_mat_from_custom_dir->setObjectName(QString::fromUtf8("cb_use_mat_from_custom_dir"));
        cb_use_mat_from_custom_dir->setChecked(true);
        cb_use_mat_from_custom_dir->setProperty("prefEntry", QVariant(QByteArray("UseMaterialsFromCustomDir")));
        cb_use_mat_from_custom_dir->setProperty("prefPath", QVariant(QByteArray("Mod/Material/Resources")));

        verticalLayout_6->addWidget(cb_use_mat_from_custom_dir);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        fc_custom_mat_dir = new Gui::PrefFileChooser(gb_4_materials);
        fc_custom_mat_dir->setObjectName(QString::fromUtf8("fc_custom_mat_dir"));
        fc_custom_mat_dir->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(fc_custom_mat_dir->sizePolicy().hasHeightForWidth());
        fc_custom_mat_dir->setSizePolicy(sizePolicy);
        fc_custom_mat_dir->setBaseSize(QSize(0, 0));
        fc_custom_mat_dir->setProperty("prefEntry", QVariant(QByteArray("CustomMaterialsDir")));
        fc_custom_mat_dir->setMode(Gui::FileChooser::Directory);
        fc_custom_mat_dir->setProperty("prefPath", QVariant(QByteArray("Mod/Material/Resources")));

        gridLayout->addWidget(fc_custom_mat_dir, 1, 1, 1, 1);

        l_custom_mat_dir = new QLabel(gb_4_materials);
        l_custom_mat_dir->setObjectName(QString::fromUtf8("l_custom_mat_dir"));
        l_custom_mat_dir->setEnabled(true);
        l_custom_mat_dir->setMinimumSize(QSize(100, 0));

        gridLayout->addWidget(l_custom_mat_dir, 1, 0, 1, 1);


        verticalLayout_6->addLayout(gridLayout);


        horizontalLayout_3->addLayout(verticalLayout_6);


        verticalLayout->addWidget(gb_4_materials);

        gb_4_materials_2 = new QGroupBox(FemGui__DlgSettingsFemMaterialImp);
        gb_4_materials_2->setObjectName(QString::fromUtf8("gb_4_materials_2"));
        horizontalLayout_4 = new QHBoxLayout(gb_4_materials_2);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        cb_delete_duplicates = new Gui::PrefCheckBox(gb_4_materials_2);
        cb_delete_duplicates->setObjectName(QString::fromUtf8("cb_delete_duplicates"));
        cb_delete_duplicates->setChecked(true);
        cb_delete_duplicates->setProperty("prefEntry", QVariant(QByteArray("DeleteDuplicates")));
        cb_delete_duplicates->setProperty("prefPath", QVariant(QByteArray("Mod/Material/Cards")));

        verticalLayout_7->addWidget(cb_delete_duplicates);

        cb_sort_by_resources = new Gui::PrefCheckBox(gb_4_materials_2);
        cb_sort_by_resources->setObjectName(QString::fromUtf8("cb_sort_by_resources"));
        cb_sort_by_resources->setChecked(true);
        cb_sort_by_resources->setProperty("prefEntry", QVariant(QByteArray("SortByResources")));
        cb_sort_by_resources->setProperty("prefPath", QVariant(QByteArray("Mod/Material/Cards")));

        verticalLayout_7->addWidget(cb_sort_by_resources);


        horizontalLayout_4->addLayout(verticalLayout_7);


        verticalLayout->addWidget(gb_4_materials_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(FemGui__DlgSettingsFemMaterialImp);

        QMetaObject::connectSlotsByName(FemGui__DlgSettingsFemMaterialImp);
    } // setupUi

    void retranslateUi(QWidget *FemGui__DlgSettingsFemMaterialImp)
    {
        FemGui__DlgSettingsFemMaterialImp->setWindowTitle(QApplication::translate("FemGui::DlgSettingsFemMaterialImp", "Material", nullptr));
        gb_4_materials->setTitle(QApplication::translate("FemGui::DlgSettingsFemMaterialImp", "Card resources", nullptr));
        cb_use_built_in_materials->setText(QApplication::translate("FemGui::DlgSettingsFemMaterialImp", "Use built-in materials", nullptr));
        cb_use_mat_from_config_dir->setText(QApplication::translate("FemGui::DlgSettingsFemMaterialImp", "Use materials from Materials directory in users FreeCAD user pref directory.", nullptr));
        cb_use_mat_from_custom_dir->setText(QApplication::translate("FemGui::DlgSettingsFemMaterialImp", "Use materials from user defined directory", nullptr));
        l_custom_mat_dir->setText(QApplication::translate("FemGui::DlgSettingsFemMaterialImp", "User directory", nullptr));
        gb_4_materials_2->setTitle(QApplication::translate("FemGui::DlgSettingsFemMaterialImp", "Card sorting and duplicates", nullptr));
        cb_delete_duplicates->setText(QApplication::translate("FemGui::DlgSettingsFemMaterialImp", "Delete card duplicates", nullptr));
        cb_sort_by_resources->setText(QApplication::translate("FemGui::DlgSettingsFemMaterialImp", "Sort by resources (opposite would be sort by cards)", nullptr));
    } // retranslateUi

};

} // namespace FemGui

namespace FemGui {
namespace Ui {
    class DlgSettingsFemMaterialImp: public Ui_DlgSettingsFemMaterialImp {};
} // namespace Ui
} // namespace FemGui

#endif // UI_DLGSETTINGSFEMMATERIAL_H
