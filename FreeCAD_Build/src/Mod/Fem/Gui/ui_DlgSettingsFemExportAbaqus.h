/********************************************************************************
** Form generated from reading UI file 'DlgSettingsFemExportAbaqus.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGSFEMEXPORTABAQUS_H
#define UI_DLGSETTINGSFEMEXPORTABAQUS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

namespace FemGui {

class Ui_DlgSettingsFemExportAbaqus
{
public:
    QGridLayout *gridLayout_4;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QGridLayout *girdLayoutAbaqus;
    Gui::PrefCheckBox *checkBoxWriteGroups;
    QLabel *label2;
    Gui::PrefComboBox *comboBoxElemChoiceParam;
    QLabel *label1;

    void setupUi(QWidget *FemGui__DlgSettingsFemExportAbaqus)
    {
        if (FemGui__DlgSettingsFemExportAbaqus->objectName().isEmpty())
            FemGui__DlgSettingsFemExportAbaqus->setObjectName(QString::fromUtf8("FemGui__DlgSettingsFemExportAbaqus"));
        FemGui__DlgSettingsFemExportAbaqus->resize(445, 298);
        gridLayout_4 = new QGridLayout(FemGui__DlgSettingsFemExportAbaqus);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        verticalSpacer = new QSpacerItem(20, 82, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 1, 0, 1, 1);

        groupBox = new QGroupBox(FemGui__DlgSettingsFemExportAbaqus);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        girdLayoutAbaqus = new QGridLayout();
        girdLayoutAbaqus->setObjectName(QString::fromUtf8("girdLayoutAbaqus"));
        checkBoxWriteGroups = new Gui::PrefCheckBox(groupBox);
        checkBoxWriteGroups->setObjectName(QString::fromUtf8("checkBoxWriteGroups"));
        checkBoxWriteGroups->setChecked(false);
        checkBoxWriteGroups->setProperty("prefEntry", QVariant(QByteArray("AbaqusWriteGroups")));
        checkBoxWriteGroups->setProperty("prefPath", QVariant(QByteArray("Mod/Fem/Abaqus")));

        girdLayoutAbaqus->addWidget(checkBoxWriteGroups, 1, 1, 1, 1);

        label2 = new QLabel(groupBox);
        label2->setObjectName(QString::fromUtf8("label2"));

        girdLayoutAbaqus->addWidget(label2, 1, 0, 1, 1);

        comboBoxElemChoiceParam = new Gui::PrefComboBox(groupBox);
        comboBoxElemChoiceParam->addItem(QString());
        comboBoxElemChoiceParam->addItem(QString());
        comboBoxElemChoiceParam->addItem(QString());
        comboBoxElemChoiceParam->setObjectName(QString::fromUtf8("comboBoxElemChoiceParam"));

        girdLayoutAbaqus->addWidget(comboBoxElemChoiceParam, 0, 1, 1, 1);

        label1 = new QLabel(groupBox);
        label1->setObjectName(QString::fromUtf8("label1"));

        girdLayoutAbaqus->addWidget(label1, 0, 0, 1, 1);


        gridLayout_3->addLayout(girdLayoutAbaqus, 0, 1, 1, 1);


        gridLayout_4->addWidget(groupBox, 0, 0, 1, 1);


        retranslateUi(FemGui__DlgSettingsFemExportAbaqus);

        QMetaObject::connectSlotsByName(FemGui__DlgSettingsFemExportAbaqus);
    } // setupUi

    void retranslateUi(QWidget *FemGui__DlgSettingsFemExportAbaqus)
    {
        FemGui__DlgSettingsFemExportAbaqus->setWindowTitle(QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "INP", nullptr));
        groupBox->setTitle(QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "Export", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxWriteGroups->setToolTip(QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "Mesh groups are exported too.\n"
"Every constraint and, if there are different materials, material\n"
"consists of two mesh groups, faces and nodes where the\n"
"constraint or material is applied.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxWriteGroups->setText(QString());
        label2->setText(QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "Export group data", nullptr));
        comboBoxElemChoiceParam->setItemText(0, QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "All", nullptr));
        comboBoxElemChoiceParam->setItemText(1, QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "Highest", nullptr));
        comboBoxElemChoiceParam->setItemText(2, QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "FEM", nullptr));

#ifndef QT_NO_TOOLTIP
        comboBoxElemChoiceParam->setToolTip(QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "All: All elements will be exported.\n"
"\n"
"Highest: Only the highest elements will be exported. This means\n"
"for means volumes for a volume mesh and faces for a shell mesh.\n"
"\n"
"FEM: Only FEM elements will be exported. This means only edges\n"
"not belonging to faces and faces not belonging to volumes.", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        comboBoxElemChoiceParam->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        comboBoxElemChoiceParam->setWhatsThis(QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "element parameter: All: all elements, highest: highest elements only, FEM: FEM elements only (only edges not belonging to faces and faces not belonging to volumes)", nullptr));
#endif // QT_NO_WHATSTHIS
        comboBoxElemChoiceParam->setProperty("prefEntry", QVariant(QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "AbaqusElementChoice", nullptr)));
        comboBoxElemChoiceParam->setProperty("prefPath", QVariant(QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "Mod/Fem/Abaqus", nullptr)));
        label1->setText(QApplication::translate("FemGui::DlgSettingsFemExportAbaqus", "Which mesh elements to export", nullptr));
    } // retranslateUi

};

} // namespace FemGui

namespace FemGui {
namespace Ui {
    class DlgSettingsFemExportAbaqus: public Ui_DlgSettingsFemExportAbaqus {};
} // namespace Ui
} // namespace FemGui

#endif // UI_DLGSETTINGSFEMEXPORTABAQUS_H
