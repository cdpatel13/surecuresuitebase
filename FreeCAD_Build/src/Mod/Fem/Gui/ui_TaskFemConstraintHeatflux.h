/********************************************************************************
** Form generated from reading UI file 'TaskFemConstraintHeatflux.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFEMCONSTRAINTHEATFLUX_H
#define UI_TASKFEMCONSTRAINTHEATFLUX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/InputField.h"

QT_BEGIN_NAMESPACE

class Ui_TaskFemConstraintHeatflux
{
public:
    QVBoxLayout *verticalLayout_2;
    QLabel *lbl_references;
    QHBoxLayout *hLayout1;
    QPushButton *btnAdd;
    QPushButton *btnRemove;
    QListWidget *lw_references;
    QHBoxLayout *horizontalLayout;
    QRadioButton *rb_convection;
    QRadioButton *rb_dflux;
    QStackedWidget *sw_heatflux;
    QWidget *page;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *layoutFilmCoef;
    QLabel *lbl_filmcoef;
    Gui::InputField *if_filmcoef;
    QHBoxLayout *layoutAmbientTemp;
    QLabel *lbl_ambienttemp;
    Gui::InputField *if_ambienttemp;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    Gui::InputField *if_heatflux;

    void setupUi(QWidget *TaskFemConstraintHeatflux)
    {
        if (TaskFemConstraintHeatflux->objectName().isEmpty())
            TaskFemConstraintHeatflux->setObjectName(QString::fromUtf8("TaskFemConstraintHeatflux"));
        TaskFemConstraintHeatflux->resize(379, 531);
        verticalLayout_2 = new QVBoxLayout(TaskFemConstraintHeatflux);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lbl_references = new QLabel(TaskFemConstraintHeatflux);
        lbl_references->setObjectName(QString::fromUtf8("lbl_references"));

        verticalLayout_2->addWidget(lbl_references);

        hLayout1 = new QHBoxLayout();
        hLayout1->setObjectName(QString::fromUtf8("hLayout1"));
        btnAdd = new QPushButton(TaskFemConstraintHeatflux);
        btnAdd->setObjectName(QString::fromUtf8("btnAdd"));

        hLayout1->addWidget(btnAdd);

        btnRemove = new QPushButton(TaskFemConstraintHeatflux);
        btnRemove->setObjectName(QString::fromUtf8("btnRemove"));

        hLayout1->addWidget(btnRemove);


        verticalLayout_2->addLayout(hLayout1);

        lw_references = new QListWidget(TaskFemConstraintHeatflux);
        lw_references->setObjectName(QString::fromUtf8("lw_references"));

        verticalLayout_2->addWidget(lw_references);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        rb_convection = new QRadioButton(TaskFemConstraintHeatflux);
        rb_convection->setObjectName(QString::fromUtf8("rb_convection"));

        horizontalLayout->addWidget(rb_convection);

        rb_dflux = new QRadioButton(TaskFemConstraintHeatflux);
        rb_dflux->setObjectName(QString::fromUtf8("rb_dflux"));

        horizontalLayout->addWidget(rb_dflux);


        verticalLayout_2->addLayout(horizontalLayout);

        sw_heatflux = new QStackedWidget(TaskFemConstraintHeatflux);
        sw_heatflux->setObjectName(QString::fromUtf8("sw_heatflux"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        verticalLayout_4 = new QVBoxLayout(page);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        layoutFilmCoef = new QHBoxLayout();
        layoutFilmCoef->setObjectName(QString::fromUtf8("layoutFilmCoef"));
        lbl_filmcoef = new QLabel(page);
        lbl_filmcoef->setObjectName(QString::fromUtf8("lbl_filmcoef"));

        layoutFilmCoef->addWidget(lbl_filmcoef);

        if_filmcoef = new Gui::InputField(page);
        if_filmcoef->setObjectName(QString::fromUtf8("if_filmcoef"));
        if_filmcoef->setProperty("unit", QVariant(QString::fromUtf8("W/m^2/K")));

        layoutFilmCoef->addWidget(if_filmcoef);


        verticalLayout->addLayout(layoutFilmCoef);

        layoutAmbientTemp = new QHBoxLayout();
        layoutAmbientTemp->setObjectName(QString::fromUtf8("layoutAmbientTemp"));
        lbl_ambienttemp = new QLabel(page);
        lbl_ambienttemp->setObjectName(QString::fromUtf8("lbl_ambienttemp"));

        layoutAmbientTemp->addWidget(lbl_ambienttemp);

        if_ambienttemp = new Gui::InputField(page);
        if_ambienttemp->setObjectName(QString::fromUtf8("if_ambienttemp"));
        if_ambienttemp->setProperty("unit", QVariant(QString::fromUtf8("K")));

        layoutAmbientTemp->addWidget(if_ambienttemp);


        verticalLayout->addLayout(layoutAmbientTemp);


        verticalLayout_4->addLayout(verticalLayout);

        sw_heatflux->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        verticalLayout_3 = new QVBoxLayout(page_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(page_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        if_heatflux = new Gui::InputField(page_2);
        if_heatflux->setObjectName(QString::fromUtf8("if_heatflux"));
        if_heatflux->setProperty("unit", QVariant(QString::fromUtf8("K")));

        horizontalLayout_2->addWidget(if_heatflux);


        verticalLayout_3->addLayout(horizontalLayout_2);

        sw_heatflux->addWidget(page_2);

        verticalLayout_2->addWidget(sw_heatflux);


        retranslateUi(TaskFemConstraintHeatflux);

        sw_heatflux->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(TaskFemConstraintHeatflux);
    } // setupUi

    void retranslateUi(QWidget *TaskFemConstraintHeatflux)
    {
        TaskFemConstraintHeatflux->setWindowTitle(QApplication::translate("TaskFemConstraintHeatflux", "TaskFemConstraintHeatflux", nullptr));
        lbl_references->setText(QApplication::translate("TaskFemConstraintHeatflux", "Select multiple face(s), click Add or Remove:", nullptr));
        btnAdd->setText(QApplication::translate("TaskFemConstraintHeatflux", "Add", nullptr));
        btnRemove->setText(QApplication::translate("TaskFemConstraintHeatflux", "Remove", nullptr));
        rb_convection->setText(QApplication::translate("TaskFemConstraintHeatflux", "Surface Convection", nullptr));
        rb_dflux->setText(QApplication::translate("TaskFemConstraintHeatflux", "Surface heat flux", nullptr));
        lbl_filmcoef->setText(QApplication::translate("TaskFemConstraintHeatflux", "Film coefficient", nullptr));
        if_filmcoef->setText(QApplication::translate("TaskFemConstraintHeatflux", "1 W/m^2/K", nullptr));
        lbl_ambienttemp->setText(QApplication::translate("TaskFemConstraintHeatflux", "Ambient Temperature", nullptr));
        if_ambienttemp->setText(QApplication::translate("TaskFemConstraintHeatflux", "300 K", nullptr));
        label->setText(QApplication::translate("TaskFemConstraintHeatflux", "Surface heat flux", nullptr));
        if_heatflux->setText(QApplication::translate("TaskFemConstraintHeatflux", "300 K", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskFemConstraintHeatflux: public Ui_TaskFemConstraintHeatflux {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKFEMCONSTRAINTHEATFLUX_H
