# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/coding_conventions.md"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/InitGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/ObjectsFem.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/TestFem.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femcommands" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femcommands/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femcommands/commands.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femcommands/manager.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femexamples" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/boxanalysis.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/ccx_cantilever_std.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/manager.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/material_multiple_twoboxes.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/material_nl_platewithhole.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/rc_wall_2d.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/thermomech_flow1d.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/thermomech_spine.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femexamples/meshes" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/meshes/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/meshes/mesh_boxanalysis_tetra10.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/meshes/mesh_boxes_2_vertikal_tetra10.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/meshes/mesh_canticcx_tetra10.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/meshes/mesh_rc_wall_2d_tria6.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/meshes/mesh_platewithhole_tetra10.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/meshes/mesh_thermomech_flow1d_seg3.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femexamples/meshes/mesh_thermomech_spine_tetra10.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/feminout" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/convert2TetGen.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/importCcxDatResults.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/importCcxFrdResults.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/importFenicsMesh.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/importInpMesh.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/importToolsFem.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/importVTKResults.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/importYamlJsonMesh.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/importZ88Mesh.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/importZ88O2Results.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/readFenicsXDMF.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/readFenicsXML.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/writeFenicsXDMF.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/feminout/writeFenicsXML.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femmesh" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femmesh/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femmesh/femmesh2mesh.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femmesh/gmshtools.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femmesh/meshtools.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femresult" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femresult/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femresult/resulttools.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femsolver" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/equationbase.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/report.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/reportdialog.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/run.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/settings.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/signal.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/solverbase.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/task.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/writerbase.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femsolver/calculix" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/calculix/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/calculix/solver.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/calculix/tasks.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/calculix/writer.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femsolver/elmer" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/sifio.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/solver.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/tasks.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/writer.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femsolver/elmer/equations" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/equations/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/equations/electrostatic.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/equations/elasticity.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/equations/equation.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/equations/flow.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/equations/fluxsolver.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/equations/heat.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/equations/linear.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/elmer/equations/nonlinear.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femsolver/fenics" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/fenics/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/fenics/fenics_tools.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femsolver/z88" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/z88/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/z88/solver.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/z88/tasks.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femsolver/z88/writer.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femtest" TYPE FILE FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/__init__.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femtest/app" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/support_utils.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/test_ccxtools.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/test_common.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/test_femimport.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/test_material.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/test_mesh.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/test_object.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/test_result.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/app/test_solverframework.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femtest/data" TYPE FILE FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/__init__.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femtest/data/ccx" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_frequency.inp"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_frequency.dat"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_frequency.frd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_frequency_expected_values"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_frequency.FCStd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_static.inp"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_static.dat"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_static.frd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_static_expected_values"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube_static.FCStd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/cube.FCStd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/multimat.inp"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/spine_thermomech.inp"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/spine_thermomech.dat"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/spine_thermomech.frd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/spine_thermomech_expected_values"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/spine_thermomech.FCStd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/Flow1D_thermomech.inp"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/Flow1D_thermomech.dat"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/Flow1D_thermomech.frd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/Flow1D_thermomech_expected_values"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/Flow1D_thermomech_inout_nodes.txt"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/ccx/Flow1D_thermomech.FCStd"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femtest/data/elmer" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/elmer/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/elmer/case.sif"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/elmer/group_mesh.geo"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/elmer/ELMERSOLVER_STARTINFO"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femtest/data/mesh" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/mesh/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/mesh/tetra10_mesh.inp"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/mesh/tetra10_mesh.unv"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/mesh/tetra10_mesh.vtk"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/mesh/tetra10_mesh.yml"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtest/data/mesh/tetra10_mesh.z88"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femtools" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtools/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtools/ccxtools.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femtools/femutils.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femobjects" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemConstraintBodyHeatSource.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemConstraintElectrostaticPotential.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemConstraintFlowVelocity.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemConstraintInitialFlowVelocity.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemConstraintSelfWeight.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemElementFluid1D.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemElementGeometry1D.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemElementGeometry2D.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemElementRotation1D.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemMaterial.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemMaterialReinforced.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemMaterialMechanicalNonlinear.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemMeshBoundaryLayer.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemMeshGmsh.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemMeshGroup.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemMeshRegion.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemMeshResult.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemResultMechanical.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/_FemSolverCalculix.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femobjects/FemConstraint.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Fem/femguiobjects" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_TaskPanelFemSolverControl.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemConstraintBodyHeatSource.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemConstraintElectrostaticPotential.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemConstraintFlowVelocity.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemConstraintInitialFlowVelocity.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemConstraintSelfWeight.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemElementFluid1D.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemElementGeometry1D.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemElementGeometry2D.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemElementRotation1D.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemMaterial.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemMaterialReinforced.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemMaterialMechanicalNonlinear.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemMeshBoundaryLayer.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemMeshGmsh.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemMeshGroup.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemMeshRegion.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemMeshResult.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemResultMechanical.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/_ViewProviderFemSolverCalculix.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/FemSelectionWidgets.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Fem/femguiobjects/ViewProviderFemConstraint.py"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Fem/App/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Fem/Gui/cmake_install.cmake")

endif()

