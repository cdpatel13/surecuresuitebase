/********************************************************************************
** Form generated from reading UI file 'DlgStartPreferences.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSTARTPREFERENCES_H
#define UI_DLGSTARTPREFERENCES_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/FileDialog.h"
#include "Gui/PrefWidgets.h"
#include "Gui/Widgets.h"

QT_BEGIN_NAMESPACE

class Ui_DlgStartPreferences
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    Gui::PrefFileChooser *fileChooser_1;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_2;
    QLabel *label_14;
    QLabel *label_9;
    QLabel *label_10;
    Gui::PrefCheckBox *checkBox_1;
    Gui::PrefCheckBox *checkBox_4;
    Gui::PrefFileChooser *fileChooser_3;
    QLabel *label_17;
    Gui::PrefCheckBox *checkBox;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    Gui::PrefColorButton *colorButton_3;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    Gui::PrefRadioButton *radioButton_2;
    Gui::PrefRadioButton *radioButton_1;
    QLabel *label_12;
    Gui::PrefColorButton *colorButton_2;
    QLabel *label_8;
    Gui::PrefFileChooser *fileChooser_2;
    Gui::PrefCheckBox *checkBox_5;
    QLabel *label_4;
    Gui::PrefColorButton *colorButton_4;
    QLabel *label_2;
    QLabel *label_5;
    Gui::PrefColorButton *colorButton_6;
    Gui::PrefColorButton *colorButton_5;
    QLabel *label_6;
    Gui::PrefColorButton *colorButton_1;
    Gui::PrefColorButton *colorButton_7;
    QLabel *label_7;
    QLabel *label_3;
    QLabel *label_15;
    QLabel *label_16;
    QHBoxLayout *horizontalLayout_2;
    Gui::PrefLineEdit *lineEdit;
    Gui::PrefSpinBox *spinBox;
    QLabel *label_18;
    Gui::PrefCheckBox *checkBox_6;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_3;
    QComboBox *AutoloadModuleCombo;
    Gui::PrefCheckBox *checkBox_2;
    QLabel *autoModuleLabel;
    QLabel *label_11;
    QLabel *label_13;
    Gui::PrefCheckBox *checkBox_3;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *DlgStartPreferences)
    {
        if (DlgStartPreferences->objectName().isEmpty())
            DlgStartPreferences->setObjectName(QString::fromUtf8("DlgStartPreferences"));
        DlgStartPreferences->resize(482, 769);
        verticalLayout = new QVBoxLayout(DlgStartPreferences);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_2 = new QGroupBox(DlgStartPreferences);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        fileChooser_1 = new Gui::PrefFileChooser(groupBox_2);
        fileChooser_1->setObjectName(QString::fromUtf8("fileChooser_1"));
        fileChooser_1->setProperty("prefEntry", QVariant(QByteArray("Template")));
        fileChooser_1->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        verticalLayout_2->addWidget(fileChooser_1);


        verticalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(DlgStartPreferences);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_2 = new QGridLayout(groupBox_3);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_2->addWidget(label_14, 3, 0, 1, 1);

        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_2->addWidget(label_9, 1, 0, 1, 1);

        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_2->addWidget(label_10, 2, 0, 1, 1);

        checkBox_1 = new Gui::PrefCheckBox(groupBox_3);
        checkBox_1->setObjectName(QString::fromUtf8("checkBox_1"));
        checkBox_1->setLayoutDirection(Qt::RightToLeft);
        checkBox_1->setChecked(true);
        checkBox_1->setProperty("prefEntry", QVariant(QByteArray("ShowExamples")));
        checkBox_1->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout_2->addWidget(checkBox_1, 1, 1, 1, 1);

        checkBox_4 = new Gui::PrefCheckBox(groupBox_3);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));
        checkBox_4->setLayoutDirection(Qt::RightToLeft);
        checkBox_4->setProperty("prefEntry", QVariant(QByteArray("ShowForum")));
        checkBox_4->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout_2->addWidget(checkBox_4, 3, 1, 1, 1);

        fileChooser_3 = new Gui::PrefFileChooser(groupBox_3);
        fileChooser_3->setObjectName(QString::fromUtf8("fileChooser_3"));
        fileChooser_3->setMode(Gui::FileChooser::Directory);
        fileChooser_3->setProperty("prefEntry", QVariant(QByteArray("ShowCustomFolder")));
        fileChooser_3->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout_2->addWidget(fileChooser_3, 2, 1, 1, 1);

        label_17 = new QLabel(groupBox_3);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout_2->addWidget(label_17, 0, 0, 1, 1);

        checkBox = new Gui::PrefCheckBox(groupBox_3);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setLayoutDirection(Qt::RightToLeft);
        checkBox->setProperty("prefEntry", QVariant(QByteArray("ShowNotes")));
        checkBox->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout_2->addWidget(checkBox, 0, 1, 1, 1);


        verticalLayout->addWidget(groupBox_3);

        groupBox = new QGroupBox(DlgStartPreferences);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        colorButton_3 = new Gui::PrefColorButton(groupBox);
        colorButton_3->setObjectName(QString::fromUtf8("colorButton_3"));
        colorButton_3->setMaximumSize(QSize(60, 60));
        colorButton_3->setColor(QColor(255, 255, 255));
        colorButton_3->setProperty("prefEntry", QVariant(QByteArray("PageColor")));
        colorButton_3->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(colorButton_3, 6, 1, 1, 1, Qt::AlignRight);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        radioButton_2 = new Gui::PrefRadioButton(groupBox);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));
        radioButton_2->setEnabled(false);
        radioButton_2->setChecked(true);
        radioButton_2->setProperty("prefEntry", QVariant(QByteArray("InBrowser")));
        radioButton_2->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        horizontalLayout->addWidget(radioButton_2);

        radioButton_1 = new Gui::PrefRadioButton(groupBox);
        radioButton_1->setObjectName(QString::fromUtf8("radioButton_1"));
        radioButton_1->setEnabled(false);
        radioButton_1->setProperty("prefEntry", QVariant(QByteArray("InWeb")));
        radioButton_1->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        horizontalLayout->addWidget(radioButton_1);


        gridLayout->addLayout(horizontalLayout, 11, 1, 1, 1);

        label_12 = new QLabel(groupBox);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setEnabled(false);

        gridLayout->addWidget(label_12, 2, 0, 1, 1);

        colorButton_2 = new Gui::PrefColorButton(groupBox);
        colorButton_2->setObjectName(QString::fromUtf8("colorButton_2"));
        colorButton_2->setMaximumSize(QSize(60, 60));
        colorButton_2->setColor(QColor(255, 251, 247));
        colorButton_2->setProperty("prefEntry", QVariant(QByteArray("BackgroundTextColor")));
        colorButton_2->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(colorButton_2, 5, 1, 1, 1, Qt::AlignRight);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 9, 0, 1, 1);

        fileChooser_2 = new Gui::PrefFileChooser(groupBox);
        fileChooser_2->setObjectName(QString::fromUtf8("fileChooser_2"));
        fileChooser_2->setProperty("prefEntry", QVariant(QByteArray("BackgroundImage")));
        fileChooser_2->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(fileChooser_2, 4, 1, 1, 1);

        checkBox_5 = new Gui::PrefCheckBox(groupBox);
        checkBox_5->setObjectName(QString::fromUtf8("checkBox_5"));
        checkBox_5->setLayoutDirection(Qt::RightToLeft);
        checkBox_5->setProperty("prefEntry", QVariant(QByteArray("UseStyleSheet")));
        checkBox_5->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(checkBox_5, 0, 1, 1, 1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 6, 0, 1, 1);

        colorButton_4 = new Gui::PrefColorButton(groupBox);
        colorButton_4->setObjectName(QString::fromUtf8("colorButton_4"));
        colorButton_4->setMaximumSize(QSize(60, 60));
        colorButton_4->setColor(QColor(0, 0, 0));
        colorButton_4->setProperty("prefEntry", QVariant(QByteArray("PageTextColor")));
        colorButton_4->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(colorButton_4, 7, 1, 1, 1, Qt::AlignRight);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 4, 0, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 7, 0, 1, 1);

        colorButton_6 = new Gui::PrefColorButton(groupBox);
        colorButton_6->setObjectName(QString::fromUtf8("colorButton_6"));
        colorButton_6->setMaximumSize(QSize(60, 60));
        colorButton_6->setColor(QColor(0, 0, 255));
        colorButton_6->setProperty("prefEntry", QVariant(QByteArray("LinkColor")));
        colorButton_6->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(colorButton_6, 9, 1, 1, 1, Qt::AlignRight);

        colorButton_5 = new Gui::PrefColorButton(groupBox);
        colorButton_5->setObjectName(QString::fromUtf8("colorButton_5"));
        colorButton_5->setMaximumSize(QSize(60, 60));
        colorButton_5->setColor(QColor(221, 221, 221));
        colorButton_5->setProperty("prefEntry", QVariant(QByteArray("BoxColor")));
        colorButton_5->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(colorButton_5, 8, 1, 1, 1, Qt::AlignRight);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 8, 0, 1, 1);

        colorButton_1 = new Gui::PrefColorButton(groupBox);
        colorButton_1->setObjectName(QString::fromUtf8("colorButton_1"));
        colorButton_1->setMaximumSize(QSize(60, 60));
        colorButton_1->setColor(QColor(79, 88, 116));
        colorButton_1->setProperty("prefEntry", QVariant(QByteArray("BackgroundColor1")));
        colorButton_1->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(colorButton_1, 1, 1, 1, 1, Qt::AlignRight);

        colorButton_7 = new Gui::PrefColorButton(groupBox);
        colorButton_7->setObjectName(QString::fromUtf8("colorButton_7"));
        colorButton_7->setEnabled(false);
        colorButton_7->setMaximumSize(QSize(60, 60));
        colorButton_7->setColor(QColor(127, 158, 181));
        colorButton_7->setProperty("prefEntry", QVariant(QByteArray("BackgroundColor2")));
        colorButton_7->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(colorButton_7, 2, 1, 1, 1, Qt::AlignRight);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setEnabled(false);

        gridLayout->addWidget(label_7, 11, 0, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 5, 0, 1, 1);

        label_15 = new QLabel(groupBox);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout->addWidget(label_15, 0, 0, 1, 1);

        label_16 = new QLabel(groupBox);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 10, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lineEdit = new Gui::PrefLineEdit(groupBox);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setProperty("prefEntry", QVariant(QByteArray("FontFamily")));
        lineEdit->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        horizontalLayout_2->addWidget(lineEdit);

        spinBox = new Gui::PrefSpinBox(groupBox);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setValue(13);
        spinBox->setProperty("prefEntry", QVariant(QByteArray("FontSize")));
        spinBox->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        horizontalLayout_2->addWidget(spinBox);


        gridLayout->addLayout(horizontalLayout_2, 10, 1, 1, 1);

        label_18 = new QLabel(groupBox);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout->addWidget(label_18, 12, 0, 1, 1);

        checkBox_6 = new Gui::PrefCheckBox(groupBox);
        checkBox_6->setObjectName(QString::fromUtf8("checkBox_6"));
        checkBox_6->setLayoutDirection(Qt::RightToLeft);
        checkBox_6->setProperty("prefEntry", QVariant(QByteArray("NewFileGradient")));
        checkBox_6->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout->addWidget(checkBox_6, 12, 1, 1, 1);


        verticalLayout->addWidget(groupBox);

        groupBox_4 = new QGroupBox(DlgStartPreferences);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_3 = new QGridLayout(groupBox_4);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        AutoloadModuleCombo = new QComboBox(groupBox_4);
        AutoloadModuleCombo->setObjectName(QString::fromUtf8("AutoloadModuleCombo"));

        gridLayout_3->addWidget(AutoloadModuleCombo, 0, 1, 1, 1);

        checkBox_2 = new Gui::PrefCheckBox(groupBox_4);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
        checkBox_2->setLayoutDirection(Qt::RightToLeft);
        checkBox_2->setProperty("prefEntry", QVariant(QByteArray("closeStart")));
        checkBox_2->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout_3->addWidget(checkBox_2, 1, 1, 1, 1);

        autoModuleLabel = new QLabel(groupBox_4);
        autoModuleLabel->setObjectName(QString::fromUtf8("autoModuleLabel"));

        gridLayout_3->addWidget(autoModuleLabel, 0, 0, 1, 1);

        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_3->addWidget(label_11, 1, 0, 1, 1);

        label_13 = new QLabel(groupBox_4);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_3->addWidget(label_13, 2, 0, 1, 1);

        checkBox_3 = new Gui::PrefCheckBox(groupBox_4);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setLayoutDirection(Qt::RightToLeft);
        checkBox_3->setProperty("prefEntry", QVariant(QByteArray("DoNotShowOnOpen")));
        checkBox_3->setProperty("prefPath", QVariant(QByteArray("Mod/Start")));

        gridLayout_3->addWidget(checkBox_3, 2, 1, 1, 1);


        verticalLayout->addWidget(groupBox_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(DlgStartPreferences);

        QMetaObject::connectSlotsByName(DlgStartPreferences);
    } // setupUi

    void retranslateUi(QWidget *DlgStartPreferences)
    {
        DlgStartPreferences->setWindowTitle(QApplication::translate("DlgStartPreferences", "Start page options", nullptr));
        groupBox_2->setTitle(QApplication::translate("DlgStartPreferences", "Start page template", nullptr));
#ifndef QT_NO_TOOLTIP
        fileChooser_1->setToolTip(QApplication::translate("DlgStartPreferences", "An optional HTML template that will be used instead of the default start page.", nullptr));
#endif // QT_NO_TOOLTIP
        groupBox_3->setTitle(QApplication::translate("DlgStartPreferences", "Contents", nullptr));
        label_14->setText(QApplication::translate("DlgStartPreferences", "Show forum", nullptr));
        label_9->setText(QApplication::translate("DlgStartPreferences", "Show examples folder contents", nullptr));
        label_10->setText(QApplication::translate("DlgStartPreferences", "Show additional folder", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBox_1->setToolTip(QApplication::translate("DlgStartPreferences", "If you want the examples to show on the first page", nullptr));
#endif // QT_NO_TOOLTIP
        checkBox_1->setText(QString());
#ifndef QT_NO_TOOLTIP
        checkBox_4->setToolTip(QApplication::translate("DlgStartPreferences", "If this is checked, the latest posts from the FreeCAD forum will be displayed on the Activity tab", nullptr));
#endif // QT_NO_TOOLTIP
        checkBox_4->setText(QString());
#ifndef QT_NO_TOOLTIP
        fileChooser_3->setToolTip(QApplication::translate("DlgStartPreferences", "An optional custom folder to be displayed at the bottom of the first page", nullptr));
#endif // QT_NO_TOOLTIP
        label_17->setText(QApplication::translate("DlgStartPreferences", "Show notepad", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBox->setToolTip(QApplication::translate("DlgStartPreferences", "Shows a notepad next to the file thumbnails, where you can keep notes across sessions", nullptr));
#endif // QT_NO_TOOLTIP
        checkBox->setText(QString());
        groupBox->setTitle(QApplication::translate("DlgStartPreferences", "Fonts and colors", nullptr));
#ifndef QT_NO_TOOLTIP
        colorButton_3->setToolTip(QApplication::translate("DlgStartPreferences", "The background of the main start page area", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("DlgStartPreferences", "Background color", nullptr));
        radioButton_2->setText(QApplication::translate("DlgStartPreferences", "in FreeCAD", nullptr));
        radioButton_1->setText(QApplication::translate("DlgStartPreferences", "In external browser", nullptr));
        label_12->setText(QApplication::translate("DlgStartPreferences", "Background color down gradient", nullptr));
#ifndef QT_NO_TOOLTIP
        colorButton_2->setToolTip(QApplication::translate("DlgStartPreferences", "The color of the version text", nullptr));
#endif // QT_NO_TOOLTIP
        label_8->setText(QApplication::translate("DlgStartPreferences", "Link color", nullptr));
#ifndef QT_NO_TOOLTIP
        fileChooser_2->setToolTip(QApplication::translate("DlgStartPreferences", "An optional image to display as background", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        checkBox_5->setToolTip(QApplication::translate("DlgStartPreferences", "If this is checked and a style sheet is specified in General preferences, it will be used and override the colors below", nullptr));
#endif // QT_NO_TOOLTIP
        checkBox_5->setText(QString());
        label_4->setText(QApplication::translate("DlgStartPreferences", "Page background color", nullptr));
#ifndef QT_NO_TOOLTIP
        colorButton_4->setToolTip(QApplication::translate("DlgStartPreferences", "The color of the text on the main pages", nullptr));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("DlgStartPreferences", "Background image", nullptr));
        label_5->setText(QApplication::translate("DlgStartPreferences", "Page text color", nullptr));
#ifndef QT_NO_TOOLTIP
        colorButton_6->setToolTip(QApplication::translate("DlgStartPreferences", "The color of the links", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        colorButton_5->setToolTip(QApplication::translate("DlgStartPreferences", "The background color of the boxes inside the pages", nullptr));
#endif // QT_NO_TOOLTIP
        label_6->setText(QApplication::translate("DlgStartPreferences", "Box background color", nullptr));
#ifndef QT_NO_TOOLTIP
        colorButton_1->setToolTip(QApplication::translate("DlgStartPreferences", "The background color behind the panels", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        colorButton_7->setToolTip(QApplication::translate("DlgStartPreferences", "The down gradient for the background color (currently unsupported)", nullptr));
#endif // QT_NO_TOOLTIP
        label_7->setText(QApplication::translate("DlgStartPreferences", "Open links", nullptr));
        label_3->setText(QApplication::translate("DlgStartPreferences", "Background text color", nullptr));
        label_15->setText(QApplication::translate("DlgStartPreferences", "Use FreeCAD style sheet", nullptr));
        label_16->setText(QApplication::translate("DlgStartPreferences", "Font family", nullptr));
#ifndef QT_NO_TOOLTIP
        lineEdit->setToolTip(QApplication::translate("DlgStartPreferences", "The font family to use on the start page. Can be a font name or a comma-separated series of fallback fonts", nullptr));
#endif // QT_NO_TOOLTIP
        lineEdit->setText(QString());
        lineEdit->setPlaceholderText(QApplication::translate("DlgStartPreferences", "Arial,Helvetica,sans", nullptr));
#ifndef QT_NO_TOOLTIP
        spinBox->setToolTip(QApplication::translate("DlgStartPreferences", "The base font size to use for all texts of the Start page", nullptr));
#endif // QT_NO_TOOLTIP
        spinBox->setSuffix(QApplication::translate("DlgStartPreferences", "px", nullptr));
        label_18->setText(QApplication::translate("DlgStartPreferences", "Use gradient for New File icon", nullptr));
        checkBox_6->setText(QString());
        groupBox_4->setTitle(QApplication::translate("DlgStartPreferences", "Options", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBox_2->setToolTip(QApplication::translate("DlgStartPreferences", "Should the start page be closed after loading?", nullptr));
#endif // QT_NO_TOOLTIP
        checkBox_2->setText(QString());
        autoModuleLabel->setText(QApplication::translate("DlgStartPreferences", "Switch workbench after loading", nullptr));
        label_11->setText(QApplication::translate("DlgStartPreferences", "Close start page after loading", nullptr));
        label_13->setText(QApplication::translate("DlgStartPreferences", "Close and switch on opening file", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBox_3->setToolTip(QApplication::translate("DlgStartPreferences", "If application is started by opening a file, apply the two settings above", nullptr));
#endif // QT_NO_TOOLTIP
        checkBox_3->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class DlgStartPreferences: public Ui_DlgStartPreferences {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLGSTARTPREFERENCES_H
