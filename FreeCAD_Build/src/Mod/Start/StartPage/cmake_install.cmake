# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Start/StartPage" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/StartPage.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/TranslationTexts.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/__init__.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/Start/StartPage" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/LoadMRU.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/LoadExample.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/LoadNew.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/LoadCustom.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/StartPage.css"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/StartPage.js"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/StartPage.html"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/EnableDownload.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/Start/StartPage/images" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/images/userhub.png"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/images/poweruserhub.png"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/images/developerhub.png"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/images/manual.png"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/images/freecad.png"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/images/installed.png"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Start/StartPage/images/new_file_thumbnail.svg"
    )
endif()

