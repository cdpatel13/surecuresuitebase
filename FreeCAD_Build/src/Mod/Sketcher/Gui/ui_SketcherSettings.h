/********************************************************************************
** Form generated from reading UI file 'SketcherSettings.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SKETCHERSETTINGS_H
#define UI_SKETCHERSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

namespace SketcherGui {

class Ui_SketcherSettings
{
public:
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_22;
    Gui::PrefCheckBox *checkBoxNotifyConstraintSubstitutions;
    QLabel *label_6;
    Gui::PrefSpinBox *EditSketcherFontSize;
    QComboBox *comboBox;
    Gui::PrefCheckBox *dialogOnDistanceConstraint;
    QLabel *label_7;
    Gui::PrefCheckBox *continueMode;
    Gui::PrefCheckBox *constraintMode;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_2;
    Gui::PrefCheckBox *checkBoxTVHideDependent;
    Gui::PrefCheckBox *checkBoxTVShowLinks;
    Gui::PrefCheckBox *checkBoxTVShowSupport;
    Gui::PrefCheckBox *checkBoxTVRestoreCamera;
    QLabel *label_9;
    QPushButton *btnTVApply;
    QLabel *label_10;
    Gui::PrefSpinBox *SegmentsPerGeometry;
    Gui::PrefCheckBox *checkBoxHideUnits;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_4;
    Gui::PrefCheckBox *checkBoxAdvancedSolverTaskBox;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_5;
    Gui::PrefCheckBox *checkBoxRecalculateInitialSolutionWhileDragging;

    void setupUi(QWidget *SketcherGui__SketcherSettings)
    {
        if (SketcherGui__SketcherSettings->objectName().isEmpty())
            SketcherGui__SketcherSettings->setObjectName(QString::fromUtf8("SketcherGui__SketcherSettings"));
        SketcherGui__SketcherSettings->resize(602, 720);
        gridLayout_3 = new QGridLayout(SketcherGui__SketcherSettings);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        groupBox = new QGroupBox(SketcherGui__SketcherSettings);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox_5 = new QGroupBox(groupBox);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox_5->sizePolicy().hasHeightForWidth());
        groupBox_5->setSizePolicy(sizePolicy);
        groupBox_5->setMinimumSize(QSize(0, 0));
        groupBox_5->setMaximumSize(QSize(16777215, 16777215));
        groupBox_5->setBaseSize(QSize(0, 0));
        verticalLayout_22 = new QVBoxLayout(groupBox_5);
        verticalLayout_22->setObjectName(QString::fromUtf8("verticalLayout_22"));
        checkBoxNotifyConstraintSubstitutions = new Gui::PrefCheckBox(groupBox_5);
        checkBoxNotifyConstraintSubstitutions->setObjectName(QString::fromUtf8("checkBoxNotifyConstraintSubstitutions"));
        checkBoxNotifyConstraintSubstitutions->setChecked(true);
        checkBoxNotifyConstraintSubstitutions->setProperty("prefEntry", QVariant(QByteArray("NotifyConstraintSubstitutions")));
        checkBoxNotifyConstraintSubstitutions->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher/General")));

        verticalLayout_22->addWidget(checkBoxNotifyConstraintSubstitutions);


        gridLayout->addWidget(groupBox_5, 8, 0, 1, 2);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(182, 0));

        gridLayout->addWidget(label_6, 0, 0, 1, 1);

        EditSketcherFontSize = new Gui::PrefSpinBox(groupBox);
        EditSketcherFontSize->setObjectName(QString::fromUtf8("EditSketcherFontSize"));
        EditSketcherFontSize->setMinimum(1);
        EditSketcherFontSize->setMaximum(100);
        EditSketcherFontSize->setValue(17);
        EditSketcherFontSize->setProperty("prefEntry", QVariant(QByteArray("EditSketcherFontSize")));
        EditSketcherFontSize->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout->addWidget(EditSketcherFontSize, 0, 1, 1, 1);

        comboBox = new QComboBox(groupBox);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 1, 1, 1, 1);

        dialogOnDistanceConstraint = new Gui::PrefCheckBox(groupBox);
        dialogOnDistanceConstraint->setObjectName(QString::fromUtf8("dialogOnDistanceConstraint"));
        dialogOnDistanceConstraint->setChecked(true);
        dialogOnDistanceConstraint->setProperty("prefEntry", QVariant(QByteArray("ShowDialogOnDistanceConstraint")));
        dialogOnDistanceConstraint->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        gridLayout->addWidget(dialogOnDistanceConstraint, 3, 0, 1, 2);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 1, 0, 1, 1);

        continueMode = new Gui::PrefCheckBox(groupBox);
        continueMode->setObjectName(QString::fromUtf8("continueMode"));
        continueMode->setChecked(true);
        continueMode->setProperty("prefEntry", QVariant(QByteArray("ContinuousCreationMode")));
        continueMode->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        gridLayout->addWidget(continueMode, 4, 0, 1, 2);

        constraintMode = new Gui::PrefCheckBox(groupBox);
        constraintMode->setObjectName(QString::fromUtf8("constraintMode"));
        constraintMode->setChecked(true);
        constraintMode->setProperty("prefEntry", QVariant(QByteArray("ContinuousConstraintMode")));
        constraintMode->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        gridLayout->addWidget(constraintMode, 5, 0, 1, 2);

        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setEnabled(true);
        sizePolicy.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy);
        groupBox_3->setMinimumSize(QSize(0, 0));
        groupBox_3->setMaximumSize(QSize(16777215, 16777215));
        groupBox_3->setBaseSize(QSize(0, 0));
        verticalLayout_2 = new QVBoxLayout(groupBox_3);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        checkBoxTVHideDependent = new Gui::PrefCheckBox(groupBox_3);
        checkBoxTVHideDependent->setObjectName(QString::fromUtf8("checkBoxTVHideDependent"));
        checkBoxTVHideDependent->setChecked(true);
        checkBoxTVHideDependent->setProperty("prefEntry", QVariant(QByteArray("HideDependent")));
        checkBoxTVHideDependent->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher/General")));

        verticalLayout_2->addWidget(checkBoxTVHideDependent);

        checkBoxTVShowLinks = new Gui::PrefCheckBox(groupBox_3);
        checkBoxTVShowLinks->setObjectName(QString::fromUtf8("checkBoxTVShowLinks"));
        checkBoxTVShowLinks->setChecked(true);
        checkBoxTVShowLinks->setProperty("prefEntry", QVariant(QByteArray("ShowLinks")));
        checkBoxTVShowLinks->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher/General")));

        verticalLayout_2->addWidget(checkBoxTVShowLinks);

        checkBoxTVShowSupport = new Gui::PrefCheckBox(groupBox_3);
        checkBoxTVShowSupport->setObjectName(QString::fromUtf8("checkBoxTVShowSupport"));
        checkBoxTVShowSupport->setChecked(true);
        checkBoxTVShowSupport->setProperty("prefEntry", QVariant(QByteArray("ShowSupport")));
        checkBoxTVShowSupport->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher/General")));

        verticalLayout_2->addWidget(checkBoxTVShowSupport);

        checkBoxTVRestoreCamera = new Gui::PrefCheckBox(groupBox_3);
        checkBoxTVRestoreCamera->setObjectName(QString::fromUtf8("checkBoxTVRestoreCamera"));
        checkBoxTVRestoreCamera->setChecked(true);
        checkBoxTVRestoreCamera->setProperty("prefEntry", QVariant(QByteArray("RestoreCamera")));
        checkBoxTVRestoreCamera->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher/General")));

        verticalLayout_2->addWidget(checkBoxTVRestoreCamera);

        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_9->sizePolicy().hasHeightForWidth());
        label_9->setSizePolicy(sizePolicy1);
        label_9->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        label_9->setWordWrap(true);

        verticalLayout_2->addWidget(label_9);

        btnTVApply = new QPushButton(groupBox_3);
        btnTVApply->setObjectName(QString::fromUtf8("btnTVApply"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(btnTVApply->sizePolicy().hasHeightForWidth());
        btnTVApply->setSizePolicy(sizePolicy2);

        verticalLayout_2->addWidget(btnTVApply);


        gridLayout->addWidget(groupBox_3, 7, 0, 1, 2);

        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 2, 0, 1, 1);

        SegmentsPerGeometry = new Gui::PrefSpinBox(groupBox);
        SegmentsPerGeometry->setObjectName(QString::fromUtf8("SegmentsPerGeometry"));
        SegmentsPerGeometry->setMinimum(50);
        SegmentsPerGeometry->setMaximum(1000);
        SegmentsPerGeometry->setProperty("prefEntry", QVariant(QByteArray("SegmentsPerGeometry")));
        SegmentsPerGeometry->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout->addWidget(SegmentsPerGeometry, 2, 1, 1, 1);

        checkBoxHideUnits = new Gui::PrefCheckBox(groupBox);
        checkBoxHideUnits->setObjectName(QString::fromUtf8("checkBoxHideUnits"));
        checkBoxHideUnits->setProperty("prefEntry", QVariant(QByteArray("HideUnits")));
        checkBoxHideUnits->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        gridLayout->addWidget(checkBoxHideUnits, 6, 0, 1, 1);


        gridLayout_3->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(SketcherGui__SketcherSettings);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_4 = new QGridLayout(groupBox_2);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        checkBoxAdvancedSolverTaskBox = new Gui::PrefCheckBox(groupBox_2);
        checkBoxAdvancedSolverTaskBox->setObjectName(QString::fromUtf8("checkBoxAdvancedSolverTaskBox"));
        checkBoxAdvancedSolverTaskBox->setProperty("prefEntry", QVariant(QByteArray("ShowSolverAdvancedWidget")));
        checkBoxAdvancedSolverTaskBox->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        gridLayout_4->addWidget(checkBoxAdvancedSolverTaskBox, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 2, 0, 1, 1);

        groupBox_4 = new QGroupBox(SketcherGui__SketcherSettings);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_5 = new QGridLayout(groupBox_4);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        checkBoxRecalculateInitialSolutionWhileDragging = new Gui::PrefCheckBox(groupBox_4);
        checkBoxRecalculateInitialSolutionWhileDragging->setObjectName(QString::fromUtf8("checkBoxRecalculateInitialSolutionWhileDragging"));
        checkBoxRecalculateInitialSolutionWhileDragging->setChecked(true);
        checkBoxRecalculateInitialSolutionWhileDragging->setProperty("prefEntry", QVariant(QByteArray("RecalculateInitialSolutionWhileDragging")));
        checkBoxRecalculateInitialSolutionWhileDragging->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        gridLayout_5->addWidget(checkBoxRecalculateInitialSolutionWhileDragging, 1, 0, 1, 2);


        gridLayout_3->addWidget(groupBox_4, 3, 0, 1, 1);

        QWidget::setTabOrder(EditSketcherFontSize, comboBox);
        QWidget::setTabOrder(comboBox, dialogOnDistanceConstraint);
        QWidget::setTabOrder(dialogOnDistanceConstraint, continueMode);
        QWidget::setTabOrder(continueMode, checkBoxTVHideDependent);
        QWidget::setTabOrder(checkBoxTVHideDependent, checkBoxTVShowLinks);
        QWidget::setTabOrder(checkBoxTVShowLinks, checkBoxTVShowSupport);
        QWidget::setTabOrder(checkBoxTVShowSupport, checkBoxTVRestoreCamera);
        QWidget::setTabOrder(checkBoxTVRestoreCamera, btnTVApply);
        QWidget::setTabOrder(btnTVApply, checkBoxAdvancedSolverTaskBox);

        retranslateUi(SketcherGui__SketcherSettings);

        comboBox->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(SketcherGui__SketcherSettings);
    } // setupUi

    void retranslateUi(QWidget *SketcherGui__SketcherSettings)
    {
        SketcherGui__SketcherSettings->setWindowTitle(QApplication::translate("SketcherGui::SketcherSettings", "General", nullptr));
        groupBox->setTitle(QApplication::translate("SketcherGui::SketcherSettings", "Sketch editing", nullptr));
        groupBox_5->setTitle(QApplication::translate("SketcherGui::SketcherSettings", "Notifications", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxNotifyConstraintSubstitutions->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Notifies about automatic constraint substitutions", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxNotifyConstraintSubstitutions->setText(QApplication::translate("SketcherGui::SketcherSettings", "Notify automatic constraint substitutions", nullptr));
        label_6->setText(QApplication::translate("SketcherGui::SketcherSettings", "Font size", nullptr));
#ifndef QT_NO_TOOLTIP
        EditSketcherFontSize->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Font size used for labels and constraints", nullptr));
#endif // QT_NO_TOOLTIP
        EditSketcherFontSize->setSuffix(QApplication::translate("SketcherGui::SketcherSettings", "px", nullptr));
#ifndef QT_NO_TOOLTIP
        comboBox->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Line pattern used for grid lines", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        dialogOnDistanceConstraint->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "A dialog will pop up to input a value for new dimensional constraints", nullptr));
#endif // QT_NO_TOOLTIP
        dialogOnDistanceConstraint->setText(QApplication::translate("SketcherGui::SketcherSettings", "Ask for value after creating a dimensional constraint", nullptr));
        label_7->setText(QApplication::translate("SketcherGui::SketcherSettings", "Grid line pattern", nullptr));
#ifndef QT_NO_TOOLTIP
        continueMode->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Current sketcher creation tool will remain active after creation", nullptr));
#endif // QT_NO_TOOLTIP
        continueMode->setText(QApplication::translate("SketcherGui::SketcherSettings", "Geometry creation \"Continue Mode\"", nullptr));
#ifndef QT_NO_TOOLTIP
        constraintMode->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Current constraint creation tool will remain active after creation", nullptr));
#endif // QT_NO_TOOLTIP
        constraintMode->setText(QApplication::translate("SketcherGui::SketcherSettings", "Constraint creation \"Continue Mode\"", nullptr));
        groupBox_3->setTitle(QApplication::translate("SketcherGui::SketcherSettings", "Visibility automation", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxTVHideDependent->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "When opening sketch, hide all features that depend on it", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxTVHideDependent->setText(QApplication::translate("SketcherGui::SketcherSettings", "Hide all objects that depend on the sketch", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxTVShowLinks->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "When opening sketch, show sources for external geometry links", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxTVShowLinks->setText(QApplication::translate("SketcherGui::SketcherSettings", "Show objects used for external geometry", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxTVShowSupport->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "When opening sketch, show objects the sketch is attached to", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxTVShowSupport->setText(QApplication::translate("SketcherGui::SketcherSettings", "Show object(s) sketch is attached to", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxTVRestoreCamera->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "When closing sketch, move camera back to where it was before sketch was opened", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxTVRestoreCamera->setText(QApplication::translate("SketcherGui::SketcherSettings", "Restore camera position after editing", nullptr));
        label_9->setText(QApplication::translate("SketcherGui::SketcherSettings", "Note: these settings are defaults applied to new sketches. The behavior is remembered for each sketch individually as properties on View tab.", nullptr));
#ifndef QT_NO_TOOLTIP
        btnTVApply->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Applies current visibility automation settings to all sketches in open documents", nullptr));
#endif // QT_NO_TOOLTIP
        btnTVApply->setText(QApplication::translate("SketcherGui::SketcherSettings", "Apply to existing sketches", nullptr));
        label_10->setText(QApplication::translate("SketcherGui::SketcherSettings", "Segments per geometry", nullptr));
#ifndef QT_NO_TOOLTIP
        SegmentsPerGeometry->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Number of polygons for geometry approximation", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        checkBoxHideUnits->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Base length units will not be displayed in constraints.\n"
"Supports all unit systems except 'US customary' and 'Building US/Euro'.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxHideUnits->setText(QApplication::translate("SketcherGui::SketcherSettings", "Hide base length units for supported unit systems", nullptr));
        groupBox_2->setTitle(QApplication::translate("SketcherGui::SketcherSettings", "Sketcher solver", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxAdvancedSolverTaskBox->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Sketcher dialog will have additional section\n"
"'Advanced solver control' to adjust solver settings", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxAdvancedSolverTaskBox->setText(QApplication::translate("SketcherGui::SketcherSettings", "Show section 'Advanced solver control' in task dialog", nullptr));
        groupBox_4->setTitle(QApplication::translate("SketcherGui::SketcherSettings", "Dragging performance", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxRecalculateInitialSolutionWhileDragging->setToolTip(QApplication::translate("SketcherGui::SketcherSettings", "Special solver algorithm will be used while dragging sketch elements.\n"
"Requires to re-enter edit mode to take effect.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxRecalculateInitialSolutionWhileDragging->setText(QApplication::translate("SketcherGui::SketcherSettings", "Improve solving while dragging", nullptr));
    } // retranslateUi

};

} // namespace SketcherGui

namespace SketcherGui {
namespace Ui {
    class SketcherSettings: public Ui_SketcherSettings {};
} // namespace Ui
} // namespace SketcherGui

#endif // UI_SKETCHERSETTINGS_H
