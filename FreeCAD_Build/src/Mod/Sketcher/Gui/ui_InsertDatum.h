/********************************************************************************
** Form generated from reading UI file 'InsertDatum.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INSERTDATUM_H
#define UI_INSERTDATUM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include "Gui/PrefWidgets.h"

namespace SketcherGui {

class Ui_InsertDatum
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label;
    Gui::PrefQuantitySpinBox *labelEdit;
    QLabel *label_2;
    QLineEdit *name;
    QSpacerItem *verticalSpacer;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SketcherGui__InsertDatum)
    {
        if (SketcherGui__InsertDatum->objectName().isEmpty())
            SketcherGui__InsertDatum->setObjectName(QString::fromUtf8("SketcherGui__InsertDatum"));
        SketcherGui__InsertDatum->setWindowModality(Qt::ApplicationModal);
        SketcherGui__InsertDatum->resize(344, 122);
        verticalLayout = new QVBoxLayout(SketcherGui__InsertDatum);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(SketcherGui__InsertDatum);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        labelEdit = new Gui::PrefQuantitySpinBox(SketcherGui__InsertDatum);
        labelEdit->setObjectName(QString::fromUtf8("labelEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(labelEdit->sizePolicy().hasHeightForWidth());
        labelEdit->setSizePolicy(sizePolicy);

        gridLayout->addWidget(labelEdit, 0, 1, 1, 1);

        label_2 = new QLabel(SketcherGui__InsertDatum);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        name = new QLineEdit(SketcherGui__InsertDatum);
        name->setObjectName(QString::fromUtf8("name"));

        gridLayout->addWidget(name, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        buttonBox = new QDialogButtonBox(SketcherGui__InsertDatum);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(SketcherGui__InsertDatum);
        QObject::connect(buttonBox, SIGNAL(accepted()), SketcherGui__InsertDatum, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SketcherGui__InsertDatum, SLOT(reject()));
        QObject::connect(labelEdit, SIGNAL(showFormulaDialog(bool)), buttonBox, SLOT(setHidden(bool)));

        QMetaObject::connectSlotsByName(SketcherGui__InsertDatum);
    } // setupUi

    void retranslateUi(QDialog *SketcherGui__InsertDatum)
    {
        SketcherGui__InsertDatum->setWindowTitle(QApplication::translate("SketcherGui::InsertDatum", "Insert datum", nullptr));
        label->setText(QApplication::translate("SketcherGui::InsertDatum", "datum:", nullptr));
        label_2->setText(QApplication::translate("SketcherGui::InsertDatum", "Name (optional)", nullptr));
    } // retranslateUi

};

} // namespace SketcherGui

namespace SketcherGui {
namespace Ui {
    class InsertDatum: public Ui_InsertDatum {};
} // namespace Ui
} // namespace SketcherGui

#endif // UI_INSERTDATUM_H
