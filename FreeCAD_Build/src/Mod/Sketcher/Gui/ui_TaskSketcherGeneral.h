/********************************************************************************
** Form generated from reading UI file 'TaskSketcherGeneral.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKSKETCHERGENERAL_H
#define UI_TASKSKETCHERGENERAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

namespace SketcherGui {

class Ui_TaskSketcherGeneral
{
public:
    QVBoxLayout *verticalLayout;
    QCheckBox *checkBoxShowGrid;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    Gui::PrefQuantitySpinBox *gridSize;
    QCheckBox *checkBoxGridSnap;
    QCheckBox *checkBoxAutoconstraints;
    Gui::PrefCheckBox *checkBoxRedundantAutoconstraints;
    QLabel *label_2;
    QListWidget *renderingOrder;

    void setupUi(QWidget *SketcherGui__TaskSketcherGeneral)
    {
        if (SketcherGui__TaskSketcherGeneral->objectName().isEmpty())
            SketcherGui__TaskSketcherGeneral->setObjectName(QString::fromUtf8("SketcherGui__TaskSketcherGeneral"));
        SketcherGui__TaskSketcherGeneral->resize(275, 210);
        verticalLayout = new QVBoxLayout(SketcherGui__TaskSketcherGeneral);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        checkBoxShowGrid = new QCheckBox(SketcherGui__TaskSketcherGeneral);
        checkBoxShowGrid->setObjectName(QString::fromUtf8("checkBoxShowGrid"));
        checkBoxShowGrid->setChecked(true);

        verticalLayout->addWidget(checkBoxShowGrid);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(SketcherGui__TaskSketcherGeneral);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        gridSize = new Gui::PrefQuantitySpinBox(SketcherGui__TaskSketcherGeneral);
        gridSize->setObjectName(QString::fromUtf8("gridSize"));
        gridSize->setProperty("unit", QVariant(QString::fromUtf8("mm")));
        gridSize->setProperty("decimals", QVariant(3));
        gridSize->setProperty("maximum", QVariant(99999999.000000000000000));
        gridSize->setProperty("minimum", QVariant(0.001000000000000));
        gridSize->setProperty("singleStep", QVariant(1.000000000000000));
        gridSize->setProperty("value", QVariant(0.000000100000000));

        horizontalLayout->addWidget(gridSize);


        verticalLayout->addLayout(horizontalLayout);

        checkBoxGridSnap = new QCheckBox(SketcherGui__TaskSketcherGeneral);
        checkBoxGridSnap->setObjectName(QString::fromUtf8("checkBoxGridSnap"));
        checkBoxGridSnap->setEnabled(true);

        verticalLayout->addWidget(checkBoxGridSnap);

        checkBoxAutoconstraints = new QCheckBox(SketcherGui__TaskSketcherGeneral);
        checkBoxAutoconstraints->setObjectName(QString::fromUtf8("checkBoxAutoconstraints"));
        checkBoxAutoconstraints->setEnabled(true);
        checkBoxAutoconstraints->setChecked(true);

        verticalLayout->addWidget(checkBoxAutoconstraints);

        checkBoxRedundantAutoconstraints = new Gui::PrefCheckBox(SketcherGui__TaskSketcherGeneral);
        checkBoxRedundantAutoconstraints->setObjectName(QString::fromUtf8("checkBoxRedundantAutoconstraints"));
        checkBoxRedundantAutoconstraints->setChecked(true);
        checkBoxRedundantAutoconstraints->setProperty("prefEntry", QVariant(QByteArray("AvoidRedundantAutoconstraints")));
        checkBoxRedundantAutoconstraints->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        verticalLayout->addWidget(checkBoxRedundantAutoconstraints);

        label_2 = new QLabel(SketcherGui__TaskSketcherGeneral);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        renderingOrder = new QListWidget(SketcherGui__TaskSketcherGeneral);
        renderingOrder->setObjectName(QString::fromUtf8("renderingOrder"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(renderingOrder->sizePolicy().hasHeightForWidth());
        renderingOrder->setSizePolicy(sizePolicy);
        renderingOrder->setMinimumSize(QSize(12, 60));
        renderingOrder->setDragEnabled(true);
        renderingOrder->setDragDropMode(QAbstractItemView::InternalMove);
        renderingOrder->setResizeMode(QListView::Fixed);
        renderingOrder->setSortingEnabled(false);

        verticalLayout->addWidget(renderingOrder);


        retranslateUi(SketcherGui__TaskSketcherGeneral);

        QMetaObject::connectSlotsByName(SketcherGui__TaskSketcherGeneral);
    } // setupUi

    void retranslateUi(QWidget *SketcherGui__TaskSketcherGeneral)
    {
        SketcherGui__TaskSketcherGeneral->setWindowTitle(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Form", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxShowGrid->setToolTip(QApplication::translate("SketcherGui::TaskSketcherGeneral", "A grid will be shown", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxShowGrid->setText(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Show grid", nullptr));
        label->setText(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Grid size:", nullptr));
#ifndef QT_NO_TOOLTIP
        gridSize->setToolTip(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Distance between two subsequent grid lines", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        checkBoxGridSnap->setToolTip(QApplication::translate("SketcherGui::TaskSketcherGeneral", "New points will snap to the nearest grid line.\n"
"Points must be set closer than a fifth of the grid size to a grid line to snap.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxGridSnap->setText(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Grid snap", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxAutoconstraints->setToolTip(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Sketcher proposes automatically sensible constraints.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxAutoconstraints->setText(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Auto constraints", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxRedundantAutoconstraints->setToolTip(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Sketcher tries not to propose redundant auto constraints", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxRedundantAutoconstraints->setText(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Avoid redundant auto constraints", nullptr));
        label_2->setText(QApplication::translate("SketcherGui::TaskSketcherGeneral", "Rendering order:", nullptr));
#ifndef QT_NO_TOOLTIP
        renderingOrder->setToolTip(QApplication::translate("SketcherGui::TaskSketcherGeneral", "To change, drag and drop a geometry type to top or bottom", nullptr));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

} // namespace SketcherGui

namespace SketcherGui {
namespace Ui {
    class TaskSketcherGeneral: public Ui_TaskSketcherGeneral {};
} // namespace Ui
} // namespace SketcherGui

#endif // UI_TASKSKETCHERGENERAL_H
