/****************************************************************************
** Meta object code from reading C++ file 'TaskSketcherConstrains.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Sketcher/Gui/TaskSketcherConstrains.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskSketcherConstrains.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SketcherGui__ConstraintView_t {
    QByteArrayData data[18];
    char stringdata0[317];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__ConstraintView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__ConstraintView_t qt_meta_stringdata_SketcherGui__ConstraintView = {
    {
QT_MOC_LITERAL(0, 0, 27), // "SketcherGui::ConstraintView"
QT_MOC_LITERAL(1, 28, 21), // "onUpdateDrivingStatus"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(4, 68, 4), // "item"
QT_MOC_LITERAL(5, 73, 6), // "status"
QT_MOC_LITERAL(6, 80, 20), // "onUpdateActiveStatus"
QT_MOC_LITERAL(7, 101, 23), // "emitCenterSelectedItems"
QT_MOC_LITERAL(8, 125, 17), // "modifyCurrentItem"
QT_MOC_LITERAL(9, 143, 17), // "renameCurrentItem"
QT_MOC_LITERAL(10, 161, 19), // "centerSelectedItems"
QT_MOC_LITERAL(11, 181, 19), // "deleteSelectedItems"
QT_MOC_LITERAL(12, 201, 19), // "doSelectConstraints"
QT_MOC_LITERAL(13, 221, 19), // "updateDrivingStatus"
QT_MOC_LITERAL(14, 241, 18), // "updateActiveStatus"
QT_MOC_LITERAL(15, 260, 24), // "swapNamedOfSelectedItems"
QT_MOC_LITERAL(16, 285, 15), // "showConstraints"
QT_MOC_LITERAL(17, 301, 15) // "hideConstraints"

    },
    "SketcherGui::ConstraintView\0"
    "onUpdateDrivingStatus\0\0QListWidgetItem*\0"
    "item\0status\0onUpdateActiveStatus\0"
    "emitCenterSelectedItems\0modifyCurrentItem\0"
    "renameCurrentItem\0centerSelectedItems\0"
    "deleteSelectedItems\0doSelectConstraints\0"
    "updateDrivingStatus\0updateActiveStatus\0"
    "swapNamedOfSelectedItems\0showConstraints\0"
    "hideConstraints"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__ConstraintView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   79,    2, 0x06 /* Public */,
       6,    2,   84,    2, 0x06 /* Public */,
       7,    0,   89,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   90,    2, 0x09 /* Protected */,
       9,    0,   91,    2, 0x09 /* Protected */,
      10,    0,   92,    2, 0x09 /* Protected */,
      11,    0,   93,    2, 0x09 /* Protected */,
      12,    0,   94,    2, 0x09 /* Protected */,
      13,    0,   95,    2, 0x09 /* Protected */,
      14,    0,   96,    2, 0x09 /* Protected */,
      15,    0,   97,    2, 0x09 /* Protected */,
      16,    0,   98,    2, 0x09 /* Protected */,
      17,    0,   99,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::Bool,    4,    5,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Bool,    4,    5,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SketcherGui::ConstraintView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ConstraintView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onUpdateDrivingStatus((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 1: _t->onUpdateActiveStatus((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 2: _t->emitCenterSelectedItems(); break;
        case 3: _t->modifyCurrentItem(); break;
        case 4: _t->renameCurrentItem(); break;
        case 5: _t->centerSelectedItems(); break;
        case 6: _t->deleteSelectedItems(); break;
        case 7: _t->doSelectConstraints(); break;
        case 8: _t->updateDrivingStatus(); break;
        case 9: _t->updateActiveStatus(); break;
        case 10: _t->swapNamedOfSelectedItems(); break;
        case 11: _t->showConstraints(); break;
        case 12: _t->hideConstraints(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ConstraintView::*)(QListWidgetItem * , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ConstraintView::onUpdateDrivingStatus)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ConstraintView::*)(QListWidgetItem * , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ConstraintView::onUpdateActiveStatus)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ConstraintView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ConstraintView::emitCenterSelectedItems)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::ConstraintView::staticMetaObject = { {
    &QListWidget::staticMetaObject,
    qt_meta_stringdata_SketcherGui__ConstraintView.data,
    qt_meta_data_SketcherGui__ConstraintView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::ConstraintView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::ConstraintView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__ConstraintView.stringdata0))
        return static_cast<void*>(this);
    return QListWidget::qt_metacast(_clname);
}

int SketcherGui::ConstraintView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QListWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void SketcherGui::ConstraintView::onUpdateDrivingStatus(QListWidgetItem * _t1, bool _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SketcherGui::ConstraintView::onUpdateActiveStatus(QListWidgetItem * _t1, bool _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void SketcherGui::ConstraintView::emitCenterSelectedItems()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
struct qt_meta_stringdata_SketcherGui__TaskSketcherConstrains_t {
    QByteArrayData data[15];
    char stringdata0[446];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__TaskSketcherConstrains_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__TaskSketcherConstrains_t qt_meta_stringdata_SketcherGui__TaskSketcherConstrains = {
    {
QT_MOC_LITERAL(0, 0, 35), // "SketcherGui::TaskSketcherCons..."
QT_MOC_LITERAL(1, 36, 37), // "on_comboBoxFilter_currentInde..."
QT_MOC_LITERAL(2, 74, 0), // ""
QT_MOC_LITERAL(3, 75, 45), // "on_listWidgetConstraints_item..."
QT_MOC_LITERAL(4, 121, 38), // "on_listWidgetConstraints_item..."
QT_MOC_LITERAL(5, 160, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(6, 177, 4), // "item"
QT_MOC_LITERAL(7, 182, 36), // "on_listWidgetConstraints_item..."
QT_MOC_LITERAL(8, 219, 44), // "on_listWidgetConstraints_upda..."
QT_MOC_LITERAL(9, 264, 6), // "status"
QT_MOC_LITERAL(10, 271, 43), // "on_listWidgetConstraints_upda..."
QT_MOC_LITERAL(11, 315, 48), // "on_listWidgetConstraints_emit..."
QT_MOC_LITERAL(12, 364, 39), // "on_filterInternalAlignment_st..."
QT_MOC_LITERAL(13, 404, 5), // "state"
QT_MOC_LITERAL(14, 410, 35) // "on_extendedInformation_stateC..."

    },
    "SketcherGui::TaskSketcherConstrains\0"
    "on_comboBoxFilter_currentIndexChanged\0"
    "\0on_listWidgetConstraints_itemSelectionChanged\0"
    "on_listWidgetConstraints_itemActivated\0"
    "QListWidgetItem*\0item\0"
    "on_listWidgetConstraints_itemChanged\0"
    "on_listWidgetConstraints_updateDrivingStatus\0"
    "status\0on_listWidgetConstraints_updateActiveStatus\0"
    "on_listWidgetConstraints_emitCenterSelectedItems\0"
    "on_filterInternalAlignment_stateChanged\0"
    "state\0on_extendedInformation_stateChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__TaskSketcherConstrains[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x0a /* Public */,
       3,    0,   62,    2, 0x0a /* Public */,
       4,    1,   63,    2, 0x0a /* Public */,
       7,    1,   66,    2, 0x0a /* Public */,
       8,    2,   69,    2, 0x0a /* Public */,
      10,    2,   74,    2, 0x0a /* Public */,
      11,    0,   79,    2, 0x0a /* Public */,
      12,    1,   80,    2, 0x0a /* Public */,
      14,    1,   83,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5, QMetaType::Bool,    6,    9,
    QMetaType::Void, 0x80000000 | 5, QMetaType::Bool,    6,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,

       0        // eod
};

void SketcherGui::TaskSketcherConstrains::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskSketcherConstrains *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_comboBoxFilter_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_listWidgetConstraints_itemSelectionChanged(); break;
        case 2: _t->on_listWidgetConstraints_itemActivated((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 3: _t->on_listWidgetConstraints_itemChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 4: _t->on_listWidgetConstraints_updateDrivingStatus((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 5: _t->on_listWidgetConstraints_updateActiveStatus((*reinterpret_cast< QListWidgetItem*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 6: _t->on_listWidgetConstraints_emitCenterSelectedItems(); break;
        case 7: _t->on_filterInternalAlignment_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_extendedInformation_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::TaskSketcherConstrains::staticMetaObject = { {
    &Gui::TaskView::TaskBox::staticMetaObject,
    qt_meta_stringdata_SketcherGui__TaskSketcherConstrains.data,
    qt_meta_data_SketcherGui__TaskSketcherConstrains,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::TaskSketcherConstrains::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::TaskSketcherConstrains::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__TaskSketcherConstrains.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionObserver"))
        return static_cast< Gui::SelectionObserver*>(this);
    return Gui::TaskView::TaskBox::qt_metacast(_clname);
}

int SketcherGui::TaskSketcherConstrains::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
