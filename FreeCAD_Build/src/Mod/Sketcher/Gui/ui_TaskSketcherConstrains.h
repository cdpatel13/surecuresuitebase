/********************************************************************************
** Form generated from reading UI file 'TaskSketcherConstrains.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKSKETCHERCONSTRAINS_H
#define UI_TASKSKETCHERCONSTRAINS_H

#include <QListWidget>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

namespace SketcherGui {

class Ui_TaskSketcherConstrains
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *comboBoxFilter;
    Gui::PrefCheckBox *filterInternalAlignment;
    Gui::PrefCheckBox *extendedInformation;
    ConstraintView *listWidgetConstraints;

    void setupUi(QWidget *SketcherGui__TaskSketcherConstrains)
    {
        if (SketcherGui__TaskSketcherConstrains->objectName().isEmpty())
            SketcherGui__TaskSketcherConstrains->setObjectName(QString::fromUtf8("SketcherGui__TaskSketcherConstrains"));
        SketcherGui__TaskSketcherConstrains->resize(212, 288);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SketcherGui__TaskSketcherConstrains->sizePolicy().hasHeightForWidth());
        SketcherGui__TaskSketcherConstrains->setSizePolicy(sizePolicy);
        SketcherGui__TaskSketcherConstrains->setMaximumSize(QSize(16777215, 288));
        verticalLayout = new QVBoxLayout(SketcherGui__TaskSketcherConstrains);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(SketcherGui__TaskSketcherConstrains);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        comboBoxFilter = new QComboBox(SketcherGui__TaskSketcherConstrains);
        comboBoxFilter->addItem(QString());
        comboBoxFilter->addItem(QString());
        comboBoxFilter->addItem(QString());
        comboBoxFilter->addItem(QString());
        comboBoxFilter->addItem(QString());
        comboBoxFilter->setObjectName(QString::fromUtf8("comboBoxFilter"));

        horizontalLayout->addWidget(comboBoxFilter);


        verticalLayout->addLayout(horizontalLayout);

        filterInternalAlignment = new Gui::PrefCheckBox(SketcherGui__TaskSketcherConstrains);
        filterInternalAlignment->setObjectName(QString::fromUtf8("filterInternalAlignment"));
        filterInternalAlignment->setChecked(true);
        filterInternalAlignment->setProperty("prefEntry", QVariant(QByteArray("HideInternalAlignment")));
        filterInternalAlignment->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        verticalLayout->addWidget(filterInternalAlignment);

        extendedInformation = new Gui::PrefCheckBox(SketcherGui__TaskSketcherConstrains);
        extendedInformation->setObjectName(QString::fromUtf8("extendedInformation"));
        extendedInformation->setChecked(false);
        extendedInformation->setProperty("prefEntry", QVariant(QByteArray("ExtendedConstraintInformation")));
        extendedInformation->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        verticalLayout->addWidget(extendedInformation);

        listWidgetConstraints = new ConstraintView(SketcherGui__TaskSketcherConstrains);
        listWidgetConstraints->setObjectName(QString::fromUtf8("listWidgetConstraints"));
        listWidgetConstraints->setModelColumn(0);

        verticalLayout->addWidget(listWidgetConstraints);


        retranslateUi(SketcherGui__TaskSketcherConstrains);

        comboBoxFilter->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SketcherGui__TaskSketcherConstrains);
    } // setupUi

    void retranslateUi(QWidget *SketcherGui__TaskSketcherConstrains)
    {
        SketcherGui__TaskSketcherConstrains->setWindowTitle(QApplication::translate("SketcherGui::TaskSketcherConstrains", "Form", nullptr));
        label->setText(QApplication::translate("SketcherGui::TaskSketcherConstrains", "Filter:", nullptr));
        comboBoxFilter->setItemText(0, QApplication::translate("SketcherGui::TaskSketcherConstrains", "All", nullptr));
        comboBoxFilter->setItemText(1, QApplication::translate("SketcherGui::TaskSketcherConstrains", "Normal", nullptr));
        comboBoxFilter->setItemText(2, QApplication::translate("SketcherGui::TaskSketcherConstrains", "Datums", nullptr));
        comboBoxFilter->setItemText(3, QApplication::translate("SketcherGui::TaskSketcherConstrains", "Named", nullptr));
        comboBoxFilter->setItemText(4, QApplication::translate("SketcherGui::TaskSketcherConstrains", "Reference", nullptr));

#ifndef QT_NO_TOOLTIP
        filterInternalAlignment->setToolTip(QApplication::translate("SketcherGui::TaskSketcherConstrains", "Internal alignments will be hidden", nullptr));
#endif // QT_NO_TOOLTIP
        filterInternalAlignment->setText(QApplication::translate("SketcherGui::TaskSketcherConstrains", "Hide internal alignment", nullptr));
#ifndef QT_NO_TOOLTIP
        extendedInformation->setToolTip(QApplication::translate("SketcherGui::TaskSketcherConstrains", "Extended information will be added to the list", nullptr));
#endif // QT_NO_TOOLTIP
        extendedInformation->setText(QApplication::translate("SketcherGui::TaskSketcherConstrains", "Extended information", nullptr));
    } // retranslateUi

};

} // namespace SketcherGui

namespace SketcherGui {
namespace Ui {
    class TaskSketcherConstrains: public Ui_TaskSketcherConstrains {};
} // namespace Ui
} // namespace SketcherGui

#endif // UI_TASKSKETCHERCONSTRAINS_H
