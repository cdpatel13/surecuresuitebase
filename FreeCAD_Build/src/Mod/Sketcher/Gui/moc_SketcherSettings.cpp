/****************************************************************************
** Meta object code from reading C++ file 'SketcherSettings.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Sketcher/Gui/SketcherSettings.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SketcherSettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SketcherGui__SketcherSettings_t {
    QByteArrayData data[3];
    char stringdata0[51];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__SketcherSettings_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__SketcherSettings_t qt_meta_stringdata_SketcherGui__SketcherSettings = {
    {
QT_MOC_LITERAL(0, 0, 29), // "SketcherGui::SketcherSettings"
QT_MOC_LITERAL(1, 30, 19), // "onBtnTVApplyClicked"
QT_MOC_LITERAL(2, 50, 0) // ""

    },
    "SketcherGui::SketcherSettings\0"
    "onBtnTVApplyClicked\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__SketcherSettings[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void SketcherGui::SketcherSettings::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SketcherSettings *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onBtnTVApplyClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::SketcherSettings::staticMetaObject = { {
    &Gui::Dialog::PreferencePage::staticMetaObject,
    qt_meta_stringdata_SketcherGui__SketcherSettings.data,
    qt_meta_data_SketcherGui__SketcherSettings,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::SketcherSettings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::SketcherSettings::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__SketcherSettings.stringdata0))
        return static_cast<void*>(this);
    return Gui::Dialog::PreferencePage::qt_metacast(_clname);
}

int SketcherGui::SketcherSettings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::Dialog::PreferencePage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_SketcherGui__SketcherSettingsColors_t {
    QByteArrayData data[1];
    char stringdata0[36];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__SketcherSettingsColors_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__SketcherSettingsColors_t qt_meta_stringdata_SketcherGui__SketcherSettingsColors = {
    {
QT_MOC_LITERAL(0, 0, 35) // "SketcherGui::SketcherSettings..."

    },
    "SketcherGui::SketcherSettingsColors"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__SketcherSettingsColors[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void SketcherGui::SketcherSettingsColors::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::SketcherSettingsColors::staticMetaObject = { {
    &Gui::Dialog::PreferencePage::staticMetaObject,
    qt_meta_stringdata_SketcherGui__SketcherSettingsColors.data,
    qt_meta_data_SketcherGui__SketcherSettingsColors,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::SketcherSettingsColors::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::SketcherSettingsColors::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__SketcherSettingsColors.stringdata0))
        return static_cast<void*>(this);
    return Gui::Dialog::PreferencePage::qt_metacast(_clname);
}

int SketcherGui::SketcherSettingsColors::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::Dialog::PreferencePage::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
