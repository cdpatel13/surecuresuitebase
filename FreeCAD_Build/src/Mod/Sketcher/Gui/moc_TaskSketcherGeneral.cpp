/****************************************************************************
** Meta object code from reading C++ file 'TaskSketcherGeneral.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Sketcher/Gui/TaskSketcherGeneral.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskSketcherGeneral.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SketcherGui__SketcherGeneralWidget_t {
    QByteArrayData data[15];
    char stringdata0[270];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__SketcherGeneralWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__SketcherGeneralWidget_t qt_meta_stringdata_SketcherGui__SketcherGeneralWidget = {
    {
QT_MOC_LITERAL(0, 0, 34), // "SketcherGui::SketcherGeneralW..."
QT_MOC_LITERAL(1, 35, 18), // "emitToggleGridView"
QT_MOC_LITERAL(2, 54, 0), // ""
QT_MOC_LITERAL(3, 55, 18), // "emitToggleGridSnap"
QT_MOC_LITERAL(4, 74, 15), // "emitSetGridSize"
QT_MOC_LITERAL(5, 90, 25), // "emitToggleAutoconstraints"
QT_MOC_LITERAL(6, 116, 22), // "emitRenderOrderChanged"
QT_MOC_LITERAL(7, 139, 16), // "onToggleGridView"
QT_MOC_LITERAL(8, 156, 2), // "on"
QT_MOC_LITERAL(9, 159, 13), // "onSetGridSize"
QT_MOC_LITERAL(10, 173, 3), // "val"
QT_MOC_LITERAL(11, 177, 16), // "onToggleGridSnap"
QT_MOC_LITERAL(12, 194, 5), // "state"
QT_MOC_LITERAL(13, 200, 20), // "onRenderOrderChanged"
QT_MOC_LITERAL(14, 221, 48) // "on_checkBoxRedundantAutoconst..."

    },
    "SketcherGui::SketcherGeneralWidget\0"
    "emitToggleGridView\0\0emitToggleGridSnap\0"
    "emitSetGridSize\0emitToggleAutoconstraints\0"
    "emitRenderOrderChanged\0onToggleGridView\0"
    "on\0onSetGridSize\0val\0onToggleGridSnap\0"
    "state\0onRenderOrderChanged\0"
    "on_checkBoxRedundantAutoconstraints_stateChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__SketcherGeneralWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x06 /* Public */,
       3,    1,   67,    2, 0x06 /* Public */,
       4,    1,   70,    2, 0x06 /* Public */,
       5,    1,   73,    2, 0x06 /* Public */,
       6,    0,   76,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    1,   77,    2, 0x08 /* Private */,
       9,    1,   80,    2, 0x08 /* Private */,
      11,    1,   83,    2, 0x08 /* Private */,
      13,    0,   86,    2, 0x08 /* Private */,
      14,    1,   87,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void SketcherGui::SketcherGeneralWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SketcherGeneralWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->emitToggleGridView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->emitToggleGridSnap((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->emitSetGridSize((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->emitToggleAutoconstraints((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->emitRenderOrderChanged(); break;
        case 5: _t->onToggleGridView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->onSetGridSize((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->onToggleGridSnap((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->onRenderOrderChanged(); break;
        case 9: _t->on_checkBoxRedundantAutoconstraints_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (SketcherGeneralWidget::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SketcherGeneralWidget::emitToggleGridView)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (SketcherGeneralWidget::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SketcherGeneralWidget::emitToggleGridSnap)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (SketcherGeneralWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SketcherGeneralWidget::emitSetGridSize)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (SketcherGeneralWidget::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SketcherGeneralWidget::emitToggleAutoconstraints)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (SketcherGeneralWidget::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SketcherGeneralWidget::emitRenderOrderChanged)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::SketcherGeneralWidget::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_SketcherGui__SketcherGeneralWidget.data,
    qt_meta_data_SketcherGui__SketcherGeneralWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::SketcherGeneralWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::SketcherGeneralWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__SketcherGeneralWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int SketcherGui::SketcherGeneralWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void SketcherGui::SketcherGeneralWidget::emitToggleGridView(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void SketcherGui::SketcherGeneralWidget::emitToggleGridSnap(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void SketcherGui::SketcherGeneralWidget::emitSetGridSize(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void SketcherGui::SketcherGeneralWidget::emitToggleAutoconstraints(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void SketcherGui::SketcherGeneralWidget::emitRenderOrderChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
struct qt_meta_stringdata_SketcherGui__TaskSketcherGeneral_t {
    QByteArrayData data[10];
    char stringdata0[140];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__TaskSketcherGeneral_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__TaskSketcherGeneral_t qt_meta_stringdata_SketcherGui__TaskSketcherGeneral = {
    {
QT_MOC_LITERAL(0, 0, 32), // "SketcherGui::TaskSketcherGeneral"
QT_MOC_LITERAL(1, 33, 16), // "onToggleGridView"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 2), // "on"
QT_MOC_LITERAL(4, 54, 13), // "onSetGridSize"
QT_MOC_LITERAL(5, 68, 3), // "val"
QT_MOC_LITERAL(6, 72, 16), // "onToggleGridSnap"
QT_MOC_LITERAL(7, 89, 5), // "state"
QT_MOC_LITERAL(8, 95, 23), // "onToggleAutoconstraints"
QT_MOC_LITERAL(9, 119, 20) // "onRenderOrderChanged"

    },
    "SketcherGui::TaskSketcherGeneral\0"
    "onToggleGridView\0\0on\0onSetGridSize\0"
    "val\0onToggleGridSnap\0state\0"
    "onToggleAutoconstraints\0onRenderOrderChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__TaskSketcherGeneral[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x0a /* Public */,
       4,    1,   42,    2, 0x0a /* Public */,
       6,    1,   45,    2, 0x0a /* Public */,
       8,    1,   48,    2, 0x0a /* Public */,
       9,    0,   51,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void,

       0        // eod
};

void SketcherGui::TaskSketcherGeneral::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskSketcherGeneral *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onToggleGridView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->onSetGridSize((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->onToggleGridSnap((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->onToggleAutoconstraints((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->onRenderOrderChanged(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::TaskSketcherGeneral::staticMetaObject = { {
    &Gui::TaskView::TaskBox::staticMetaObject,
    qt_meta_stringdata_SketcherGui__TaskSketcherGeneral.data,
    qt_meta_data_SketcherGui__TaskSketcherGeneral,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::TaskSketcherGeneral::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::TaskSketcherGeneral::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__TaskSketcherGeneral.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionSingleton::ObserverType"))
        return static_cast< Gui::SelectionSingleton::ObserverType*>(this);
    return Gui::TaskView::TaskBox::qt_metacast(_clname);
}

int SketcherGui::TaskSketcherGeneral::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
