/********************************************************************************
** Form generated from reading UI file 'TaskSketcherMessages.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKSKETCHERMESSAGES_H
#define UI_TASKSKETCHERMESSAGES_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

QT_BEGIN_NAMESPACE

class Ui_TaskSketcherMessages
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *labelConstrainStatus;
    QLabel *labelSolverStatus;
    Gui::PrefCheckBox *autoRemoveRedundants;
    QHBoxLayout *horizontalLayout;
    Gui::PrefCheckBox *autoUpdate;
    QPushButton *manualUpdate;

    void setupUi(QWidget *TaskSketcherMessages)
    {
        if (TaskSketcherMessages->objectName().isEmpty())
            TaskSketcherMessages->setObjectName(QString::fromUtf8("TaskSketcherMessages"));
        TaskSketcherMessages->resize(253, 108);
        verticalLayout = new QVBoxLayout(TaskSketcherMessages);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        labelConstrainStatus = new QLabel(TaskSketcherMessages);
        labelConstrainStatus->setObjectName(QString::fromUtf8("labelConstrainStatus"));
        QFont font;
        font.setFamily(QString::fromUtf8("Bitstream Charter"));
        font.setPointSize(9);
        labelConstrainStatus->setFont(font);
        labelConstrainStatus->setWordWrap(true);

        verticalLayout->addWidget(labelConstrainStatus);

        labelSolverStatus = new QLabel(TaskSketcherMessages);
        labelSolverStatus->setObjectName(QString::fromUtf8("labelSolverStatus"));
        labelSolverStatus->setFont(font);
        labelSolverStatus->setWordWrap(true);

        verticalLayout->addWidget(labelSolverStatus);

        autoRemoveRedundants = new Gui::PrefCheckBox(TaskSketcherMessages);
        autoRemoveRedundants->setObjectName(QString::fromUtf8("autoRemoveRedundants"));
        autoRemoveRedundants->setChecked(false);
        autoRemoveRedundants->setProperty("prefEntry", QVariant(QByteArray("AutoRemoveRedundants")));
        autoRemoveRedundants->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        verticalLayout->addWidget(autoRemoveRedundants);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        autoUpdate = new Gui::PrefCheckBox(TaskSketcherMessages);
        autoUpdate->setObjectName(QString::fromUtf8("autoUpdate"));
        autoUpdate->setChecked(false);
        autoUpdate->setProperty("prefEntry", QVariant(QByteArray("AutoRecompute")));
        autoUpdate->setProperty("prefPath", QVariant(QByteArray("Mod/Sketcher")));

        horizontalLayout->addWidget(autoUpdate);

        manualUpdate = new QPushButton(TaskSketcherMessages);
        manualUpdate->setObjectName(QString::fromUtf8("manualUpdate"));

        horizontalLayout->addWidget(manualUpdate);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(TaskSketcherMessages);

        QMetaObject::connectSlotsByName(TaskSketcherMessages);
    } // setupUi

    void retranslateUi(QWidget *TaskSketcherMessages)
    {
        TaskSketcherMessages->setWindowTitle(QApplication::translate("TaskSketcherMessages", "Form", nullptr));
        labelConstrainStatus->setText(QApplication::translate("TaskSketcherMessages", "Undefined degrees of freedom", nullptr));
        labelSolverStatus->setText(QApplication::translate("TaskSketcherMessages", "Not solved yet", nullptr));
#ifndef QT_NO_TOOLTIP
        autoRemoveRedundants->setToolTip(QApplication::translate("TaskSketcherMessages", "New constraints that would be redundant will automatically be removed", nullptr));
#endif // QT_NO_TOOLTIP
        autoRemoveRedundants->setText(QApplication::translate("TaskSketcherMessages", "Auto remove redundants", nullptr));
#ifndef QT_NO_TOOLTIP
        autoUpdate->setToolTip(QApplication::translate("TaskSketcherMessages", "Executes a recomputation of active document after every sketch action", nullptr));
#endif // QT_NO_TOOLTIP
        autoUpdate->setText(QApplication::translate("TaskSketcherMessages", "Auto update", nullptr));
#ifndef QT_NO_TOOLTIP
        manualUpdate->setToolTip(QApplication::translate("TaskSketcherMessages", "Forces recomputation of active document", nullptr));
#endif // QT_NO_TOOLTIP
        manualUpdate->setText(QApplication::translate("TaskSketcherMessages", "Update", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskSketcherMessages: public Ui_TaskSketcherMessages {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKSKETCHERMESSAGES_H
