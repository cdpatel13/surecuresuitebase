/********************************************************************************
** Form generated from reading UI file 'SketcherSettingsColors.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SKETCHERSETTINGSCOLORS_H
#define UI_SKETCHERSETTINGSCOLORS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"
#include "Gui/Widgets.h"

namespace SketcherGui {

class Ui_SketcherSettingsColors
{
public:
    QGridLayout *gridLayout;
    QGroupBox *groupBoxSketcherColor;
    QHBoxLayout *horizontalLayout_2;
    QGridLayout *gridLayout_2;
    QLabel *label_17;
    Gui::PrefColorButton *SketchEdgeColor;
    QLabel *label_18;
    Gui::PrefColorButton *SketchVertexColor;
    QLabel *label_6;
    Gui::PrefColorButton *CreateLineColor;
    QLabel *label;
    Gui::PrefColorButton *EditedEdgeColor;
    QLabel *label_2;
    Gui::PrefColorButton *EditedVertexColor;
    QLabel *label_3;
    Gui::PrefColorButton *ConstructionColor;
    QLabel *label_20;
    Gui::PrefColorButton *ExternalColor;
    QLabel *label_4;
    Gui::PrefColorButton *FullyConstrainedColor;
    QLabel *label_14;
    Gui::PrefColorButton *ConstrainedColor;
    QLabel *label_8;
    Gui::PrefColorButton *NonDrivingConstraintColor;
    QLabel *labelexpr;
    Gui::PrefColorButton *ExprBasedConstrDimColor;
    QLabel *labeldeact;
    Gui::PrefColorButton *DeactivatedConstrDimColor;
    QLabel *label_15;
    Gui::PrefColorButton *DatumColor;
    QLabel *label_16;
    Gui::PrefSpinBox *SketcherDatumWidth;
    QLabel *label_12;
    Gui::PrefSpinBox *DefaultSketcherVertexWidth;
    QLabel *label_13;
    Gui::PrefSpinBox *DefaultSketcherLineWidth;
    QLabel *label_5;
    Gui::PrefColorButton *CursorTextColor;
    QLabel *label_19;
    Gui::PrefColorButton *CursorCrosshairColor;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *SketcherGui__SketcherSettingsColors)
    {
        if (SketcherGui__SketcherSettingsColors->objectName().isEmpty())
            SketcherGui__SketcherSettingsColors->setObjectName(QString::fromUtf8("SketcherGui__SketcherSettingsColors"));
        SketcherGui__SketcherSettingsColors->resize(438, 474);
        gridLayout = new QGridLayout(SketcherGui__SketcherSettingsColors);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBoxSketcherColor = new QGroupBox(SketcherGui__SketcherSettingsColors);
        groupBoxSketcherColor->setObjectName(QString::fromUtf8("groupBoxSketcherColor"));
        horizontalLayout_2 = new QHBoxLayout(groupBoxSketcherColor);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_17 = new QLabel(groupBoxSketcherColor);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_17, 0, 0, 1, 1);

        SketchEdgeColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        SketchEdgeColor->setObjectName(QString::fromUtf8("SketchEdgeColor"));
        SketchEdgeColor->setColor(QColor(255, 255, 255));
        SketchEdgeColor->setProperty("prefEntry", QVariant(QByteArray("SketchEdgeColor")));
        SketchEdgeColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(SketchEdgeColor, 0, 1, 1, 1);

        label_18 = new QLabel(groupBoxSketcherColor);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_18, 1, 0, 1, 1);

        SketchVertexColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        SketchVertexColor->setObjectName(QString::fromUtf8("SketchVertexColor"));
        SketchVertexColor->setColor(QColor(255, 255, 255));
        SketchVertexColor->setProperty("prefEntry", QVariant(QByteArray("SketchVertexColor")));
        SketchVertexColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(SketchVertexColor, 1, 1, 1, 1);

        label_6 = new QLabel(groupBoxSketcherColor);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_6, 2, 0, 1, 1);

        CreateLineColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        CreateLineColor->setObjectName(QString::fromUtf8("CreateLineColor"));
        CreateLineColor->setColor(QColor(204, 204, 204));
        CreateLineColor->setProperty("prefEntry", QVariant(QByteArray("CreateLineColor")));
        CreateLineColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(CreateLineColor, 2, 1, 1, 1);

        label = new QLabel(groupBoxSketcherColor);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label, 3, 0, 1, 1);

        EditedEdgeColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        EditedEdgeColor->setObjectName(QString::fromUtf8("EditedEdgeColor"));
        EditedEdgeColor->setColor(QColor(255, 255, 255));
        EditedEdgeColor->setProperty("prefEntry", QVariant(QByteArray("EditedEdgeColor")));
        EditedEdgeColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(EditedEdgeColor, 3, 1, 1, 1);

        label_2 = new QLabel(groupBoxSketcherColor);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_2, 4, 0, 1, 1);

        EditedVertexColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        EditedVertexColor->setObjectName(QString::fromUtf8("EditedVertexColor"));
        EditedVertexColor->setColor(QColor(255, 38, 0));
        EditedVertexColor->setProperty("prefEntry", QVariant(QByteArray("EditedVertexColor")));
        EditedVertexColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(EditedVertexColor, 4, 1, 1, 1);

        label_3 = new QLabel(groupBoxSketcherColor);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_3, 5, 0, 1, 1);

        ConstructionColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        ConstructionColor->setObjectName(QString::fromUtf8("ConstructionColor"));
        ConstructionColor->setColor(QColor(0, 0, 220));
        ConstructionColor->setProperty("prefEntry", QVariant(QByteArray("ConstructionColor")));
        ConstructionColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(ConstructionColor, 5, 1, 1, 1);

        label_20 = new QLabel(groupBoxSketcherColor);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_20, 6, 0, 1, 1);

        ExternalColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        ExternalColor->setObjectName(QString::fromUtf8("ExternalColor"));
        ExternalColor->setColor(QColor(204, 51, 115));
        ExternalColor->setProperty("prefEntry", QVariant(QByteArray("ExternalColor")));
        ExternalColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(ExternalColor, 6, 1, 1, 1);

        label_4 = new QLabel(groupBoxSketcherColor);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_4, 7, 0, 1, 1);

        FullyConstrainedColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        FullyConstrainedColor->setObjectName(QString::fromUtf8("FullyConstrainedColor"));
        FullyConstrainedColor->setColor(QColor(0, 255, 0));
        FullyConstrainedColor->setProperty("prefEntry", QVariant(QByteArray("FullyConstrainedColor")));
        FullyConstrainedColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(FullyConstrainedColor, 7, 1, 1, 1);

        label_14 = new QLabel(groupBoxSketcherColor);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_14, 8, 0, 1, 1);

        ConstrainedColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        ConstrainedColor->setObjectName(QString::fromUtf8("ConstrainedColor"));
        ConstrainedColor->setColor(QColor(255, 38, 0));
        ConstrainedColor->setProperty("prefEntry", QVariant(QByteArray("ConstrainedIcoColor")));
        ConstrainedColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(ConstrainedColor, 8, 1, 1, 1);

        label_8 = new QLabel(groupBoxSketcherColor);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_2->addWidget(label_8, 9, 0, 1, 1);

        NonDrivingConstraintColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        NonDrivingConstraintColor->setObjectName(QString::fromUtf8("NonDrivingConstraintColor"));
        NonDrivingConstraintColor->setColor(QColor(0, 38, 255));
        NonDrivingConstraintColor->setProperty("prefEntry", QVariant(QByteArray("NonDrivingConstrDimColor")));
        NonDrivingConstraintColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(NonDrivingConstraintColor, 9, 1, 1, 1);

        labelexpr = new QLabel(groupBoxSketcherColor);
        labelexpr->setObjectName(QString::fromUtf8("labelexpr"));

        gridLayout_2->addWidget(labelexpr, 10, 0, 1, 1);

        ExprBasedConstrDimColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        ExprBasedConstrDimColor->setObjectName(QString::fromUtf8("ExprBasedConstrDimColor"));
        ExprBasedConstrDimColor->setColor(QColor(255, 127, 38));
        ExprBasedConstrDimColor->setProperty("prefEntry", QVariant(QByteArray("ExprBasedConstrDimColor")));
        ExprBasedConstrDimColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(ExprBasedConstrDimColor, 10, 1, 1, 1);

        labeldeact = new QLabel(groupBoxSketcherColor);
        labeldeact->setObjectName(QString::fromUtf8("labeldeact"));

        gridLayout_2->addWidget(labeldeact, 11, 0, 1, 1);

        DeactivatedConstrDimColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        DeactivatedConstrDimColor->setObjectName(QString::fromUtf8("DeactivatedConstrDimColor"));
        DeactivatedConstrDimColor->setColor(QColor(127, 127, 127));
        DeactivatedConstrDimColor->setProperty("prefEntry", QVariant(QByteArray("DeactivatedConstrDimColor")));
        DeactivatedConstrDimColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(DeactivatedConstrDimColor, 11, 1, 1, 1);

        label_15 = new QLabel(groupBoxSketcherColor);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_15, 12, 0, 1, 1);

        DatumColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        DatumColor->setObjectName(QString::fromUtf8("DatumColor"));
        DatumColor->setColor(QColor(255, 38, 0));
        DatumColor->setProperty("prefEntry", QVariant(QByteArray("ConstrainedDimColor")));
        DatumColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(DatumColor, 12, 1, 1, 1);

        label_16 = new QLabel(groupBoxSketcherColor);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_16, 13, 0, 1, 1);

        SketcherDatumWidth = new Gui::PrefSpinBox(groupBoxSketcherColor);
        SketcherDatumWidth->setObjectName(QString::fromUtf8("SketcherDatumWidth"));
        SketcherDatumWidth->setMaximum(9);
        SketcherDatumWidth->setValue(2);
        SketcherDatumWidth->setProperty("prefEntry", QVariant(QByteArray("DefaultSketcherVertexWidth")));
        SketcherDatumWidth->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(SketcherDatumWidth, 13, 1, 1, 1);

        label_12 = new QLabel(groupBoxSketcherColor);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_12, 14, 0, 1, 1);

        DefaultSketcherVertexWidth = new Gui::PrefSpinBox(groupBoxSketcherColor);
        DefaultSketcherVertexWidth->setObjectName(QString::fromUtf8("DefaultSketcherVertexWidth"));
        DefaultSketcherVertexWidth->setMaximum(9);
        DefaultSketcherVertexWidth->setValue(2);
        DefaultSketcherVertexWidth->setProperty("prefEntry", QVariant(QByteArray("DefaultSketcherVertexWidth")));
        DefaultSketcherVertexWidth->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(DefaultSketcherVertexWidth, 14, 1, 1, 1);

        label_13 = new QLabel(groupBoxSketcherColor);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_13, 15, 0, 1, 1);

        DefaultSketcherLineWidth = new Gui::PrefSpinBox(groupBoxSketcherColor);
        DefaultSketcherLineWidth->setObjectName(QString::fromUtf8("DefaultSketcherLineWidth"));
        DefaultSketcherLineWidth->setMaximum(9);
        DefaultSketcherLineWidth->setValue(2);
        DefaultSketcherLineWidth->setProperty("prefEntry", QVariant(QByteArray("DefaultShapeLineWidth")));
        DefaultSketcherLineWidth->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(DefaultSketcherLineWidth, 15, 1, 1, 1);

        label_5 = new QLabel(groupBoxSketcherColor);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_5, 16, 0, 1, 1);

        CursorTextColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        CursorTextColor->setObjectName(QString::fromUtf8("CursorTextColor"));
        CursorTextColor->setColor(QColor(0, 0, 255));
        CursorTextColor->setProperty("prefEntry", QVariant(QByteArray("CursorTextColor")));
        CursorTextColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(CursorTextColor, 16, 1, 1, 1);

        label_19 = new QLabel(groupBoxSketcherColor);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setMinimumSize(QSize(182, 0));

        gridLayout_2->addWidget(label_19, 17, 0, 1, 1);

        CursorCrosshairColor = new Gui::PrefColorButton(groupBoxSketcherColor);
        CursorCrosshairColor->setObjectName(QString::fromUtf8("CursorCrosshairColor"));
        CursorCrosshairColor->setColor(QColor(255, 255, 255));
        CursorCrosshairColor->setProperty("prefEntry", QVariant(QByteArray("CursorCrosshairColor")));
        CursorCrosshairColor->setProperty("prefPath", QVariant(QByteArray("View")));

        gridLayout_2->addWidget(CursorCrosshairColor, 17, 1, 1, 1);


        horizontalLayout_2->addLayout(gridLayout_2);


        gridLayout->addWidget(groupBoxSketcherColor, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 0, 1, 1);


        retranslateUi(SketcherGui__SketcherSettingsColors);

        QMetaObject::connectSlotsByName(SketcherGui__SketcherSettingsColors);
    } // setupUi

    void retranslateUi(QWidget *SketcherGui__SketcherSettingsColors)
    {
        SketcherGui__SketcherSettingsColors->setWindowTitle(QApplication::translate("SketcherGui::SketcherSettingsColors", "Colors", nullptr));
        groupBoxSketcherColor->setTitle(QApplication::translate("SketcherGui::SketcherSettingsColors", "Sketcher colors", nullptr));
        label_17->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Default edge color", nullptr));
#ifndef QT_NO_TOOLTIP
        SketchEdgeColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of edges", nullptr));
#endif // QT_NO_TOOLTIP
        label_18->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Default vertex color", nullptr));
#ifndef QT_NO_TOOLTIP
        SketchVertexColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of vertices", nullptr));
#endif // QT_NO_TOOLTIP
        label_6->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Making line color", nullptr));
#ifndef QT_NO_TOOLTIP
        CreateLineColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color used while new sketch elements are created", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Edit edge color", nullptr));
#ifndef QT_NO_TOOLTIP
        EditedEdgeColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of edges being edited", nullptr));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Edit vertex color", nullptr));
#ifndef QT_NO_TOOLTIP
        EditedVertexColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of vertices being edited", nullptr));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Construction geometry", nullptr));
#ifndef QT_NO_TOOLTIP
        ConstructionColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of construction geometry in edit mode", nullptr));
#endif // QT_NO_TOOLTIP
        label_20->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "External geometry", nullptr));
#ifndef QT_NO_TOOLTIP
        ExternalColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of external geometry in edit mode", nullptr));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Fully constrained geometry", nullptr));
#ifndef QT_NO_TOOLTIP
        FullyConstrainedColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of fully constrained geometry in edit mode", nullptr));
#endif // QT_NO_TOOLTIP
        label_14->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Constraint color", nullptr));
#ifndef QT_NO_TOOLTIP
        ConstrainedColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of driving constraints in edit mode", nullptr));
#endif // QT_NO_TOOLTIP
        label_8->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Reference constraint color", nullptr));
#ifndef QT_NO_TOOLTIP
        NonDrivingConstraintColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of reference constraints in edit mode", nullptr));
#endif // QT_NO_TOOLTIP
        labelexpr->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Expression dependent constraint color", nullptr));
#ifndef QT_NO_TOOLTIP
        ExprBasedConstrDimColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of expression dependent constraints in edit mode", nullptr));
#endif // QT_NO_TOOLTIP
        labeldeact->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Deactivated constraint color", nullptr));
#ifndef QT_NO_TOOLTIP
        DeactivatedConstrDimColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of deactivated constraints in edit mode", nullptr));
#endif // QT_NO_TOOLTIP
        label_15->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Datum color", nullptr));
#ifndef QT_NO_TOOLTIP
        DatumColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of the datum portion of a driving constraint", nullptr));
#endif // QT_NO_TOOLTIP
        label_16->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Datum text size", nullptr));
#ifndef QT_NO_TOOLTIP
        SketcherDatumWidth->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "The default line thickness for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
        SketcherDatumWidth->setSuffix(QApplication::translate("SketcherGui::SketcherSettingsColors", "px", nullptr));
        label_12->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Default vertex size", nullptr));
#ifndef QT_NO_TOOLTIP
        DefaultSketcherVertexWidth->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "The default line thickness for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
        DefaultSketcherVertexWidth->setSuffix(QApplication::translate("SketcherGui::SketcherSettingsColors", "px", nullptr));
        label_13->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Default line width", nullptr));
#ifndef QT_NO_TOOLTIP
        DefaultSketcherLineWidth->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "The default line thickness for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
        DefaultSketcherLineWidth->setSuffix(QApplication::translate("SketcherGui::SketcherSettingsColors", "px", nullptr));
        label_5->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Coordinate text color", nullptr));
#ifndef QT_NO_TOOLTIP
        CursorTextColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Text color of the coordinates", nullptr));
#endif // QT_NO_TOOLTIP
        label_19->setText(QApplication::translate("SketcherGui::SketcherSettingsColors", "Cursor crosshair color", nullptr));
#ifndef QT_NO_TOOLTIP
        CursorCrosshairColor->setToolTip(QApplication::translate("SketcherGui::SketcherSettingsColors", "Color of crosshair cursor.\n"
"(The one you get when creating a new sketch element.)", nullptr));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

} // namespace SketcherGui

namespace SketcherGui {
namespace Ui {
    class SketcherSettingsColors: public Ui_SketcherSettingsColors {};
} // namespace Ui
} // namespace SketcherGui

#endif // UI_SKETCHERSETTINGSCOLORS_H
