/****************************************************************************
** Meta object code from reading C++ file 'TaskSketcherValidation.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Sketcher/Gui/TaskSketcherValidation.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskSketcherValidation.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SketcherGui__SketcherValidation_t {
    QByteArrayData data[12];
    char stringdata0[284];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__SketcherValidation_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__SketcherValidation_t qt_meta_stringdata_SketcherGui__SketcherValidation = {
    {
QT_MOC_LITERAL(0, 0, 31), // "SketcherGui::SketcherValidation"
QT_MOC_LITERAL(1, 32, 21), // "on_findButton_clicked"
QT_MOC_LITERAL(2, 54, 0), // ""
QT_MOC_LITERAL(3, 55, 20), // "on_fixButton_clicked"
QT_MOC_LITERAL(4, 76, 26), // "on_highlightButton_clicked"
QT_MOC_LITERAL(5, 103, 25), // "on_findConstraint_clicked"
QT_MOC_LITERAL(6, 129, 24), // "on_fixConstraint_clicked"
QT_MOC_LITERAL(7, 154, 23), // "on_findReversed_clicked"
QT_MOC_LITERAL(8, 178, 23), // "on_swapReversed_clicked"
QT_MOC_LITERAL(9, 202, 27), // "on_orientLockEnable_clicked"
QT_MOC_LITERAL(10, 230, 28), // "on_orientLockDisable_clicked"
QT_MOC_LITERAL(11, 259, 24) // "on_delConstrExtr_clicked"

    },
    "SketcherGui::SketcherValidation\0"
    "on_findButton_clicked\0\0on_fixButton_clicked\0"
    "on_highlightButton_clicked\0"
    "on_findConstraint_clicked\0"
    "on_fixConstraint_clicked\0"
    "on_findReversed_clicked\0on_swapReversed_clicked\0"
    "on_orientLockEnable_clicked\0"
    "on_orientLockDisable_clicked\0"
    "on_delConstrExtr_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__SketcherValidation[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x08 /* Private */,
       3,    0,   65,    2, 0x08 /* Private */,
       4,    0,   66,    2, 0x08 /* Private */,
       5,    0,   67,    2, 0x08 /* Private */,
       6,    0,   68,    2, 0x08 /* Private */,
       7,    0,   69,    2, 0x08 /* Private */,
       8,    0,   70,    2, 0x08 /* Private */,
       9,    0,   71,    2, 0x08 /* Private */,
      10,    0,   72,    2, 0x08 /* Private */,
      11,    0,   73,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SketcherGui::SketcherValidation::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SketcherValidation *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_findButton_clicked(); break;
        case 1: _t->on_fixButton_clicked(); break;
        case 2: _t->on_highlightButton_clicked(); break;
        case 3: _t->on_findConstraint_clicked(); break;
        case 4: _t->on_fixConstraint_clicked(); break;
        case 5: _t->on_findReversed_clicked(); break;
        case 6: _t->on_swapReversed_clicked(); break;
        case 7: _t->on_orientLockEnable_clicked(); break;
        case 8: _t->on_orientLockDisable_clicked(); break;
        case 9: _t->on_delConstrExtr_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::SketcherValidation::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_SketcherGui__SketcherValidation.data,
    qt_meta_data_SketcherGui__SketcherValidation,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::SketcherValidation::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::SketcherValidation::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__SketcherValidation.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int SketcherGui::SketcherValidation::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
struct qt_meta_stringdata_SketcherGui__TaskSketcherValidation_t {
    QByteArrayData data[1];
    char stringdata0[36];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__TaskSketcherValidation_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__TaskSketcherValidation_t qt_meta_stringdata_SketcherGui__TaskSketcherValidation = {
    {
QT_MOC_LITERAL(0, 0, 35) // "SketcherGui::TaskSketcherVali..."

    },
    "SketcherGui::TaskSketcherValidation"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__TaskSketcherValidation[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void SketcherGui::TaskSketcherValidation::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::TaskSketcherValidation::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_SketcherGui__TaskSketcherValidation.data,
    qt_meta_data_SketcherGui__TaskSketcherValidation,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::TaskSketcherValidation::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::TaskSketcherValidation::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__TaskSketcherValidation.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int SketcherGui::TaskSketcherValidation::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
