/********************************************************************************
** Form generated from reading UI file 'TaskSketcherElements.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKSKETCHERELEMENTS_H
#define UI_TASKSKETCHERELEMENTS_H

#include <QListWidget>
#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace SketcherGui {

class Ui_TaskSketcherElements
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *comboBoxElementFilter;
    QHBoxLayout *horizontalLayout1;
    QLabel *label1;
    QComboBox *comboBoxModeFilter;
    ElementView *listWidgetElements;
    QCheckBox *namingBox;
    QCheckBox *autoSwitchBox;
    QLabel *Explanation;

    void setupUi(QWidget *SketcherGui__TaskSketcherElements)
    {
        if (SketcherGui__TaskSketcherElements->objectName().isEmpty())
            SketcherGui__TaskSketcherElements->setObjectName(QString::fromUtf8("SketcherGui__TaskSketcherElements"));
        SketcherGui__TaskSketcherElements->resize(214, 401);
        verticalLayout = new QVBoxLayout(SketcherGui__TaskSketcherElements);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(SketcherGui__TaskSketcherElements);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        comboBoxElementFilter = new QComboBox(SketcherGui__TaskSketcherElements);
        comboBoxElementFilter->addItem(QString());
        comboBoxElementFilter->addItem(QString());
        comboBoxElementFilter->addItem(QString());
        comboBoxElementFilter->addItem(QString());
        comboBoxElementFilter->setObjectName(QString::fromUtf8("comboBoxElementFilter"));
        comboBoxElementFilter->setEnabled(false);
        comboBoxElementFilter->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        comboBoxElementFilter->setEditable(false);

        horizontalLayout->addWidget(comboBoxElementFilter);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout1 = new QHBoxLayout();
        horizontalLayout1->setObjectName(QString::fromUtf8("horizontalLayout1"));
        label1 = new QLabel(SketcherGui__TaskSketcherElements);
        label1->setObjectName(QString::fromUtf8("label1"));

        horizontalLayout1->addWidget(label1);

        comboBoxModeFilter = new QComboBox(SketcherGui__TaskSketcherElements);
        comboBoxModeFilter->addItem(QString());
        comboBoxModeFilter->addItem(QString());
        comboBoxModeFilter->addItem(QString());
        comboBoxModeFilter->addItem(QString());
        comboBoxModeFilter->setObjectName(QString::fromUtf8("comboBoxModeFilter"));
        comboBoxModeFilter->setEnabled(false);
        comboBoxModeFilter->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        comboBoxModeFilter->setEditable(false);

        horizontalLayout1->addWidget(comboBoxModeFilter);


        verticalLayout->addLayout(horizontalLayout1);

        listWidgetElements = new ElementView(SketcherGui__TaskSketcherElements);
        listWidgetElements->setObjectName(QString::fromUtf8("listWidgetElements"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(listWidgetElements->sizePolicy().hasHeightForWidth());
        listWidgetElements->setSizePolicy(sizePolicy);
        listWidgetElements->setMaximumSize(QSize(16777215, 237));
        listWidgetElements->setModelColumn(0);

        verticalLayout->addWidget(listWidgetElements);

        namingBox = new QCheckBox(SketcherGui__TaskSketcherElements);
        namingBox->setObjectName(QString::fromUtf8("namingBox"));
        namingBox->setChecked(false);

        verticalLayout->addWidget(namingBox);

        autoSwitchBox = new QCheckBox(SketcherGui__TaskSketcherElements);
        autoSwitchBox->setObjectName(QString::fromUtf8("autoSwitchBox"));
        autoSwitchBox->setChecked(false);

        verticalLayout->addWidget(autoSwitchBox);

        Explanation = new QLabel(SketcherGui__TaskSketcherElements);
        Explanation->setObjectName(QString::fromUtf8("Explanation"));
        Explanation->setText(QString::fromUtf8("<html><head/><body><p>&quot;Ctrl&quot;: multiple selection</p><p>&quot;Z&quot;: switch to next valid type</p></body></html>"));

        verticalLayout->addWidget(Explanation);


        retranslateUi(SketcherGui__TaskSketcherElements);

        comboBoxElementFilter->setCurrentIndex(0);
        comboBoxModeFilter->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SketcherGui__TaskSketcherElements);
    } // setupUi

    void retranslateUi(QWidget *SketcherGui__TaskSketcherElements)
    {
        SketcherGui__TaskSketcherElements->setWindowTitle(QApplication::translate("SketcherGui::TaskSketcherElements", "Form", nullptr));
        label->setText(QApplication::translate("SketcherGui::TaskSketcherElements", "Type:", nullptr));
        comboBoxElementFilter->setItemText(0, QApplication::translate("SketcherGui::TaskSketcherElements", "Edge", nullptr));
        comboBoxElementFilter->setItemText(1, QApplication::translate("SketcherGui::TaskSketcherElements", "Starting Point", nullptr));
        comboBoxElementFilter->setItemText(2, QApplication::translate("SketcherGui::TaskSketcherElements", "End Point", nullptr));
        comboBoxElementFilter->setItemText(3, QApplication::translate("SketcherGui::TaskSketcherElements", "Center Point", nullptr));

        label1->setText(QApplication::translate("SketcherGui::TaskSketcherElements", "Mode:", nullptr));
        comboBoxModeFilter->setItemText(0, QApplication::translate("SketcherGui::TaskSketcherElements", "All", nullptr));
        comboBoxModeFilter->setItemText(1, QApplication::translate("SketcherGui::TaskSketcherElements", "Normal", nullptr));
        comboBoxModeFilter->setItemText(2, QApplication::translate("SketcherGui::TaskSketcherElements", "Construction", nullptr));
        comboBoxModeFilter->setItemText(3, QApplication::translate("SketcherGui::TaskSketcherElements", "External", nullptr));

#ifndef QT_NO_TOOLTIP
        namingBox->setToolTip(QApplication::translate("SketcherGui::TaskSketcherElements", "Extended naming containing info about element mode", nullptr));
#endif // QT_NO_TOOLTIP
        namingBox->setText(QApplication::translate("SketcherGui::TaskSketcherElements", "Extended naming", nullptr));
#ifndef QT_NO_TOOLTIP
        autoSwitchBox->setToolTip(QApplication::translate("SketcherGui::TaskSketcherElements", "Only the type 'Edge' will be available for the list", nullptr));
#endif // QT_NO_TOOLTIP
        autoSwitchBox->setText(QApplication::translate("SketcherGui::TaskSketcherElements", "Auto-switch to Edge", nullptr));
    } // retranslateUi

};

} // namespace SketcherGui

namespace SketcherGui {
namespace Ui {
    class TaskSketcherElements: public Ui_TaskSketcherElements {};
} // namespace Ui
} // namespace SketcherGui

#endif // UI_TASKSKETCHERELEMENTS_H
