/****************************************************************************
** Meta object code from reading C++ file 'TaskSketcherElements.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Sketcher/Gui/TaskSketcherElements.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskSketcherElements.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SketcherGui__ElementView_t {
    QByteArrayData data[28];
    char stringdata0[524];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__ElementView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__ElementView_t qt_meta_stringdata_SketcherGui__ElementView = {
    {
QT_MOC_LITERAL(0, 0, 24), // "SketcherGui::ElementView"
QT_MOC_LITERAL(1, 25, 23), // "onFilterShortcutPressed"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 16), // "signalCloseShape"
QT_MOC_LITERAL(4, 67, 19), // "deleteSelectedItems"
QT_MOC_LITERAL(5, 87, 20), // "doHorizontalDistance"
QT_MOC_LITERAL(6, 108, 18), // "doVerticalDistance"
QT_MOC_LITERAL(7, 127, 22), // "doHorizontalConstraint"
QT_MOC_LITERAL(8, 150, 20), // "doVerticalConstraint"
QT_MOC_LITERAL(9, 171, 16), // "doLockConstraint"
QT_MOC_LITERAL(10, 188, 18), // "doPointCoincidence"
QT_MOC_LITERAL(11, 207, 20), // "doParallelConstraint"
QT_MOC_LITERAL(12, 228, 25), // "doPerpendicularConstraint"
QT_MOC_LITERAL(13, 254, 18), // "doLengthConstraint"
QT_MOC_LITERAL(14, 273, 18), // "doRadiusConstraint"
QT_MOC_LITERAL(15, 292, 20), // "doDiameterConstraint"
QT_MOC_LITERAL(16, 313, 17), // "doAngleConstraint"
QT_MOC_LITERAL(17, 331, 17), // "doEqualConstraint"
QT_MOC_LITERAL(18, 349, 25), // "doPointOnObjectConstraint"
QT_MOC_LITERAL(19, 375, 21), // "doSymmetricConstraint"
QT_MOC_LITERAL(20, 397, 19), // "doTangentConstraint"
QT_MOC_LITERAL(21, 417, 20), // "doToggleConstruction"
QT_MOC_LITERAL(22, 438, 12), // "doCloseShape"
QT_MOC_LITERAL(23, 451, 9), // "doConnect"
QT_MOC_LITERAL(24, 461, 14), // "doSelectOrigin"
QT_MOC_LITERAL(25, 476, 13), // "doSelectHAxis"
QT_MOC_LITERAL(26, 490, 13), // "doSelectVAxis"
QT_MOC_LITERAL(27, 504, 19) // "doSelectConstraints"

    },
    "SketcherGui::ElementView\0"
    "onFilterShortcutPressed\0\0signalCloseShape\0"
    "deleteSelectedItems\0doHorizontalDistance\0"
    "doVerticalDistance\0doHorizontalConstraint\0"
    "doVerticalConstraint\0doLockConstraint\0"
    "doPointCoincidence\0doParallelConstraint\0"
    "doPerpendicularConstraint\0doLengthConstraint\0"
    "doRadiusConstraint\0doDiameterConstraint\0"
    "doAngleConstraint\0doEqualConstraint\0"
    "doPointOnObjectConstraint\0"
    "doSymmetricConstraint\0doTangentConstraint\0"
    "doToggleConstruction\0doCloseShape\0"
    "doConnect\0doSelectOrigin\0doSelectHAxis\0"
    "doSelectVAxis\0doSelectConstraints"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__ElementView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  144,    2, 0x06 /* Public */,
       3,    0,  145,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,  146,    2, 0x09 /* Protected */,
       5,    0,  147,    2, 0x09 /* Protected */,
       6,    0,  148,    2, 0x09 /* Protected */,
       7,    0,  149,    2, 0x09 /* Protected */,
       8,    0,  150,    2, 0x09 /* Protected */,
       9,    0,  151,    2, 0x09 /* Protected */,
      10,    0,  152,    2, 0x09 /* Protected */,
      11,    0,  153,    2, 0x09 /* Protected */,
      12,    0,  154,    2, 0x09 /* Protected */,
      13,    0,  155,    2, 0x09 /* Protected */,
      14,    0,  156,    2, 0x09 /* Protected */,
      15,    0,  157,    2, 0x09 /* Protected */,
      16,    0,  158,    2, 0x09 /* Protected */,
      17,    0,  159,    2, 0x09 /* Protected */,
      18,    0,  160,    2, 0x09 /* Protected */,
      19,    0,  161,    2, 0x09 /* Protected */,
      20,    0,  162,    2, 0x09 /* Protected */,
      21,    0,  163,    2, 0x09 /* Protected */,
      22,    0,  164,    2, 0x09 /* Protected */,
      23,    0,  165,    2, 0x09 /* Protected */,
      24,    0,  166,    2, 0x09 /* Protected */,
      25,    0,  167,    2, 0x09 /* Protected */,
      26,    0,  168,    2, 0x09 /* Protected */,
      27,    0,  169,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SketcherGui::ElementView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ElementView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onFilterShortcutPressed(); break;
        case 1: _t->signalCloseShape(); break;
        case 2: _t->deleteSelectedItems(); break;
        case 3: _t->doHorizontalDistance(); break;
        case 4: _t->doVerticalDistance(); break;
        case 5: _t->doHorizontalConstraint(); break;
        case 6: _t->doVerticalConstraint(); break;
        case 7: _t->doLockConstraint(); break;
        case 8: _t->doPointCoincidence(); break;
        case 9: _t->doParallelConstraint(); break;
        case 10: _t->doPerpendicularConstraint(); break;
        case 11: _t->doLengthConstraint(); break;
        case 12: _t->doRadiusConstraint(); break;
        case 13: _t->doDiameterConstraint(); break;
        case 14: _t->doAngleConstraint(); break;
        case 15: _t->doEqualConstraint(); break;
        case 16: _t->doPointOnObjectConstraint(); break;
        case 17: _t->doSymmetricConstraint(); break;
        case 18: _t->doTangentConstraint(); break;
        case 19: _t->doToggleConstruction(); break;
        case 20: _t->doCloseShape(); break;
        case 21: _t->doConnect(); break;
        case 22: _t->doSelectOrigin(); break;
        case 23: _t->doSelectHAxis(); break;
        case 24: _t->doSelectVAxis(); break;
        case 25: _t->doSelectConstraints(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ElementView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ElementView::onFilterShortcutPressed)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ElementView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ElementView::signalCloseShape)) {
                *result = 1;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::ElementView::staticMetaObject = { {
    &QListWidget::staticMetaObject,
    qt_meta_stringdata_SketcherGui__ElementView.data,
    qt_meta_data_SketcherGui__ElementView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::ElementView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::ElementView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__ElementView.stringdata0))
        return static_cast<void*>(this);
    return QListWidget::qt_metacast(_clname);
}

int SketcherGui::ElementView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QListWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void SketcherGui::ElementView::onFilterShortcutPressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void SketcherGui::ElementView::signalCloseShape()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
struct qt_meta_stringdata_SketcherGui__TaskSketcherElements_t {
    QByteArrayData data[13];
    char stringdata0[336];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__TaskSketcherElements_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__TaskSketcherElements_t qt_meta_stringdata_SketcherGui__TaskSketcherElements = {
    {
QT_MOC_LITERAL(0, 0, 33), // "SketcherGui::TaskSketcherElem..."
QT_MOC_LITERAL(1, 34, 42), // "on_listWidgetElements_itemSel..."
QT_MOC_LITERAL(2, 77, 0), // ""
QT_MOC_LITERAL(3, 78, 33), // "on_listWidgetElements_itemEnt..."
QT_MOC_LITERAL(4, 112, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(5, 129, 4), // "item"
QT_MOC_LITERAL(6, 134, 43), // "on_listWidgetElements_filterS..."
QT_MOC_LITERAL(7, 178, 42), // "on_listWidgetElements_current..."
QT_MOC_LITERAL(8, 221, 5), // "index"
QT_MOC_LITERAL(9, 227, 46), // "on_listWidgetElements_current..."
QT_MOC_LITERAL(10, 274, 25), // "on_namingBox_stateChanged"
QT_MOC_LITERAL(11, 300, 5), // "state"
QT_MOC_LITERAL(12, 306, 29) // "on_autoSwitchBox_stateChanged"

    },
    "SketcherGui::TaskSketcherElements\0"
    "on_listWidgetElements_itemSelectionChanged\0"
    "\0on_listWidgetElements_itemEntered\0"
    "QListWidgetItem*\0item\0"
    "on_listWidgetElements_filterShortcutPressed\0"
    "on_listWidgetElements_currentFilterChanged\0"
    "index\0on_listWidgetElements_currentModeFilterChanged\0"
    "on_namingBox_stateChanged\0state\0"
    "on_autoSwitchBox_stateChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__TaskSketcherElements[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x0a /* Public */,
       3,    1,   50,    2, 0x0a /* Public */,
       6,    0,   53,    2, 0x0a /* Public */,
       7,    1,   54,    2, 0x0a /* Public */,
       9,    1,   57,    2, 0x0a /* Public */,
      10,    1,   60,    2, 0x0a /* Public */,
      12,    1,   63,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,

       0        // eod
};

void SketcherGui::TaskSketcherElements::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskSketcherElements *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_listWidgetElements_itemSelectionChanged(); break;
        case 1: _t->on_listWidgetElements_itemEntered((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 2: _t->on_listWidgetElements_filterShortcutPressed(); break;
        case 3: _t->on_listWidgetElements_currentFilterChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_listWidgetElements_currentModeFilterChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->on_namingBox_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_autoSwitchBox_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::TaskSketcherElements::staticMetaObject = { {
    &Gui::TaskView::TaskBox::staticMetaObject,
    qt_meta_stringdata_SketcherGui__TaskSketcherElements.data,
    qt_meta_data_SketcherGui__TaskSketcherElements,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::TaskSketcherElements::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::TaskSketcherElements::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__TaskSketcherElements.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionObserver"))
        return static_cast< Gui::SelectionObserver*>(this);
    return Gui::TaskView::TaskBox::qt_metacast(_clname);
}

int SketcherGui::TaskSketcherElements::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
