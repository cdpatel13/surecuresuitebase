/****************************************************************************
** Meta object code from reading C++ file 'TaskSketcherSolverAdvanced.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Sketcher/Gui/TaskSketcherSolverAdvanced.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskSketcherSolverAdvanced.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SketcherGui__TaskSketcherSolverAdvanced_t {
    QByteArrayData data[26];
    char stringdata0[927];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SketcherGui__TaskSketcherSolverAdvanced_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SketcherGui__TaskSketcherSolverAdvanced_t qt_meta_stringdata_SketcherGui__TaskSketcherSolverAdvanced = {
    {
QT_MOC_LITERAL(0, 0, 39), // "SketcherGui::TaskSketcherSolv..."
QT_MOC_LITERAL(1, 40, 44), // "on_comboBoxDefaultSolver_curr..."
QT_MOC_LITERAL(2, 85, 0), // ""
QT_MOC_LITERAL(3, 86, 5), // "index"
QT_MOC_LITERAL(4, 92, 46), // "on_comboBoxDogLegGaussStep_cu..."
QT_MOC_LITERAL(5, 139, 30), // "on_spinBoxMaxIter_valueChanged"
QT_MOC_LITERAL(6, 170, 1), // "i"
QT_MOC_LITERAL(7, 172, 44), // "on_checkBoxSketchSizeMultipli..."
QT_MOC_LITERAL(8, 217, 5), // "state"
QT_MOC_LITERAL(9, 223, 38), // "on_lineEditConvergence_editin..."
QT_MOC_LITERAL(10, 262, 39), // "on_comboBoxQRMethod_currentIn..."
QT_MOC_LITERAL(11, 302, 43), // "on_lineEditQRPivotThreshold_e..."
QT_MOC_LITERAL(12, 346, 53), // "on_comboBoxRedundantDefaultSo..."
QT_MOC_LITERAL(13, 400, 47), // "on_lineEditRedundantConvergen..."
QT_MOC_LITERAL(14, 448, 51), // "on_spinBoxRedundantSolverMaxI..."
QT_MOC_LITERAL(15, 500, 53), // "on_checkBoxRedundantSketchSiz..."
QT_MOC_LITERAL(16, 554, 40), // "on_comboBoxDebugMode_currentI..."
QT_MOC_LITERAL(17, 595, 39), // "on_lineEditSolverParam1_editi..."
QT_MOC_LITERAL(18, 635, 48), // "on_lineEditRedundantSolverPar..."
QT_MOC_LITERAL(19, 684, 39), // "on_lineEditSolverParam2_editi..."
QT_MOC_LITERAL(20, 724, 48), // "on_lineEditRedundantSolverPar..."
QT_MOC_LITERAL(21, 773, 39), // "on_lineEditSolverParam3_editi..."
QT_MOC_LITERAL(22, 813, 48), // "on_lineEditRedundantSolverPar..."
QT_MOC_LITERAL(23, 862, 29), // "on_pushButtonDefaults_clicked"
QT_MOC_LITERAL(24, 892, 7), // "checked"
QT_MOC_LITERAL(25, 900, 26) // "on_pushButtonSolve_clicked"

    },
    "SketcherGui::TaskSketcherSolverAdvanced\0"
    "on_comboBoxDefaultSolver_currentIndexChanged\0"
    "\0index\0on_comboBoxDogLegGaussStep_currentIndexChanged\0"
    "on_spinBoxMaxIter_valueChanged\0i\0"
    "on_checkBoxSketchSizeMultiplier_stateChanged\0"
    "state\0on_lineEditConvergence_editingFinished\0"
    "on_comboBoxQRMethod_currentIndexChanged\0"
    "on_lineEditQRPivotThreshold_editingFinished\0"
    "on_comboBoxRedundantDefaultSolver_currentIndexChanged\0"
    "on_lineEditRedundantConvergence_editingFinished\0"
    "on_spinBoxRedundantSolverMaxIterations_valueChanged\0"
    "on_checkBoxRedundantSketchSizeMultiplier_stateChanged\0"
    "on_comboBoxDebugMode_currentIndexChanged\0"
    "on_lineEditSolverParam1_editingFinished\0"
    "on_lineEditRedundantSolverParam1_editingFinished\0"
    "on_lineEditSolverParam2_editingFinished\0"
    "on_lineEditRedundantSolverParam2_editingFinished\0"
    "on_lineEditSolverParam3_editingFinished\0"
    "on_lineEditRedundantSolverParam3_editingFinished\0"
    "on_pushButtonDefaults_clicked\0checked\0"
    "on_pushButtonSolve_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SketcherGui__TaskSketcherSolverAdvanced[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  124,    2, 0x08 /* Private */,
       4,    1,  127,    2, 0x08 /* Private */,
       5,    1,  130,    2, 0x08 /* Private */,
       7,    1,  133,    2, 0x08 /* Private */,
       9,    0,  136,    2, 0x08 /* Private */,
      10,    1,  137,    2, 0x08 /* Private */,
      11,    0,  140,    2, 0x08 /* Private */,
      12,    1,  141,    2, 0x08 /* Private */,
      13,    0,  144,    2, 0x08 /* Private */,
      14,    1,  145,    2, 0x08 /* Private */,
      15,    1,  148,    2, 0x08 /* Private */,
      16,    1,  151,    2, 0x08 /* Private */,
      17,    0,  154,    2, 0x08 /* Private */,
      18,    0,  155,    2, 0x08 /* Private */,
      19,    0,  156,    2, 0x08 /* Private */,
      20,    0,  157,    2, 0x08 /* Private */,
      21,    0,  158,    2, 0x08 /* Private */,
      22,    0,  159,    2, 0x08 /* Private */,
      23,    1,  160,    2, 0x08 /* Private */,
      23,    0,  163,    2, 0x28 /* Private | MethodCloned */,
      25,    1,  164,    2, 0x08 /* Private */,
      25,    0,  167,    2, 0x28 /* Private | MethodCloned */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,

       0        // eod
};

void SketcherGui::TaskSketcherSolverAdvanced::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskSketcherSolverAdvanced *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_comboBoxDefaultSolver_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_comboBoxDogLegGaussStep_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_spinBoxMaxIter_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_checkBoxSketchSizeMultiplier_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_lineEditConvergence_editingFinished(); break;
        case 5: _t->on_comboBoxQRMethod_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_lineEditQRPivotThreshold_editingFinished(); break;
        case 7: _t->on_comboBoxRedundantDefaultSolver_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_lineEditRedundantConvergence_editingFinished(); break;
        case 9: _t->on_spinBoxRedundantSolverMaxIterations_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_checkBoxRedundantSketchSizeMultiplier_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_comboBoxDebugMode_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_lineEditSolverParam1_editingFinished(); break;
        case 13: _t->on_lineEditRedundantSolverParam1_editingFinished(); break;
        case 14: _t->on_lineEditSolverParam2_editingFinished(); break;
        case 15: _t->on_lineEditRedundantSolverParam2_editingFinished(); break;
        case 16: _t->on_lineEditSolverParam3_editingFinished(); break;
        case 17: _t->on_lineEditRedundantSolverParam3_editingFinished(); break;
        case 18: _t->on_pushButtonDefaults_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->on_pushButtonDefaults_clicked(); break;
        case 20: _t->on_pushButtonSolve_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->on_pushButtonSolve_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SketcherGui::TaskSketcherSolverAdvanced::staticMetaObject = { {
    &Gui::TaskView::TaskBox::staticMetaObject,
    qt_meta_stringdata_SketcherGui__TaskSketcherSolverAdvanced.data,
    qt_meta_data_SketcherGui__TaskSketcherSolverAdvanced,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SketcherGui::TaskSketcherSolverAdvanced::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SketcherGui::TaskSketcherSolverAdvanced::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SketcherGui__TaskSketcherSolverAdvanced.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskBox::qt_metacast(_clname);
}

int SketcherGui::TaskSketcherSolverAdvanced::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
