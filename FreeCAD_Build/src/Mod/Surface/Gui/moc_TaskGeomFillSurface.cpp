/****************************************************************************
** Meta object code from reading C++ file 'TaskGeomFillSurface.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Surface/Gui/TaskGeomFillSurface.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskGeomFillSurface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SurfaceGui__GeomFillSurface_t {
    QByteArrayData data[12];
    char stringdata0[258];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SurfaceGui__GeomFillSurface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SurfaceGui__GeomFillSurface_t qt_meta_stringdata_SurfaceGui__GeomFillSurface = {
    {
QT_MOC_LITERAL(0, 0, 27), // "SurfaceGui::GeomFillSurface"
QT_MOC_LITERAL(1, 28, 27), // "on_fillType_stretch_clicked"
QT_MOC_LITERAL(2, 56, 0), // ""
QT_MOC_LITERAL(3, 57, 25), // "on_fillType_coons_clicked"
QT_MOC_LITERAL(4, 83, 26), // "on_fillType_curved_clicked"
QT_MOC_LITERAL(5, 110, 24), // "on_buttonEdgeAdd_clicked"
QT_MOC_LITERAL(6, 135, 27), // "on_buttonEdgeRemove_clicked"
QT_MOC_LITERAL(7, 163, 31), // "on_listWidget_itemDoubleClicked"
QT_MOC_LITERAL(8, 195, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(9, 212, 12), // "onDeleteEdge"
QT_MOC_LITERAL(10, 225, 17), // "onFlipOrientation"
QT_MOC_LITERAL(11, 243, 14) // "clearSelection"

    },
    "SurfaceGui::GeomFillSurface\0"
    "on_fillType_stretch_clicked\0\0"
    "on_fillType_coons_clicked\0"
    "on_fillType_curved_clicked\0"
    "on_buttonEdgeAdd_clicked\0"
    "on_buttonEdgeRemove_clicked\0"
    "on_listWidget_itemDoubleClicked\0"
    "QListWidgetItem*\0onDeleteEdge\0"
    "onFlipOrientation\0clearSelection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SurfaceGui__GeomFillSurface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    0,   61,    2, 0x08 /* Private */,
       5,    0,   62,    2, 0x08 /* Private */,
       6,    0,   63,    2, 0x08 /* Private */,
       7,    1,   64,    2, 0x08 /* Private */,
       9,    0,   67,    2, 0x08 /* Private */,
      10,    0,   68,    2, 0x08 /* Private */,
      11,    0,   69,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SurfaceGui::GeomFillSurface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<GeomFillSurface *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_fillType_stretch_clicked(); break;
        case 1: _t->on_fillType_coons_clicked(); break;
        case 2: _t->on_fillType_curved_clicked(); break;
        case 3: _t->on_buttonEdgeAdd_clicked(); break;
        case 4: _t->on_buttonEdgeRemove_clicked(); break;
        case 5: _t->on_listWidget_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 6: _t->onDeleteEdge(); break;
        case 7: _t->onFlipOrientation(); break;
        case 8: _t->clearSelection(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SurfaceGui::GeomFillSurface::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_SurfaceGui__GeomFillSurface.data,
    qt_meta_data_SurfaceGui__GeomFillSurface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SurfaceGui::GeomFillSurface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SurfaceGui::GeomFillSurface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SurfaceGui__GeomFillSurface.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionObserver"))
        return static_cast< Gui::SelectionObserver*>(this);
    if (!strcmp(_clname, "Gui::DocumentObserver"))
        return static_cast< Gui::DocumentObserver*>(this);
    return QWidget::qt_metacast(_clname);
}

int SurfaceGui::GeomFillSurface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
struct qt_meta_stringdata_SurfaceGui__TaskGeomFillSurface_t {
    QByteArrayData data[1];
    char stringdata0[32];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SurfaceGui__TaskGeomFillSurface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SurfaceGui__TaskGeomFillSurface_t qt_meta_stringdata_SurfaceGui__TaskGeomFillSurface = {
    {
QT_MOC_LITERAL(0, 0, 31) // "SurfaceGui::TaskGeomFillSurface"

    },
    "SurfaceGui::TaskGeomFillSurface"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SurfaceGui__TaskGeomFillSurface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void SurfaceGui::TaskGeomFillSurface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SurfaceGui::TaskGeomFillSurface::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_SurfaceGui__TaskGeomFillSurface.data,
    qt_meta_data_SurfaceGui__TaskGeomFillSurface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SurfaceGui::TaskGeomFillSurface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SurfaceGui::TaskGeomFillSurface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SurfaceGui__TaskGeomFillSurface.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int SurfaceGui::TaskGeomFillSurface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
