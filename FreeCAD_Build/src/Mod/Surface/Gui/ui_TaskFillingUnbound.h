/********************************************************************************
** Form generated from reading UI file 'TaskFillingUnbound.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFILLINGUNBOUND_H
#define UI_TASKFILLINGUNBOUND_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

namespace SurfaceGui {

class Ui_TaskFillingUnbound
{
public:
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QListWidget *listUnbound;
    QLabel *label;
    QComboBox *comboBoxUnboundFaces;
    QLabel *label_2;
    QComboBox *comboBoxUnboundCont;
    QSpacerItem *horizontalSpacer;
    QPushButton *buttonUnboundAccept;
    QPushButton *buttonUnboundIgnore;
    QHBoxLayout *horizontalLayout;
    QToolButton *buttonUnboundEdgeAdd;
    QToolButton *buttonUnboundEdgeRemove;
    QLabel *statusLabel;

    void setupUi(QWidget *SurfaceGui__TaskFillingUnbound)
    {
        if (SurfaceGui__TaskFillingUnbound->objectName().isEmpty())
            SurfaceGui__TaskFillingUnbound->setObjectName(QString::fromUtf8("SurfaceGui__TaskFillingUnbound"));
        SurfaceGui__TaskFillingUnbound->resize(360, 400);
        QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SurfaceGui__TaskFillingUnbound->sizePolicy().hasHeightForWidth());
        SurfaceGui__TaskFillingUnbound->setSizePolicy(sizePolicy);
        gridLayout_2 = new QGridLayout(SurfaceGui__TaskFillingUnbound);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        groupBox = new QGroupBox(SurfaceGui__TaskFillingUnbound);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        listUnbound = new QListWidget(groupBox);
        listUnbound->setObjectName(QString::fromUtf8("listUnbound"));

        gridLayout->addWidget(listUnbound, 1, 0, 1, 3);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(label, 3, 0, 1, 1);

        comboBoxUnboundFaces = new QComboBox(groupBox);
        comboBoxUnboundFaces->setObjectName(QString::fromUtf8("comboBoxUnboundFaces"));
        comboBoxUnboundFaces->setEnabled(false);

        gridLayout->addWidget(comboBoxUnboundFaces, 3, 1, 1, 2);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(label_2, 4, 0, 1, 1);

        comboBoxUnboundCont = new QComboBox(groupBox);
        comboBoxUnboundCont->setObjectName(QString::fromUtf8("comboBoxUnboundCont"));
        comboBoxUnboundCont->setEnabled(false);

        gridLayout->addWidget(comboBoxUnboundCont, 4, 1, 1, 2);

        horizontalSpacer = new QSpacerItem(74, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 5, 0, 1, 1);

        buttonUnboundAccept = new QPushButton(groupBox);
        buttonUnboundAccept->setObjectName(QString::fromUtf8("buttonUnboundAccept"));
        buttonUnboundAccept->setEnabled(false);

        gridLayout->addWidget(buttonUnboundAccept, 5, 1, 1, 1);

        buttonUnboundIgnore = new QPushButton(groupBox);
        buttonUnboundIgnore->setObjectName(QString::fromUtf8("buttonUnboundIgnore"));
        buttonUnboundIgnore->setEnabled(false);

        gridLayout->addWidget(buttonUnboundIgnore, 5, 2, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        buttonUnboundEdgeAdd = new QToolButton(groupBox);
        buttonUnboundEdgeAdd->setObjectName(QString::fromUtf8("buttonUnboundEdgeAdd"));
        buttonUnboundEdgeAdd->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(buttonUnboundEdgeAdd->sizePolicy().hasHeightForWidth());
        buttonUnboundEdgeAdd->setSizePolicy(sizePolicy2);
        buttonUnboundEdgeAdd->setChecked(false);

        horizontalLayout->addWidget(buttonUnboundEdgeAdd);

        buttonUnboundEdgeRemove = new QToolButton(groupBox);
        buttonUnboundEdgeRemove->setObjectName(QString::fromUtf8("buttonUnboundEdgeRemove"));
        sizePolicy2.setHeightForWidth(buttonUnboundEdgeRemove->sizePolicy().hasHeightForWidth());
        buttonUnboundEdgeRemove->setSizePolicy(sizePolicy2);

        horizontalLayout->addWidget(buttonUnboundEdgeRemove);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 3);

        statusLabel = new QLabel(groupBox);
        statusLabel->setObjectName(QString::fromUtf8("statusLabel"));
        statusLabel->setText(QString::fromUtf8("Status messages"));

        gridLayout->addWidget(statusLabel, 2, 0, 1, 3);


        gridLayout_2->addWidget(groupBox, 0, 0, 1, 1);


        retranslateUi(SurfaceGui__TaskFillingUnbound);

        QMetaObject::connectSlotsByName(SurfaceGui__TaskFillingUnbound);
    } // setupUi

    void retranslateUi(QWidget *SurfaceGui__TaskFillingUnbound)
    {
        SurfaceGui__TaskFillingUnbound->setWindowTitle(QApplication::translate("SurfaceGui::TaskFillingUnbound", "Unbound Edges", nullptr));
        groupBox->setTitle(QApplication::translate("SurfaceGui::TaskFillingUnbound", "Unbound Edges", nullptr));
        label->setText(QApplication::translate("SurfaceGui::TaskFillingUnbound", "Faces:", nullptr));
        label_2->setText(QApplication::translate("SurfaceGui::TaskFillingUnbound", "Continuity:", nullptr));
        buttonUnboundAccept->setText(QApplication::translate("SurfaceGui::TaskFillingUnbound", "Accept", nullptr));
        buttonUnboundIgnore->setText(QApplication::translate("SurfaceGui::TaskFillingUnbound", "Ignore", nullptr));
        buttonUnboundEdgeAdd->setText(QApplication::translate("SurfaceGui::TaskFillingUnbound", "Add Edge", nullptr));
        buttonUnboundEdgeRemove->setText(QApplication::translate("SurfaceGui::TaskFillingUnbound", "Remove Edge", nullptr));
    } // retranslateUi

};

} // namespace SurfaceGui

namespace SurfaceGui {
namespace Ui {
    class TaskFillingUnbound: public Ui_TaskFillingUnbound {};
} // namespace Ui
} // namespace SurfaceGui

#endif // UI_TASKFILLINGUNBOUND_H
