/****************************************************************************
** Meta object code from reading C++ file 'TaskFilling.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Surface/Gui/TaskFilling.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskFilling.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SurfaceGui__FillingPanel_t {
    QByteArrayData data[12];
    char stringdata0[264];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SurfaceGui__FillingPanel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SurfaceGui__FillingPanel_t qt_meta_stringdata_SurfaceGui__FillingPanel = {
    {
QT_MOC_LITERAL(0, 0, 24), // "SurfaceGui::FillingPanel"
QT_MOC_LITERAL(1, 25, 25), // "on_buttonInitFace_clicked"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 24), // "on_buttonEdgeAdd_clicked"
QT_MOC_LITERAL(4, 77, 27), // "on_buttonEdgeRemove_clicked"
QT_MOC_LITERAL(5, 105, 31), // "on_lineInitFaceName_textChanged"
QT_MOC_LITERAL(6, 137, 33), // "on_listBoundary_itemDoubleCli..."
QT_MOC_LITERAL(7, 171, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(8, 188, 23), // "on_buttonAccept_clicked"
QT_MOC_LITERAL(9, 212, 23), // "on_buttonIgnore_clicked"
QT_MOC_LITERAL(10, 236, 12), // "onDeleteEdge"
QT_MOC_LITERAL(11, 249, 14) // "clearSelection"

    },
    "SurfaceGui::FillingPanel\0"
    "on_buttonInitFace_clicked\0\0"
    "on_buttonEdgeAdd_clicked\0"
    "on_buttonEdgeRemove_clicked\0"
    "on_lineInitFaceName_textChanged\0"
    "on_listBoundary_itemDoubleClicked\0"
    "QListWidgetItem*\0on_buttonAccept_clicked\0"
    "on_buttonIgnore_clicked\0onDeleteEdge\0"
    "clearSelection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SurfaceGui__FillingPanel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    0,   61,    2, 0x08 /* Private */,
       5,    1,   62,    2, 0x08 /* Private */,
       6,    1,   65,    2, 0x08 /* Private */,
       8,    0,   68,    2, 0x08 /* Private */,
       9,    0,   69,    2, 0x08 /* Private */,
      10,    0,   70,    2, 0x08 /* Private */,
      11,    0,   71,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, 0x80000000 | 7,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SurfaceGui::FillingPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<FillingPanel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_buttonInitFace_clicked(); break;
        case 1: _t->on_buttonEdgeAdd_clicked(); break;
        case 2: _t->on_buttonEdgeRemove_clicked(); break;
        case 3: _t->on_lineInitFaceName_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->on_listBoundary_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 5: _t->on_buttonAccept_clicked(); break;
        case 6: _t->on_buttonIgnore_clicked(); break;
        case 7: _t->onDeleteEdge(); break;
        case 8: _t->clearSelection(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SurfaceGui::FillingPanel::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_SurfaceGui__FillingPanel.data,
    qt_meta_data_SurfaceGui__FillingPanel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SurfaceGui::FillingPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SurfaceGui::FillingPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SurfaceGui__FillingPanel.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionObserver"))
        return static_cast< Gui::SelectionObserver*>(this);
    if (!strcmp(_clname, "Gui::DocumentObserver"))
        return static_cast< Gui::DocumentObserver*>(this);
    return QWidget::qt_metacast(_clname);
}

int SurfaceGui::FillingPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
struct qt_meta_stringdata_SurfaceGui__TaskFilling_t {
    QByteArrayData data[1];
    char stringdata0[24];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SurfaceGui__TaskFilling_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SurfaceGui__TaskFilling_t qt_meta_stringdata_SurfaceGui__TaskFilling = {
    {
QT_MOC_LITERAL(0, 0, 23) // "SurfaceGui::TaskFilling"

    },
    "SurfaceGui::TaskFilling"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SurfaceGui__TaskFilling[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void SurfaceGui::TaskFilling::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SurfaceGui::TaskFilling::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_SurfaceGui__TaskFilling.data,
    qt_meta_data_SurfaceGui__TaskFilling,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SurfaceGui::TaskFilling::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SurfaceGui::TaskFilling::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SurfaceGui__TaskFilling.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int SurfaceGui::TaskFilling::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
