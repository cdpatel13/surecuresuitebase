/****************************************************************************
** Meta object code from reading C++ file 'TaskFillingVertex.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Surface/Gui/TaskFillingVertex.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskFillingVertex.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SurfaceGui__FillingVertexPanel_t {
    QByteArrayData data[6];
    char stringdata0[119];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SurfaceGui__FillingVertexPanel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SurfaceGui__FillingVertexPanel_t qt_meta_stringdata_SurfaceGui__FillingVertexPanel = {
    {
QT_MOC_LITERAL(0, 0, 30), // "SurfaceGui::FillingVertexPanel"
QT_MOC_LITERAL(1, 31, 26), // "on_buttonVertexAdd_clicked"
QT_MOC_LITERAL(2, 58, 0), // ""
QT_MOC_LITERAL(3, 59, 29), // "on_buttonVertexRemove_clicked"
QT_MOC_LITERAL(4, 89, 14), // "onDeleteVertex"
QT_MOC_LITERAL(5, 104, 14) // "clearSelection"

    },
    "SurfaceGui::FillingVertexPanel\0"
    "on_buttonVertexAdd_clicked\0\0"
    "on_buttonVertexRemove_clicked\0"
    "onDeleteVertex\0clearSelection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SurfaceGui__FillingVertexPanel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x08 /* Private */,
       3,    0,   35,    2, 0x08 /* Private */,
       4,    0,   36,    2, 0x08 /* Private */,
       5,    0,   37,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SurfaceGui::FillingVertexPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<FillingVertexPanel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_buttonVertexAdd_clicked(); break;
        case 1: _t->on_buttonVertexRemove_clicked(); break;
        case 2: _t->onDeleteVertex(); break;
        case 3: _t->clearSelection(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SurfaceGui::FillingVertexPanel::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_SurfaceGui__FillingVertexPanel.data,
    qt_meta_data_SurfaceGui__FillingVertexPanel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SurfaceGui::FillingVertexPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SurfaceGui::FillingVertexPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SurfaceGui__FillingVertexPanel.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionObserver"))
        return static_cast< Gui::SelectionObserver*>(this);
    if (!strcmp(_clname, "Gui::DocumentObserver"))
        return static_cast< Gui::DocumentObserver*>(this);
    return QWidget::qt_metacast(_clname);
}

int SurfaceGui::FillingVertexPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
