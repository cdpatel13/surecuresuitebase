# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Test" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/BaseTests.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/Document.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/Menu.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/TestApp.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/TestGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/UnicodeTests.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/UnitTests.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/Workbench.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/unittestgui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/testmakeWireString.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/TestPythonSyntax.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Test/InitGui.py"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Test/Gui/cmake_install.cmake")

endif()

