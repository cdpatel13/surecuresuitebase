# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/PartDesign" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/TestPartDesignApp.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/InitGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/TestPartDesignGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/InvoluteGearFeature.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/InvoluteGearFeature.ui"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/PartDesign/Scripts" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/Scripts/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/Scripts/DistanceBolt.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/Scripts/Epitrochoid.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/Scripts/FilletArc.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/Scripts/Gear.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/Scripts/Parallelepiped.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/Scripts/RadialCopy.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/Scripts/Spring.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/PartDesign/PartDesignTests" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestDatum.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestShapeBinder.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestPad.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestPocket.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestHole.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestRevolve.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestLoft.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestPipe.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestPrimitive.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestMirrored.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestLinearPattern.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestPolarPattern.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestMultiTransform.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestBoolean.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestFillet.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestChamfer.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestDraft.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/PartDesignTests/TestThickness.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/PartDesign/fcgear" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/fcgear/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/fcgear/fcgear.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/fcgear/fcgeardialog.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/fcgear/involute.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/fcgear/svggear.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/PartDesign/WizardShaft" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/WizardShaft/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/WizardShaft/WizardShaft.svg"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/WizardShaft/WizardShaft.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/WizardShaft/WizardShaftTable.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/WizardShaft/Shaft.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/WizardShaft/ShaftFeature.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/WizardShaft/ShaftDiagram.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/PartDesign/WizardShaft/SegmentFunction.py"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/PartDesign/App/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/PartDesign/Gui/cmake_install.cmake")

endif()

