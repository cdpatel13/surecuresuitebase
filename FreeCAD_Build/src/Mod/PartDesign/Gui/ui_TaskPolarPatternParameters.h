/********************************************************************************
** Form generated from reading UI file 'TaskPolarPatternParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKPOLARPATTERNPARAMETERS_H
#define UI_TASKPOLARPATTERNPARAMETERS_H

#include <Gui/SpinBox.h>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"

namespace PartDesignGui {

class Ui_TaskPolarPatternParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *buttonAddFeature;
    QPushButton *buttonRemoveFeature;
    QListWidget *listWidgetFeatures;
    QHBoxLayout *horizontalLayout_3;
    QLabel *labelAxis;
    QComboBox *comboAxis;
    QCheckBox *checkReverse;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    Gui::QuantitySpinBox *polarAngle;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    Gui::UIntSpinBox *spinOccurrences;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *buttonOK;
    QCheckBox *checkBoxUpdateView;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *PartDesignGui__TaskPolarPatternParameters)
    {
        if (PartDesignGui__TaskPolarPatternParameters->objectName().isEmpty())
            PartDesignGui__TaskPolarPatternParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskPolarPatternParameters"));
        PartDesignGui__TaskPolarPatternParameters->resize(253, 366);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskPolarPatternParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        buttonAddFeature = new QPushButton(PartDesignGui__TaskPolarPatternParameters);
        buttonAddFeature->setObjectName(QString::fromUtf8("buttonAddFeature"));
        buttonAddFeature->setCheckable(true);

        horizontalLayout_4->addWidget(buttonAddFeature);

        buttonRemoveFeature = new QPushButton(PartDesignGui__TaskPolarPatternParameters);
        buttonRemoveFeature->setObjectName(QString::fromUtf8("buttonRemoveFeature"));
        buttonRemoveFeature->setCheckable(true);

        horizontalLayout_4->addWidget(buttonRemoveFeature);


        verticalLayout->addLayout(horizontalLayout_4);

        listWidgetFeatures = new QListWidget(PartDesignGui__TaskPolarPatternParameters);
        listWidgetFeatures->setObjectName(QString::fromUtf8("listWidgetFeatures"));

        verticalLayout->addWidget(listWidgetFeatures);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        labelAxis = new QLabel(PartDesignGui__TaskPolarPatternParameters);
        labelAxis->setObjectName(QString::fromUtf8("labelAxis"));

        horizontalLayout_3->addWidget(labelAxis);

        comboAxis = new QComboBox(PartDesignGui__TaskPolarPatternParameters);
        comboAxis->setObjectName(QString::fromUtf8("comboAxis"));

        horizontalLayout_3->addWidget(comboAxis);


        verticalLayout->addLayout(horizontalLayout_3);

        checkReverse = new QCheckBox(PartDesignGui__TaskPolarPatternParameters);
        checkReverse->setObjectName(QString::fromUtf8("checkReverse"));

        verticalLayout->addWidget(checkReverse);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(PartDesignGui__TaskPolarPatternParameters);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        polarAngle = new Gui::QuantitySpinBox(PartDesignGui__TaskPolarPatternParameters);
        polarAngle->setObjectName(QString::fromUtf8("polarAngle"));
        polarAngle->setProperty("unit", QVariant(QString::fromUtf8("deg")));
        polarAngle->setProperty("minimum", QVariant(0.000000000000000));
        polarAngle->setProperty("maximum", QVariant(360.000000000000000));
        polarAngle->setProperty("value", QVariant(360.000000000000000));

        horizontalLayout_2->addWidget(polarAngle);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(PartDesignGui__TaskPolarPatternParameters);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        spinOccurrences = new Gui::UIntSpinBox(PartDesignGui__TaskPolarPatternParameters);
        spinOccurrences->setObjectName(QString::fromUtf8("spinOccurrences"));

        horizontalLayout->addWidget(spinOccurrences);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        buttonOK = new QPushButton(PartDesignGui__TaskPolarPatternParameters);
        buttonOK->setObjectName(QString::fromUtf8("buttonOK"));

        horizontalLayout_5->addWidget(buttonOK);


        verticalLayout->addLayout(horizontalLayout_5);

        checkBoxUpdateView = new QCheckBox(PartDesignGui__TaskPolarPatternParameters);
        checkBoxUpdateView->setObjectName(QString::fromUtf8("checkBoxUpdateView"));
        checkBoxUpdateView->setChecked(true);

        verticalLayout->addWidget(checkBoxUpdateView);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(PartDesignGui__TaskPolarPatternParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskPolarPatternParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskPolarPatternParameters)
    {
        PartDesignGui__TaskPolarPatternParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskPolarPatternParameters", "Form", nullptr));
        buttonAddFeature->setText(QApplication::translate("PartDesignGui::TaskPolarPatternParameters", "Add feature", nullptr));
        buttonRemoveFeature->setText(QApplication::translate("PartDesignGui::TaskPolarPatternParameters", "Remove feature", nullptr));
        labelAxis->setText(QApplication::translate("PartDesignGui::TaskPolarPatternParameters", "Axis", nullptr));
        checkReverse->setText(QApplication::translate("PartDesignGui::TaskPolarPatternParameters", "Reverse direction", nullptr));
        label_2->setText(QApplication::translate("PartDesignGui::TaskPolarPatternParameters", "Angle", nullptr));
        label->setText(QApplication::translate("PartDesignGui::TaskPolarPatternParameters", "Occurrences", nullptr));
        buttonOK->setText(QApplication::translate("PartDesignGui::TaskPolarPatternParameters", "OK", nullptr));
        checkBoxUpdateView->setText(QApplication::translate("PartDesignGui::TaskPolarPatternParameters", "Update view", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskPolarPatternParameters: public Ui_TaskPolarPatternParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKPOLARPATTERNPARAMETERS_H
