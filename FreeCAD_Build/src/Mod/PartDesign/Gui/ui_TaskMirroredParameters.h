/********************************************************************************
** Form generated from reading UI file 'TaskMirroredParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKMIRROREDPARAMETERS_H
#define UI_TASKMIRROREDPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace PartDesignGui {

class Ui_TaskMirroredParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *buttonAddFeature;
    QPushButton *buttonRemoveFeature;
    QListWidget *listWidgetFeatures;
    QHBoxLayout *horizontalLayout;
    QLabel *labelPlane;
    QComboBox *comboPlane;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *buttonOK;
    QCheckBox *checkBoxUpdateView;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *PartDesignGui__TaskMirroredParameters)
    {
        if (PartDesignGui__TaskMirroredParameters->objectName().isEmpty())
            PartDesignGui__TaskMirroredParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskMirroredParameters"));
        PartDesignGui__TaskMirroredParameters->resize(253, 260);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskMirroredParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        buttonAddFeature = new QPushButton(PartDesignGui__TaskMirroredParameters);
        buttonAddFeature->setObjectName(QString::fromUtf8("buttonAddFeature"));
        buttonAddFeature->setCheckable(true);

        horizontalLayout_2->addWidget(buttonAddFeature);

        buttonRemoveFeature = new QPushButton(PartDesignGui__TaskMirroredParameters);
        buttonRemoveFeature->setObjectName(QString::fromUtf8("buttonRemoveFeature"));
        buttonRemoveFeature->setCheckable(true);

        horizontalLayout_2->addWidget(buttonRemoveFeature);


        verticalLayout->addLayout(horizontalLayout_2);

        listWidgetFeatures = new QListWidget(PartDesignGui__TaskMirroredParameters);
        listWidgetFeatures->setObjectName(QString::fromUtf8("listWidgetFeatures"));

        verticalLayout->addWidget(listWidgetFeatures);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        labelPlane = new QLabel(PartDesignGui__TaskMirroredParameters);
        labelPlane->setObjectName(QString::fromUtf8("labelPlane"));

        horizontalLayout->addWidget(labelPlane);

        comboPlane = new QComboBox(PartDesignGui__TaskMirroredParameters);
        comboPlane->setObjectName(QString::fromUtf8("comboPlane"));

        horizontalLayout->addWidget(comboPlane);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        buttonOK = new QPushButton(PartDesignGui__TaskMirroredParameters);
        buttonOK->setObjectName(QString::fromUtf8("buttonOK"));

        horizontalLayout_3->addWidget(buttonOK);


        verticalLayout->addLayout(horizontalLayout_3);

        checkBoxUpdateView = new QCheckBox(PartDesignGui__TaskMirroredParameters);
        checkBoxUpdateView->setObjectName(QString::fromUtf8("checkBoxUpdateView"));
        checkBoxUpdateView->setChecked(true);

        verticalLayout->addWidget(checkBoxUpdateView);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(PartDesignGui__TaskMirroredParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskMirroredParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskMirroredParameters)
    {
        PartDesignGui__TaskMirroredParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskMirroredParameters", "Form", nullptr));
        buttonAddFeature->setText(QApplication::translate("PartDesignGui::TaskMirroredParameters", "Add feature", nullptr));
        buttonRemoveFeature->setText(QApplication::translate("PartDesignGui::TaskMirroredParameters", "Remove feature", nullptr));
        labelPlane->setText(QApplication::translate("PartDesignGui::TaskMirroredParameters", "Plane", nullptr));
        buttonOK->setText(QApplication::translate("PartDesignGui::TaskMirroredParameters", "OK", nullptr));
        checkBoxUpdateView->setText(QApplication::translate("PartDesignGui::TaskMirroredParameters", "Update view", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskMirroredParameters: public Ui_TaskMirroredParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKMIRROREDPARAMETERS_H
