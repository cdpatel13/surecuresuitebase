/********************************************************************************
** Form generated from reading UI file 'TaskFilletParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKFILLETPARAMETERS_H
#define UI_TASKFILLETPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"

namespace PartDesignGui {

class Ui_TaskFilletParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QToolButton *buttonRefAdd;
    QToolButton *buttonRefRemove;
    QListWidget *listWidgetReferences;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    Gui::QuantitySpinBox *filletRadius;

    void setupUi(QWidget *PartDesignGui__TaskFilletParameters)
    {
        if (PartDesignGui__TaskFilletParameters->objectName().isEmpty())
            PartDesignGui__TaskFilletParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskFilletParameters"));
        PartDesignGui__TaskFilletParameters->resize(208, 164);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskFilletParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        buttonRefAdd = new QToolButton(PartDesignGui__TaskFilletParameters);
        buttonRefAdd->setObjectName(QString::fromUtf8("buttonRefAdd"));
        buttonRefAdd->setCheckable(true);

        horizontalLayout->addWidget(buttonRefAdd);

        buttonRefRemove = new QToolButton(PartDesignGui__TaskFilletParameters);
        buttonRefRemove->setObjectName(QString::fromUtf8("buttonRefRemove"));
        buttonRefRemove->setCheckable(true);

        horizontalLayout->addWidget(buttonRefRemove);


        verticalLayout->addLayout(horizontalLayout);

        listWidgetReferences = new QListWidget(PartDesignGui__TaskFilletParameters);
        listWidgetReferences->setObjectName(QString::fromUtf8("listWidgetReferences"));

        verticalLayout->addWidget(listWidgetReferences);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(PartDesignGui__TaskFilletParameters);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        filletRadius = new Gui::QuantitySpinBox(PartDesignGui__TaskFilletParameters);
        filletRadius->setObjectName(QString::fromUtf8("filletRadius"));

        horizontalLayout_2->addWidget(filletRadius);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(PartDesignGui__TaskFilletParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskFilletParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskFilletParameters)
    {
        PartDesignGui__TaskFilletParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskFilletParameters", "Form", nullptr));
        buttonRefAdd->setText(QApplication::translate("PartDesignGui::TaskFilletParameters", "Add ref", nullptr));
        buttonRefRemove->setText(QApplication::translate("PartDesignGui::TaskFilletParameters", "Remove ref", nullptr));
        label->setText(QApplication::translate("PartDesignGui::TaskFilletParameters", "Radius:", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskFilletParameters: public Ui_TaskFilletParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKFILLETPARAMETERS_H
