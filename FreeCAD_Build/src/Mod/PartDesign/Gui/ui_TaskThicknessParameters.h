/********************************************************************************
** Form generated from reading UI file 'TaskThicknessParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKTHICKNESSPARAMETERS_H
#define UI_TASKTHICKNESSPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"

namespace PartDesignGui {

class Ui_TaskThicknessParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QToolButton *buttonRefAdd;
    QToolButton *buttonRefRemove;
    QListWidget *listWidgetReferences;
    QGridLayout *gridLayout_2;
    QLabel *label;
    Gui::QuantitySpinBox *Value;
    QLabel *label_2;
    QLabel *label_3;
    QComboBox *modeComboBox;
    QComboBox *joinComboBox;
    QCheckBox *checkIntersection;
    QCheckBox *checkReverse;

    void setupUi(QWidget *PartDesignGui__TaskThicknessParameters)
    {
        if (PartDesignGui__TaskThicknessParameters->objectName().isEmpty())
            PartDesignGui__TaskThicknessParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskThicknessParameters"));
        PartDesignGui__TaskThicknessParameters->resize(321, 509);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskThicknessParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        buttonRefAdd = new QToolButton(PartDesignGui__TaskThicknessParameters);
        buttonRefAdd->setObjectName(QString::fromUtf8("buttonRefAdd"));
        buttonRefAdd->setCheckable(true);

        horizontalLayout->addWidget(buttonRefAdd);

        buttonRefRemove = new QToolButton(PartDesignGui__TaskThicknessParameters);
        buttonRefRemove->setObjectName(QString::fromUtf8("buttonRefRemove"));
        buttonRefRemove->setCheckable(true);

        horizontalLayout->addWidget(buttonRefRemove);


        verticalLayout->addLayout(horizontalLayout);

        listWidgetReferences = new QListWidget(PartDesignGui__TaskThicknessParameters);
        listWidgetReferences->setObjectName(QString::fromUtf8("listWidgetReferences"));

        verticalLayout->addWidget(listWidgetReferences);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label = new QLabel(PartDesignGui__TaskThicknessParameters);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        Value = new Gui::QuantitySpinBox(PartDesignGui__TaskThicknessParameters);
        Value->setObjectName(QString::fromUtf8("Value"));
        Value->setFocusPolicy(Qt::TabFocus);
        Value->setProperty("unit", QVariant(QString::fromUtf8("mm")));
        Value->setProperty("minimum", QVariant(0.000000000000000));
        Value->setProperty("maximum", QVariant(999999999.000000000000000));
        Value->setProperty("singleStep", QVariant(0.100000000000000));
        Value->setProperty("value", QVariant(1.000000000000000));

        gridLayout_2->addWidget(Value, 0, 1, 1, 1);

        label_2 = new QLabel(PartDesignGui__TaskThicknessParameters);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        label_3 = new QLabel(PartDesignGui__TaskThicknessParameters);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 2, 0, 1, 1);

        modeComboBox = new QComboBox(PartDesignGui__TaskThicknessParameters);
        modeComboBox->addItem(QString());
        modeComboBox->addItem(QString());
        modeComboBox->addItem(QString());
        modeComboBox->setObjectName(QString::fromUtf8("modeComboBox"));

        gridLayout_2->addWidget(modeComboBox, 1, 1, 1, 1);

        joinComboBox = new QComboBox(PartDesignGui__TaskThicknessParameters);
        joinComboBox->addItem(QString());
        joinComboBox->addItem(QString());
        joinComboBox->setObjectName(QString::fromUtf8("joinComboBox"));

        gridLayout_2->addWidget(joinComboBox, 2, 1, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        checkIntersection = new QCheckBox(PartDesignGui__TaskThicknessParameters);
        checkIntersection->setObjectName(QString::fromUtf8("checkIntersection"));

        verticalLayout->addWidget(checkIntersection);

        checkReverse = new QCheckBox(PartDesignGui__TaskThicknessParameters);
        checkReverse->setObjectName(QString::fromUtf8("checkReverse"));

        verticalLayout->addWidget(checkReverse);

        QWidget::setTabOrder(Value, modeComboBox);
        QWidget::setTabOrder(modeComboBox, joinComboBox);
        QWidget::setTabOrder(joinComboBox, checkIntersection);
        QWidget::setTabOrder(checkIntersection, checkReverse);
        QWidget::setTabOrder(checkReverse, buttonRefAdd);
        QWidget::setTabOrder(buttonRefAdd, buttonRefRemove);
        QWidget::setTabOrder(buttonRefRemove, listWidgetReferences);

        retranslateUi(PartDesignGui__TaskThicknessParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskThicknessParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskThicknessParameters)
    {
        PartDesignGui__TaskThicknessParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskThicknessParameters", "Form", nullptr));
        buttonRefAdd->setText(QApplication::translate("PartDesignGui::TaskThicknessParameters", "Add face", nullptr));
        buttonRefRemove->setText(QApplication::translate("PartDesignGui::TaskThicknessParameters", "Remove face", nullptr));
        label->setText(QApplication::translate("PartDesignGui::TaskThicknessParameters", "Thickness", nullptr));
        label_2->setText(QApplication::translate("PartDesignGui::TaskThicknessParameters", "Mode", nullptr));
        label_3->setText(QApplication::translate("PartDesignGui::TaskThicknessParameters", "Join Type", nullptr));
        modeComboBox->setItemText(0, QApplication::translate("PartDesignGui::TaskThicknessParameters", "Skin", nullptr));
        modeComboBox->setItemText(1, QApplication::translate("PartDesignGui::TaskThicknessParameters", "Pipe", nullptr));
        modeComboBox->setItemText(2, QApplication::translate("PartDesignGui::TaskThicknessParameters", "Recto Verso", nullptr));

        joinComboBox->setItemText(0, QApplication::translate("PartDesignGui::TaskThicknessParameters", "Arc", nullptr));
        joinComboBox->setItemText(1, QApplication::translate("PartDesignGui::TaskThicknessParameters", "Intersection", nullptr));

        checkIntersection->setText(QApplication::translate("PartDesignGui::TaskThicknessParameters", "Intersection", nullptr));
        checkReverse->setText(QApplication::translate("PartDesignGui::TaskThicknessParameters", "Make thickness inwards", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskThicknessParameters: public Ui_TaskThicknessParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKTHICKNESSPARAMETERS_H
