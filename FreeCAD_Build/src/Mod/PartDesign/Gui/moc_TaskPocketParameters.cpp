/****************************************************************************
** Meta object code from reading C++ file 'TaskPocketParameters.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/PartDesign/Gui/TaskPocketParameters.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskPocketParameters.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PartDesignGui__TaskPocketParameters_t {
    QByteArrayData data[12];
    char stringdata0[173];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskPocketParameters_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskPocketParameters_t qt_meta_stringdata_PartDesignGui__TaskPocketParameters = {
    {
QT_MOC_LITERAL(0, 0, 35), // "PartDesignGui::TaskPocketPara..."
QT_MOC_LITERAL(1, 36, 15), // "onLengthChanged"
QT_MOC_LITERAL(2, 52, 0), // ""
QT_MOC_LITERAL(3, 53, 16), // "onLength2Changed"
QT_MOC_LITERAL(4, 70, 15), // "onOffsetChanged"
QT_MOC_LITERAL(5, 86, 17), // "onMidplaneChanged"
QT_MOC_LITERAL(6, 104, 17), // "onReversedChanged"
QT_MOC_LITERAL(7, 122, 12), // "onButtonFace"
QT_MOC_LITERAL(8, 135, 7), // "pressed"
QT_MOC_LITERAL(9, 143, 10), // "onFaceName"
QT_MOC_LITERAL(10, 154, 4), // "text"
QT_MOC_LITERAL(11, 159, 13) // "onModeChanged"

    },
    "PartDesignGui::TaskPocketParameters\0"
    "onLengthChanged\0\0onLength2Changed\0"
    "onOffsetChanged\0onMidplaneChanged\0"
    "onReversedChanged\0onButtonFace\0pressed\0"
    "onFaceName\0text\0onModeChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskPocketParameters[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x08 /* Private */,
       3,    1,   62,    2, 0x08 /* Private */,
       4,    1,   65,    2, 0x08 /* Private */,
       5,    1,   68,    2, 0x08 /* Private */,
       6,    1,   71,    2, 0x08 /* Private */,
       7,    1,   74,    2, 0x08 /* Private */,
       7,    0,   77,    2, 0x28 /* Private | MethodCloned */,
       9,    1,   78,    2, 0x08 /* Private */,
      11,    1,   81,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void PartDesignGui::TaskPocketParameters::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPocketParameters *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onLengthChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->onLength2Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->onOffsetChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->onMidplaneChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->onReversedChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->onButtonFace((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 6: _t->onButtonFace(); break;
        case 7: _t->onFaceName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->onModeChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskPocketParameters::staticMetaObject = { {
    &TaskSketchBasedParameters::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskPocketParameters.data,
    qt_meta_data_PartDesignGui__TaskPocketParameters,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskPocketParameters::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskPocketParameters::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskPocketParameters.stringdata0))
        return static_cast<void*>(this);
    return TaskSketchBasedParameters::qt_metacast(_clname);
}

int PartDesignGui::TaskPocketParameters::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskSketchBasedParameters::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
struct qt_meta_stringdata_PartDesignGui__TaskDlgPocketParameters_t {
    QByteArrayData data[1];
    char stringdata0[39];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskDlgPocketParameters_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskDlgPocketParameters_t qt_meta_stringdata_PartDesignGui__TaskDlgPocketParameters = {
    {
QT_MOC_LITERAL(0, 0, 38) // "PartDesignGui::TaskDlgPocketP..."

    },
    "PartDesignGui::TaskDlgPocketParameters"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskDlgPocketParameters[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PartDesignGui::TaskDlgPocketParameters::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskDlgPocketParameters::staticMetaObject = { {
    &TaskDlgSketchBasedParameters::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskDlgPocketParameters.data,
    qt_meta_data_PartDesignGui__TaskDlgPocketParameters,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskDlgPocketParameters::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskDlgPocketParameters::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskDlgPocketParameters.stringdata0))
        return static_cast<void*>(this);
    return TaskDlgSketchBasedParameters::qt_metacast(_clname);
}

int PartDesignGui::TaskDlgPocketParameters::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskDlgSketchBasedParameters::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
