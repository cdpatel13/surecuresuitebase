/********************************************************************************
** Form generated from reading UI file 'TaskChamferParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKCHAMFERPARAMETERS_H
#define UI_TASKCHAMFERPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"

namespace PartDesignGui {

class Ui_TaskChamferParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QToolButton *buttonRefAdd;
    QToolButton *buttonRefRemove;
    QListWidget *listWidgetReferences;
    Gui::QuantitySpinBox *chamferDistance;
    QLabel *label;

    void setupUi(QWidget *PartDesignGui__TaskChamferParameters)
    {
        if (PartDesignGui__TaskChamferParameters->objectName().isEmpty())
            PartDesignGui__TaskChamferParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskChamferParameters"));
        PartDesignGui__TaskChamferParameters->resize(182, 185);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskChamferParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        buttonRefAdd = new QToolButton(PartDesignGui__TaskChamferParameters);
        buttonRefAdd->setObjectName(QString::fromUtf8("buttonRefAdd"));
        buttonRefAdd->setCheckable(true);

        horizontalLayout->addWidget(buttonRefAdd);

        buttonRefRemove = new QToolButton(PartDesignGui__TaskChamferParameters);
        buttonRefRemove->setObjectName(QString::fromUtf8("buttonRefRemove"));
        buttonRefRemove->setCheckable(true);

        horizontalLayout->addWidget(buttonRefRemove);


        verticalLayout->addLayout(horizontalLayout);

        listWidgetReferences = new QListWidget(PartDesignGui__TaskChamferParameters);
        listWidgetReferences->setObjectName(QString::fromUtf8("listWidgetReferences"));

        verticalLayout->addWidget(listWidgetReferences);

        chamferDistance = new Gui::QuantitySpinBox(PartDesignGui__TaskChamferParameters);
        chamferDistance->setObjectName(QString::fromUtf8("chamferDistance"));

        verticalLayout->addWidget(chamferDistance);

        label = new QLabel(PartDesignGui__TaskChamferParameters);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);


        retranslateUi(PartDesignGui__TaskChamferParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskChamferParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskChamferParameters)
    {
        PartDesignGui__TaskChamferParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskChamferParameters", "Form", nullptr));
        buttonRefAdd->setText(QApplication::translate("PartDesignGui::TaskChamferParameters", "Add ref", nullptr));
        buttonRefRemove->setText(QApplication::translate("PartDesignGui::TaskChamferParameters", "Remove ref", nullptr));
        label->setText(QApplication::translate("PartDesignGui::TaskChamferParameters", "Size:", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskChamferParameters: public Ui_TaskChamferParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKCHAMFERPARAMETERS_H
