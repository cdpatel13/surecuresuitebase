/****************************************************************************
** Meta object code from reading C++ file 'TaskHoleParameters.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/PartDesign/Gui/TaskHoleParameters.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskHoleParameters.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PartDesignGui__TaskHoleParameters_t {
    QByteArrayData data[26];
    char stringdata0[495];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskHoleParameters_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskHoleParameters_t qt_meta_stringdata_PartDesignGui__TaskHoleParameters = {
    {
QT_MOC_LITERAL(0, 0, 33), // "PartDesignGui::TaskHoleParame..."
QT_MOC_LITERAL(1, 34, 15), // "threadedChanged"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 17), // "threadTypeChanged"
QT_MOC_LITERAL(4, 69, 5), // "index"
QT_MOC_LITERAL(5, 75, 17), // "threadSizeChanged"
QT_MOC_LITERAL(6, 93, 18), // "threadClassChanged"
QT_MOC_LITERAL(7, 112, 16), // "threadFitChanged"
QT_MOC_LITERAL(8, 129, 24), // "modelActualThreadChanged"
QT_MOC_LITERAL(9, 154, 18), // "threadPitchChanged"
QT_MOC_LITERAL(10, 173, 5), // "value"
QT_MOC_LITERAL(11, 179, 24), // "threadCutOffOuterChanged"
QT_MOC_LITERAL(12, 204, 24), // "threadCutOffInnerChanged"
QT_MOC_LITERAL(13, 229, 18), // "threadAngleChanged"
QT_MOC_LITERAL(14, 248, 21), // "threadDiameterChanged"
QT_MOC_LITERAL(15, 270, 22), // "threadDirectionChanged"
QT_MOC_LITERAL(16, 293, 14), // "holeCutChanged"
QT_MOC_LITERAL(17, 308, 22), // "holeCutDiameterChanged"
QT_MOC_LITERAL(18, 331, 19), // "holeCutDepthChanged"
QT_MOC_LITERAL(19, 351, 30), // "holeCutCountersinkAngleChanged"
QT_MOC_LITERAL(20, 382, 12), // "depthChanged"
QT_MOC_LITERAL(21, 395, 17), // "depthValueChanged"
QT_MOC_LITERAL(22, 413, 17), // "drillPointChanged"
QT_MOC_LITERAL(23, 431, 28), // "drillPointAngledValueChanged"
QT_MOC_LITERAL(24, 460, 14), // "taperedChanged"
QT_MOC_LITERAL(25, 475, 19) // "taperedAngleChanged"

    },
    "PartDesignGui::TaskHoleParameters\0"
    "threadedChanged\0\0threadTypeChanged\0"
    "index\0threadSizeChanged\0threadClassChanged\0"
    "threadFitChanged\0modelActualThreadChanged\0"
    "threadPitchChanged\0value\0"
    "threadCutOffOuterChanged\0"
    "threadCutOffInnerChanged\0threadAngleChanged\0"
    "threadDiameterChanged\0threadDirectionChanged\0"
    "holeCutChanged\0holeCutDiameterChanged\0"
    "holeCutDepthChanged\0holeCutCountersinkAngleChanged\0"
    "depthChanged\0depthValueChanged\0"
    "drillPointChanged\0drillPointAngledValueChanged\0"
    "taperedChanged\0taperedAngleChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskHoleParameters[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x08 /* Private */,
       3,    1,  125,    2, 0x08 /* Private */,
       5,    1,  128,    2, 0x08 /* Private */,
       6,    1,  131,    2, 0x08 /* Private */,
       7,    1,  134,    2, 0x08 /* Private */,
       8,    0,  137,    2, 0x08 /* Private */,
       9,    1,  138,    2, 0x08 /* Private */,
      11,    1,  141,    2, 0x08 /* Private */,
      12,    1,  144,    2, 0x08 /* Private */,
      13,    1,  147,    2, 0x08 /* Private */,
      14,    1,  150,    2, 0x08 /* Private */,
      15,    0,  153,    2, 0x08 /* Private */,
      16,    1,  154,    2, 0x08 /* Private */,
      17,    1,  157,    2, 0x08 /* Private */,
      18,    1,  160,    2, 0x08 /* Private */,
      19,    1,  163,    2, 0x08 /* Private */,
      20,    1,  166,    2, 0x08 /* Private */,
      21,    1,  169,    2, 0x08 /* Private */,
      22,    0,  172,    2, 0x08 /* Private */,
      23,    1,  173,    2, 0x08 /* Private */,
      24,    0,  176,    2, 0x08 /* Private */,
      25,    1,  177,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   10,

       0        // eod
};

void PartDesignGui::TaskHoleParameters::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskHoleParameters *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->threadedChanged(); break;
        case 1: _t->threadTypeChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->threadSizeChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->threadClassChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->threadFitChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->modelActualThreadChanged(); break;
        case 6: _t->threadPitchChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->threadCutOffOuterChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->threadCutOffInnerChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->threadAngleChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: _t->threadDiameterChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->threadDirectionChanged(); break;
        case 12: _t->holeCutChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->holeCutDiameterChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->holeCutDepthChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: _t->holeCutCountersinkAngleChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 16: _t->depthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->depthValueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->drillPointChanged(); break;
        case 19: _t->drillPointAngledValueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 20: _t->taperedChanged(); break;
        case 21: _t->taperedAngleChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskHoleParameters::staticMetaObject = { {
    &TaskSketchBasedParameters::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskHoleParameters.data,
    qt_meta_data_PartDesignGui__TaskHoleParameters,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskHoleParameters::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskHoleParameters::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskHoleParameters.stringdata0))
        return static_cast<void*>(this);
    return TaskSketchBasedParameters::qt_metacast(_clname);
}

int PartDesignGui::TaskHoleParameters::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskSketchBasedParameters::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}
struct qt_meta_stringdata_PartDesignGui__TaskDlgHoleParameters_t {
    QByteArrayData data[1];
    char stringdata0[37];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskDlgHoleParameters_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskDlgHoleParameters_t qt_meta_stringdata_PartDesignGui__TaskDlgHoleParameters = {
    {
QT_MOC_LITERAL(0, 0, 36) // "PartDesignGui::TaskDlgHolePar..."

    },
    "PartDesignGui::TaskDlgHoleParameters"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskDlgHoleParameters[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PartDesignGui::TaskDlgHoleParameters::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskDlgHoleParameters::staticMetaObject = { {
    &TaskDlgSketchBasedParameters::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskDlgHoleParameters.data,
    qt_meta_data_PartDesignGui__TaskDlgHoleParameters,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskDlgHoleParameters::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskDlgHoleParameters::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskDlgHoleParameters.stringdata0))
        return static_cast<void*>(this);
    return TaskDlgSketchBasedParameters::qt_metacast(_clname);
}

int PartDesignGui::TaskDlgHoleParameters::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskDlgSketchBasedParameters::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
