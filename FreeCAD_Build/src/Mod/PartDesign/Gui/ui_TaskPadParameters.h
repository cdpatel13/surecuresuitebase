/********************************************************************************
** Form generated from reading UI file 'TaskPadParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKPADPARAMETERS_H
#define UI_TASKPADPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

namespace PartDesignGui {

class Ui_TaskPadParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *textLabel1;
    QComboBox *changeMode;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelLength;
    Gui::PrefQuantitySpinBox *lengthEdit;
    QHBoxLayout *horizontalLayout_5;
    QLabel *labelOffset;
    Gui::PrefQuantitySpinBox *offsetEdit;
    QCheckBox *checkBoxMidplane;
    QCheckBox *checkBoxReversed;
    QHBoxLayout *horizontalLayout_4;
    QLabel *labelLength2;
    Gui::PrefQuantitySpinBox *lengthEdit2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *buttonFace;
    QLineEdit *lineFaceName;
    QFrame *line;
    QCheckBox *checkBoxUpdateView;

    void setupUi(QWidget *PartDesignGui__TaskPadParameters)
    {
        if (PartDesignGui__TaskPadParameters->objectName().isEmpty())
            PartDesignGui__TaskPadParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskPadParameters"));
        PartDesignGui__TaskPadParameters->resize(272, 271);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskPadParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        textLabel1 = new QLabel(PartDesignGui__TaskPadParameters);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));

        horizontalLayout->addWidget(textLabel1);

        changeMode = new QComboBox(PartDesignGui__TaskPadParameters);
        changeMode->addItem(QString());
        changeMode->setObjectName(QString::fromUtf8("changeMode"));

        horizontalLayout->addWidget(changeMode);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        labelLength = new QLabel(PartDesignGui__TaskPadParameters);
        labelLength->setObjectName(QString::fromUtf8("labelLength"));

        horizontalLayout_2->addWidget(labelLength);

        lengthEdit = new Gui::PrefQuantitySpinBox(PartDesignGui__TaskPadParameters);
        lengthEdit->setObjectName(QString::fromUtf8("lengthEdit"));
        lengthEdit->setMinimum(0.000000000000000);

        horizontalLayout_2->addWidget(lengthEdit);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        labelOffset = new QLabel(PartDesignGui__TaskPadParameters);
        labelOffset->setObjectName(QString::fromUtf8("labelOffset"));

        horizontalLayout_5->addWidget(labelOffset);

        offsetEdit = new Gui::PrefQuantitySpinBox(PartDesignGui__TaskPadParameters);
        offsetEdit->setObjectName(QString::fromUtf8("offsetEdit"));

        horizontalLayout_5->addWidget(offsetEdit);


        verticalLayout->addLayout(horizontalLayout_5);

        checkBoxMidplane = new QCheckBox(PartDesignGui__TaskPadParameters);
        checkBoxMidplane->setObjectName(QString::fromUtf8("checkBoxMidplane"));
        checkBoxMidplane->setEnabled(true);

        verticalLayout->addWidget(checkBoxMidplane);

        checkBoxReversed = new QCheckBox(PartDesignGui__TaskPadParameters);
        checkBoxReversed->setObjectName(QString::fromUtf8("checkBoxReversed"));

        verticalLayout->addWidget(checkBoxReversed);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        labelLength2 = new QLabel(PartDesignGui__TaskPadParameters);
        labelLength2->setObjectName(QString::fromUtf8("labelLength2"));

        horizontalLayout_4->addWidget(labelLength2);

        lengthEdit2 = new Gui::PrefQuantitySpinBox(PartDesignGui__TaskPadParameters);
        lengthEdit2->setObjectName(QString::fromUtf8("lengthEdit2"));
        lengthEdit2->setMinimum(0.000000000000000);

        horizontalLayout_4->addWidget(lengthEdit2);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        buttonFace = new QPushButton(PartDesignGui__TaskPadParameters);
        buttonFace->setObjectName(QString::fromUtf8("buttonFace"));

        horizontalLayout_3->addWidget(buttonFace);

        lineFaceName = new QLineEdit(PartDesignGui__TaskPadParameters);
        lineFaceName->setObjectName(QString::fromUtf8("lineFaceName"));

        horizontalLayout_3->addWidget(lineFaceName);


        verticalLayout->addLayout(horizontalLayout_3);

        line = new QFrame(PartDesignGui__TaskPadParameters);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        checkBoxUpdateView = new QCheckBox(PartDesignGui__TaskPadParameters);
        checkBoxUpdateView->setObjectName(QString::fromUtf8("checkBoxUpdateView"));
        checkBoxUpdateView->setChecked(true);

        verticalLayout->addWidget(checkBoxUpdateView);


        retranslateUi(PartDesignGui__TaskPadParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskPadParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskPadParameters)
    {
        PartDesignGui__TaskPadParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskPadParameters", "Form", nullptr));
        textLabel1->setText(QApplication::translate("PartDesignGui::TaskPadParameters", "Type", nullptr));
        changeMode->setItemText(0, QApplication::translate("PartDesignGui::TaskPadParameters", "Dimension", nullptr));

        labelLength->setText(QApplication::translate("PartDesignGui::TaskPadParameters", "Length", nullptr));
        labelOffset->setText(QApplication::translate("PartDesignGui::TaskPadParameters", "Offset", nullptr));
        checkBoxMidplane->setText(QApplication::translate("PartDesignGui::TaskPadParameters", "Symmetric to plane", nullptr));
        checkBoxReversed->setText(QApplication::translate("PartDesignGui::TaskPadParameters", "Reversed", nullptr));
        labelLength2->setText(QApplication::translate("PartDesignGui::TaskPadParameters", "2nd length", nullptr));
        buttonFace->setText(QApplication::translate("PartDesignGui::TaskPadParameters", "Face", nullptr));
        checkBoxUpdateView->setText(QApplication::translate("PartDesignGui::TaskPadParameters", "Update view", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskPadParameters: public Ui_TaskPadParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKPADPARAMETERS_H
