/********************************************************************************
** Form generated from reading UI file 'TaskHoleParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKHOLEPARAMETERS_H
#define UI_TASKHOLEPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

QT_BEGIN_NAMESPACE

class Ui_TaskHoleParameters
{
public:
    QGridLayout *gridLayout;
    Gui::PrefQuantitySpinBox *Diameter;
    QLabel *label;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_CutoffInner;
    QLabel *label_5;
    QCheckBox *Tapered;
    Gui::PrefQuantitySpinBox *Depth;
    QLabel *label_8;
    QComboBox *HoleCutType;
    QLabel *label_3;
    Gui::PrefQuantitySpinBox *HoleCutDepth;
    Gui::PrefQuantitySpinBox *HoleCutDiameter;
    QComboBox *ThreadSize;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout_2;
    QRadioButton *drillPointFlat;
    QHBoxLayout *horizontalLayout;
    QRadioButton *drillPointAngled;
    Gui::PrefQuantitySpinBox *DrillPointAngle;
    QLabel *label_Pitch;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QRadioButton *directionRightHand;
    QRadioButton *directionLeftHand;
    QCheckBox *ModelActualThread;
    QCheckBox *Threaded;
    QLabel *label_Angle;
    QSpacerItem *verticalSpacer;
    QLabel *label_2;
    QComboBox *ThreadClass;
    QLabel *label_10;
    QLabel *label_15;
    QLabel *label_7;
    QComboBox *ThreadType;
    QLabel *label_6;
    Gui::PrefQuantitySpinBox *HoleCutCountersinkAngle;
    QComboBox *DepthType;
    QLabel *label_4;
    QComboBox *ThreadFit;
    QLabel *label_CutoffOuter;
    Gui::PrefQuantitySpinBox *ThreadPitch;
    Gui::PrefQuantitySpinBox *ThreadAngle;
    Gui::PrefQuantitySpinBox *ThreadCutOffInner;
    Gui::PrefQuantitySpinBox *ThreadCutOffOuter;
    QLabel *label_9;
    QLabel *label_16;
    QLabel *label_14;
    QLabel *label_13;
    Gui::PrefQuantitySpinBox *TaperedAngle;

    void setupUi(QWidget *TaskHoleParameters)
    {
        if (TaskHoleParameters->objectName().isEmpty())
            TaskHoleParameters->setObjectName(QString::fromUtf8("TaskHoleParameters"));
        TaskHoleParameters->resize(356, 660);
        gridLayout = new QGridLayout(TaskHoleParameters);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        Diameter = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        Diameter->setObjectName(QString::fromUtf8("Diameter"));
        Diameter->setProperty("unit", QVariant(QString::fromUtf8("mm")));

        gridLayout->addWidget(Diameter, 11, 5, 1, 2);

        label = new QLabel(TaskHoleParameters);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);

        gridLayout->addWidget(label, 14, 0, 1, 1);

        label_11 = new QLabel(TaskHoleParameters);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 15, 2, 1, 2);

        label_12 = new QLabel(TaskHoleParameters);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 16, 2, 1, 2);

        label_CutoffInner = new QLabel(TaskHoleParameters);
        label_CutoffInner->setObjectName(QString::fromUtf8("label_CutoffInner"));
        label_CutoffInner->setEnabled(false);

        gridLayout->addWidget(label_CutoffInner, 6, 2, 1, 1);

        label_5 = new QLabel(TaskHoleParameters);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        sizePolicy.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy);

        gridLayout->addWidget(label_5, 11, 0, 1, 2);

        Tapered = new QCheckBox(TaskHoleParameters);
        Tapered->setObjectName(QString::fromUtf8("Tapered"));

        gridLayout->addWidget(Tapered, 21, 0, 1, 2);

        Depth = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        Depth->setObjectName(QString::fromUtf8("Depth"));
        Depth->setProperty("unit", QVariant(QString::fromUtf8("mm")));

        gridLayout->addWidget(Depth, 12, 5, 1, 2);

        label_8 = new QLabel(TaskHoleParameters);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        sizePolicy.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy);
        label_8->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(label_8, 8, 0, 1, 2);

        HoleCutType = new QComboBox(TaskHoleParameters);
        HoleCutType->setObjectName(QString::fromUtf8("HoleCutType"));

        gridLayout->addWidget(HoleCutType, 14, 2, 1, 5);

        label_3 = new QLabel(TaskHoleParameters);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 10, 0, 1, 2);

        HoleCutDepth = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        HoleCutDepth->setObjectName(QString::fromUtf8("HoleCutDepth"));
        HoleCutDepth->setProperty("unit", QVariant(QString::fromUtf8("mm")));

        gridLayout->addWidget(HoleCutDepth, 16, 4, 1, 3);

        HoleCutDiameter = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        HoleCutDiameter->setObjectName(QString::fromUtf8("HoleCutDiameter"));
        HoleCutDiameter->setProperty("unit", QVariant(QString::fromUtf8("mm")));

        gridLayout->addWidget(HoleCutDiameter, 15, 4, 1, 3);

        ThreadSize = new QComboBox(TaskHoleParameters);
        ThreadSize->setObjectName(QString::fromUtf8("ThreadSize"));

        gridLayout->addWidget(ThreadSize, 9, 2, 1, 5);

        widget_2 = new QWidget(TaskHoleParameters);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        verticalLayout_2 = new QVBoxLayout(widget_2);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        drillPointFlat = new QRadioButton(widget_2);
        drillPointFlat->setObjectName(QString::fromUtf8("drillPointFlat"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(drillPointFlat->sizePolicy().hasHeightForWidth());
        drillPointFlat->setSizePolicy(sizePolicy1);

        verticalLayout_2->addWidget(drillPointFlat);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        drillPointAngled = new QRadioButton(widget_2);
        drillPointAngled->setObjectName(QString::fromUtf8("drillPointAngled"));
        sizePolicy1.setHeightForWidth(drillPointAngled->sizePolicy().hasHeightForWidth());
        drillPointAngled->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(drillPointAngled);

        DrillPointAngle = new Gui::PrefQuantitySpinBox(widget_2);
        DrillPointAngle->setObjectName(QString::fromUtf8("DrillPointAngle"));
        DrillPointAngle->setProperty("unit", QVariant(QString::fromUtf8("deg")));

        horizontalLayout->addWidget(DrillPointAngle);


        verticalLayout_2->addLayout(horizontalLayout);


        gridLayout->addWidget(widget_2, 19, 2, 1, 5);

        label_Pitch = new QLabel(TaskHoleParameters);
        label_Pitch->setObjectName(QString::fromUtf8("label_Pitch"));
        label_Pitch->setEnabled(false);

        gridLayout->addWidget(label_Pitch, 4, 2, 1, 1);

        widget = new QWidget(TaskHoleParameters);
        widget->setObjectName(QString::fromUtf8("widget"));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        directionRightHand = new QRadioButton(widget);
        directionRightHand->setObjectName(QString::fromUtf8("directionRightHand"));

        verticalLayout->addWidget(directionRightHand);

        directionLeftHand = new QRadioButton(widget);
        directionLeftHand->setObjectName(QString::fromUtf8("directionLeftHand"));

        verticalLayout->addWidget(directionLeftHand);


        gridLayout->addWidget(widget, 8, 2, 1, 5);

        ModelActualThread = new QCheckBox(TaskHoleParameters);
        ModelActualThread->setObjectName(QString::fromUtf8("ModelActualThread"));
        ModelActualThread->setEnabled(false);

        gridLayout->addWidget(ModelActualThread, 3, 2, 1, 5);

        Threaded = new QCheckBox(TaskHoleParameters);
        Threaded->setObjectName(QString::fromUtf8("Threaded"));

        gridLayout->addWidget(Threaded, 2, 2, 1, 5);

        label_Angle = new QLabel(TaskHoleParameters);
        label_Angle->setObjectName(QString::fromUtf8("label_Angle"));
        label_Angle->setEnabled(false);

        gridLayout->addWidget(label_Angle, 5, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 22, 2, 1, 1);

        label_2 = new QLabel(TaskHoleParameters);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);

        gridLayout->addWidget(label_2, 1, 0, 1, 2);

        ThreadClass = new QComboBox(TaskHoleParameters);
        ThreadClass->setObjectName(QString::fromUtf8("ThreadClass"));

        gridLayout->addWidget(ThreadClass, 11, 2, 1, 1);

        label_10 = new QLabel(TaskHoleParameters);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 17, 2, 1, 2);

        label_15 = new QLabel(TaskHoleParameters);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(label_15, 19, 0, 1, 2);

        label_7 = new QLabel(TaskHoleParameters);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        sizePolicy.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy);

        gridLayout->addWidget(label_7, 11, 3, 1, 2);

        ThreadType = new QComboBox(TaskHoleParameters);
        ThreadType->setObjectName(QString::fromUtf8("ThreadType"));

        gridLayout->addWidget(ThreadType, 1, 2, 1, 5);

        label_6 = new QLabel(TaskHoleParameters);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        sizePolicy.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy);

        gridLayout->addWidget(label_6, 12, 0, 1, 2);

        HoleCutCountersinkAngle = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        HoleCutCountersinkAngle->setObjectName(QString::fromUtf8("HoleCutCountersinkAngle"));
        HoleCutCountersinkAngle->setProperty("unit", QVariant(QString::fromUtf8("deg")));

        gridLayout->addWidget(HoleCutCountersinkAngle, 17, 4, 1, 3);

        DepthType = new QComboBox(TaskHoleParameters);
        DepthType->addItem(QString());
        DepthType->addItem(QString());
        DepthType->setObjectName(QString::fromUtf8("DepthType"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(DepthType->sizePolicy().hasHeightForWidth());
        DepthType->setSizePolicy(sizePolicy2);

        gridLayout->addWidget(DepthType, 12, 2, 1, 2);

        label_4 = new QLabel(TaskHoleParameters);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);

        gridLayout->addWidget(label_4, 9, 0, 1, 1);

        ThreadFit = new QComboBox(TaskHoleParameters);
        ThreadFit->addItem(QString());
        ThreadFit->addItem(QString());
        ThreadFit->setObjectName(QString::fromUtf8("ThreadFit"));

        gridLayout->addWidget(ThreadFit, 10, 2, 1, 5);

        label_CutoffOuter = new QLabel(TaskHoleParameters);
        label_CutoffOuter->setObjectName(QString::fromUtf8("label_CutoffOuter"));
        label_CutoffOuter->setEnabled(false);

        gridLayout->addWidget(label_CutoffOuter, 7, 2, 1, 1);

        ThreadPitch = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        ThreadPitch->setObjectName(QString::fromUtf8("ThreadPitch"));
        ThreadPitch->setEnabled(false);
        ThreadPitch->setProperty("unit", QVariant(QString::fromUtf8("mm")));

        gridLayout->addWidget(ThreadPitch, 4, 3, 1, 4);

        ThreadAngle = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        ThreadAngle->setObjectName(QString::fromUtf8("ThreadAngle"));
        ThreadAngle->setEnabled(false);
        ThreadAngle->setProperty("unit", QVariant(QString::fromUtf8("deg")));

        gridLayout->addWidget(ThreadAngle, 5, 3, 1, 4);

        ThreadCutOffInner = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        ThreadCutOffInner->setObjectName(QString::fromUtf8("ThreadCutOffInner"));
        ThreadCutOffInner->setEnabled(false);
        ThreadCutOffInner->setProperty("unit", QVariant(QString::fromUtf8("mm")));

        gridLayout->addWidget(ThreadCutOffInner, 6, 3, 1, 4);

        ThreadCutOffOuter = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        ThreadCutOffOuter->setObjectName(QString::fromUtf8("ThreadCutOffOuter"));
        ThreadCutOffOuter->setEnabled(false);
        ThreadCutOffOuter->setProperty("unit", QVariant(QString::fromUtf8("mm")));

        gridLayout->addWidget(ThreadCutOffOuter, 7, 3, 1, 4);

        label_9 = new QLabel(TaskHoleParameters);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        sizePolicy.setHeightForWidth(label_9->sizePolicy().hasHeightForWidth());
        label_9->setSizePolicy(sizePolicy);
        label_9->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(label_9, 18, 0, 1, 7);

        label_16 = new QLabel(TaskHoleParameters);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 20, 0, 1, 7);

        label_14 = new QLabel(TaskHoleParameters);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout->addWidget(label_14, 13, 0, 1, 7);

        label_13 = new QLabel(TaskHoleParameters);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout->addWidget(label_13, 0, 0, 1, 7);

        TaperedAngle = new Gui::PrefQuantitySpinBox(TaskHoleParameters);
        TaperedAngle->setObjectName(QString::fromUtf8("TaperedAngle"));
        TaperedAngle->setProperty("unit", QVariant(QString::fromUtf8("deg")));

        gridLayout->addWidget(TaperedAngle, 21, 2, 1, 2);


        retranslateUi(TaskHoleParameters);

        QMetaObject::connectSlotsByName(TaskHoleParameters);
    } // setupUi

    void retranslateUi(QWidget *TaskHoleParameters)
    {
        TaskHoleParameters->setWindowTitle(QApplication::translate("TaskHoleParameters", "Task Hole Parameters", nullptr));
        label->setText(QApplication::translate("TaskHoleParameters", "Type", nullptr));
        label_11->setText(QApplication::translate("TaskHoleParameters", "Diameter", nullptr));
        label_12->setText(QApplication::translate("TaskHoleParameters", "Depth", nullptr));
        label_CutoffInner->setText(QApplication::translate("TaskHoleParameters", "Cutoff inner", nullptr));
        label_5->setText(QApplication::translate("TaskHoleParameters", "Class", nullptr));
        Tapered->setText(QApplication::translate("TaskHoleParameters", "Tapered", nullptr));
        label_8->setText(QApplication::translate("TaskHoleParameters", "Direction", nullptr));
        label_3->setText(QApplication::translate("TaskHoleParameters", "Fit", nullptr));
        drillPointFlat->setText(QApplication::translate("TaskHoleParameters", "Flat", nullptr));
        drillPointAngled->setText(QApplication::translate("TaskHoleParameters", "Angled", nullptr));
        label_Pitch->setText(QApplication::translate("TaskHoleParameters", "Pitch", nullptr));
        directionRightHand->setText(QApplication::translate("TaskHoleParameters", "Right hand", nullptr));
        directionLeftHand->setText(QApplication::translate("TaskHoleParameters", "Left hand", nullptr));
        ModelActualThread->setText(QApplication::translate("TaskHoleParameters", "Model actual thread", nullptr));
        Threaded->setText(QApplication::translate("TaskHoleParameters", "Threaded", nullptr));
        label_Angle->setText(QApplication::translate("TaskHoleParameters", "Angle", nullptr));
        label_2->setText(QApplication::translate("TaskHoleParameters", "Profile", nullptr));
        label_10->setText(QApplication::translate("TaskHoleParameters", "Countersink angle", nullptr));
        label_15->setText(QApplication::translate("TaskHoleParameters", "Type", nullptr));
        label_7->setText(QApplication::translate("TaskHoleParameters", "Diameter", nullptr));
        label_6->setText(QApplication::translate("TaskHoleParameters", "Depth", nullptr));
        DepthType->setItemText(0, QApplication::translate("TaskHoleParameters", "Dimension", nullptr));
        DepthType->setItemText(1, QApplication::translate("TaskHoleParameters", "Through all", nullptr));

        label_4->setText(QApplication::translate("TaskHoleParameters", "Size", nullptr));
        ThreadFit->setItemText(0, QApplication::translate("TaskHoleParameters", "Standard fit", nullptr));
        ThreadFit->setItemText(1, QApplication::translate("TaskHoleParameters", "Close fit", nullptr));

        label_CutoffOuter->setText(QApplication::translate("TaskHoleParameters", "Cutoff outer", nullptr));
        label_9->setText(QApplication::translate("TaskHoleParameters", "<b>Drill point</b>", nullptr));
        label_16->setText(QApplication::translate("TaskHoleParameters", "<b>Misc</b>", nullptr));
        label_14->setText(QApplication::translate("TaskHoleParameters", "<b>Hole cut</b>", nullptr));
        label_13->setText(QApplication::translate("TaskHoleParameters", "<b>Threading and size</b>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskHoleParameters: public Ui_TaskHoleParameters {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKHOLEPARAMETERS_H
