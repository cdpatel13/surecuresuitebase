/********************************************************************************
** Form generated from reading UI file 'TaskLinearPatternParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKLINEARPATTERNPARAMETERS_H
#define UI_TASKLINEARPATTERNPARAMETERS_H

#include <Gui/SpinBox.h>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"

namespace PartDesignGui {

class Ui_TaskLinearPatternParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *buttonAddFeature;
    QPushButton *buttonRemoveFeature;
    QListWidget *listWidgetFeatures;
    QHBoxLayout *horizontalLayout_3;
    QLabel *labelDirection;
    QComboBox *comboDirection;
    QCheckBox *checkReverse;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    Gui::QuantitySpinBox *spinLength;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    Gui::UIntSpinBox *spinOccurrences;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *buttonOK;
    QCheckBox *checkBoxUpdateView;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *PartDesignGui__TaskLinearPatternParameters)
    {
        if (PartDesignGui__TaskLinearPatternParameters->objectName().isEmpty())
            PartDesignGui__TaskLinearPatternParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskLinearPatternParameters"));
        PartDesignGui__TaskLinearPatternParameters->resize(270, 366);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskLinearPatternParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        buttonAddFeature = new QPushButton(PartDesignGui__TaskLinearPatternParameters);
        buttonAddFeature->setObjectName(QString::fromUtf8("buttonAddFeature"));
        buttonAddFeature->setCheckable(true);

        horizontalLayout_4->addWidget(buttonAddFeature);

        buttonRemoveFeature = new QPushButton(PartDesignGui__TaskLinearPatternParameters);
        buttonRemoveFeature->setObjectName(QString::fromUtf8("buttonRemoveFeature"));
        buttonRemoveFeature->setCheckable(true);

        horizontalLayout_4->addWidget(buttonRemoveFeature);


        verticalLayout->addLayout(horizontalLayout_4);

        listWidgetFeatures = new QListWidget(PartDesignGui__TaskLinearPatternParameters);
        listWidgetFeatures->setObjectName(QString::fromUtf8("listWidgetFeatures"));

        verticalLayout->addWidget(listWidgetFeatures);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        labelDirection = new QLabel(PartDesignGui__TaskLinearPatternParameters);
        labelDirection->setObjectName(QString::fromUtf8("labelDirection"));

        horizontalLayout_3->addWidget(labelDirection);

        comboDirection = new QComboBox(PartDesignGui__TaskLinearPatternParameters);
        comboDirection->setObjectName(QString::fromUtf8("comboDirection"));

        horizontalLayout_3->addWidget(comboDirection);


        verticalLayout->addLayout(horizontalLayout_3);

        checkReverse = new QCheckBox(PartDesignGui__TaskLinearPatternParameters);
        checkReverse->setObjectName(QString::fromUtf8("checkReverse"));

        verticalLayout->addWidget(checkReverse);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(PartDesignGui__TaskLinearPatternParameters);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        spinLength = new Gui::QuantitySpinBox(PartDesignGui__TaskLinearPatternParameters);
        spinLength->setObjectName(QString::fromUtf8("spinLength"));
        spinLength->setProperty("unit", QVariant(QString::fromUtf8("mm")));
        spinLength->setProperty("value", QVariant(100.000000000000000));

        horizontalLayout_2->addWidget(spinLength);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(PartDesignGui__TaskLinearPatternParameters);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        spinOccurrences = new Gui::UIntSpinBox(PartDesignGui__TaskLinearPatternParameters);
        spinOccurrences->setObjectName(QString::fromUtf8("spinOccurrences"));

        horizontalLayout->addWidget(spinOccurrences);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        buttonOK = new QPushButton(PartDesignGui__TaskLinearPatternParameters);
        buttonOK->setObjectName(QString::fromUtf8("buttonOK"));

        horizontalLayout_5->addWidget(buttonOK);


        verticalLayout->addLayout(horizontalLayout_5);

        checkBoxUpdateView = new QCheckBox(PartDesignGui__TaskLinearPatternParameters);
        checkBoxUpdateView->setObjectName(QString::fromUtf8("checkBoxUpdateView"));
        checkBoxUpdateView->setChecked(true);

        verticalLayout->addWidget(checkBoxUpdateView);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(PartDesignGui__TaskLinearPatternParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskLinearPatternParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskLinearPatternParameters)
    {
        PartDesignGui__TaskLinearPatternParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskLinearPatternParameters", "Form", nullptr));
        buttonAddFeature->setText(QApplication::translate("PartDesignGui::TaskLinearPatternParameters", "Add feature", nullptr));
        buttonRemoveFeature->setText(QApplication::translate("PartDesignGui::TaskLinearPatternParameters", "Remove feature", nullptr));
        labelDirection->setText(QApplication::translate("PartDesignGui::TaskLinearPatternParameters", "Direction", nullptr));
        checkReverse->setText(QApplication::translate("PartDesignGui::TaskLinearPatternParameters", "Reverse direction", nullptr));
        label_2->setText(QApplication::translate("PartDesignGui::TaskLinearPatternParameters", "Length", nullptr));
        label->setText(QApplication::translate("PartDesignGui::TaskLinearPatternParameters", "Occurrences", nullptr));
        buttonOK->setText(QApplication::translate("PartDesignGui::TaskLinearPatternParameters", "OK", nullptr));
        checkBoxUpdateView->setText(QApplication::translate("PartDesignGui::TaskLinearPatternParameters", "Update view", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskLinearPatternParameters: public Ui_TaskLinearPatternParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKLINEARPATTERNPARAMETERS_H
