/********************************************************************************
** Form generated from reading UI file 'TaskPocketParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKPOCKETPARAMETERS_H
#define UI_TASKPOCKETPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

namespace PartDesignGui {

class Ui_TaskPocketParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *textLabel1;
    QComboBox *changeMode;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelLength;
    Gui::PrefQuantitySpinBox *lengthEdit;
    QHBoxLayout *horizontalLayout_5;
    QLabel *labelOffset;
    Gui::PrefQuantitySpinBox *offsetEdit;
    QCheckBox *checkBoxMidplane;
    QCheckBox *checkBoxReversed;
    QHBoxLayout *horizontalLayout_4;
    QLabel *labelLength2;
    Gui::PrefQuantitySpinBox *lengthEdit2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *buttonFace;
    QLineEdit *lineFaceName;
    QFrame *line;
    QCheckBox *checkBoxUpdateView;

    void setupUi(QWidget *PartDesignGui__TaskPocketParameters)
    {
        if (PartDesignGui__TaskPocketParameters->objectName().isEmpty())
            PartDesignGui__TaskPocketParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskPocketParameters"));
        PartDesignGui__TaskPocketParameters->resize(241, 233);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskPocketParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        textLabel1 = new QLabel(PartDesignGui__TaskPocketParameters);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));

        horizontalLayout->addWidget(textLabel1);

        changeMode = new QComboBox(PartDesignGui__TaskPocketParameters);
        changeMode->addItem(QString());
        changeMode->setObjectName(QString::fromUtf8("changeMode"));

        horizontalLayout->addWidget(changeMode);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        labelLength = new QLabel(PartDesignGui__TaskPocketParameters);
        labelLength->setObjectName(QString::fromUtf8("labelLength"));

        horizontalLayout_2->addWidget(labelLength);

        lengthEdit = new Gui::PrefQuantitySpinBox(PartDesignGui__TaskPocketParameters);
        lengthEdit->setObjectName(QString::fromUtf8("lengthEdit"));
        lengthEdit->setMinimum(0.000000000000000);

        horizontalLayout_2->addWidget(lengthEdit);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        labelOffset = new QLabel(PartDesignGui__TaskPocketParameters);
        labelOffset->setObjectName(QString::fromUtf8("labelOffset"));

        horizontalLayout_5->addWidget(labelOffset);

        offsetEdit = new Gui::PrefQuantitySpinBox(PartDesignGui__TaskPocketParameters);
        offsetEdit->setObjectName(QString::fromUtf8("offsetEdit"));

        horizontalLayout_5->addWidget(offsetEdit);


        verticalLayout->addLayout(horizontalLayout_5);

        checkBoxMidplane = new QCheckBox(PartDesignGui__TaskPocketParameters);
        checkBoxMidplane->setObjectName(QString::fromUtf8("checkBoxMidplane"));
        checkBoxMidplane->setEnabled(true);

        verticalLayout->addWidget(checkBoxMidplane);

        checkBoxReversed = new QCheckBox(PartDesignGui__TaskPocketParameters);
        checkBoxReversed->setObjectName(QString::fromUtf8("checkBoxReversed"));

        verticalLayout->addWidget(checkBoxReversed);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        labelLength2 = new QLabel(PartDesignGui__TaskPocketParameters);
        labelLength2->setObjectName(QString::fromUtf8("labelLength2"));

        horizontalLayout_4->addWidget(labelLength2);

        lengthEdit2 = new Gui::PrefQuantitySpinBox(PartDesignGui__TaskPocketParameters);
        lengthEdit2->setObjectName(QString::fromUtf8("lengthEdit2"));
        lengthEdit2->setMinimum(0.000000000000000);

        horizontalLayout_4->addWidget(lengthEdit2);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        buttonFace = new QPushButton(PartDesignGui__TaskPocketParameters);
        buttonFace->setObjectName(QString::fromUtf8("buttonFace"));

        horizontalLayout_3->addWidget(buttonFace);

        lineFaceName = new QLineEdit(PartDesignGui__TaskPocketParameters);
        lineFaceName->setObjectName(QString::fromUtf8("lineFaceName"));

        horizontalLayout_3->addWidget(lineFaceName);


        verticalLayout->addLayout(horizontalLayout_3);

        line = new QFrame(PartDesignGui__TaskPocketParameters);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        checkBoxUpdateView = new QCheckBox(PartDesignGui__TaskPocketParameters);
        checkBoxUpdateView->setObjectName(QString::fromUtf8("checkBoxUpdateView"));
        checkBoxUpdateView->setChecked(true);

        verticalLayout->addWidget(checkBoxUpdateView);


        retranslateUi(PartDesignGui__TaskPocketParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskPocketParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskPocketParameters)
    {
        PartDesignGui__TaskPocketParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskPocketParameters", "Form", nullptr));
        textLabel1->setText(QApplication::translate("PartDesignGui::TaskPocketParameters", "Type", nullptr));
        changeMode->setItemText(0, QApplication::translate("PartDesignGui::TaskPocketParameters", "Dimension", nullptr));

        labelLength->setText(QApplication::translate("PartDesignGui::TaskPocketParameters", "Length", nullptr));
        labelOffset->setText(QApplication::translate("PartDesignGui::TaskPocketParameters", "Offset", nullptr));
        checkBoxMidplane->setText(QApplication::translate("PartDesignGui::TaskPocketParameters", "Symmetric to plane", nullptr));
        checkBoxReversed->setText(QApplication::translate("PartDesignGui::TaskPocketParameters", "Reversed", nullptr));
        labelLength2->setText(QApplication::translate("PartDesignGui::TaskPocketParameters", "2nd length", nullptr));
        buttonFace->setText(QApplication::translate("PartDesignGui::TaskPocketParameters", "Face", nullptr));
        checkBoxUpdateView->setText(QApplication::translate("PartDesignGui::TaskPocketParameters", "Update view", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskPocketParameters: public Ui_TaskPocketParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKPOCKETPARAMETERS_H
