/****************************************************************************
** Meta object code from reading C++ file 'TaskPrimitiveParameters.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/PartDesign/Gui/TaskPrimitiveParameters.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskPrimitiveParameters.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PartDesignGui__TaskBoxPrimitives_t {
    QByteArrayData data[40];
    char stringdata0[849];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskBoxPrimitives_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskBoxPrimitives_t qt_meta_stringdata_PartDesignGui__TaskBoxPrimitives = {
    {
QT_MOC_LITERAL(0, 0, 32), // "PartDesignGui::TaskBoxPrimitives"
QT_MOC_LITERAL(1, 33, 18), // "onBoxLengthChanged"
QT_MOC_LITERAL(2, 52, 0), // ""
QT_MOC_LITERAL(3, 53, 17), // "onBoxWidthChanged"
QT_MOC_LITERAL(4, 71, 18), // "onBoxHeightChanged"
QT_MOC_LITERAL(5, 90, 23), // "onCylinderRadiusChanged"
QT_MOC_LITERAL(6, 114, 23), // "onCylinderHeightChanged"
QT_MOC_LITERAL(7, 138, 22), // "onCylinderAngleChanged"
QT_MOC_LITERAL(8, 161, 21), // "onSphereRadiusChanged"
QT_MOC_LITERAL(9, 183, 21), // "onSphereAngle1Changed"
QT_MOC_LITERAL(10, 205, 21), // "onSphereAngle2Changed"
QT_MOC_LITERAL(11, 227, 21), // "onSphereAngle3Changed"
QT_MOC_LITERAL(12, 249, 20), // "onConeRadius1Changed"
QT_MOC_LITERAL(13, 270, 20), // "onConeRadius2Changed"
QT_MOC_LITERAL(14, 291, 18), // "onConeAngleChanged"
QT_MOC_LITERAL(15, 310, 19), // "onConeHeightChanged"
QT_MOC_LITERAL(16, 330, 25), // "onEllipsoidRadius1Changed"
QT_MOC_LITERAL(17, 356, 25), // "onEllipsoidRadius2Changed"
QT_MOC_LITERAL(18, 382, 25), // "onEllipsoidRadius3Changed"
QT_MOC_LITERAL(19, 408, 24), // "onEllipsoidAngle1Changed"
QT_MOC_LITERAL(20, 433, 24), // "onEllipsoidAngle2Changed"
QT_MOC_LITERAL(21, 458, 24), // "onEllipsoidAngle3Changed"
QT_MOC_LITERAL(22, 483, 21), // "onTorusRadius1Changed"
QT_MOC_LITERAL(23, 505, 21), // "onTorusRadius2Changed"
QT_MOC_LITERAL(24, 527, 20), // "onTorusAngle1Changed"
QT_MOC_LITERAL(25, 548, 20), // "onTorusAngle2Changed"
QT_MOC_LITERAL(26, 569, 20), // "onTorusAngle3Changed"
QT_MOC_LITERAL(27, 590, 26), // "onPrismCircumradiusChanged"
QT_MOC_LITERAL(28, 617, 20), // "onPrismHeightChanged"
QT_MOC_LITERAL(29, 638, 21), // "onPrismPolygonChanged"
QT_MOC_LITERAL(30, 660, 18), // "onWedgeXmaxChanged"
QT_MOC_LITERAL(31, 679, 17), // "onWedgeXinChanged"
QT_MOC_LITERAL(32, 697, 18), // "onWedgeYmaxChanged"
QT_MOC_LITERAL(33, 716, 17), // "onWedgeYinChanged"
QT_MOC_LITERAL(34, 734, 18), // "onWedgeZmaxChanged"
QT_MOC_LITERAL(35, 753, 17), // "onWedgeZinChanged"
QT_MOC_LITERAL(36, 771, 19), // "onWedgeX2maxChanged"
QT_MOC_LITERAL(37, 791, 18), // "onWedgeX2inChanged"
QT_MOC_LITERAL(38, 810, 19), // "onWedgeZ2maxChanged"
QT_MOC_LITERAL(39, 830, 18) // "onWedgeZ2inChanged"

    },
    "PartDesignGui::TaskBoxPrimitives\0"
    "onBoxLengthChanged\0\0onBoxWidthChanged\0"
    "onBoxHeightChanged\0onCylinderRadiusChanged\0"
    "onCylinderHeightChanged\0onCylinderAngleChanged\0"
    "onSphereRadiusChanged\0onSphereAngle1Changed\0"
    "onSphereAngle2Changed\0onSphereAngle3Changed\0"
    "onConeRadius1Changed\0onConeRadius2Changed\0"
    "onConeAngleChanged\0onConeHeightChanged\0"
    "onEllipsoidRadius1Changed\0"
    "onEllipsoidRadius2Changed\0"
    "onEllipsoidRadius3Changed\0"
    "onEllipsoidAngle1Changed\0"
    "onEllipsoidAngle2Changed\0"
    "onEllipsoidAngle3Changed\0onTorusRadius1Changed\0"
    "onTorusRadius2Changed\0onTorusAngle1Changed\0"
    "onTorusAngle2Changed\0onTorusAngle3Changed\0"
    "onPrismCircumradiusChanged\0"
    "onPrismHeightChanged\0onPrismPolygonChanged\0"
    "onWedgeXmaxChanged\0onWedgeXinChanged\0"
    "onWedgeYmaxChanged\0onWedgeYinChanged\0"
    "onWedgeZmaxChanged\0onWedgeZinChanged\0"
    "onWedgeX2maxChanged\0onWedgeX2inChanged\0"
    "onWedgeZ2maxChanged\0onWedgeZ2inChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskBoxPrimitives[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      38,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  204,    2, 0x0a /* Public */,
       3,    1,  207,    2, 0x0a /* Public */,
       4,    1,  210,    2, 0x0a /* Public */,
       5,    1,  213,    2, 0x0a /* Public */,
       6,    1,  216,    2, 0x0a /* Public */,
       7,    1,  219,    2, 0x0a /* Public */,
       8,    1,  222,    2, 0x0a /* Public */,
       9,    1,  225,    2, 0x0a /* Public */,
      10,    1,  228,    2, 0x0a /* Public */,
      11,    1,  231,    2, 0x0a /* Public */,
      12,    1,  234,    2, 0x0a /* Public */,
      13,    1,  237,    2, 0x0a /* Public */,
      14,    1,  240,    2, 0x0a /* Public */,
      15,    1,  243,    2, 0x0a /* Public */,
      16,    1,  246,    2, 0x0a /* Public */,
      17,    1,  249,    2, 0x0a /* Public */,
      18,    1,  252,    2, 0x0a /* Public */,
      19,    1,  255,    2, 0x0a /* Public */,
      20,    1,  258,    2, 0x0a /* Public */,
      21,    1,  261,    2, 0x0a /* Public */,
      22,    1,  264,    2, 0x0a /* Public */,
      23,    1,  267,    2, 0x0a /* Public */,
      24,    1,  270,    2, 0x0a /* Public */,
      25,    1,  273,    2, 0x0a /* Public */,
      26,    1,  276,    2, 0x0a /* Public */,
      27,    1,  279,    2, 0x0a /* Public */,
      28,    1,  282,    2, 0x0a /* Public */,
      29,    1,  285,    2, 0x0a /* Public */,
      30,    1,  288,    2, 0x0a /* Public */,
      31,    1,  291,    2, 0x0a /* Public */,
      32,    1,  294,    2, 0x0a /* Public */,
      33,    1,  297,    2, 0x0a /* Public */,
      34,    1,  300,    2, 0x0a /* Public */,
      35,    1,  303,    2, 0x0a /* Public */,
      36,    1,  306,    2, 0x0a /* Public */,
      37,    1,  309,    2, 0x0a /* Public */,
      38,    1,  312,    2, 0x0a /* Public */,
      39,    1,  315,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,

       0        // eod
};

void PartDesignGui::TaskBoxPrimitives::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskBoxPrimitives *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onBoxLengthChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->onBoxWidthChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->onBoxHeightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->onCylinderRadiusChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->onCylinderHeightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->onCylinderAngleChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->onSphereRadiusChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->onSphereAngle1Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->onSphereAngle2Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->onSphereAngle3Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: _t->onConeRadius1Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->onConeRadius2Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 12: _t->onConeAngleChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 13: _t->onConeHeightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->onEllipsoidRadius1Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: _t->onEllipsoidRadius2Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 16: _t->onEllipsoidRadius3Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 17: _t->onEllipsoidAngle1Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->onEllipsoidAngle2Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 19: _t->onEllipsoidAngle3Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 20: _t->onTorusRadius1Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 21: _t->onTorusRadius2Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 22: _t->onTorusAngle1Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 23: _t->onTorusAngle2Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 24: _t->onTorusAngle3Changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 25: _t->onPrismCircumradiusChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 26: _t->onPrismHeightChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 27: _t->onPrismPolygonChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: _t->onWedgeXmaxChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 29: _t->onWedgeXinChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 30: _t->onWedgeYmaxChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 31: _t->onWedgeYinChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 32: _t->onWedgeZmaxChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 33: _t->onWedgeZinChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 34: _t->onWedgeX2maxChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 35: _t->onWedgeX2inChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 36: _t->onWedgeZ2maxChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 37: _t->onWedgeZ2inChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskBoxPrimitives::staticMetaObject = { {
    &Gui::TaskView::TaskBox::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskBoxPrimitives.data,
    qt_meta_data_PartDesignGui__TaskBoxPrimitives,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskBoxPrimitives::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskBoxPrimitives::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskBoxPrimitives.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::DocumentObserver"))
        return static_cast< Gui::DocumentObserver*>(this);
    return Gui::TaskView::TaskBox::qt_metacast(_clname);
}

int PartDesignGui::TaskBoxPrimitives::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 38)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 38;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 38)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 38;
    }
    return _id;
}
struct qt_meta_stringdata_PartDesignGui__TaskPrimitiveParameters_t {
    QByteArrayData data[1];
    char stringdata0[39];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskPrimitiveParameters_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskPrimitiveParameters_t qt_meta_stringdata_PartDesignGui__TaskPrimitiveParameters = {
    {
QT_MOC_LITERAL(0, 0, 38) // "PartDesignGui::TaskPrimitiveP..."

    },
    "PartDesignGui::TaskPrimitiveParameters"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskPrimitiveParameters[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PartDesignGui::TaskPrimitiveParameters::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskPrimitiveParameters::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskPrimitiveParameters.data,
    qt_meta_data_PartDesignGui__TaskPrimitiveParameters,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskPrimitiveParameters::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskPrimitiveParameters::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskPrimitiveParameters.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int PartDesignGui::TaskPrimitiveParameters::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
