/********************************************************************************
** Form generated from reading UI file 'TaskRevolutionParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKREVOLUTIONPARAMETERS_H
#define UI_TASKREVOLUTIONPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"

namespace PartDesignGui {

class Ui_TaskRevolutionParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *textLabel1;
    QComboBox *axis;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    Gui::QuantitySpinBox *revolveAngle;
    QCheckBox *checkBoxMidplane;
    QCheckBox *checkBoxReversed;
    QFrame *line;
    QCheckBox *checkBoxUpdateView;

    void setupUi(QWidget *PartDesignGui__TaskRevolutionParameters)
    {
        if (PartDesignGui__TaskRevolutionParameters->objectName().isEmpty())
            PartDesignGui__TaskRevolutionParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskRevolutionParameters"));
        PartDesignGui__TaskRevolutionParameters->resize(278, 193);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskRevolutionParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        textLabel1 = new QLabel(PartDesignGui__TaskRevolutionParameters);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));

        horizontalLayout->addWidget(textLabel1);

        axis = new QComboBox(PartDesignGui__TaskRevolutionParameters);
        axis->addItem(QString());
        axis->addItem(QString());
        axis->addItem(QString());
        axis->addItem(QString());
        axis->addItem(QString());
        axis->addItem(QString());
        axis->setObjectName(QString::fromUtf8("axis"));

        horizontalLayout->addWidget(axis);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label = new QLabel(PartDesignGui__TaskRevolutionParameters);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_3->addWidget(label);

        revolveAngle = new Gui::QuantitySpinBox(PartDesignGui__TaskRevolutionParameters);
        revolveAngle->setObjectName(QString::fromUtf8("revolveAngle"));
        revolveAngle->setProperty("unit", QVariant(QString::fromUtf8("deg")));
        revolveAngle->setProperty("minimum", QVariant(0.000000000000000));
        revolveAngle->setProperty("maximum", QVariant(360.000000000000000));
        revolveAngle->setProperty("singleStep", QVariant(10.000000000000000));
        revolveAngle->setProperty("value", QVariant(360.000000000000000));

        horizontalLayout_3->addWidget(revolveAngle);


        verticalLayout->addLayout(horizontalLayout_3);

        checkBoxMidplane = new QCheckBox(PartDesignGui__TaskRevolutionParameters);
        checkBoxMidplane->setObjectName(QString::fromUtf8("checkBoxMidplane"));
        checkBoxMidplane->setEnabled(true);

        verticalLayout->addWidget(checkBoxMidplane);

        checkBoxReversed = new QCheckBox(PartDesignGui__TaskRevolutionParameters);
        checkBoxReversed->setObjectName(QString::fromUtf8("checkBoxReversed"));

        verticalLayout->addWidget(checkBoxReversed);

        line = new QFrame(PartDesignGui__TaskRevolutionParameters);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        checkBoxUpdateView = new QCheckBox(PartDesignGui__TaskRevolutionParameters);
        checkBoxUpdateView->setObjectName(QString::fromUtf8("checkBoxUpdateView"));
        checkBoxUpdateView->setChecked(true);

        verticalLayout->addWidget(checkBoxUpdateView);


        retranslateUi(PartDesignGui__TaskRevolutionParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskRevolutionParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskRevolutionParameters)
    {
        PartDesignGui__TaskRevolutionParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Form", nullptr));
        textLabel1->setText(QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Axis:", nullptr));
        axis->setItemText(0, QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Base X axis", nullptr));
        axis->setItemText(1, QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Base Y axis", nullptr));
        axis->setItemText(2, QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Base Z axis", nullptr));
        axis->setItemText(3, QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Horizontal sketch axis", nullptr));
        axis->setItemText(4, QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Vertical sketch axis", nullptr));
        axis->setItemText(5, QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Select reference...", nullptr));

        label->setText(QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Angle:", nullptr));
        checkBoxMidplane->setText(QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Symmetric to plane", nullptr));
        checkBoxReversed->setText(QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Reversed", nullptr));
        checkBoxUpdateView->setText(QApplication::translate("PartDesignGui::TaskRevolutionParameters", "Update view", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskRevolutionParameters: public Ui_TaskRevolutionParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKREVOLUTIONPARAMETERS_H
