/********************************************************************************
** Form generated from reading UI file 'TaskDraftParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKDRAFTPARAMETERS_H
#define UI_TASKDRAFTPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"

namespace PartDesignGui {

class Ui_TaskDraftParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QToolButton *buttonRefAdd;
    QToolButton *buttonRefRemove;
    QListWidget *listWidgetReferences;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    Gui::QuantitySpinBox *draftAngle;
    QHBoxLayout *horizontalLayout_3;
    QToolButton *buttonPlane;
    QLineEdit *linePlane;
    QHBoxLayout *horizontalLayout_4;
    QToolButton *buttonLine;
    QLineEdit *lineLine;
    QCheckBox *checkReverse;

    void setupUi(QWidget *PartDesignGui__TaskDraftParameters)
    {
        if (PartDesignGui__TaskDraftParameters->objectName().isEmpty())
            PartDesignGui__TaskDraftParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskDraftParameters"));
        PartDesignGui__TaskDraftParameters->resize(257, 285);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskDraftParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        buttonRefAdd = new QToolButton(PartDesignGui__TaskDraftParameters);
        buttonRefAdd->setObjectName(QString::fromUtf8("buttonRefAdd"));
        buttonRefAdd->setCheckable(true);

        horizontalLayout->addWidget(buttonRefAdd);

        buttonRefRemove = new QToolButton(PartDesignGui__TaskDraftParameters);
        buttonRefRemove->setObjectName(QString::fromUtf8("buttonRefRemove"));
        buttonRefRemove->setCheckable(true);

        horizontalLayout->addWidget(buttonRefRemove);


        verticalLayout->addLayout(horizontalLayout);

        listWidgetReferences = new QListWidget(PartDesignGui__TaskDraftParameters);
        listWidgetReferences->setObjectName(QString::fromUtf8("listWidgetReferences"));

        verticalLayout->addWidget(listWidgetReferences);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(PartDesignGui__TaskDraftParameters);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        draftAngle = new Gui::QuantitySpinBox(PartDesignGui__TaskDraftParameters);
        draftAngle->setObjectName(QString::fromUtf8("draftAngle"));
        draftAngle->setProperty("unit", QVariant(QString::fromUtf8("deg")));
        draftAngle->setMinimum(0.000000000000000);
        draftAngle->setMaximum(89.999999999999986);
        draftAngle->setSingleStep(0.100000000000000);
        draftAngle->setValue(1.500000000000000);

        horizontalLayout_2->addWidget(draftAngle);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        buttonPlane = new QToolButton(PartDesignGui__TaskDraftParameters);
        buttonPlane->setObjectName(QString::fromUtf8("buttonPlane"));
        buttonPlane->setCheckable(true);

        horizontalLayout_3->addWidget(buttonPlane);

        linePlane = new QLineEdit(PartDesignGui__TaskDraftParameters);
        linePlane->setObjectName(QString::fromUtf8("linePlane"));

        horizontalLayout_3->addWidget(linePlane);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        buttonLine = new QToolButton(PartDesignGui__TaskDraftParameters);
        buttonLine->setObjectName(QString::fromUtf8("buttonLine"));
        buttonLine->setCheckable(true);

        horizontalLayout_4->addWidget(buttonLine);

        lineLine = new QLineEdit(PartDesignGui__TaskDraftParameters);
        lineLine->setObjectName(QString::fromUtf8("lineLine"));

        horizontalLayout_4->addWidget(lineLine);


        verticalLayout->addLayout(horizontalLayout_4);

        checkReverse = new QCheckBox(PartDesignGui__TaskDraftParameters);
        checkReverse->setObjectName(QString::fromUtf8("checkReverse"));

        verticalLayout->addWidget(checkReverse);

        checkReverse->raise();
        listWidgetReferences->raise();

        retranslateUi(PartDesignGui__TaskDraftParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskDraftParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskDraftParameters)
    {
        PartDesignGui__TaskDraftParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskDraftParameters", "Form", nullptr));
        buttonRefAdd->setText(QApplication::translate("PartDesignGui::TaskDraftParameters", "Add face", nullptr));
        buttonRefRemove->setText(QApplication::translate("PartDesignGui::TaskDraftParameters", "Remove face", nullptr));
        label->setText(QApplication::translate("PartDesignGui::TaskDraftParameters", "Draft angle", nullptr));
        buttonPlane->setText(QApplication::translate("PartDesignGui::TaskDraftParameters", "Neutral plane", nullptr));
        buttonLine->setText(QApplication::translate("PartDesignGui::TaskDraftParameters", "Pull direction", nullptr));
        checkReverse->setText(QApplication::translate("PartDesignGui::TaskDraftParameters", "Reverse pull direction", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskDraftParameters: public Ui_TaskDraftParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKDRAFTPARAMETERS_H
