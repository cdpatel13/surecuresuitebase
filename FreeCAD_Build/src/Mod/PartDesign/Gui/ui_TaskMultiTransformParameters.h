/********************************************************************************
** Form generated from reading UI file 'TaskMultiTransformParameters.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKMULTITRANSFORMPARAMETERS_H
#define UI_TASKMULTITRANSFORMPARAMETERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace PartDesignGui {

class Ui_TaskMultiTransformParameters
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *buttonAddFeature;
    QPushButton *buttonRemoveFeature;
    QListWidget *listWidgetFeatures;
    QLabel *label;
    QListWidget *listTransformFeatures;
    QCheckBox *checkBoxUpdateView;

    void setupUi(QWidget *PartDesignGui__TaskMultiTransformParameters)
    {
        if (PartDesignGui__TaskMultiTransformParameters->objectName().isEmpty())
            PartDesignGui__TaskMultiTransformParameters->setObjectName(QString::fromUtf8("PartDesignGui__TaskMultiTransformParameters"));
        PartDesignGui__TaskMultiTransformParameters->resize(256, 266);
        verticalLayout = new QVBoxLayout(PartDesignGui__TaskMultiTransformParameters);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        buttonAddFeature = new QPushButton(PartDesignGui__TaskMultiTransformParameters);
        buttonAddFeature->setObjectName(QString::fromUtf8("buttonAddFeature"));
        buttonAddFeature->setCheckable(true);

        horizontalLayout->addWidget(buttonAddFeature);

        buttonRemoveFeature = new QPushButton(PartDesignGui__TaskMultiTransformParameters);
        buttonRemoveFeature->setObjectName(QString::fromUtf8("buttonRemoveFeature"));
        buttonRemoveFeature->setCheckable(true);

        horizontalLayout->addWidget(buttonRemoveFeature);


        verticalLayout->addLayout(horizontalLayout);

        listWidgetFeatures = new QListWidget(PartDesignGui__TaskMultiTransformParameters);
        listWidgetFeatures->setObjectName(QString::fromUtf8("listWidgetFeatures"));

        verticalLayout->addWidget(listWidgetFeatures);

        label = new QLabel(PartDesignGui__TaskMultiTransformParameters);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        listTransformFeatures = new QListWidget(PartDesignGui__TaskMultiTransformParameters);
        listTransformFeatures->setObjectName(QString::fromUtf8("listTransformFeatures"));
        listTransformFeatures->setMaximumSize(QSize(16777215, 80));

        verticalLayout->addWidget(listTransformFeatures);

        checkBoxUpdateView = new QCheckBox(PartDesignGui__TaskMultiTransformParameters);
        checkBoxUpdateView->setObjectName(QString::fromUtf8("checkBoxUpdateView"));
        checkBoxUpdateView->setChecked(true);

        verticalLayout->addWidget(checkBoxUpdateView);


        retranslateUi(PartDesignGui__TaskMultiTransformParameters);

        QMetaObject::connectSlotsByName(PartDesignGui__TaskMultiTransformParameters);
    } // setupUi

    void retranslateUi(QWidget *PartDesignGui__TaskMultiTransformParameters)
    {
        PartDesignGui__TaskMultiTransformParameters->setWindowTitle(QApplication::translate("PartDesignGui::TaskMultiTransformParameters", "Form", nullptr));
        buttonAddFeature->setText(QApplication::translate("PartDesignGui::TaskMultiTransformParameters", "Add feature", nullptr));
        buttonRemoveFeature->setText(QApplication::translate("PartDesignGui::TaskMultiTransformParameters", "Remove feature", nullptr));
        label->setText(QApplication::translate("PartDesignGui::TaskMultiTransformParameters", "Transformations", nullptr));
        checkBoxUpdateView->setText(QApplication::translate("PartDesignGui::TaskMultiTransformParameters", "Update view", nullptr));
    } // retranslateUi

};

} // namespace PartDesignGui

namespace PartDesignGui {
namespace Ui {
    class TaskMultiTransformParameters: public Ui_TaskMultiTransformParameters {};
} // namespace Ui
} // namespace PartDesignGui

#endif // UI_TASKMULTITRANSFORMPARAMETERS_H
