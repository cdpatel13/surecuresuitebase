/****************************************************************************
** Meta object code from reading C++ file 'TaskPipeParameters.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/PartDesign/Gui/TaskPipeParameters.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskPipeParameters.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PartDesignGui__TaskPipeParameters_t {
    QByteArrayData data[9];
    char stringdata0[142];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskPipeParameters_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskPipeParameters_t qt_meta_stringdata_PartDesignGui__TaskPipeParameters = {
    {
QT_MOC_LITERAL(0, 0, 33), // "PartDesignGui::TaskPipeParame..."
QT_MOC_LITERAL(1, 34, 16), // "onTangentChanged"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 7), // "checked"
QT_MOC_LITERAL(4, 60, 19), // "onTransitionChanged"
QT_MOC_LITERAL(5, 80, 14), // "onButtonRefAdd"
QT_MOC_LITERAL(6, 95, 17), // "onButtonRefRemove"
QT_MOC_LITERAL(7, 113, 12), // "onBaseButton"
QT_MOC_LITERAL(8, 126, 15) // "onProfileButton"

    },
    "PartDesignGui::TaskPipeParameters\0"
    "onTangentChanged\0\0checked\0onTransitionChanged\0"
    "onButtonRefAdd\0onButtonRefRemove\0"
    "onBaseButton\0onProfileButton"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskPipeParameters[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x08 /* Private */,
       4,    1,   47,    2, 0x08 /* Private */,
       5,    1,   50,    2, 0x08 /* Private */,
       6,    1,   53,    2, 0x08 /* Private */,
       7,    1,   56,    2, 0x08 /* Private */,
       8,    1,   59,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,

       0        // eod
};

void PartDesignGui::TaskPipeParameters::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPipeParameters *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onTangentChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->onTransitionChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->onButtonRefAdd((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->onButtonRefRemove((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->onBaseButton((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->onProfileButton((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskPipeParameters::staticMetaObject = { {
    &TaskSketchBasedParameters::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskPipeParameters.data,
    qt_meta_data_PartDesignGui__TaskPipeParameters,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskPipeParameters::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskPipeParameters::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskPipeParameters.stringdata0))
        return static_cast<void*>(this);
    return TaskSketchBasedParameters::qt_metacast(_clname);
}

int PartDesignGui::TaskPipeParameters::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskSketchBasedParameters::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
struct qt_meta_stringdata_PartDesignGui__TaskPipeOrientation_t {
    QByteArrayData data[11];
    char stringdata0[163];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskPipeOrientation_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskPipeOrientation_t qt_meta_stringdata_PartDesignGui__TaskPipeOrientation = {
    {
QT_MOC_LITERAL(0, 0, 34), // "PartDesignGui::TaskPipeOrient..."
QT_MOC_LITERAL(1, 35, 20), // "onOrientationChanged"
QT_MOC_LITERAL(2, 56, 0), // ""
QT_MOC_LITERAL(3, 57, 14), // "onButtonRefAdd"
QT_MOC_LITERAL(4, 72, 7), // "checked"
QT_MOC_LITERAL(5, 80, 17), // "onButtonRefRemove"
QT_MOC_LITERAL(6, 98, 8), // "updateUI"
QT_MOC_LITERAL(7, 107, 3), // "idx"
QT_MOC_LITERAL(8, 111, 12), // "onBaseButton"
QT_MOC_LITERAL(9, 124, 20), // "onCurvelinearChanged"
QT_MOC_LITERAL(10, 145, 17) // "onBinormalChanged"

    },
    "PartDesignGui::TaskPipeOrientation\0"
    "onOrientationChanged\0\0onButtonRefAdd\0"
    "checked\0onButtonRefRemove\0updateUI\0"
    "idx\0onBaseButton\0onCurvelinearChanged\0"
    "onBinormalChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskPipeOrientation[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x08 /* Private */,
       3,    1,   52,    2, 0x08 /* Private */,
       5,    1,   55,    2, 0x08 /* Private */,
       6,    1,   58,    2, 0x08 /* Private */,
       8,    1,   61,    2, 0x08 /* Private */,
       9,    1,   64,    2, 0x08 /* Private */,
      10,    1,   67,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Double,    2,

       0        // eod
};

void PartDesignGui::TaskPipeOrientation::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPipeOrientation *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onOrientationChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->onButtonRefAdd((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->onButtonRefRemove((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->updateUI((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->onBaseButton((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->onCurvelinearChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->onBinormalChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskPipeOrientation::staticMetaObject = { {
    &TaskSketchBasedParameters::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskPipeOrientation.data,
    qt_meta_data_PartDesignGui__TaskPipeOrientation,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskPipeOrientation::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskPipeOrientation::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskPipeOrientation.stringdata0))
        return static_cast<void*>(this);
    return TaskSketchBasedParameters::qt_metacast(_clname);
}

int PartDesignGui::TaskPipeOrientation::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskSketchBasedParameters::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
struct qt_meta_stringdata_PartDesignGui__TaskPipeScaling_t {
    QByteArrayData data[8];
    char stringdata0[103];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskPipeScaling_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskPipeScaling_t qt_meta_stringdata_PartDesignGui__TaskPipeScaling = {
    {
QT_MOC_LITERAL(0, 0, 30), // "PartDesignGui::TaskPipeScaling"
QT_MOC_LITERAL(1, 31, 16), // "onScalingChanged"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 14), // "onButtonRefAdd"
QT_MOC_LITERAL(4, 64, 7), // "checked"
QT_MOC_LITERAL(5, 72, 17), // "onButtonRefRemove"
QT_MOC_LITERAL(6, 90, 8), // "updateUI"
QT_MOC_LITERAL(7, 99, 3) // "idx"

    },
    "PartDesignGui::TaskPipeScaling\0"
    "onScalingChanged\0\0onButtonRefAdd\0"
    "checked\0onButtonRefRemove\0updateUI\0"
    "idx"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskPipeScaling[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x08 /* Private */,
       3,    1,   37,    2, 0x08 /* Private */,
       5,    1,   40,    2, 0x08 /* Private */,
       6,    1,   43,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Int,    7,

       0        // eod
};

void PartDesignGui::TaskPipeScaling::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskPipeScaling *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onScalingChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->onButtonRefAdd((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->onButtonRefRemove((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->updateUI((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskPipeScaling::staticMetaObject = { {
    &TaskSketchBasedParameters::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskPipeScaling.data,
    qt_meta_data_PartDesignGui__TaskPipeScaling,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskPipeScaling::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskPipeScaling::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskPipeScaling.stringdata0))
        return static_cast<void*>(this);
    return TaskSketchBasedParameters::qt_metacast(_clname);
}

int PartDesignGui::TaskPipeScaling::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskSketchBasedParameters::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_PartDesignGui__TaskDlgPipeParameters_t {
    QByteArrayData data[1];
    char stringdata0[37];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartDesignGui__TaskDlgPipeParameters_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartDesignGui__TaskDlgPipeParameters_t qt_meta_stringdata_PartDesignGui__TaskDlgPipeParameters = {
    {
QT_MOC_LITERAL(0, 0, 36) // "PartDesignGui::TaskDlgPipePar..."

    },
    "PartDesignGui::TaskDlgPipeParameters"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartDesignGui__TaskDlgPipeParameters[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PartDesignGui::TaskDlgPipeParameters::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PartDesignGui::TaskDlgPipeParameters::staticMetaObject = { {
    &TaskDlgSketchBasedParameters::staticMetaObject,
    qt_meta_stringdata_PartDesignGui__TaskDlgPipeParameters.data,
    qt_meta_data_PartDesignGui__TaskDlgPipeParameters,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartDesignGui::TaskDlgPipeParameters::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartDesignGui::TaskDlgPipeParameters::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartDesignGui__TaskDlgPipeParameters.stringdata0))
        return static_cast<void*>(this);
    return TaskDlgSketchBasedParameters::qt_metacast(_clname);
}

int PartDesignGui::TaskDlgPipeParameters::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = TaskDlgSketchBasedParameters::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
