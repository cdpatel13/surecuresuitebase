/****************************************************************************
** Meta object code from reading C++ file 'TaskOrthoViews.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Drawing/Gui/TaskOrthoViews.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskOrthoViews.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DrawingGui__TaskOrthoViews_t {
    QByteArrayData data[21];
    char stringdata0[197];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DrawingGui__TaskOrthoViews_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DrawingGui__TaskOrthoViews_t qt_meta_stringdata_DrawingGui__TaskOrthoViews = {
    {
QT_MOC_LITERAL(0, 0, 26), // "DrawingGui::TaskOrthoViews"
QT_MOC_LITERAL(1, 27, 15), // "ShowContextMenu"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 3), // "pos"
QT_MOC_LITERAL(4, 48, 10), // "setPrimary"
QT_MOC_LITERAL(5, 59, 3), // "dir"
QT_MOC_LITERAL(6, 63, 10), // "cb_toggled"
QT_MOC_LITERAL(7, 74, 6), // "toggle"
QT_MOC_LITERAL(8, 81, 17), // "projectionChanged"
QT_MOC_LITERAL(9, 99, 5), // "index"
QT_MOC_LITERAL(10, 105, 6), // "hidden"
QT_MOC_LITERAL(11, 112, 1), // "i"
QT_MOC_LITERAL(12, 114, 6), // "smooth"
QT_MOC_LITERAL(13, 121, 11), // "toggle_auto"
QT_MOC_LITERAL(14, 133, 12), // "data_entered"
QT_MOC_LITERAL(15, 146, 4), // "text"
QT_MOC_LITERAL(16, 151, 10), // "change_axo"
QT_MOC_LITERAL(17, 162, 1), // "p"
QT_MOC_LITERAL(18, 164, 10), // "axo_button"
QT_MOC_LITERAL(19, 175, 9), // "axo_scale"
QT_MOC_LITERAL(20, 185, 11) // "text_return"

    },
    "DrawingGui::TaskOrthoViews\0ShowContextMenu\0"
    "\0pos\0setPrimary\0dir\0cb_toggled\0toggle\0"
    "projectionChanged\0index\0hidden\0i\0"
    "smooth\0toggle_auto\0data_entered\0text\0"
    "change_axo\0p\0axo_button\0axo_scale\0"
    "text_return"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DrawingGui__TaskOrthoViews[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x09 /* Protected */,
       4,    1,   82,    2, 0x09 /* Protected */,
       6,    1,   85,    2, 0x09 /* Protected */,
       8,    1,   88,    2, 0x09 /* Protected */,
      10,    1,   91,    2, 0x09 /* Protected */,
      12,    1,   94,    2, 0x09 /* Protected */,
      13,    1,   97,    2, 0x09 /* Protected */,
      14,    1,  100,    2, 0x09 /* Protected */,
      16,    1,  103,    2, 0x09 /* Protected */,
      16,    0,  106,    2, 0x29 /* Protected | MethodCloned */,
      18,    0,  107,    2, 0x09 /* Protected */,
      19,    1,  108,    2, 0x09 /* Protected */,
      20,    0,  111,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::QPoint,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void,

       0        // eod
};

void DrawingGui::TaskOrthoViews::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskOrthoViews *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ShowContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 1: _t->setPrimary((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->cb_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->projectionChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->hidden((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->smooth((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->toggle_auto((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->data_entered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->change_axo((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->change_axo(); break;
        case 10: _t->axo_button(); break;
        case 11: _t->axo_scale((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->text_return(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DrawingGui::TaskOrthoViews::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_DrawingGui__TaskOrthoViews.data,
    qt_meta_data_DrawingGui__TaskOrthoViews,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DrawingGui::TaskOrthoViews::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DrawingGui::TaskOrthoViews::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DrawingGui__TaskOrthoViews.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int DrawingGui::TaskOrthoViews::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
struct qt_meta_stringdata_DrawingGui__TaskDlgOrthoViews_t {
    QByteArrayData data[1];
    char stringdata0[30];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DrawingGui__TaskDlgOrthoViews_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DrawingGui__TaskDlgOrthoViews_t qt_meta_stringdata_DrawingGui__TaskDlgOrthoViews = {
    {
QT_MOC_LITERAL(0, 0, 29) // "DrawingGui::TaskDlgOrthoViews"

    },
    "DrawingGui::TaskDlgOrthoViews"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DrawingGui__TaskDlgOrthoViews[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void DrawingGui::TaskDlgOrthoViews::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject DrawingGui::TaskDlgOrthoViews::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_DrawingGui__TaskDlgOrthoViews.data,
    qt_meta_data_DrawingGui__TaskDlgOrthoViews,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DrawingGui::TaskDlgOrthoViews::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DrawingGui::TaskDlgOrthoViews::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DrawingGui__TaskDlgOrthoViews.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int DrawingGui::TaskDlgOrthoViews::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
