/****************************************************************************
** Meta object code from reading C++ file 'DrawingView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Drawing/Gui/DrawingView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DrawingView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DrawingGui__SvgView_t {
    QByteArrayData data[7];
    char stringdata0[112];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DrawingGui__SvgView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DrawingGui__SvgView_t qt_meta_stringdata_DrawingGui__SvgView = {
    {
QT_MOC_LITERAL(0, 0, 19), // "DrawingGui::SvgView"
QT_MOC_LITERAL(1, 20, 26), // "setHighQualityAntialiasing"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 23), // "highQualityAntialiasing"
QT_MOC_LITERAL(4, 72, 17), // "setViewBackground"
QT_MOC_LITERAL(5, 90, 6), // "enable"
QT_MOC_LITERAL(6, 97, 14) // "setViewOutline"

    },
    "DrawingGui::SvgView\0setHighQualityAntialiasing\0"
    "\0highQualityAntialiasing\0setViewBackground\0"
    "enable\0setViewOutline"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DrawingGui__SvgView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x0a /* Public */,
       4,    1,   32,    2, 0x0a /* Public */,
       6,    1,   35,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, QMetaType::Bool,    5,

       0        // eod
};

void DrawingGui::SvgView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SvgView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setHighQualityAntialiasing((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->setViewBackground((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->setViewOutline((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DrawingGui::SvgView::staticMetaObject = { {
    &QGraphicsView::staticMetaObject,
    qt_meta_stringdata_DrawingGui__SvgView.data,
    qt_meta_data_DrawingGui__SvgView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DrawingGui::SvgView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DrawingGui::SvgView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DrawingGui__SvgView.stringdata0))
        return static_cast<void*>(this);
    return QGraphicsView::qt_metacast(_clname);
}

int DrawingGui::SvgView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
struct qt_meta_stringdata_DrawingGui__DrawingView_t {
    QByteArrayData data[8];
    char stringdata0[71];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DrawingGui__DrawingView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DrawingGui__DrawingView_t qt_meta_stringdata_DrawingGui__DrawingView = {
    {
QT_MOC_LITERAL(0, 0, 23), // "DrawingGui::DrawingView"
QT_MOC_LITERAL(1, 24, 4), // "load"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 4), // "path"
QT_MOC_LITERAL(4, 35, 11), // "setRenderer"
QT_MOC_LITERAL(5, 47, 8), // "QAction*"
QT_MOC_LITERAL(6, 56, 6), // "action"
QT_MOC_LITERAL(7, 63, 7) // "viewAll"

    },
    "DrawingGui::DrawingView\0load\0\0path\0"
    "setRenderer\0QAction*\0action\0viewAll"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DrawingGui__DrawingView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x0a /* Public */,
       1,    0,   37,    2, 0x2a /* Public | MethodCloned */,
       4,    1,   38,    2, 0x0a /* Public */,
       7,    0,   41,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,

       0        // eod
};

void DrawingGui::DrawingView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DrawingView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->load((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->load(); break;
        case 2: _t->setRenderer((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 3: _t->viewAll(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DrawingGui::DrawingView::staticMetaObject = { {
    &Gui::MDIView::staticMetaObject,
    qt_meta_stringdata_DrawingGui__DrawingView.data,
    qt_meta_data_DrawingGui__DrawingView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DrawingGui::DrawingView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DrawingGui::DrawingView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DrawingGui__DrawingView.stringdata0))
        return static_cast<void*>(this);
    return Gui::MDIView::qt_metacast(_clname);
}

int DrawingGui::DrawingView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::MDIView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
