# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Drawing

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Drawing" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Drawing/README.md"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Drawing/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Drawing/DrawingExample.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Drawing/DrawingTests.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Drawing/DrawingPatterns.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Drawing/InitGui.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/Drawing" TYPE DIRECTORY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Drawing/Templates" FILES_MATCHING REGEX "/[^/]*\\.svg[^/]*$" REGEX "/[^/]*\\.dxf[^/]*$")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Drawing/App/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Drawing/Gui/cmake_install.cmake")

endif()

