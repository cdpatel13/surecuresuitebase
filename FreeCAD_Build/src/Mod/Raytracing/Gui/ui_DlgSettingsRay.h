/********************************************************************************
** Form generated from reading UI file 'DlgSettingsRay.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGSRAY_H
#define UI_DLGSETTINGSRAY_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/FileDialog.h"
#include "Gui/PrefWidgets.h"

namespace RaytracingGui {

class Ui_DlgSettingsRay
{
public:
    QGridLayout *gridLayout1;
    QGroupBox *groupBox5;
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *textLabel2;
    Gui::PrefDoubleSpinBox *prefFloatSpinBox1;
    Gui::PrefCheckBox *prefCheckBox8;
    Gui::PrefCheckBox *prefCheckBox9;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout2;
    Gui::PrefFileChooser *prefFileChooser2;
    QLabel *label;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    Gui::PrefLineEdit *prefLineEdit1;
    QLabel *label_3;
    Gui::PrefSpinBox *prefIntSpinBox1;
    QLabel *label_4;
    Gui::PrefSpinBox *prefIntSpinBox2;
    QLabel *label_5;
    Gui::PrefFileChooser *prefFileChooser3;
    QGroupBox *GroupBox12;
    QGridLayout *gridLayout3;
    QGridLayout *gridLayout4;
    QLabel *textLabel1_3;
    QLabel *textLabel1_2;
    Gui::PrefFileChooser *prefFileChooser1;
    QLabel *textLabel1;
    Gui::PrefLineEdit *prefLineEdit2;
    Gui::PrefLineEdit *prefLineEdit3;
    QSpacerItem *spacerItem;

    void setupUi(QWidget *RaytracingGui__DlgSettingsRay)
    {
        if (RaytracingGui__DlgSettingsRay->objectName().isEmpty())
            RaytracingGui__DlgSettingsRay->setObjectName(QString::fromUtf8("RaytracingGui__DlgSettingsRay"));
        RaytracingGui__DlgSettingsRay->resize(555, 412);
        gridLayout1 = new QGridLayout(RaytracingGui__DlgSettingsRay);
        gridLayout1->setSpacing(6);
        gridLayout1->setContentsMargins(9, 9, 9, 9);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        groupBox5 = new QGroupBox(RaytracingGui__DlgSettingsRay);
        groupBox5->setObjectName(QString::fromUtf8("groupBox5"));
        vboxLayout = new QVBoxLayout(groupBox5);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(10, 10, 10, 10);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        textLabel2 = new QLabel(groupBox5);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(textLabel2->sizePolicy().hasHeightForWidth());
        textLabel2->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(textLabel2);

        prefFloatSpinBox1 = new Gui::PrefDoubleSpinBox(groupBox5);
        prefFloatSpinBox1->setObjectName(QString::fromUtf8("prefFloatSpinBox1"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(prefFloatSpinBox1->sizePolicy().hasHeightForWidth());
        prefFloatSpinBox1->setSizePolicy(sizePolicy1);
        prefFloatSpinBox1->setValue(0.100000000000000);
        prefFloatSpinBox1->setProperty("prefEntry", QVariant(QByteArray("MeshDeviation")));
        prefFloatSpinBox1->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        hboxLayout->addWidget(prefFloatSpinBox1);


        vboxLayout->addLayout(hboxLayout);

        prefCheckBox8 = new Gui::PrefCheckBox(groupBox5);
        prefCheckBox8->setObjectName(QString::fromUtf8("prefCheckBox8"));
        prefCheckBox8->setProperty("prefEntry", QVariant(QByteArray("NotWriteVertexNormals")));
        prefCheckBox8->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        vboxLayout->addWidget(prefCheckBox8);

        prefCheckBox9 = new Gui::PrefCheckBox(groupBox5);
        prefCheckBox9->setObjectName(QString::fromUtf8("prefCheckBox9"));
        prefCheckBox9->setProperty("prefEntry", QVariant(QByteArray("WriteUVCoordinates")));
        prefCheckBox9->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        vboxLayout->addWidget(prefCheckBox9);


        gridLayout1->addWidget(groupBox5, 2, 0, 1, 1);

        groupBox = new QGroupBox(RaytracingGui__DlgSettingsRay);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout2 = new QGridLayout();
        gridLayout2->setSpacing(6);
        gridLayout2->setObjectName(QString::fromUtf8("gridLayout2"));
        prefFileChooser2 = new Gui::PrefFileChooser(groupBox);
        prefFileChooser2->setObjectName(QString::fromUtf8("prefFileChooser2"));
        prefFileChooser2->setProperty("prefEntry", QVariant(QByteArray("PovrayExecutable")));
        prefFileChooser2->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        gridLayout2->addWidget(prefFileChooser2, 0, 1, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout2->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout2->addWidget(label_2, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        prefLineEdit1 = new Gui::PrefLineEdit(groupBox);
        prefLineEdit1->setObjectName(QString::fromUtf8("prefLineEdit1"));
        prefLineEdit1->setProperty("prefEntry", QVariant(QByteArray("OutputParameters")));
        prefLineEdit1->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        horizontalLayout->addWidget(prefLineEdit1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout->addWidget(label_3);

        prefIntSpinBox1 = new Gui::PrefSpinBox(groupBox);
        prefIntSpinBox1->setObjectName(QString::fromUtf8("prefIntSpinBox1"));
        prefIntSpinBox1->setMaximum(9999);
        prefIntSpinBox1->setValue(800);
        prefIntSpinBox1->setProperty("prefEntry", QVariant(QByteArray("OutputWidth")));
        prefIntSpinBox1->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        horizontalLayout->addWidget(prefIntSpinBox1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout->addWidget(label_4);

        prefIntSpinBox2 = new Gui::PrefSpinBox(groupBox);
        prefIntSpinBox2->setObjectName(QString::fromUtf8("prefIntSpinBox2"));
        prefIntSpinBox2->setMaximum(9999);
        prefIntSpinBox2->setValue(600);
        prefIntSpinBox2->setProperty("prefEntry", QVariant(QByteArray("OutputHeight")));
        prefIntSpinBox2->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        horizontalLayout->addWidget(prefIntSpinBox2);


        gridLayout2->addLayout(horizontalLayout, 1, 1, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout2->addWidget(label_5, 2, 0, 1, 1);

        prefFileChooser3 = new Gui::PrefFileChooser(groupBox);
        prefFileChooser3->setObjectName(QString::fromUtf8("prefFileChooser3"));
        prefFileChooser3->setProperty("prefEntry", QVariant(QByteArray("LuxrenderExecutable")));
        prefFileChooser3->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        gridLayout2->addWidget(prefFileChooser3, 2, 1, 1, 1);


        verticalLayout->addLayout(gridLayout2);


        gridLayout1->addWidget(groupBox, 0, 0, 1, 1);

        GroupBox12 = new QGroupBox(RaytracingGui__DlgSettingsRay);
        GroupBox12->setObjectName(QString::fromUtf8("GroupBox12"));
        gridLayout3 = new QGridLayout(GroupBox12);
        gridLayout3->setSpacing(6);
        gridLayout3->setContentsMargins(9, 9, 9, 9);
        gridLayout3->setObjectName(QString::fromUtf8("gridLayout3"));
        gridLayout4 = new QGridLayout();
        gridLayout4->setSpacing(6);
        gridLayout4->setContentsMargins(0, 0, 0, 0);
        gridLayout4->setObjectName(QString::fromUtf8("gridLayout4"));
        textLabel1_3 = new QLabel(GroupBox12);
        textLabel1_3->setObjectName(QString::fromUtf8("textLabel1_3"));

        gridLayout4->addWidget(textLabel1_3, 3, 0, 1, 1);

        textLabel1_2 = new QLabel(GroupBox12);
        textLabel1_2->setObjectName(QString::fromUtf8("textLabel1_2"));

        gridLayout4->addWidget(textLabel1_2, 2, 0, 1, 1);

        prefFileChooser1 = new Gui::PrefFileChooser(GroupBox12);
        prefFileChooser1->setObjectName(QString::fromUtf8("prefFileChooser1"));
        prefFileChooser1->setFocusPolicy(Qt::StrongFocus);
        prefFileChooser1->setMode(Gui::FileChooser::Directory);
        prefFileChooser1->setProperty("prefEntry", QVariant(QByteArray("ProjectPath")));
        prefFileChooser1->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        gridLayout4->addWidget(prefFileChooser1, 1, 1, 1, 1);

        textLabel1 = new QLabel(GroupBox12);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));

        gridLayout4->addWidget(textLabel1, 1, 0, 1, 1);

        prefLineEdit2 = new Gui::PrefLineEdit(GroupBox12);
        prefLineEdit2->setObjectName(QString::fromUtf8("prefLineEdit2"));
        prefLineEdit2->setProperty("prefEntry", QVariant(QByteArray("CameraName")));
        prefLineEdit2->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        gridLayout4->addWidget(prefLineEdit2, 2, 1, 1, 1);

        prefLineEdit3 = new Gui::PrefLineEdit(GroupBox12);
        prefLineEdit3->setObjectName(QString::fromUtf8("prefLineEdit3"));
        prefLineEdit3->setProperty("prefEntry", QVariant(QByteArray("PartName")));
        prefLineEdit3->setProperty("prefPath", QVariant(QByteArray("Mod/Raytracing")));

        gridLayout4->addWidget(prefLineEdit3, 3, 1, 1, 1);


        gridLayout3->addLayout(gridLayout4, 0, 0, 1, 1);


        gridLayout1->addWidget(GroupBox12, 1, 0, 1, 1);

        spacerItem = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout1->addItem(spacerItem, 3, 0, 1, 1);

        QWidget::setTabOrder(prefFileChooser1, prefLineEdit2);
        QWidget::setTabOrder(prefLineEdit2, prefLineEdit3);
        QWidget::setTabOrder(prefLineEdit3, prefFloatSpinBox1);
        QWidget::setTabOrder(prefFloatSpinBox1, prefCheckBox8);
        QWidget::setTabOrder(prefCheckBox8, prefCheckBox9);

        retranslateUi(RaytracingGui__DlgSettingsRay);

        QMetaObject::connectSlotsByName(RaytracingGui__DlgSettingsRay);
    } // setupUi

    void retranslateUi(QWidget *RaytracingGui__DlgSettingsRay)
    {
        RaytracingGui__DlgSettingsRay->setWindowTitle(QApplication::translate("RaytracingGui::DlgSettingsRay", "Raytracing", nullptr));
        groupBox5->setTitle(QApplication::translate("RaytracingGui::DlgSettingsRay", "Mesh export settings", nullptr));
        textLabel2->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "Max mesh deviation:", nullptr));
        prefCheckBox8->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "Do not calculate vertex normals", nullptr));
        prefCheckBox9->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "Write u,v coordinates", nullptr));
        groupBox->setTitle(QApplication::translate("RaytracingGui::DlgSettingsRay", "Render", nullptr));
        label->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "POV-Ray executable:", nullptr));
        label_2->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "POV-Ray output parameters:", nullptr));
#ifndef QT_NO_TOOLTIP
        prefLineEdit1->setToolTip(QApplication::translate("RaytracingGui::DlgSettingsRay", "The POV-Ray parameters to be passed to the render.", nullptr));
#endif // QT_NO_TOOLTIP
        prefLineEdit1->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "+P +A", nullptr));
        label_3->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "+W: ", nullptr));
#ifndef QT_NO_TOOLTIP
        prefIntSpinBox1->setToolTip(QApplication::translate("RaytracingGui::DlgSettingsRay", "The width of the rendered image", nullptr));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", " +H : ", nullptr));
#ifndef QT_NO_TOOLTIP
        prefIntSpinBox2->setToolTip(QApplication::translate("RaytracingGui::DlgSettingsRay", "The height of the rendered image", nullptr));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "Luxrender executable:", nullptr));
#ifndef QT_NO_TOOLTIP
        prefFileChooser3->setToolTip(QApplication::translate("RaytracingGui::DlgSettingsRay", "The path to the luxrender (or luxconsole) executable", nullptr));
#endif // QT_NO_TOOLTIP
        GroupBox12->setTitle(QApplication::translate("RaytracingGui::DlgSettingsRay", "Directories", nullptr));
        textLabel1_3->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "Part file name:", nullptr));
        textLabel1_2->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "Camera file name:", nullptr));
#ifndef QT_NO_TOOLTIP
        prefFileChooser1->setToolTip(QApplication::translate("RaytracingGui::DlgSettingsRay", "Used by utility tools", nullptr));
#endif // QT_NO_TOOLTIP
        textLabel1->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "Default Project dir:", nullptr));
#ifndef QT_NO_TOOLTIP
        prefLineEdit2->setToolTip(QApplication::translate("RaytracingGui::DlgSettingsRay", "Used by utility tools", nullptr));
#endif // QT_NO_TOOLTIP
        prefLineEdit2->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "TempCamera.inc", nullptr));
#ifndef QT_NO_TOOLTIP
        prefLineEdit3->setToolTip(QApplication::translate("RaytracingGui::DlgSettingsRay", "Used by utility tools", nullptr));
#endif // QT_NO_TOOLTIP
        prefLineEdit3->setText(QApplication::translate("RaytracingGui::DlgSettingsRay", "TempPart.inc", nullptr));
    } // retranslateUi

};

} // namespace RaytracingGui

namespace RaytracingGui {
namespace Ui {
    class DlgSettingsRay: public Ui_DlgSettingsRay {};
} // namespace Ui
} // namespace RaytracingGui

#endif // UI_DLGSETTINGSRAY_H
