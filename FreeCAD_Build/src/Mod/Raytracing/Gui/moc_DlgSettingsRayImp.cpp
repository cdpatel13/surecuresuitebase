/****************************************************************************
** Meta object code from reading C++ file 'DlgSettingsRayImp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Raytracing/Gui/DlgSettingsRayImp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgSettingsRayImp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_RaytracingGui__DlgSettingsRayImp_t {
    QByteArrayData data[1];
    char stringdata0[33];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RaytracingGui__DlgSettingsRayImp_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RaytracingGui__DlgSettingsRayImp_t qt_meta_stringdata_RaytracingGui__DlgSettingsRayImp = {
    {
QT_MOC_LITERAL(0, 0, 32) // "RaytracingGui::DlgSettingsRayImp"

    },
    "RaytracingGui::DlgSettingsRayImp"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RaytracingGui__DlgSettingsRayImp[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void RaytracingGui::DlgSettingsRayImp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject RaytracingGui::DlgSettingsRayImp::staticMetaObject = { {
    &Gui::Dialog::PreferencePage::staticMetaObject,
    qt_meta_stringdata_RaytracingGui__DlgSettingsRayImp.data,
    qt_meta_data_RaytracingGui__DlgSettingsRayImp,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *RaytracingGui::DlgSettingsRayImp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RaytracingGui::DlgSettingsRayImp::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_RaytracingGui__DlgSettingsRayImp.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui_DlgSettingsRay"))
        return static_cast< Ui_DlgSettingsRay*>(this);
    return Gui::Dialog::PreferencePage::qt_metacast(_clname);
}

int RaytracingGui::DlgSettingsRayImp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::Dialog::PreferencePage::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
