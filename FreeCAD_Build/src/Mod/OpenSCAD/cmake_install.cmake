# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/OpenSCAD/ply" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/ply/lex.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/ply/README"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/ply/yacc.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/ply/__init__.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/OpenSCAD" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/InitGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/OpenSCAD2Dgeom.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/OpenSCADFeatures.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/OpenSCADUtils.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/OpenSCADCommands.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/exportCSG.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/importCSG.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/tokrules.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/colorcodeshapes.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/expandplacements.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/replaceobj.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/OpenSCAD/OpenSCAD_rc.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/OpenSCAD/Resources/icons" TYPE FILE FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/OpenSCAD/Resources/icons/OpenSCADWorkbench.svg")
endif()

