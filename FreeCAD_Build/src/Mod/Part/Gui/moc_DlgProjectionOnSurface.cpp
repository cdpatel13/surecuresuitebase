/****************************************************************************
** Meta object code from reading C++ file 'DlgProjectionOnSurface.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Part/Gui/DlgProjectionOnSurface.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgProjectionOnSurface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PartGui__DlgProjectionOnSurface_t {
    QByteArrayData data[16];
    char stringdata0[443];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__DlgProjectionOnSurface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__DlgProjectionOnSurface_t qt_meta_stringdata_PartGui__DlgProjectionOnSurface = {
    {
QT_MOC_LITERAL(0, 0, 31), // "PartGui::DlgProjectionOnSurface"
QT_MOC_LITERAL(1, 32, 28), // "on_pushButtonAddFace_clicked"
QT_MOC_LITERAL(2, 61, 0), // ""
QT_MOC_LITERAL(3, 62, 28), // "on_pushButtonAddEdge_clicked"
QT_MOC_LITERAL(4, 91, 37), // "on_pushButtonGetCurrentCamDir..."
QT_MOC_LITERAL(5, 129, 25), // "on_pushButtonDirX_clicked"
QT_MOC_LITERAL(6, 155, 25), // "on_pushButtonDirY_clicked"
QT_MOC_LITERAL(7, 181, 25), // "on_pushButtonDirZ_clicked"
QT_MOC_LITERAL(8, 207, 32), // "on_pushButtonAddProjFace_clicked"
QT_MOC_LITERAL(9, 240, 29), // "on_radioButtonShowAll_clicked"
QT_MOC_LITERAL(10, 270, 27), // "on_radioButtonFaces_clicked"
QT_MOC_LITERAL(11, 298, 27), // "on_radioButtonEdges_clicked"
QT_MOC_LITERAL(12, 326, 42), // "on_doubleSpinBoxExtrudeHeight..."
QT_MOC_LITERAL(13, 369, 4), // "arg1"
QT_MOC_LITERAL(14, 374, 28), // "on_pushButtonAddWire_clicked"
QT_MOC_LITERAL(15, 403, 39) // "on_doubleSpinBoxSolidDepth_va..."

    },
    "PartGui::DlgProjectionOnSurface\0"
    "on_pushButtonAddFace_clicked\0\0"
    "on_pushButtonAddEdge_clicked\0"
    "on_pushButtonGetCurrentCamDir_clicked\0"
    "on_pushButtonDirX_clicked\0"
    "on_pushButtonDirY_clicked\0"
    "on_pushButtonDirZ_clicked\0"
    "on_pushButtonAddProjFace_clicked\0"
    "on_radioButtonShowAll_clicked\0"
    "on_radioButtonFaces_clicked\0"
    "on_radioButtonEdges_clicked\0"
    "on_doubleSpinBoxExtrudeHeight_valueChanged\0"
    "arg1\0on_pushButtonAddWire_clicked\0"
    "on_doubleSpinBoxSolidDepth_valueChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__DlgProjectionOnSurface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x08 /* Private */,
       3,    0,   80,    2, 0x08 /* Private */,
       4,    0,   81,    2, 0x08 /* Private */,
       5,    0,   82,    2, 0x08 /* Private */,
       6,    0,   83,    2, 0x08 /* Private */,
       7,    0,   84,    2, 0x08 /* Private */,
       8,    0,   85,    2, 0x08 /* Private */,
       9,    0,   86,    2, 0x08 /* Private */,
      10,    0,   87,    2, 0x08 /* Private */,
      11,    0,   88,    2, 0x08 /* Private */,
      12,    1,   89,    2, 0x08 /* Private */,
      14,    0,   92,    2, 0x08 /* Private */,
      15,    1,   93,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   13,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   13,

       0        // eod
};

void PartGui::DlgProjectionOnSurface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgProjectionOnSurface *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_pushButtonAddFace_clicked(); break;
        case 1: _t->on_pushButtonAddEdge_clicked(); break;
        case 2: _t->on_pushButtonGetCurrentCamDir_clicked(); break;
        case 3: _t->on_pushButtonDirX_clicked(); break;
        case 4: _t->on_pushButtonDirY_clicked(); break;
        case 5: _t->on_pushButtonDirZ_clicked(); break;
        case 6: _t->on_pushButtonAddProjFace_clicked(); break;
        case 7: _t->on_radioButtonShowAll_clicked(); break;
        case 8: _t->on_radioButtonFaces_clicked(); break;
        case 9: _t->on_radioButtonEdges_clicked(); break;
        case 10: _t->on_doubleSpinBoxExtrudeHeight_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->on_pushButtonAddWire_clicked(); break;
        case 12: _t->on_doubleSpinBoxSolidDepth_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartGui::DlgProjectionOnSurface::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_PartGui__DlgProjectionOnSurface.data,
    qt_meta_data_PartGui__DlgProjectionOnSurface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::DlgProjectionOnSurface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::DlgProjectionOnSurface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__DlgProjectionOnSurface.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionObserver"))
        return static_cast< Gui::SelectionObserver*>(this);
    if (!strcmp(_clname, "App::DocumentObserver"))
        return static_cast< App::DocumentObserver*>(this);
    return QWidget::qt_metacast(_clname);
}

int PartGui::DlgProjectionOnSurface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
struct qt_meta_stringdata_PartGui__TaskProjectionOnSurface_t {
    QByteArrayData data[1];
    char stringdata0[33];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__TaskProjectionOnSurface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__TaskProjectionOnSurface_t qt_meta_stringdata_PartGui__TaskProjectionOnSurface = {
    {
QT_MOC_LITERAL(0, 0, 32) // "PartGui::TaskProjectionOnSurface"

    },
    "PartGui::TaskProjectionOnSurface"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__TaskProjectionOnSurface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PartGui::TaskProjectionOnSurface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PartGui::TaskProjectionOnSurface::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_PartGui__TaskProjectionOnSurface.data,
    qt_meta_data_PartGui__TaskProjectionOnSurface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::TaskProjectionOnSurface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::TaskProjectionOnSurface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__TaskProjectionOnSurface.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int PartGui::TaskProjectionOnSurface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
