/********************************************************************************
** Form generated from reading UI file 'DlgImportExportStep.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGIMPORTEXPORTSTEP_H
#define UI_DLGIMPORTEXPORTSTEP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"

namespace PartGui {

class Ui_DlgImportExportStep
{
public:
    QGridLayout *gridLayout_4;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QRadioButton *radioButtonAP203;
    QRadioButton *radioButtonAP214;
    Gui::PrefCheckBox *checkBoxExportHiddenObj;
    QLabel *label;
    QCheckBox *checkBoxPcurves;
    QComboBox *comboBoxUnits;
    QSpacerItem *spacerItem;
    Gui::PrefCheckBox *checkBoxExportLegacy;
    Gui::PrefCheckBox *checkBoxKeepPlacement;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBoxHeader;
    QGridLayout *gridLayout_2;
    QLineEdit *lineEditAuthor;
    QLineEdit *lineEditProduct;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_2;
    QLineEdit *lineEditCompany;
    QGroupBox *GroupBox2;
    QVBoxLayout *verticalLayout;
    Gui::PrefCheckBox *checkBoxMergeCompound;
    Gui::PrefCheckBox *checkBoxUseLinkGroup;
    Gui::PrefCheckBox *checkBoxImportHiddenObj;
    Gui::PrefCheckBox *checkBoxReduceObjects;
    Gui::PrefCheckBox *checkBoxExpandCompound;
    Gui::PrefCheckBox *checkBoxShowProgress;
    Gui::PrefCheckBox *checkBoxUseBaseName;
    QHBoxLayout *horizontalLayout;
    QLabel *label_5;
    Gui::PrefComboBox *comboBoxImportMode;

    void setupUi(QWidget *PartGui__DlgImportExportStep)
    {
        if (PartGui__DlgImportExportStep->objectName().isEmpty())
            PartGui__DlgImportExportStep->setObjectName(QString::fromUtf8("PartGui__DlgImportExportStep"));
        PartGui__DlgImportExportStep->resize(445, 637);
        gridLayout_4 = new QGridLayout(PartGui__DlgImportExportStep);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        groupBox = new QGroupBox(PartGui__DlgImportExportStep);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        radioButtonAP203 = new QRadioButton(groupBox_2);
        radioButtonAP203->setObjectName(QString::fromUtf8("radioButtonAP203"));
        radioButtonAP203->setText(QString::fromUtf8("AP 203"));
        radioButtonAP203->setChecked(true);

        gridLayout->addWidget(radioButtonAP203, 0, 0, 1, 1);

        radioButtonAP214 = new QRadioButton(groupBox_2);
        radioButtonAP214->setObjectName(QString::fromUtf8("radioButtonAP214"));
        radioButtonAP214->setText(QString::fromUtf8("AP 214"));

        gridLayout->addWidget(radioButtonAP214, 0, 1, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 6, 0, 1, 3);

        checkBoxExportHiddenObj = new Gui::PrefCheckBox(groupBox);
        checkBoxExportHiddenObj->setObjectName(QString::fromUtf8("checkBoxExportHiddenObj"));

        gridLayout_3->addWidget(checkBoxExportHiddenObj, 3, 0, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_3->addWidget(label, 0, 0, 1, 1);

        checkBoxPcurves = new QCheckBox(groupBox);
        checkBoxPcurves->setObjectName(QString::fromUtf8("checkBoxPcurves"));

        gridLayout_3->addWidget(checkBoxPcurves, 2, 0, 1, 3);

        comboBoxUnits = new QComboBox(groupBox);
        comboBoxUnits->addItem(QString());
        comboBoxUnits->addItem(QString());
        comboBoxUnits->addItem(QString());
        comboBoxUnits->setObjectName(QString::fromUtf8("comboBoxUnits"));

        gridLayout_3->addWidget(comboBoxUnits, 0, 2, 1, 1);

        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(spacerItem, 0, 1, 1, 1);

        checkBoxExportLegacy = new Gui::PrefCheckBox(groupBox);
        checkBoxExportLegacy->setObjectName(QString::fromUtf8("checkBoxExportLegacy"));

        gridLayout_3->addWidget(checkBoxExportLegacy, 5, 0, 1, 1);

        checkBoxKeepPlacement = new Gui::PrefCheckBox(groupBox);
        checkBoxKeepPlacement->setObjectName(QString::fromUtf8("checkBoxKeepPlacement"));

        gridLayout_3->addWidget(checkBoxKeepPlacement, 4, 0, 1, 1);


        gridLayout_4->addWidget(groupBox, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 82, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 4, 0, 1, 1);

        groupBoxHeader = new QGroupBox(PartGui__DlgImportExportStep);
        groupBoxHeader->setObjectName(QString::fromUtf8("groupBoxHeader"));
        gridLayout_2 = new QGridLayout(groupBoxHeader);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lineEditAuthor = new QLineEdit(groupBoxHeader);
        lineEditAuthor->setObjectName(QString::fromUtf8("lineEditAuthor"));

        gridLayout_2->addWidget(lineEditAuthor, 1, 1, 1, 1);

        lineEditProduct = new QLineEdit(groupBoxHeader);
        lineEditProduct->setObjectName(QString::fromUtf8("lineEditProduct"));

        gridLayout_2->addWidget(lineEditProduct, 2, 1, 1, 1);

        label_3 = new QLabel(groupBoxHeader);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 1, 0, 1, 1);

        label_4 = new QLabel(groupBoxHeader);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 2, 0, 1, 1);

        label_2 = new QLabel(groupBoxHeader);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 0, 0, 1, 1);

        lineEditCompany = new QLineEdit(groupBoxHeader);
        lineEditCompany->setObjectName(QString::fromUtf8("lineEditCompany"));

        gridLayout_2->addWidget(lineEditCompany, 0, 1, 1, 1);


        gridLayout_4->addWidget(groupBoxHeader, 3, 0, 1, 1);

        GroupBox2 = new QGroupBox(PartGui__DlgImportExportStep);
        GroupBox2->setObjectName(QString::fromUtf8("GroupBox2"));
        verticalLayout = new QVBoxLayout(GroupBox2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        checkBoxMergeCompound = new Gui::PrefCheckBox(GroupBox2);
        checkBoxMergeCompound->setObjectName(QString::fromUtf8("checkBoxMergeCompound"));
        checkBoxMergeCompound->setChecked(true);
        checkBoxMergeCompound->setProperty("prefEntry", QVariant(QByteArray("ReadShapeCompoundMode")));
        checkBoxMergeCompound->setProperty("prefPath", QVariant(QByteArray("Mod/Import/hSTEP")));

        verticalLayout->addWidget(checkBoxMergeCompound);

        checkBoxUseLinkGroup = new Gui::PrefCheckBox(GroupBox2);
        checkBoxUseLinkGroup->setObjectName(QString::fromUtf8("checkBoxUseLinkGroup"));

        verticalLayout->addWidget(checkBoxUseLinkGroup);

        checkBoxImportHiddenObj = new Gui::PrefCheckBox(GroupBox2);
        checkBoxImportHiddenObj->setObjectName(QString::fromUtf8("checkBoxImportHiddenObj"));

        verticalLayout->addWidget(checkBoxImportHiddenObj);

        checkBoxReduceObjects = new Gui::PrefCheckBox(GroupBox2);
        checkBoxReduceObjects->setObjectName(QString::fromUtf8("checkBoxReduceObjects"));

        verticalLayout->addWidget(checkBoxReduceObjects);

        checkBoxExpandCompound = new Gui::PrefCheckBox(GroupBox2);
        checkBoxExpandCompound->setObjectName(QString::fromUtf8("checkBoxExpandCompound"));

        verticalLayout->addWidget(checkBoxExpandCompound);

        checkBoxShowProgress = new Gui::PrefCheckBox(GroupBox2);
        checkBoxShowProgress->setObjectName(QString::fromUtf8("checkBoxShowProgress"));

        verticalLayout->addWidget(checkBoxShowProgress);

        checkBoxUseBaseName = new Gui::PrefCheckBox(GroupBox2);
        checkBoxUseBaseName->setObjectName(QString::fromUtf8("checkBoxUseBaseName"));

        verticalLayout->addWidget(checkBoxUseBaseName);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_5 = new QLabel(GroupBox2);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout->addWidget(label_5);

        comboBoxImportMode = new Gui::PrefComboBox(GroupBox2);
        comboBoxImportMode->addItem(QString());
        comboBoxImportMode->addItem(QString());
        comboBoxImportMode->addItem(QString());
        comboBoxImportMode->addItem(QString());
        comboBoxImportMode->addItem(QString());
        comboBoxImportMode->setObjectName(QString::fromUtf8("comboBoxImportMode"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(comboBoxImportMode->sizePolicy().hasHeightForWidth());
        comboBoxImportMode->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(comboBoxImportMode);


        verticalLayout->addLayout(horizontalLayout);


        gridLayout_4->addWidget(GroupBox2, 2, 0, 1, 1);

        QWidget::setTabOrder(comboBoxUnits, checkBoxPcurves);
        QWidget::setTabOrder(checkBoxPcurves, checkBoxExportHiddenObj);
        QWidget::setTabOrder(checkBoxExportHiddenObj, radioButtonAP203);
        QWidget::setTabOrder(radioButtonAP203, radioButtonAP214);
        QWidget::setTabOrder(radioButtonAP214, checkBoxMergeCompound);
        QWidget::setTabOrder(checkBoxMergeCompound, checkBoxUseLinkGroup);
        QWidget::setTabOrder(checkBoxUseLinkGroup, checkBoxImportHiddenObj);
        QWidget::setTabOrder(checkBoxImportHiddenObj, checkBoxReduceObjects);
        QWidget::setTabOrder(checkBoxReduceObjects, checkBoxExpandCompound);
        QWidget::setTabOrder(checkBoxExpandCompound, checkBoxShowProgress);
        QWidget::setTabOrder(checkBoxShowProgress, checkBoxUseBaseName);
        QWidget::setTabOrder(checkBoxUseBaseName, comboBoxImportMode);
        QWidget::setTabOrder(comboBoxImportMode, lineEditCompany);
        QWidget::setTabOrder(lineEditCompany, lineEditAuthor);
        QWidget::setTabOrder(lineEditAuthor, lineEditProduct);

        retranslateUi(PartGui__DlgImportExportStep);

        QMetaObject::connectSlotsByName(PartGui__DlgImportExportStep);
    } // setupUi

    void retranslateUi(QWidget *PartGui__DlgImportExportStep)
    {
        PartGui__DlgImportExportStep->setWindowTitle(QApplication::translate("PartGui::DlgImportExportStep", "STEP", nullptr));
        groupBox->setTitle(QApplication::translate("PartGui::DlgImportExportStep", "Export", nullptr));
        groupBox_2->setTitle(QApplication::translate("PartGui::DlgImportExportStep", "Scheme", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxExportHiddenObj->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "Uncheck this to skip invisible object when exporting, which is useful for CADs that do not support invisibility STEP styling.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxExportHiddenObj->setText(QApplication::translate("PartGui::DlgImportExportStep", "Export invisible objects", nullptr));
        checkBoxExportHiddenObj->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "ExportHiddenObject", nullptr)));
        checkBoxExportHiddenObj->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
        label->setText(QApplication::translate("PartGui::DlgImportExportStep", "Units for export of STEP", nullptr));
        checkBoxPcurves->setText(QApplication::translate("PartGui::DlgImportExportStep", "Write out curves in parametric space of surface", nullptr));
        comboBoxUnits->setItemText(0, QApplication::translate("PartGui::DlgImportExportStep", "Millimeter", nullptr));
        comboBoxUnits->setItemText(1, QApplication::translate("PartGui::DlgImportExportStep", "Meter", nullptr));
        comboBoxUnits->setItemText(2, QApplication::translate("PartGui::DlgImportExportStep", "Inch", nullptr));

        checkBoxExportLegacy->setText(QApplication::translate("PartGui::DlgImportExportStep", "Use legacy exporter", nullptr));
        checkBoxExportLegacy->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
        checkBoxExportLegacy->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "ExportLegacy", nullptr)));
#ifndef QT_NO_TOOLTIP
        checkBoxKeepPlacement->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "Check this option to keep the placement information when exporting\n"
"a single object. Please note that when import back the STEP file, the\n"
"placement will be encoded into the shape geometry, instead of keeping\n"
"it inside the Placement property.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxKeepPlacement->setText(QApplication::translate("PartGui::DlgImportExportStep", "Export single object placement", nullptr));
        checkBoxKeepPlacement->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "ExportKeepPlacement", nullptr)));
        checkBoxKeepPlacement->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
#ifndef QT_NO_TOOLTIP
        groupBoxHeader->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "If not empty, field contents will be used in the STEP file header.", nullptr));
#endif // QT_NO_TOOLTIP
        groupBoxHeader->setTitle(QApplication::translate("PartGui::DlgImportExportStep", "Header", nullptr));
        label_3->setText(QApplication::translate("PartGui::DlgImportExportStep", "Author", nullptr));
        label_4->setText(QApplication::translate("PartGui::DlgImportExportStep", "Product", nullptr));
        label_2->setText(QApplication::translate("PartGui::DlgImportExportStep", "Company", nullptr));
        GroupBox2->setTitle(QApplication::translate("PartGui::DlgImportExportStep", "Import", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxMergeCompound->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "If checked, no Compound merge will be done\n"
"during file reading (slower but higher details).", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxMergeCompound->setText(QApplication::translate("PartGui::DlgImportExportStep", "Enable STEP Compound merge", nullptr));
#ifndef QT_NO_TOOLTIP
        checkBoxUseLinkGroup->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "Select this to use App::LinkGroup as group container, or else use App::Part.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxUseLinkGroup->setText(QApplication::translate("PartGui::DlgImportExportStep", "Use LinkGroup", nullptr));
        checkBoxUseLinkGroup->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "UseLinkGroup", nullptr)));
        checkBoxUseLinkGroup->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
#ifndef QT_NO_TOOLTIP
        checkBoxImportHiddenObj->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "Select this to not import any invisible objects.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxImportHiddenObj->setText(QApplication::translate("PartGui::DlgImportExportStep", "Import invisible objects", nullptr));
        checkBoxImportHiddenObj->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "ImportHiddenObject", nullptr)));
        checkBoxImportHiddenObj->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
#ifndef QT_NO_TOOLTIP
        checkBoxReduceObjects->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "Reduce number of objects using Link array", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxReduceObjects->setText(QApplication::translate("PartGui::DlgImportExportStep", "Reduce number of objects", nullptr));
        checkBoxReduceObjects->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "ReduceObjects", nullptr)));
        checkBoxReduceObjects->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
#ifndef QT_NO_TOOLTIP
        checkBoxExpandCompound->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "Expand compound shape with multiple solids", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxExpandCompound->setText(QApplication::translate("PartGui::DlgImportExportStep", "Expand compound shape", nullptr));
        checkBoxExpandCompound->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "ExpandCompound", nullptr)));
        checkBoxExpandCompound->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
#ifndef QT_NO_TOOLTIP
        checkBoxShowProgress->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "Show progress bar when importing", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxShowProgress->setText(QApplication::translate("PartGui::DlgImportExportStep", "Show progress bar when importing", nullptr));
        checkBoxShowProgress->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "ShowProgress", nullptr)));
        checkBoxShowProgress->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
#ifndef QT_NO_TOOLTIP
        checkBoxUseBaseName->setToolTip(QApplication::translate("PartGui::DlgImportExportStep", "Do not use instance name. Useful for some legacy STEP file with non-meaningful auto generated instance names.", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxUseBaseName->setText(QApplication::translate("PartGui::DlgImportExportStep", "Ignore instance names", nullptr));
        checkBoxUseBaseName->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "UseBaseName", nullptr)));
        checkBoxUseBaseName->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
        label_5->setText(QApplication::translate("PartGui::DlgImportExportStep", "Mode", nullptr));
        comboBoxImportMode->setItemText(0, QApplication::translate("PartGui::DlgImportExportStep", "Single document", nullptr));
        comboBoxImportMode->setItemText(1, QApplication::translate("PartGui::DlgImportExportStep", "Assembly per document", nullptr));
        comboBoxImportMode->setItemText(2, QApplication::translate("PartGui::DlgImportExportStep", "Assembly per document in sub-directory", nullptr));
        comboBoxImportMode->setItemText(3, QApplication::translate("PartGui::DlgImportExportStep", "Object per document", nullptr));
        comboBoxImportMode->setItemText(4, QApplication::translate("PartGui::DlgImportExportStep", "Object per document in sub-directory", nullptr));

        comboBoxImportMode->setProperty("prefEntry", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "ImportMode", nullptr)));
        comboBoxImportMode->setProperty("prefPath", QVariant(QApplication::translate("PartGui::DlgImportExportStep", "Mod/Import", nullptr)));
    } // retranslateUi

};

} // namespace PartGui

namespace PartGui {
namespace Ui {
    class DlgImportExportStep: public Ui_DlgImportExportStep {};
} // namespace Ui
} // namespace PartGui

#endif // UI_DLGIMPORTEXPORTSTEP_H
