/********************************************************************************
** Form generated from reading UI file 'DlgExtrusion.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGEXTRUSION_H
#define UI_DLGEXTRUSION_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include "Gui/QuantitySpinBox.h"

namespace PartGui {

class Ui_DlgExtrusion
{
public:
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QCheckBox *chkReversed;
    QRadioButton *rbDirModeCustom;
    QRadioButton *rbDirModeNormal;
    QPushButton *btnSelectEdge;
    QLineEdit *txtLink;
    QRadioButton *rbDirModeEdge;
    QGridLayout *gridLayout_3;
    QPushButton *btnX;
    QDoubleSpinBox *dirX;
    QPushButton *btnY;
    QDoubleSpinBox *dirY;
    QPushButton *btnZ;
    QDoubleSpinBox *dirZ;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *label_5;
    Gui::QuantitySpinBox *spinLenFwd;
    QLabel *label_6;
    Gui::QuantitySpinBox *spinLenRev;
    QCheckBox *chkSymmetric;
    QGridLayout *gridLayout_4;
    QLabel *label;
    Gui::QuantitySpinBox *spinTaperAngle;
    Gui::QuantitySpinBox *spinTaperAngleRev;
    QCheckBox *chkSolid;
    QFrame *line;
    QTreeWidget *treeWidget;
    QLabel *statusLabel;

    void setupUi(QDialog *PartGui__DlgExtrusion)
    {
        if (PartGui__DlgExtrusion->objectName().isEmpty())
            PartGui__DlgExtrusion->setObjectName(QString::fromUtf8("PartGui__DlgExtrusion"));
        PartGui__DlgExtrusion->resize(343, 798);
        verticalLayout_2 = new QVBoxLayout(PartGui__DlgExtrusion);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox_2 = new QGroupBox(PartGui__DlgExtrusion);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy);
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        chkReversed = new QCheckBox(groupBox_2);
        chkReversed->setObjectName(QString::fromUtf8("chkReversed"));

        gridLayout_2->addWidget(chkReversed, 4, 2, 1, 1);

        rbDirModeCustom = new QRadioButton(groupBox_2);
        rbDirModeCustom->setObjectName(QString::fromUtf8("rbDirModeCustom"));

        gridLayout_2->addWidget(rbDirModeCustom, 6, 0, 1, 1);

        rbDirModeNormal = new QRadioButton(groupBox_2);
        rbDirModeNormal->setObjectName(QString::fromUtf8("rbDirModeNormal"));
        rbDirModeNormal->setChecked(true);

        gridLayout_2->addWidget(rbDirModeNormal, 3, 0, 1, 1);

        btnSelectEdge = new QPushButton(groupBox_2);
        btnSelectEdge->setObjectName(QString::fromUtf8("btnSelectEdge"));

        gridLayout_2->addWidget(btnSelectEdge, 5, 2, 1, 1);

        txtLink = new QLineEdit(groupBox_2);
        txtLink->setObjectName(QString::fromUtf8("txtLink"));

        gridLayout_2->addWidget(txtLink, 5, 0, 1, 1);

        rbDirModeEdge = new QRadioButton(groupBox_2);
        rbDirModeEdge->setObjectName(QString::fromUtf8("rbDirModeEdge"));

        gridLayout_2->addWidget(rbDirModeEdge, 4, 0, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        btnX = new QPushButton(groupBox_2);
        btnX->setObjectName(QString::fromUtf8("btnX"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btnX->sizePolicy().hasHeightForWidth());
        btnX->setSizePolicy(sizePolicy1);

        gridLayout_3->addWidget(btnX, 0, 0, 1, 1);

        dirX = new QDoubleSpinBox(groupBox_2);
        dirX->setObjectName(QString::fromUtf8("dirX"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(dirX->sizePolicy().hasHeightForWidth());
        dirX->setSizePolicy(sizePolicy2);
        dirX->setMinimum(-2147480000.000000000000000);
        dirX->setMaximum(2147480000.000000000000000);

        gridLayout_3->addWidget(dirX, 0, 1, 1, 1);

        btnY = new QPushButton(groupBox_2);
        btnY->setObjectName(QString::fromUtf8("btnY"));

        gridLayout_3->addWidget(btnY, 1, 0, 1, 1);

        dirY = new QDoubleSpinBox(groupBox_2);
        dirY->setObjectName(QString::fromUtf8("dirY"));
        sizePolicy2.setHeightForWidth(dirY->sizePolicy().hasHeightForWidth());
        dirY->setSizePolicy(sizePolicy2);
        dirY->setMinimum(-2147480000.000000000000000);
        dirY->setMaximum(2147480000.000000000000000);

        gridLayout_3->addWidget(dirY, 1, 1, 1, 1);

        btnZ = new QPushButton(groupBox_2);
        btnZ->setObjectName(QString::fromUtf8("btnZ"));

        gridLayout_3->addWidget(btnZ, 2, 0, 1, 1);

        dirZ = new QDoubleSpinBox(groupBox_2);
        dirZ->setObjectName(QString::fromUtf8("dirZ"));
        sizePolicy2.setHeightForWidth(dirZ->sizePolicy().hasHeightForWidth());
        dirZ->setSizePolicy(sizePolicy2);
        dirZ->setKeyboardTracking(false);
        dirZ->setMinimum(-2147480000.000000000000000);
        dirZ->setMaximum(2147480000.000000000000000);
        dirZ->setValue(1.000000000000000);

        gridLayout_3->addWidget(dirZ, 2, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout_3, 7, 0, 1, 3);


        verticalLayout_2->addWidget(groupBox_2);

        groupBox = new QGroupBox(PartGui__DlgExtrusion);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 0, 0, 1, 1);

        spinLenFwd = new Gui::QuantitySpinBox(groupBox);
        spinLenFwd->setObjectName(QString::fromUtf8("spinLenFwd"));
        sizePolicy2.setHeightForWidth(spinLenFwd->sizePolicy().hasHeightForWidth());
        spinLenFwd->setSizePolicy(sizePolicy2);
        spinLenFwd->setMinimumSize(QSize(0, 0));
        spinLenFwd->setProperty("unit", QVariant(QString::fromUtf8("mm")));
        spinLenFwd->setMinimum(-2147480000.000000000000000);
        spinLenFwd->setMaximum(2147480000.000000000000000);
        spinLenFwd->setValue(10.000000000000000);

        gridLayout->addWidget(spinLenFwd, 0, 1, 1, 1);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 1, 0, 1, 1);

        spinLenRev = new Gui::QuantitySpinBox(groupBox);
        spinLenRev->setObjectName(QString::fromUtf8("spinLenRev"));
        sizePolicy2.setHeightForWidth(spinLenRev->sizePolicy().hasHeightForWidth());
        spinLenRev->setSizePolicy(sizePolicy2);
        spinLenRev->setMinimumSize(QSize(0, 0));
        spinLenRev->setProperty("unit", QVariant(QString::fromUtf8("mm")));
        spinLenRev->setMinimum(-2147480000.000000000000000);
        spinLenRev->setMaximum(2147480000.000000000000000);
        spinLenRev->setValue(0.000000000000000);

        gridLayout->addWidget(spinLenRev, 1, 1, 1, 1);

        chkSymmetric = new QCheckBox(groupBox);
        chkSymmetric->setObjectName(QString::fromUtf8("chkSymmetric"));

        gridLayout->addWidget(chkSymmetric, 2, 0, 1, 2);


        verticalLayout_2->addWidget(groupBox);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label = new QLabel(PartGui__DlgExtrusion);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_4->addWidget(label, 0, 0, 1, 1);

        spinTaperAngle = new Gui::QuantitySpinBox(PartGui__DlgExtrusion);
        spinTaperAngle->setObjectName(QString::fromUtf8("spinTaperAngle"));
        sizePolicy2.setHeightForWidth(spinTaperAngle->sizePolicy().hasHeightForWidth());
        spinTaperAngle->setSizePolicy(sizePolicy2);
        spinTaperAngle->setProperty("unit", QVariant(QString::fromUtf8("deg")));
        spinTaperAngle->setMinimum(-89.999999000000003);
        spinTaperAngle->setMaximum(89.999999000000003);
        spinTaperAngle->setSingleStep(5.000000000000000);

        gridLayout_4->addWidget(spinTaperAngle, 0, 1, 1, 1);

        spinTaperAngleRev = new Gui::QuantitySpinBox(PartGui__DlgExtrusion);
        spinTaperAngleRev->setObjectName(QString::fromUtf8("spinTaperAngleRev"));
        sizePolicy2.setHeightForWidth(spinTaperAngleRev->sizePolicy().hasHeightForWidth());
        spinTaperAngleRev->setSizePolicy(sizePolicy2);
        spinTaperAngleRev->setProperty("unit", QVariant(QString::fromUtf8("deg")));
        spinTaperAngleRev->setMinimum(-89.999999000000003);
        spinTaperAngleRev->setMaximum(89.999999000000003);
        spinTaperAngleRev->setSingleStep(5.000000000000000);

        gridLayout_4->addWidget(spinTaperAngleRev, 1, 1, 1, 1);


        verticalLayout_2->addLayout(gridLayout_4);

        chkSolid = new QCheckBox(PartGui__DlgExtrusion);
        chkSolid->setObjectName(QString::fromUtf8("chkSolid"));

        verticalLayout_2->addWidget(chkSolid);

        line = new QFrame(PartGui__DlgExtrusion);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line);

        treeWidget = new QTreeWidget(PartGui__DlgExtrusion);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(treeWidget->sizePolicy().hasHeightForWidth());
        treeWidget->setSizePolicy(sizePolicy3);
        treeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
        treeWidget->setIndentation(20);
        treeWidget->setRootIsDecorated(false);

        verticalLayout_2->addWidget(treeWidget);

        statusLabel = new QLabel(PartGui__DlgExtrusion);
        statusLabel->setObjectName(QString::fromUtf8("statusLabel"));
        statusLabel->setText(QString::fromUtf8("StatusLabel"));

        verticalLayout_2->addWidget(statusLabel);

        groupBox_2->raise();
        chkSolid->raise();
        treeWidget->raise();
        statusLabel->raise();
        groupBox->raise();
        line->raise();
        QWidget::setTabOrder(rbDirModeNormal, rbDirModeEdge);
        QWidget::setTabOrder(rbDirModeEdge, chkReversed);
        QWidget::setTabOrder(chkReversed, txtLink);
        QWidget::setTabOrder(txtLink, btnSelectEdge);
        QWidget::setTabOrder(btnSelectEdge, rbDirModeCustom);
        QWidget::setTabOrder(rbDirModeCustom, btnX);
        QWidget::setTabOrder(btnX, dirX);
        QWidget::setTabOrder(dirX, btnY);
        QWidget::setTabOrder(btnY, dirY);
        QWidget::setTabOrder(dirY, btnZ);
        QWidget::setTabOrder(btnZ, dirZ);
        QWidget::setTabOrder(dirZ, spinLenFwd);
        QWidget::setTabOrder(spinLenFwd, spinLenRev);
        QWidget::setTabOrder(spinLenRev, chkSymmetric);
        QWidget::setTabOrder(chkSymmetric, spinTaperAngle);
        QWidget::setTabOrder(spinTaperAngle, spinTaperAngleRev);
        QWidget::setTabOrder(spinTaperAngleRev, chkSolid);
        QWidget::setTabOrder(chkSolid, treeWidget);

        retranslateUi(PartGui__DlgExtrusion);

        QMetaObject::connectSlotsByName(PartGui__DlgExtrusion);
    } // setupUi

    void retranslateUi(QDialog *PartGui__DlgExtrusion)
    {
        PartGui__DlgExtrusion->setWindowTitle(QApplication::translate("PartGui::DlgExtrusion", "Extrude", nullptr));
        groupBox_2->setTitle(QApplication::translate("PartGui::DlgExtrusion", "Direction", nullptr));
#ifndef QT_NO_TOOLTIP
        chkReversed->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "If checked, direction of extrusion is reversed.", nullptr));
#endif // QT_NO_TOOLTIP
        chkReversed->setText(QApplication::translate("PartGui::DlgExtrusion", "Reversed", nullptr));
#ifndef QT_NO_TOOLTIP
        rbDirModeCustom->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "Specify direction manually using X,Y,Z values.", nullptr));
#endif // QT_NO_TOOLTIP
        rbDirModeCustom->setText(QApplication::translate("PartGui::DlgExtrusion", "Custom direction:", nullptr));
#ifndef QT_NO_TOOLTIP
        rbDirModeNormal->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "Extrude perpendicularly to plane of input shape.", nullptr));
#endif // QT_NO_TOOLTIP
        rbDirModeNormal->setText(QApplication::translate("PartGui::DlgExtrusion", "Along normal", nullptr));
#ifndef QT_NO_TOOLTIP
        btnSelectEdge->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "Click to start selecting an edge in 3d view.", nullptr));
#endif // QT_NO_TOOLTIP
        btnSelectEdge->setText(QApplication::translate("PartGui::DlgExtrusion", "Select", nullptr));
#ifndef QT_NO_TOOLTIP
        rbDirModeEdge->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "Set direction to match a direction of straight edge. Hint: to account for length of the edge too, set both lengths to zero.", nullptr));
#endif // QT_NO_TOOLTIP
        rbDirModeEdge->setText(QApplication::translate("PartGui::DlgExtrusion", "Along edge:", nullptr));
#ifndef QT_NO_TOOLTIP
        btnX->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        btnX->setText(QApplication::translate("PartGui::DlgExtrusion", "X:", nullptr));
        btnY->setText(QApplication::translate("PartGui::DlgExtrusion", "Y:", nullptr));
        btnZ->setText(QApplication::translate("PartGui::DlgExtrusion", "Z:", nullptr));
        groupBox->setTitle(QApplication::translate("PartGui::DlgExtrusion", "Length", nullptr));
        label_5->setText(QApplication::translate("PartGui::DlgExtrusion", "Along:", nullptr));
#ifndef QT_NO_TOOLTIP
        spinLenFwd->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "Length to extrude along direction (can be negative). If both lengths are zero, magnitude of direction is used.", nullptr));
#endif // QT_NO_TOOLTIP
        label_6->setText(QApplication::translate("PartGui::DlgExtrusion", "Against:", nullptr));
#ifndef QT_NO_TOOLTIP
        spinLenRev->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "Length to extrude against direction (can be negative).", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        chkSymmetric->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "Distribute extrusion length equally to both sides.", nullptr));
#endif // QT_NO_TOOLTIP
        chkSymmetric->setText(QApplication::translate("PartGui::DlgExtrusion", "Symmetric", nullptr));
        label->setText(QApplication::translate("PartGui::DlgExtrusion", "Taper outward angle", nullptr));
#ifndef QT_NO_TOOLTIP
        spinTaperAngle->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "Apply slope (draft) to extrusion side faces.", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        spinTaperAngleRev->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "Apply slope (draft) to extrusion side faces.", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        chkSolid->setToolTip(QApplication::translate("PartGui::DlgExtrusion", "If checked, extruding closed wires will give solids, not shells.", nullptr));
#endif // QT_NO_TOOLTIP
        chkSolid->setText(QApplication::translate("PartGui::DlgExtrusion", "Create solid", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("PartGui::DlgExtrusion", "Shape", nullptr));
    } // retranslateUi

};

} // namespace PartGui

namespace PartGui {
namespace Ui {
    class DlgExtrusion: public Ui_DlgExtrusion {};
} // namespace Ui
} // namespace PartGui

#endif // UI_DLGEXTRUSION_H
