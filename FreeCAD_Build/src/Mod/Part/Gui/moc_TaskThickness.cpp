/****************************************************************************
** Meta object code from reading C++ file 'TaskThickness.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Part/Gui/TaskThickness.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskThickness.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PartGui__ThicknessWidget_t {
    QByteArrayData data[9];
    char stringdata0[194];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__ThicknessWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__ThicknessWidget_t qt_meta_stringdata_PartGui__ThicknessWidget = {
    {
QT_MOC_LITERAL(0, 0, 24), // "PartGui::ThicknessWidget"
QT_MOC_LITERAL(1, 25, 26), // "on_spinOffset_valueChanged"
QT_MOC_LITERAL(2, 52, 0), // ""
QT_MOC_LITERAL(3, 53, 21), // "on_modeType_activated"
QT_MOC_LITERAL(4, 75, 21), // "on_joinType_activated"
QT_MOC_LITERAL(5, 97, 23), // "on_intersection_toggled"
QT_MOC_LITERAL(6, 121, 27), // "on_selfIntersection_toggled"
QT_MOC_LITERAL(7, 149, 22), // "on_facesButton_clicked"
QT_MOC_LITERAL(8, 172, 21) // "on_updateView_toggled"

    },
    "PartGui::ThicknessWidget\0"
    "on_spinOffset_valueChanged\0\0"
    "on_modeType_activated\0on_joinType_activated\0"
    "on_intersection_toggled\0"
    "on_selfIntersection_toggled\0"
    "on_facesButton_clicked\0on_updateView_toggled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__ThicknessWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x08 /* Private */,
       3,    1,   52,    2, 0x08 /* Private */,
       4,    1,   55,    2, 0x08 /* Private */,
       5,    1,   58,    2, 0x08 /* Private */,
       6,    1,   61,    2, 0x08 /* Private */,
       7,    0,   64,    2, 0x08 /* Private */,
       8,    1,   65,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void PartGui::ThicknessWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ThicknessWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_spinOffset_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->on_modeType_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_joinType_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_intersection_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_selfIntersection_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_facesButton_clicked(); break;
        case 6: _t->on_updateView_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartGui::ThicknessWidget::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_PartGui__ThicknessWidget.data,
    qt_meta_data_PartGui__ThicknessWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::ThicknessWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::ThicknessWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__ThicknessWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int PartGui::ThicknessWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
struct qt_meta_stringdata_PartGui__TaskThickness_t {
    QByteArrayData data[1];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__TaskThickness_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__TaskThickness_t qt_meta_stringdata_PartGui__TaskThickness = {
    {
QT_MOC_LITERAL(0, 0, 22) // "PartGui::TaskThickness"

    },
    "PartGui::TaskThickness"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__TaskThickness[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PartGui::TaskThickness::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PartGui::TaskThickness::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_PartGui__TaskThickness.data,
    qt_meta_data_PartGui__TaskThickness,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::TaskThickness::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::TaskThickness::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__TaskThickness.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int PartGui::TaskThickness::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
