/****************************************************************************
** Meta object code from reading C++ file 'TaskCheckGeometry.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Part/Gui/TaskCheckGeometry.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskCheckGeometry.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PartGui__ResultModel_t {
    QByteArrayData data[1];
    char stringdata0[21];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__ResultModel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__ResultModel_t qt_meta_stringdata_PartGui__ResultModel = {
    {
QT_MOC_LITERAL(0, 0, 20) // "PartGui::ResultModel"

    },
    "PartGui::ResultModel"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__ResultModel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PartGui::ResultModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PartGui::ResultModel::staticMetaObject = { {
    &QAbstractItemModel::staticMetaObject,
    qt_meta_stringdata_PartGui__ResultModel.data,
    qt_meta_data_PartGui__ResultModel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::ResultModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::ResultModel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__ResultModel.stringdata0))
        return static_cast<void*>(this);
    return QAbstractItemModel::qt_metacast(_clname);
}

int PartGui::ResultModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractItemModel::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_PartGui__TaskCheckGeometryResults_t {
    QByteArrayData data[6];
    char stringdata0[82];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__TaskCheckGeometryResults_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__TaskCheckGeometryResults_t qt_meta_stringdata_PartGui__TaskCheckGeometryResults = {
    {
QT_MOC_LITERAL(0, 0, 33), // "PartGui::TaskCheckGeometryRes..."
QT_MOC_LITERAL(1, 34, 17), // "currentRowChanged"
QT_MOC_LITERAL(2, 52, 0), // ""
QT_MOC_LITERAL(3, 53, 11), // "QModelIndex"
QT_MOC_LITERAL(4, 65, 7), // "current"
QT_MOC_LITERAL(5, 73, 8) // "previous"

    },
    "PartGui::TaskCheckGeometryResults\0"
    "currentRowChanged\0\0QModelIndex\0current\0"
    "previous"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__TaskCheckGeometryResults[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    2,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,

       0        // eod
};

void PartGui::TaskCheckGeometryResults::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskCheckGeometryResults *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->currentRowChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartGui::TaskCheckGeometryResults::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_PartGui__TaskCheckGeometryResults.data,
    qt_meta_data_PartGui__TaskCheckGeometryResults,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::TaskCheckGeometryResults::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::TaskCheckGeometryResults::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__TaskCheckGeometryResults.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int PartGui::TaskCheckGeometryResults::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_PartGui__TaskCheckGeometryDialog_t {
    QByteArrayData data[20];
    char stringdata0[536];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__TaskCheckGeometryDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__TaskCheckGeometryDialog_t qt_meta_stringdata_PartGui__TaskCheckGeometryDialog = {
    {
QT_MOC_LITERAL(0, 0, 32), // "PartGui::TaskCheckGeometryDialog"
QT_MOC_LITERAL(1, 33, 25), // "on_runBOPCheckBox_toggled"
QT_MOC_LITERAL(2, 59, 0), // ""
QT_MOC_LITERAL(3, 60, 4), // "isOn"
QT_MOC_LITERAL(4, 65, 36), // "on_runSingleThreadedCheckBox_..."
QT_MOC_LITERAL(5, 102, 28), // "on_logErrorsCheckBox_toggled"
QT_MOC_LITERAL(6, 131, 37), // "on_expandShapeContentCheckBox..."
QT_MOC_LITERAL(7, 169, 26), // "on_autoRunCheckBox_toggled"
QT_MOC_LITERAL(8, 196, 35), // "on_argumentTypeModeCheckBox_t..."
QT_MOC_LITERAL(9, 232, 32), // "on_selfInterModeCheckBox_toggled"
QT_MOC_LITERAL(10, 265, 32), // "on_smallEdgeModeCheckBox_toggled"
QT_MOC_LITERAL(11, 298, 34), // "on_rebuildFaceModeCheckBox_to..."
QT_MOC_LITERAL(12, 333, 33), // "on_continuityModeCheckBox_tog..."
QT_MOC_LITERAL(13, 367, 30), // "on_tangentModeCheckBox_toggled"
QT_MOC_LITERAL(14, 398, 34), // "on_mergeVertexModeCheckBox_to..."
QT_MOC_LITERAL(15, 433, 32), // "on_mergeEdgeModeCheckBox_toggled"
QT_MOC_LITERAL(16, 466, 37), // "on_curveOnSurfaceModeCheckBox..."
QT_MOC_LITERAL(17, 504, 10), // "on_clicked"
QT_MOC_LITERAL(18, 515, 16), // "QAbstractButton*"
QT_MOC_LITERAL(19, 532, 3) // "btn"

    },
    "PartGui::TaskCheckGeometryDialog\0"
    "on_runBOPCheckBox_toggled\0\0isOn\0"
    "on_runSingleThreadedCheckBox_toggled\0"
    "on_logErrorsCheckBox_toggled\0"
    "on_expandShapeContentCheckBox_toggled\0"
    "on_autoRunCheckBox_toggled\0"
    "on_argumentTypeModeCheckBox_toggled\0"
    "on_selfInterModeCheckBox_toggled\0"
    "on_smallEdgeModeCheckBox_toggled\0"
    "on_rebuildFaceModeCheckBox_toggled\0"
    "on_continuityModeCheckBox_toggled\0"
    "on_tangentModeCheckBox_toggled\0"
    "on_mergeVertexModeCheckBox_toggled\0"
    "on_mergeEdgeModeCheckBox_toggled\0"
    "on_curveOnSurfaceModeCheckBox_toggled\0"
    "on_clicked\0QAbstractButton*\0btn"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__TaskCheckGeometryDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   89,    2, 0x08 /* Private */,
       4,    1,   92,    2, 0x08 /* Private */,
       5,    1,   95,    2, 0x08 /* Private */,
       6,    1,   98,    2, 0x08 /* Private */,
       7,    1,  101,    2, 0x08 /* Private */,
       8,    1,  104,    2, 0x08 /* Private */,
       9,    1,  107,    2, 0x08 /* Private */,
      10,    1,  110,    2, 0x08 /* Private */,
      11,    1,  113,    2, 0x08 /* Private */,
      12,    1,  116,    2, 0x08 /* Private */,
      13,    1,  119,    2, 0x08 /* Private */,
      14,    1,  122,    2, 0x08 /* Private */,
      15,    1,  125,    2, 0x08 /* Private */,
      16,    1,  128,    2, 0x08 /* Private */,
      17,    1,  131,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, 0x80000000 | 18,   19,

       0        // eod
};

void PartGui::TaskCheckGeometryDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskCheckGeometryDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_runBOPCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->on_runSingleThreadedCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->on_logErrorsCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_expandShapeContentCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_autoRunCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_argumentTypeModeCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->on_selfInterModeCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_smallEdgeModeCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_rebuildFaceModeCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_continuityModeCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->on_tangentModeCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->on_mergeVertexModeCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->on_mergeEdgeModeCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: _t->on_curveOnSurfaceModeCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->on_clicked((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartGui::TaskCheckGeometryDialog::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_PartGui__TaskCheckGeometryDialog.data,
    qt_meta_data_PartGui__TaskCheckGeometryDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::TaskCheckGeometryDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::TaskCheckGeometryDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__TaskCheckGeometryDialog.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int PartGui::TaskCheckGeometryDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
