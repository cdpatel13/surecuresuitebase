/****************************************************************************
** Meta object code from reading C++ file 'TaskDimension.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Part/Gui/TaskDimension.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskDimension.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PartGui__SteppedSelection_t {
    QByteArrayData data[5];
    char stringdata0[62];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__SteppedSelection_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__SteppedSelection_t qt_meta_stringdata_PartGui__SteppedSelection = {
    {
QT_MOC_LITERAL(0, 0, 25), // "PartGui::SteppedSelection"
QT_MOC_LITERAL(1, 26, 13), // "selectionSlot"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 7), // "checked"
QT_MOC_LITERAL(4, 49, 12) // "buildPixmaps"

    },
    "PartGui::SteppedSelection\0selectionSlot\0"
    "\0checked\0buildPixmaps"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__SteppedSelection[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x08 /* Private */,
       4,    0,   27,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void,

       0        // eod
};

void PartGui::SteppedSelection::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SteppedSelection *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->selectionSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->buildPixmaps(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartGui::SteppedSelection::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_PartGui__SteppedSelection.data,
    qt_meta_data_PartGui__SteppedSelection,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::SteppedSelection::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::SteppedSelection::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__SteppedSelection.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int PartGui::SteppedSelection::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_PartGui__DimensionControl_t {
    QByteArrayData data[5];
    char stringdata0[69];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__DimensionControl_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__DimensionControl_t qt_meta_stringdata_PartGui__DimensionControl = {
    {
QT_MOC_LITERAL(0, 0, 25), // "PartGui::DimensionControl"
QT_MOC_LITERAL(1, 26, 12), // "toggle3dSlot"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 15), // "toggleDeltaSlot"
QT_MOC_LITERAL(4, 56, 12) // "clearAllSlot"

    },
    "PartGui::DimensionControl\0toggle3dSlot\0"
    "\0toggleDeltaSlot\0clearAllSlot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__DimensionControl[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x0a /* Public */,
       3,    1,   32,    2, 0x0a /* Public */,
       4,    1,   35,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void PartGui::DimensionControl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DimensionControl *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->toggle3dSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->toggleDeltaSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->clearAllSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartGui::DimensionControl::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_PartGui__DimensionControl.data,
    qt_meta_data_PartGui__DimensionControl,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::DimensionControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::DimensionControl::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__DimensionControl.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int PartGui::DimensionControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
struct qt_meta_stringdata_PartGui__TaskMeasureLinear_t {
    QByteArrayData data[10];
    char stringdata0[150];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__TaskMeasureLinear_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__TaskMeasureLinear_t qt_meta_stringdata_PartGui__TaskMeasureLinear = {
    {
QT_MOC_LITERAL(0, 0, 26), // "PartGui::TaskMeasureLinear"
QT_MOC_LITERAL(1, 27, 14), // "selection1Slot"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 7), // "checked"
QT_MOC_LITERAL(4, 51, 14), // "selection2Slot"
QT_MOC_LITERAL(5, 66, 15), // "resetDialogSlot"
QT_MOC_LITERAL(6, 82, 12), // "toggle3dSlot"
QT_MOC_LITERAL(7, 95, 15), // "toggleDeltaSlot"
QT_MOC_LITERAL(8, 111, 12), // "clearAllSlot"
QT_MOC_LITERAL(9, 124, 25) // "selectionClearDelayedSlot"

    },
    "PartGui::TaskMeasureLinear\0selection1Slot\0"
    "\0checked\0selection2Slot\0resetDialogSlot\0"
    "toggle3dSlot\0toggleDeltaSlot\0clearAllSlot\0"
    "selectionClearDelayedSlot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__TaskMeasureLinear[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x09 /* Protected */,
       4,    1,   52,    2, 0x09 /* Protected */,
       5,    1,   55,    2, 0x09 /* Protected */,
       6,    1,   58,    2, 0x09 /* Protected */,
       7,    1,   61,    2, 0x09 /* Protected */,
       8,    1,   64,    2, 0x09 /* Protected */,
       9,    0,   67,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,

       0        // eod
};

void PartGui::TaskMeasureLinear::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskMeasureLinear *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->selection1Slot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->selection2Slot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->resetDialogSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->toggle3dSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->toggleDeltaSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->clearAllSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->selectionClearDelayedSlot(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartGui::TaskMeasureLinear::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_PartGui__TaskMeasureLinear.data,
    qt_meta_data_PartGui__TaskMeasureLinear,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::TaskMeasureLinear::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::TaskMeasureLinear::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__TaskMeasureLinear.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionObserver"))
        return static_cast< Gui::SelectionObserver*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int PartGui::TaskMeasureLinear::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
struct qt_meta_stringdata_PartGui__TaskMeasureAngular_t {
    QByteArrayData data[10];
    char stringdata0[151];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__TaskMeasureAngular_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__TaskMeasureAngular_t qt_meta_stringdata_PartGui__TaskMeasureAngular = {
    {
QT_MOC_LITERAL(0, 0, 27), // "PartGui::TaskMeasureAngular"
QT_MOC_LITERAL(1, 28, 14), // "selection1Slot"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 7), // "checked"
QT_MOC_LITERAL(4, 52, 14), // "selection2Slot"
QT_MOC_LITERAL(5, 67, 15), // "resetDialogSlot"
QT_MOC_LITERAL(6, 83, 12), // "toggle3dSlot"
QT_MOC_LITERAL(7, 96, 15), // "toggleDeltaSlot"
QT_MOC_LITERAL(8, 112, 12), // "clearAllSlot"
QT_MOC_LITERAL(9, 125, 25) // "selectionClearDelayedSlot"

    },
    "PartGui::TaskMeasureAngular\0selection1Slot\0"
    "\0checked\0selection2Slot\0resetDialogSlot\0"
    "toggle3dSlot\0toggleDeltaSlot\0clearAllSlot\0"
    "selectionClearDelayedSlot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__TaskMeasureAngular[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x09 /* Protected */,
       4,    1,   52,    2, 0x09 /* Protected */,
       5,    1,   55,    2, 0x09 /* Protected */,
       6,    1,   58,    2, 0x09 /* Protected */,
       7,    1,   61,    2, 0x09 /* Protected */,
       8,    1,   64,    2, 0x09 /* Protected */,
       9,    0,   67,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,

       0        // eod
};

void PartGui::TaskMeasureAngular::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskMeasureAngular *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->selection1Slot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->selection2Slot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->resetDialogSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->toggle3dSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->toggleDeltaSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->clearAllSlot((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->selectionClearDelayedSlot(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartGui::TaskMeasureAngular::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_PartGui__TaskMeasureAngular.data,
    qt_meta_data_PartGui__TaskMeasureAngular,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::TaskMeasureAngular::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::TaskMeasureAngular::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__TaskMeasureAngular.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionObserver"))
        return static_cast< Gui::SelectionObserver*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int PartGui::TaskMeasureAngular::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
