/****************************************************************************
** Meta object code from reading C++ file 'DlgExtrusion.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Part/Gui/DlgExtrusion.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DlgExtrusion.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PartGui__DlgExtrusion_t {
    QByteArrayData data[12];
    char stringdata0[225];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__DlgExtrusion_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__DlgExtrusion_t qt_meta_stringdata_PartGui__DlgExtrusion = {
    {
QT_MOC_LITERAL(0, 0, 21), // "PartGui::DlgExtrusion"
QT_MOC_LITERAL(1, 22, 26), // "on_rbDirModeCustom_toggled"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 2), // "on"
QT_MOC_LITERAL(4, 53, 24), // "on_rbDirModeEdge_toggled"
QT_MOC_LITERAL(5, 78, 26), // "on_rbDirModeNormal_toggled"
QT_MOC_LITERAL(6, 105, 24), // "on_btnSelectEdge_clicked"
QT_MOC_LITERAL(7, 130, 15), // "on_btnX_clicked"
QT_MOC_LITERAL(8, 146, 15), // "on_btnY_clicked"
QT_MOC_LITERAL(9, 162, 15), // "on_btnZ_clicked"
QT_MOC_LITERAL(10, 178, 23), // "on_chkSymmetric_toggled"
QT_MOC_LITERAL(11, 202, 22) // "on_txtLink_textChanged"

    },
    "PartGui::DlgExtrusion\0on_rbDirModeCustom_toggled\0"
    "\0on\0on_rbDirModeEdge_toggled\0"
    "on_rbDirModeNormal_toggled\0"
    "on_btnSelectEdge_clicked\0on_btnX_clicked\0"
    "on_btnY_clicked\0on_btnZ_clicked\0"
    "on_chkSymmetric_toggled\0on_txtLink_textChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__DlgExtrusion[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x08 /* Private */,
       4,    1,   62,    2, 0x08 /* Private */,
       5,    1,   65,    2, 0x08 /* Private */,
       6,    0,   68,    2, 0x08 /* Private */,
       7,    0,   69,    2, 0x08 /* Private */,
       8,    0,   70,    2, 0x08 /* Private */,
       9,    0,   71,    2, 0x08 /* Private */,
      10,    1,   72,    2, 0x08 /* Private */,
      11,    1,   75,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::QString,    2,

       0        // eod
};

void PartGui::DlgExtrusion::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DlgExtrusion *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_rbDirModeCustom_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->on_rbDirModeEdge_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->on_rbDirModeNormal_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_btnSelectEdge_clicked(); break;
        case 4: _t->on_btnX_clicked(); break;
        case 5: _t->on_btnY_clicked(); break;
        case 6: _t->on_btnZ_clicked(); break;
        case 7: _t->on_chkSymmetric_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_txtLink_textChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PartGui::DlgExtrusion::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_PartGui__DlgExtrusion.data,
    qt_meta_data_PartGui__DlgExtrusion,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::DlgExtrusion::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::DlgExtrusion::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__DlgExtrusion.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Gui::SelectionObserver"))
        return static_cast< Gui::SelectionObserver*>(this);
    return QDialog::qt_metacast(_clname);
}

int PartGui::DlgExtrusion::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
struct qt_meta_stringdata_PartGui__TaskExtrusion_t {
    QByteArrayData data[1];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PartGui__TaskExtrusion_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PartGui__TaskExtrusion_t qt_meta_stringdata_PartGui__TaskExtrusion = {
    {
QT_MOC_LITERAL(0, 0, 22) // "PartGui::TaskExtrusion"

    },
    "PartGui::TaskExtrusion"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PartGui__TaskExtrusion[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void PartGui::TaskExtrusion::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject PartGui::TaskExtrusion::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_PartGui__TaskExtrusion.data,
    qt_meta_data_PartGui__TaskExtrusion,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PartGui::TaskExtrusion::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PartGui::TaskExtrusion::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PartGui__TaskExtrusion.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int PartGui::TaskExtrusion::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
