# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Part" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/JoinFeatures.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/MakeBottle.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/TestPartApp.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/InitGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/TestPartGui.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Part/AttachmentEditor" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/AttachmentEditor/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/AttachmentEditor/Commands.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/AttachmentEditor/FrozenClass.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/AttachmentEditor/TaskAttachmentEditor.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/AttachmentEditor/TaskAttachmentEditor.ui"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Part/BasicShapes" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BasicShapes/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BasicShapes/Shapes.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Part/BOPTools" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BOPTools/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BOPTools/GeneralFuseResult.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BOPTools/JoinAPI.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BOPTools/JoinFeatures.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BOPTools/ShapeMerge.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BOPTools/SplitAPI.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BOPTools/SplitFeatures.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/BOPTools/Utils.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Part/CompoundTools" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/CompoundTools/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/CompoundTools/_CommandCompoundFilter.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/CompoundTools/_CommandExplodeCompound.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/CompoundTools/CompoundFilter.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Part/CompoundTools/Explode.py"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Part/App/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Part/Gui/cmake_install.cmake")

endif()

