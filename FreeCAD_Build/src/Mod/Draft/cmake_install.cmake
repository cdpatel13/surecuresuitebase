# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Draft" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/InitGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/Draft.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftTools.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftSnap.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftTrackers.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftVecUtils.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftGeomUtils.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftLayer.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftEdit.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftFillet.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/DraftSelectPlane.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/WorkingPlane.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/getSVG.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/importDXF.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/importOCA.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/importSVG.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/importDWG.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/importAirfoilDAT.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/TestDraft.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Draft/Draft_rc.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/Draft/Resources/icons" TYPE FILE FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Draft/Resources/icons/DraftWorkbench.svg")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Draft/App/cmake_install.cmake")

endif()

