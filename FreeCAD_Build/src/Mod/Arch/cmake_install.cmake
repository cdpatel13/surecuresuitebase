# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Arch" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/InitGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchComponent.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchIFC.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchIFCView.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchIFCSchema.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchProject.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchWall.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/importIFC.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/importIFClegacy.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/importIFCHelper.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/exportIFCHelper.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/Arch.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchBuilding.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchFloor.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchSite.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchStructure.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchCommands.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchSectionPlane.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/importDAE.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/importOBJ.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchWindow.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchAxis.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchVRM.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchRoof.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchStairs.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/importWebGL.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/importJSON.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchSpace.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchRebar.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/TestArch.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchFrame.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchPanel.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchEquipment.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchCutPlane.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchMaterial.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchSchedule.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchProfile.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/import3DS.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchPrecast.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/importSH3D.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchPipe.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchNesting.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchBuildingPart.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchReference.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/ArchFence.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/OfflineRenderingUtils.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/exportIFC.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Arch/Arch_rc.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Arch/Dice3DS" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/Dice3DS/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/Dice3DS/util.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/Dice3DS/dom3ds.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/Arch" TYPE DIRECTORY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/Presets")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/Arch/Resources/icons" TYPE FILE FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Arch/Resources/icons/ArchWorkbench.svg")
endif()

