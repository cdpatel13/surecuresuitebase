/****************************************************************************
** Meta object code from reading C++ file 'CrossSections.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/MeshPart/Gui/CrossSections.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CrossSections.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MeshPartGui__CrossSections_t {
    QByteArrayData data[10];
    char stringdata0[214];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MeshPartGui__CrossSections_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MeshPartGui__CrossSections_t qt_meta_stringdata_MeshPartGui__CrossSections = {
    {
QT_MOC_LITERAL(0, 0, 26), // "MeshPartGui::CrossSections"
QT_MOC_LITERAL(1, 27, 18), // "on_xyPlane_clicked"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 18), // "on_xzPlane_clicked"
QT_MOC_LITERAL(4, 66, 18), // "on_yzPlane_clicked"
QT_MOC_LITERAL(5, 85, 24), // "on_position_valueChanged"
QT_MOC_LITERAL(6, 110, 24), // "on_distance_valueChanged"
QT_MOC_LITERAL(7, 135, 29), // "on_countSections_valueChanged"
QT_MOC_LITERAL(8, 165, 25), // "on_checkBothSides_toggled"
QT_MOC_LITERAL(9, 191, 22) // "on_sectionsBox_toggled"

    },
    "MeshPartGui::CrossSections\0"
    "on_xyPlane_clicked\0\0on_xzPlane_clicked\0"
    "on_yzPlane_clicked\0on_position_valueChanged\0"
    "on_distance_valueChanged\0"
    "on_countSections_valueChanged\0"
    "on_checkBothSides_toggled\0"
    "on_sectionsBox_toggled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MeshPartGui__CrossSections[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    1,   57,    2, 0x08 /* Private */,
       6,    1,   60,    2, 0x08 /* Private */,
       7,    1,   63,    2, 0x08 /* Private */,
       8,    1,   66,    2, 0x08 /* Private */,
       9,    1,   69,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Double,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void MeshPartGui::CrossSections::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CrossSections *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_xyPlane_clicked(); break;
        case 1: _t->on_xzPlane_clicked(); break;
        case 2: _t->on_yzPlane_clicked(); break;
        case 3: _t->on_position_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->on_distance_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->on_countSections_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_checkBothSides_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_sectionsBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MeshPartGui::CrossSections::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_MeshPartGui__CrossSections.data,
    qt_meta_data_MeshPartGui__CrossSections,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MeshPartGui::CrossSections::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MeshPartGui::CrossSections::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MeshPartGui__CrossSections.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int MeshPartGui::CrossSections::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
struct qt_meta_stringdata_MeshPartGui__TaskCrossSections_t {
    QByteArrayData data[1];
    char stringdata0[31];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MeshPartGui__TaskCrossSections_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MeshPartGui__TaskCrossSections_t qt_meta_stringdata_MeshPartGui__TaskCrossSections = {
    {
QT_MOC_LITERAL(0, 0, 30) // "MeshPartGui::TaskCrossSections"

    },
    "MeshPartGui::TaskCrossSections"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MeshPartGui__TaskCrossSections[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void MeshPartGui::TaskCrossSections::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject MeshPartGui::TaskCrossSections::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_MeshPartGui__TaskCrossSections.data,
    qt_meta_data_MeshPartGui__TaskCrossSections,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MeshPartGui::TaskCrossSections::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MeshPartGui::TaskCrossSections::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MeshPartGui__TaskCrossSections.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int MeshPartGui::TaskCrossSections::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
