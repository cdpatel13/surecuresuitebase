/********************************************************************************
** Form generated from reading UI file 'Sheet.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHEET_H
#define UI_SHEET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "SheetTableView.h"
#include "SpreadsheetView.h"

QT_BEGIN_NAMESPACE

class Ui_Sheet
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    SpreadsheetGui::LineEdit *cellContent;
    SpreadsheetGui::SheetTableView *cells;

    void setupUi(QWidget *Sheet)
    {
        if (Sheet->objectName().isEmpty())
            Sheet->setObjectName(QString::fromUtf8("Sheet"));
        Sheet->resize(727, 596);
        verticalLayout = new QVBoxLayout(Sheet);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(Sheet);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        cellContent = new SpreadsheetGui::LineEdit(Sheet);
        cellContent->setObjectName(QString::fromUtf8("cellContent"));
        cellContent->setEnabled(false);

        horizontalLayout->addWidget(cellContent);


        verticalLayout->addLayout(horizontalLayout);

        cells = new SpreadsheetGui::SheetTableView(Sheet);
        cells->setObjectName(QString::fromUtf8("cells"));

        verticalLayout->addWidget(cells);

#ifndef QT_NO_SHORTCUT
        label->setBuddy(cellContent);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(cells, cellContent);

        retranslateUi(Sheet);

        QMetaObject::connectSlotsByName(Sheet);
    } // setupUi

    void retranslateUi(QWidget *Sheet)
    {
        Sheet->setWindowTitle(QApplication::translate("Sheet", "Form", nullptr));
        label->setText(QApplication::translate("Sheet", "&Contents", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Sheet: public Ui_Sheet {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHEET_H
