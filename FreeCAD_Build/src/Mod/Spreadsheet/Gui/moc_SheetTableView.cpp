/****************************************************************************
** Meta object code from reading C++ file 'SheetTableView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Spreadsheet/Gui/SheetTableView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SheetTableView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SpreadsheetGui__SheetViewHeader_t {
    QByteArrayData data[3];
    char stringdata0[48];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SpreadsheetGui__SheetViewHeader_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SpreadsheetGui__SheetViewHeader_t qt_meta_stringdata_SpreadsheetGui__SheetViewHeader = {
    {
QT_MOC_LITERAL(0, 0, 31), // "SpreadsheetGui::SheetViewHeader"
QT_MOC_LITERAL(1, 32, 14), // "resizeFinished"
QT_MOC_LITERAL(2, 47, 0) // ""

    },
    "SpreadsheetGui::SheetViewHeader\0"
    "resizeFinished\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SpreadsheetGui__SheetViewHeader[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

       0        // eod
};

void SpreadsheetGui::SheetViewHeader::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SheetViewHeader *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resizeFinished(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (SheetViewHeader::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SheetViewHeader::resizeFinished)) {
                *result = 0;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SpreadsheetGui::SheetViewHeader::staticMetaObject = { {
    &QHeaderView::staticMetaObject,
    qt_meta_stringdata_SpreadsheetGui__SheetViewHeader.data,
    qt_meta_data_SpreadsheetGui__SheetViewHeader,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SpreadsheetGui::SheetViewHeader::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SpreadsheetGui::SheetViewHeader::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SpreadsheetGui__SheetViewHeader.stringdata0))
        return static_cast<void*>(this);
    return QHeaderView::qt_metacast(_clname);
}

int SpreadsheetGui::SheetViewHeader::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QHeaderView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void SpreadsheetGui::SheetViewHeader::resizeFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_SpreadsheetGui__SheetTableView_t {
    QByteArrayData data[13];
    char stringdata0[164];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SpreadsheetGui__SheetTableView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SpreadsheetGui__SheetTableView_t qt_meta_stringdata_SpreadsheetGui__SheetTableView = {
    {
QT_MOC_LITERAL(0, 0, 30), // "SpreadsheetGui::SheetTableView"
QT_MOC_LITERAL(1, 31, 10), // "commitData"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 8), // "QWidget*"
QT_MOC_LITERAL(4, 52, 6), // "editor"
QT_MOC_LITERAL(5, 59, 14), // "updateCellSpan"
QT_MOC_LITERAL(6, 74, 16), // "App::CellAddress"
QT_MOC_LITERAL(7, 91, 7), // "address"
QT_MOC_LITERAL(8, 99, 10), // "insertRows"
QT_MOC_LITERAL(9, 110, 10), // "removeRows"
QT_MOC_LITERAL(10, 121, 13), // "insertColumns"
QT_MOC_LITERAL(11, 135, 13), // "removeColumns"
QT_MOC_LITERAL(12, 149, 14) // "cellProperties"

    },
    "SpreadsheetGui::SheetTableView\0"
    "commitData\0\0QWidget*\0editor\0updateCellSpan\0"
    "App::CellAddress\0address\0insertRows\0"
    "removeRows\0insertColumns\0removeColumns\0"
    "cellProperties"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SpreadsheetGui__SheetTableView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x09 /* Protected */,
       5,    1,   52,    2, 0x09 /* Protected */,
       8,    0,   55,    2, 0x09 /* Protected */,
       9,    0,   56,    2, 0x09 /* Protected */,
      10,    0,   57,    2, 0x09 /* Protected */,
      11,    0,   58,    2, 0x09 /* Protected */,
      12,    0,   59,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SpreadsheetGui::SheetTableView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SheetTableView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->commitData((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 1: _t->updateCellSpan((*reinterpret_cast< App::CellAddress(*)>(_a[1]))); break;
        case 2: _t->insertRows(); break;
        case 3: _t->removeRows(); break;
        case 4: _t->insertColumns(); break;
        case 5: _t->removeColumns(); break;
        case 6: _t->cellProperties(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SpreadsheetGui::SheetTableView::staticMetaObject = { {
    &QTableView::staticMetaObject,
    qt_meta_stringdata_SpreadsheetGui__SheetTableView.data,
    qt_meta_data_SpreadsheetGui__SheetTableView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SpreadsheetGui::SheetTableView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SpreadsheetGui::SheetTableView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SpreadsheetGui__SheetTableView.stringdata0))
        return static_cast<void*>(this);
    return QTableView::qt_metacast(_clname);
}

int SpreadsheetGui::SheetTableView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTableView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
