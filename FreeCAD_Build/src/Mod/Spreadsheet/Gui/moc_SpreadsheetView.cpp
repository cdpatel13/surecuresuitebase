/****************************************************************************
** Meta object code from reading C++ file 'SpreadsheetView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/Spreadsheet/Gui/SpreadsheetView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SpreadsheetView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SpreadsheetGui__SheetView_t {
    QByteArrayData data[18];
    char stringdata0[208];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SpreadsheetGui__SheetView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SpreadsheetGui__SheetView_t qt_meta_stringdata_SpreadsheetGui__SheetView = {
    {
QT_MOC_LITERAL(0, 0, 25), // "SpreadsheetGui::SheetView"
QT_MOC_LITERAL(1, 26, 15), // "editingFinished"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 14), // "currentChanged"
QT_MOC_LITERAL(4, 58, 11), // "QModelIndex"
QT_MOC_LITERAL(5, 70, 7), // "current"
QT_MOC_LITERAL(6, 78, 8), // "previous"
QT_MOC_LITERAL(7, 87, 13), // "columnResized"
QT_MOC_LITERAL(8, 101, 3), // "col"
QT_MOC_LITERAL(9, 105, 7), // "oldSize"
QT_MOC_LITERAL(10, 113, 7), // "newSize"
QT_MOC_LITERAL(11, 121, 10), // "rowResized"
QT_MOC_LITERAL(12, 132, 3), // "row"
QT_MOC_LITERAL(13, 136, 20), // "columnResizeFinished"
QT_MOC_LITERAL(14, 157, 17), // "rowResizeFinished"
QT_MOC_LITERAL(15, 175, 12), // "modelUpdated"
QT_MOC_LITERAL(16, 188, 7), // "topLeft"
QT_MOC_LITERAL(17, 196, 11) // "bottomRight"

    },
    "SpreadsheetGui::SheetView\0editingFinished\0"
    "\0currentChanged\0QModelIndex\0current\0"
    "previous\0columnResized\0col\0oldSize\0"
    "newSize\0rowResized\0row\0columnResizeFinished\0"
    "rowResizeFinished\0modelUpdated\0topLeft\0"
    "bottomRight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SpreadsheetGui__SheetView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x09 /* Protected */,
       3,    2,   50,    2, 0x09 /* Protected */,
       7,    3,   55,    2, 0x09 /* Protected */,
      11,    3,   62,    2, 0x09 /* Protected */,
      13,    0,   69,    2, 0x09 /* Protected */,
      14,    0,   70,    2, 0x09 /* Protected */,
      15,    2,   71,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, 0x80000000 | 4,    5,    6,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,    8,    9,   10,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,   12,    9,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, 0x80000000 | 4,   16,   17,

       0        // eod
};

void SpreadsheetGui::SheetView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SheetView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->editingFinished(); break;
        case 1: _t->currentChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 2: _t->columnResized((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->rowResized((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->columnResizeFinished(); break;
        case 5: _t->rowResizeFinished(); break;
        case 6: _t->modelUpdated((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SpreadsheetGui::SheetView::staticMetaObject = { {
    &Gui::MDIView::staticMetaObject,
    qt_meta_stringdata_SpreadsheetGui__SheetView.data,
    qt_meta_data_SpreadsheetGui__SheetView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SpreadsheetGui::SheetView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SpreadsheetGui::SheetView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SpreadsheetGui__SheetView.stringdata0))
        return static_cast<void*>(this);
    return Gui::MDIView::qt_metacast(_clname);
}

int SpreadsheetGui::SheetView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::MDIView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
