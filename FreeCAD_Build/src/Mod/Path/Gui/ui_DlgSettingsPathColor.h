/********************************************************************************
** Form generated from reading UI file 'DlgSettingsPathColor.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGSETTINGSPATHCOLOR_H
#define UI_DLGSETTINGSPATHCOLOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"
#include "Gui/Widgets.h"

namespace PathGui {

class Ui_DlgSettingsPathColor
{
public:
    QFormLayout *formLayout;
    QGroupBox *groupBoxDefaultColors;
    QGridLayout *gridLayout;
    QLabel *label_10;
    Gui::PrefColorButton *DefaultBBoxNormalColor;
    Gui::PrefColorButton *DefaultNormalPathColor;
    Gui::PrefColorButton *DefaultPathMarkerColor;
    QLabel *label_7;
    Gui::PrefSpinBox *DefaultPathLineWidth;
    QLabel *label_9;
    QLabel *label_6;
    Gui::PrefColorButton *DefaultRapidPathColor;
    QLabel *label_8;
    Gui::PrefColorButton *DefaultProbePathColor;
    QLabel *label;
    Gui::PrefColorButton *DefaultExtentsColor;
    QLabel *label_11;
    Gui::PrefColorButton *DefaultHighlightPathColor;
    QLabel *label_13;
    QLabel *label_14;
    Gui::PrefColorButton *DefaultBBoxSelectionColor;
    QSpacerItem *horizontalSpacer;
    QGroupBox *groupBox_3;
    QFormLayout *formLayout_3;
    QLabel *label_12;
    Gui::PrefComboBox *DefaultSelectionStyle;
    QLabel *label_2;
    Gui::PrefComboBox *DefaultTaskPanelLayout;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *PathGui__DlgSettingsPathColor)
    {
        if (PathGui__DlgSettingsPathColor->objectName().isEmpty())
            PathGui__DlgSettingsPathColor->setObjectName(QString::fromUtf8("PathGui__DlgSettingsPathColor"));
        PathGui__DlgSettingsPathColor->resize(483, 536);
        formLayout = new QFormLayout(PathGui__DlgSettingsPathColor);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        groupBoxDefaultColors = new QGroupBox(PathGui__DlgSettingsPathColor);
        groupBoxDefaultColors->setObjectName(QString::fromUtf8("groupBoxDefaultColors"));
        gridLayout = new QGridLayout(groupBoxDefaultColors);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_10 = new QLabel(groupBoxDefaultColors);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(182, 0));

        gridLayout->addWidget(label_10, 2, 0, 1, 1);

        DefaultBBoxNormalColor = new Gui::PrefColorButton(groupBoxDefaultColors);
        DefaultBBoxNormalColor->setObjectName(QString::fromUtf8("DefaultBBoxNormalColor"));
        DefaultBBoxNormalColor->setProperty("color", QVariant(QColor(255, 255, 255)));
        DefaultBBoxNormalColor->setProperty("prefEntry", QVariant(QByteArray("DefaultBBoxNormalColor")));
        DefaultBBoxNormalColor->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        gridLayout->addWidget(DefaultBBoxNormalColor, 7, 1, 1, 1);

        DefaultNormalPathColor = new Gui::PrefColorButton(groupBoxDefaultColors);
        DefaultNormalPathColor->setObjectName(QString::fromUtf8("DefaultNormalPathColor"));
        DefaultNormalPathColor->setProperty("color", QVariant(QColor(0, 170, 0)));
        DefaultNormalPathColor->setProperty("prefEntry", QVariant(QByteArray("DefaultNormalPathColor")));
        DefaultNormalPathColor->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        gridLayout->addWidget(DefaultNormalPathColor, 0, 1, 1, 1);

        DefaultPathMarkerColor = new Gui::PrefColorButton(groupBoxDefaultColors);
        DefaultPathMarkerColor->setObjectName(QString::fromUtf8("DefaultPathMarkerColor"));
        DefaultPathMarkerColor->setProperty("color", QVariant(QColor(85, 255, 0)));
        DefaultPathMarkerColor->setProperty("prefEntry", QVariant(QByteArray("DefaultPathMarkerColor")));
        DefaultPathMarkerColor->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        gridLayout->addWidget(DefaultPathMarkerColor, 2, 1, 1, 1);

        label_7 = new QLabel(groupBoxDefaultColors);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(182, 0));

        gridLayout->addWidget(label_7, 3, 0, 1, 1);

        DefaultPathLineWidth = new Gui::PrefSpinBox(groupBoxDefaultColors);
        DefaultPathLineWidth->setObjectName(QString::fromUtf8("DefaultPathLineWidth"));
        DefaultPathLineWidth->setMaximum(9);
        DefaultPathLineWidth->setValue(1);
        DefaultPathLineWidth->setProperty("prefEntry", QVariant(QByteArray("DefaultPathLineWidth")));
        DefaultPathLineWidth->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        gridLayout->addWidget(DefaultPathLineWidth, 1, 1, 1, 1);

        label_9 = new QLabel(groupBoxDefaultColors);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(182, 0));

        gridLayout->addWidget(label_9, 1, 0, 1, 1);

        label_6 = new QLabel(groupBoxDefaultColors);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(182, 0));

        gridLayout->addWidget(label_6, 0, 0, 1, 1);

        DefaultRapidPathColor = new Gui::PrefColorButton(groupBoxDefaultColors);
        DefaultRapidPathColor->setObjectName(QString::fromUtf8("DefaultRapidPathColor"));
        DefaultRapidPathColor->setProperty("color", QVariant(QColor(170, 0, 0)));
        DefaultRapidPathColor->setProperty("prefEntry", QVariant(QByteArray("DefaultRapidPathColor")));
        DefaultRapidPathColor->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        gridLayout->addWidget(DefaultRapidPathColor, 3, 1, 1, 1);

        label_8 = new QLabel(groupBoxDefaultColors);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(182, 0));

        gridLayout->addWidget(label_8, 4, 0, 1, 1);

        DefaultProbePathColor = new Gui::PrefColorButton(groupBoxDefaultColors);
        DefaultProbePathColor->setObjectName(QString::fromUtf8("DefaultProbePathColor"));
        DefaultProbePathColor->setProperty("color", QVariant(QColor(255, 255, 5)));
        DefaultProbePathColor->setProperty("prefEntry", QVariant(QByteArray("DefaultProbePathColor")));
        DefaultProbePathColor->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        gridLayout->addWidget(DefaultProbePathColor, 4, 1, 1, 1);

        label = new QLabel(groupBoxDefaultColors);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 5, 0, 1, 1);

        DefaultExtentsColor = new Gui::PrefColorButton(groupBoxDefaultColors);
        DefaultExtentsColor->setObjectName(QString::fromUtf8("DefaultExtentsColor"));
        DefaultExtentsColor->setProperty("prefEntry", QVariant(QByteArray("DefaultExtentsColor")));
        DefaultExtentsColor->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        gridLayout->addWidget(DefaultExtentsColor, 5, 1, 1, 1);

        label_11 = new QLabel(groupBoxDefaultColors);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(182, 0));

        gridLayout->addWidget(label_11, 6, 0, 1, 1);

        DefaultHighlightPathColor = new Gui::PrefColorButton(groupBoxDefaultColors);
        DefaultHighlightPathColor->setObjectName(QString::fromUtf8("DefaultHighlightPathColor"));
        DefaultHighlightPathColor->setProperty("color", QVariant(QColor(255, 125, 0)));
        DefaultHighlightPathColor->setProperty("prefEntry", QVariant(QByteArray("DefaultHighlightPathColor")));
        DefaultHighlightPathColor->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        gridLayout->addWidget(DefaultHighlightPathColor, 6, 1, 1, 1);

        label_13 = new QLabel(groupBoxDefaultColors);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(182, 0));

        gridLayout->addWidget(label_13, 7, 0, 1, 1);

        label_14 = new QLabel(groupBoxDefaultColors);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(182, 0));

        gridLayout->addWidget(label_14, 8, 0, 1, 1);

        DefaultBBoxSelectionColor = new Gui::PrefColorButton(groupBoxDefaultColors);
        DefaultBBoxSelectionColor->setObjectName(QString::fromUtf8("DefaultBBoxSelectionColor"));
        DefaultBBoxSelectionColor->setProperty("color", QVariant(QColor(200, 255, 255)));
        DefaultBBoxSelectionColor->setProperty("prefEntry", QVariant(QByteArray("DefaultBBoxSelectionColor")));
        DefaultBBoxSelectionColor->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        gridLayout->addWidget(DefaultBBoxSelectionColor, 8, 1, 1, 1);

        label_6->raise();
        DefaultNormalPathColor->raise();
        label_9->raise();
        DefaultPathLineWidth->raise();
        label_10->raise();
        DefaultPathMarkerColor->raise();
        label_7->raise();
        DefaultRapidPathColor->raise();
        label->raise();
        DefaultExtentsColor->raise();
        label_8->raise();
        DefaultProbePathColor->raise();
        label_11->raise();
        DefaultHighlightPathColor->raise();
        label_13->raise();
        DefaultBBoxNormalColor->raise();
        label_14->raise();
        DefaultBBoxSelectionColor->raise();

        formLayout->setWidget(0, QFormLayout::LabelRole, groupBoxDefaultColors);

        horizontalSpacer = new QSpacerItem(7, 220, QSizePolicy::Expanding, QSizePolicy::Minimum);

        formLayout->setItem(0, QFormLayout::FieldRole, horizontalSpacer);

        groupBox_3 = new QGroupBox(PathGui__DlgSettingsPathColor);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy);
        formLayout_3 = new QFormLayout(groupBox_3);
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        formLayout_3->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_12);

        DefaultSelectionStyle = new Gui::PrefComboBox(groupBox_3);
        DefaultSelectionStyle->addItem(QString());
        DefaultSelectionStyle->addItem(QString());
        DefaultSelectionStyle->addItem(QString());
        DefaultSelectionStyle->setObjectName(QString::fromUtf8("DefaultSelectionStyle"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(DefaultSelectionStyle->sizePolicy().hasHeightForWidth());
        DefaultSelectionStyle->setSizePolicy(sizePolicy1);
        DefaultSelectionStyle->setProperty("prefEntry", QVariant(QByteArray("DefaultSelectionStyle")));
        DefaultSelectionStyle->setProperty("prefPath", QVariant(QByteArray("Mod/Path")));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, DefaultSelectionStyle);

        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, label_2);

        DefaultTaskPanelLayout = new Gui::PrefComboBox(groupBox_3);
        DefaultTaskPanelLayout->addItem(QString());
        DefaultTaskPanelLayout->addItem(QString());
        DefaultTaskPanelLayout->addItem(QString());
        DefaultTaskPanelLayout->addItem(QString());
        DefaultTaskPanelLayout->setObjectName(QString::fromUtf8("DefaultTaskPanelLayout"));

        formLayout_3->setWidget(1, QFormLayout::FieldRole, DefaultTaskPanelLayout);


        formLayout->setWidget(1, QFormLayout::LabelRole, groupBox_3);

        verticalSpacer = new QSpacerItem(20, 217, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(3, QFormLayout::SpanningRole, verticalSpacer);

        QWidget::setTabOrder(DefaultNormalPathColor, DefaultPathLineWidth);

        retranslateUi(PathGui__DlgSettingsPathColor);

        QMetaObject::connectSlotsByName(PathGui__DlgSettingsPathColor);
    } // setupUi

    void retranslateUi(QWidget *PathGui__DlgSettingsPathColor)
    {
        PathGui__DlgSettingsPathColor->setWindowTitle(QApplication::translate("PathGui::DlgSettingsPathColor", "Path colors", nullptr));
        groupBoxDefaultColors->setTitle(QApplication::translate("PathGui::DlgSettingsPathColor", "Default Path colors", nullptr));
        label_10->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Default path marker color", nullptr));
#ifndef QT_NO_TOOLTIP
        DefaultBBoxNormalColor->setToolTip(QApplication::translate("PathGui::DlgSettingsPathColor", "The default line color for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        DefaultNormalPathColor->setToolTip(QApplication::translate("PathGui::DlgSettingsPathColor", "The default color for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        DefaultPathMarkerColor->setToolTip(QApplication::translate("PathGui::DlgSettingsPathColor", "The default line color for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
        label_7->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Rapid path color", nullptr));
#ifndef QT_NO_TOOLTIP
        DefaultPathLineWidth->setToolTip(QApplication::translate("PathGui::DlgSettingsPathColor", "The default line thickness for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
        DefaultPathLineWidth->setSuffix(QApplication::translate("PathGui::DlgSettingsPathColor", "px", nullptr));
        label_9->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Default pathline width", nullptr));
        label_6->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Default normal path color", nullptr));
#ifndef QT_NO_TOOLTIP
        DefaultRapidPathColor->setToolTip(QApplication::translate("PathGui::DlgSettingsPathColor", "The default line color for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
        label_8->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Probe Path color", nullptr));
#ifndef QT_NO_TOOLTIP
        DefaultProbePathColor->setToolTip(QApplication::translate("PathGui::DlgSettingsPathColor", "The default line color for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Machine extents color", nullptr));
        label_11->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Path Highlight Color", nullptr));
#ifndef QT_NO_TOOLTIP
        DefaultHighlightPathColor->setToolTip(QApplication::translate("PathGui::DlgSettingsPathColor", "The default line color for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
        label_13->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Bounding Box Normal Color", nullptr));
        label_14->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Bounding Box Selection Color", nullptr));
#ifndef QT_NO_TOOLTIP
        DefaultBBoxSelectionColor->setToolTip(QApplication::translate("PathGui::DlgSettingsPathColor", "The default line color for new shapes", nullptr));
#endif // QT_NO_TOOLTIP
        groupBox_3->setTitle(QApplication::translate("PathGui::DlgSettingsPathColor", "UI Settings", nullptr));
        label_12->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Path Selection Style", nullptr));
        DefaultSelectionStyle->setItemText(0, QApplication::translate("PathGui::DlgSettingsPathColor", "Shape", nullptr));
        DefaultSelectionStyle->setItemText(1, QApplication::translate("PathGui::DlgSettingsPathColor", "Bounding Box", nullptr));
        DefaultSelectionStyle->setItemText(2, QApplication::translate("PathGui::DlgSettingsPathColor", "None", nullptr));

#ifndef QT_NO_TOOLTIP
        DefaultSelectionStyle->setToolTip(QApplication::translate("PathGui::DlgSettingsPathColor", "Default path shape selection behavior in 3D viewer", nullptr));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("PathGui::DlgSettingsPathColor", "Task Panel Layout", nullptr));
        DefaultTaskPanelLayout->setItemText(0, QApplication::translate("PathGui::DlgSettingsPathColor", "Classic", nullptr));
        DefaultTaskPanelLayout->setItemText(1, QApplication::translate("PathGui::DlgSettingsPathColor", "Classic - reversed", nullptr));
        DefaultTaskPanelLayout->setItemText(2, QApplication::translate("PathGui::DlgSettingsPathColor", "Multi Panel", nullptr));
        DefaultTaskPanelLayout->setItemText(3, QApplication::translate("PathGui::DlgSettingsPathColor", "Multi Panel - reversed", nullptr));

        DefaultTaskPanelLayout->setProperty("prefEntry", QVariant(QApplication::translate("PathGui::DlgSettingsPathColor", "DefaultTaskPanelLayout", nullptr)));
        DefaultTaskPanelLayout->setProperty("prefPath", QVariant(QApplication::translate("PathGui::DlgSettingsPathColor", "Mod/Path", nullptr)));
    } // retranslateUi

};

} // namespace PathGui

namespace PathGui {
namespace Ui {
    class DlgSettingsPathColor: public Ui_DlgSettingsPathColor {};
} // namespace Ui
} // namespace PathGui

#endif // UI_DLGSETTINGSPATHCOLOR_H
