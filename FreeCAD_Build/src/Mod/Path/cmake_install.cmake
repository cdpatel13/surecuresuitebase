# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Path" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathCommands.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/TestPathApp.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/InitGui.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Path/PathScripts" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathCommands.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathAreaOp.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathArray.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathCircularHoleBase.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathCircularHoleBaseGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathComment.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathCopy.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathCustom.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDeburr.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDeburrGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressup.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressupAxisMap.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressupDogbone.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressupDragknife.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressupHoldingTags.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressupLeadInOut.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressupRampEntry.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressupTag.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressupTagGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDressupTagPreferences.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDrilling.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathDrillingGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathEngrave.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathEngraveBase.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathEngraveGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathFixture.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathGeom.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathGetPoint.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathGuiInit.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathHelix.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathHelixGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathHop.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathIconViewProvider.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathInspect.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathJob.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathJobCmd.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathJobDlg.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathJobGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathLog.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathMillFace.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathMillFaceGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathOp.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathOpGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathOpTools.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPocket.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPocketBase.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPocketBaseGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPocketGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPocketShape.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPocketShapeGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPost.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPostProcessor.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPreferences.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPreferencesPathDressup.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathPreferencesPathJob.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathProfileBase.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathProfileBaseGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathProfileContour.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathProfileContourGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathProfileEdges.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathProfileEdgesGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathProfileFaces.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathProfileFacesGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSanity.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSelection.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSetupSheet.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSetupSheetGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSetupSheetOpPrototype.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSetupSheetOpPrototypeGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSimpleCopy.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathStock.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathStop.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSurface.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSurfaceGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathToolController.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathToolControllerGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathToolEdit.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathToolLibraryManager.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathToolLibraryEditor.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathUtil.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathUtils.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathUtilsGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathSimulatorGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PostUtils.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathAdaptiveGui.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/PathAdaptive.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/__init__.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Path/PathTests" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/PathTestUtils.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathCore.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathDeburr.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathDepthParams.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathDressupDogbone.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathDressupHoldingTags.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathGeom.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathHelix.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathLog.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathOpTools.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathPost.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathSetupSheet.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathStock.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathTool.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathToolController.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathTooltable.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/TestPathUtil.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/boxtest.fcstd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/test_centroid_00.ngc"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/test_geomop.fcstd"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/test_linuxcnc_00.ngc"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathTests/test_holes00.fcstd"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Path/PathScripts/post" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/centroid_post.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/comparams_post.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/dynapath_post.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/example_pre.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/grbl_post.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/jtech_post.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/linuxcnc_post.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/opensbp_post.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/opensbp_pre.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/philips_post.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/rml_post.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/slic3r_pre.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/PathScripts/post/smoothie_post.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Path/Images/Ops" TYPE FILE FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/Images/Ops/chamfer.svg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/Path/Images/Tools" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/Images/Tools/drill.svg"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/Images/Tools/endmill.svg"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/Path/Images/Tools/v-bit.svg"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Path/App/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Path/libarea/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Path/PathSimulator/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/Path/Gui/cmake_install.cmake")

endif()

