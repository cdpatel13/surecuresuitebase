/********************************************************************************
** Form generated from reading UI file 'DlgPrefsTechDraw.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGPREFSTECHDRAW_H
#define UI_DLGPREFSTECHDRAW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/FileDialog.h"
#include "Gui/PrefWidgets.h"
#include "Gui/QuantitySpinBox.h"
#include "Gui/Widgets.h"

namespace TechDrawGui {

class Ui_DlgPrefsTechDrawImp
{
public:
    QGridLayout *gridLayout_5;
    QGroupBox *gb_Templates;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout_3;
    Gui::PrefFileChooser *pfc_DefTemp;
    QLabel *label_11;
    QLabel *label_2;
    QLabel *label;
    Gui::PrefFileChooser *pfc_DefDir;
    QLabel *lbl_Hatch;
    QLabel *label_10;
    Gui::PrefFileChooser *pfc_HatchFile;
    QLabel *label_9;
    Gui::PrefFileChooser *pfc_FilePattern;
    Gui::PrefFileChooser *pfc_LineGroup;
    Gui::PrefFileChooser *pfc_Welding;
    QGridLayout *gridLayout_4;
    Gui::PrefLineEdit *le_NamePattern;
    QLabel *label_8;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *verticalSpacer;
    QGroupBox *gb_Font;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QLabel *lbl_LabelFont;
    QLabel *label_6;
    Gui::PrefFontBox *pfb_LabelFont;
    QLabel *lbl_TemplateDot;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_7;
    Gui::PrefUnitSpinBox *plsb_LabelSize;
    Gui::PrefUnitSpinBox *plsb_TemplateDot;
    QSpacerItem *verticalSpacer_5;
    QSpacerItem *verticalSpacer_2;
    QGroupBox *gb_Colors;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout;
    QLabel *label_5;
    QLabel *label_3;
    Gui::PrefColorButton *pcb_Background;
    Gui::PrefColorButton *pcb_PreSelect;
    QLabel *lbl_Normal;
    Gui::PrefColorButton *pcb_Hidden;
    QSpacerItem *horizontalSpacer;
    Gui::PrefColorButton *pcb_Select;
    QSpacerItem *horizontalSpacer_4;
    Gui::PrefColorButton *pcb_Normal;
    QSpacerItem *horizontalSpacer_3;
    QLabel *lbl_PreSelect;
    QLabel *lbl_Hidden;
    Gui::PrefColorButton *pcb_Surface;
    QLabel *lbl_Select;
    Gui::PrefColorButton *pcb_Hatch;
    QLabel *label_4;
    QLabel *label_7;
    Gui::PrefColorButton *pcb_GeomHatch;
    Gui::PrefColorButton *pcb_Face;
    Gui::PrefCheckBox *pcb_PaintFaces;
    QSpacerItem *verticalSpacer_3;
    QGroupBox *gbMisc;
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *lbl_Angle;
    Gui::PrefComboBox *cb_Angle;
    QLabel *lbl_HidLine;
    Gui::PrefComboBox *cb_HidLine;
    Gui::PrefCheckBox *cb_Faces;
    Gui::PrefCheckBox *cb_SectionEdges;
    Gui::PrefCheckBox *cb_AutoDist;
    QLabel *label_12;
    Gui::PrefCheckBox *cb_Global;
    Gui::PrefCheckBox *cb_Override;
    Gui::PrefCheckBox *cb_PageUpdate;
    QSpacerItem *verticalSpacer_4;

    void setupUi(QWidget *TechDrawGui__DlgPrefsTechDrawImp)
    {
        if (TechDrawGui__DlgPrefsTechDrawImp->objectName().isEmpty())
            TechDrawGui__DlgPrefsTechDrawImp->setObjectName(QString::fromUtf8("TechDrawGui__DlgPrefsTechDrawImp"));
        TechDrawGui__DlgPrefsTechDrawImp->resize(558, 1110);
        gridLayout_5 = new QGridLayout(TechDrawGui__DlgPrefsTechDrawImp);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gb_Templates = new QGroupBox(TechDrawGui__DlgPrefsTechDrawImp);
        gb_Templates->setObjectName(QString::fromUtf8("gb_Templates"));
        verticalLayout_3 = new QVBoxLayout(gb_Templates);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        pfc_DefTemp = new Gui::PrefFileChooser(gb_Templates);
        pfc_DefTemp->setObjectName(QString::fromUtf8("pfc_DefTemp"));
        pfc_DefTemp->setProperty("prefEntry", QVariant(QByteArray("TemplateFile")));
        pfc_DefTemp->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Files")));

        gridLayout_3->addWidget(pfc_DefTemp, 0, 1, 1, 1);

        label_11 = new QLabel(gb_Templates);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_3->addWidget(label_11, 4, 0, 1, 1);

        label_2 = new QLabel(gb_Templates);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_3->addWidget(label_2, 1, 0, 1, 1);

        label = new QLabel(gb_Templates);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_3->addWidget(label, 0, 0, 1, 1);

        pfc_DefDir = new Gui::PrefFileChooser(gb_Templates);
        pfc_DefDir->setObjectName(QString::fromUtf8("pfc_DefDir"));
        pfc_DefDir->setMode(Gui::FileChooser::Directory);
        pfc_DefDir->setProperty("prefEntry", QVariant(QByteArray("TemplateDir")));
        pfc_DefDir->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Files")));

        gridLayout_3->addWidget(pfc_DefDir, 1, 1, 1, 1);

        lbl_Hatch = new QLabel(gb_Templates);
        lbl_Hatch->setObjectName(QString::fromUtf8("lbl_Hatch"));

        gridLayout_3->addWidget(lbl_Hatch, 2, 0, 1, 1);

        label_10 = new QLabel(gb_Templates);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_3->addWidget(label_10, 3, 0, 1, 1);

        pfc_HatchFile = new Gui::PrefFileChooser(gb_Templates);
        pfc_HatchFile->setObjectName(QString::fromUtf8("pfc_HatchFile"));
        pfc_HatchFile->setProperty("prefEntry", QVariant(QByteArray("FileHatch")));
        pfc_HatchFile->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Files")));

        gridLayout_3->addWidget(pfc_HatchFile, 2, 1, 1, 1);

        label_9 = new QLabel(gb_Templates);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_3->addWidget(label_9, 5, 0, 1, 1);

        pfc_FilePattern = new Gui::PrefFileChooser(gb_Templates);
        pfc_FilePattern->setObjectName(QString::fromUtf8("pfc_FilePattern"));
        pfc_FilePattern->setProperty("prefEntry", QVariant(QByteArray("FilePattern")));
        pfc_FilePattern->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/PAT")));

        gridLayout_3->addWidget(pfc_FilePattern, 5, 1, 1, 1);

        pfc_LineGroup = new Gui::PrefFileChooser(gb_Templates);
        pfc_LineGroup->setObjectName(QString::fromUtf8("pfc_LineGroup"));
        pfc_LineGroup->setProperty("prefEntry", QVariant(QByteArray("LineGroupFile")));
        pfc_LineGroup->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Files")));

        gridLayout_3->addWidget(pfc_LineGroup, 3, 1, 1, 1);

        pfc_Welding = new Gui::PrefFileChooser(gb_Templates);
        pfc_Welding->setObjectName(QString::fromUtf8("pfc_Welding"));
        pfc_Welding->setMode(Gui::FileChooser::Directory);
        pfc_Welding->setProperty("prefEntry", QVariant(QByteArray("WeldingDir")));
        pfc_Welding->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Files")));

        gridLayout_3->addWidget(pfc_Welding, 4, 1, 1, 1);


        verticalLayout_3->addLayout(gridLayout_3);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        le_NamePattern = new Gui::PrefLineEdit(gb_Templates);
        le_NamePattern->setObjectName(QString::fromUtf8("le_NamePattern"));
        le_NamePattern->setProperty("prefEntry", QVariant(QByteArray("NamePattern")));
        le_NamePattern->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/PAT")));

        gridLayout_4->addWidget(le_NamePattern, 0, 2, 1, 1);

        label_8 = new QLabel(gb_Templates);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_4->addWidget(label_8, 0, 0, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_5, 0, 1, 1, 1);


        verticalLayout_3->addLayout(gridLayout_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        gridLayout_5->addWidget(gb_Templates, 3, 0, 1, 1);

        gb_Font = new QGroupBox(TechDrawGui__DlgPrefsTechDrawImp);
        gb_Font->setObjectName(QString::fromUtf8("gb_Font"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(gb_Font->sizePolicy().hasHeightForWidth());
        gb_Font->setSizePolicy(sizePolicy);
        verticalLayout_4 = new QVBoxLayout(gb_Font);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 0, 1, 1, 1);

        lbl_LabelFont = new QLabel(gb_Font);
        lbl_LabelFont->setObjectName(QString::fromUtf8("lbl_LabelFont"));

        gridLayout_2->addWidget(lbl_LabelFont, 0, 0, 1, 1);

        label_6 = new QLabel(gb_Font);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_2->addWidget(label_6, 1, 0, 1, 1);

        pfb_LabelFont = new Gui::PrefFontBox(gb_Font);
        pfb_LabelFont->setObjectName(QString::fromUtf8("pfb_LabelFont"));
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Sans"));
        pfb_LabelFont->setCurrentFont(font);
        pfb_LabelFont->setProperty("prefEntry", QVariant(QByteArray("LabelFont")));
        pfb_LabelFont->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Labels")));

        gridLayout_2->addWidget(pfb_LabelFont, 0, 2, 1, 1);

        lbl_TemplateDot = new QLabel(gb_Font);
        lbl_TemplateDot->setObjectName(QString::fromUtf8("lbl_TemplateDot"));

        gridLayout_2->addWidget(lbl_TemplateDot, 2, 0, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_6, 1, 1, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_7, 2, 1, 1, 1);

        plsb_LabelSize = new Gui::PrefUnitSpinBox(gb_Font);
        plsb_LabelSize->setObjectName(QString::fromUtf8("plsb_LabelSize"));
        plsb_LabelSize->setValue(8.000000000000000);
        plsb_LabelSize->setProperty("prefEntry", QVariant(QByteArray("LabelSize")));
        plsb_LabelSize->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Labels")));

        gridLayout_2->addWidget(plsb_LabelSize, 1, 2, 1, 1);

        plsb_TemplateDot = new Gui::PrefUnitSpinBox(gb_Font);
        plsb_TemplateDot->setObjectName(QString::fromUtf8("plsb_TemplateDot"));
        plsb_TemplateDot->setValue(3.000000000000000);
        plsb_TemplateDot->setProperty("prefEntry", QVariant(QByteArray("TemplateDotSize")));
        plsb_TemplateDot->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/General")));

        gridLayout_2->addWidget(plsb_TemplateDot, 2, 2, 1, 1);


        verticalLayout_4->addLayout(gridLayout_2);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_5);


        gridLayout_5->addWidget(gb_Font, 2, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_5->addItem(verticalSpacer_2, 4, 0, 1, 1);

        gb_Colors = new QGroupBox(TechDrawGui__DlgPrefsTechDrawImp);
        gb_Colors->setObjectName(QString::fromUtf8("gb_Colors"));
        verticalLayout_2 = new QVBoxLayout(gb_Colors);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_5 = new QLabel(gb_Colors);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 2, 4, 1, 1);

        label_3 = new QLabel(gb_Colors);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 1, 4, 1, 1);

        pcb_Background = new Gui::PrefColorButton(gb_Colors);
        pcb_Background->setObjectName(QString::fromUtf8("pcb_Background"));
        pcb_Background->setColor(QColor(80, 80, 80));
        pcb_Background->setProperty("prefEntry", QVariant(QByteArray("Background")));
        pcb_Background->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_Background, 3, 2, 1, 1);

        pcb_PreSelect = new Gui::PrefColorButton(gb_Colors);
        pcb_PreSelect->setObjectName(QString::fromUtf8("pcb_PreSelect"));
        pcb_PreSelect->setColor(QColor(255, 255, 20));
        pcb_PreSelect->setProperty("prefEntry", QVariant(QByteArray("PreSelectColor")));
        pcb_PreSelect->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_PreSelect, 1, 2, 1, 1);

        lbl_Normal = new QLabel(gb_Colors);
        lbl_Normal->setObjectName(QString::fromUtf8("lbl_Normal"));

        gridLayout->addWidget(lbl_Normal, 0, 0, 1, 1);

        pcb_Hidden = new Gui::PrefColorButton(gb_Colors);
        pcb_Hidden->setObjectName(QString::fromUtf8("pcb_Hidden"));
        pcb_Hidden->setProperty("prefEntry", QVariant(QByteArray("HiddenColor")));
        pcb_Hidden->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_Hidden, 0, 6, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 1, 1, 1);

        pcb_Select = new Gui::PrefColorButton(gb_Colors);
        pcb_Select->setObjectName(QString::fromUtf8("pcb_Select"));
        pcb_Select->setColor(QColor(28, 173, 28));
        pcb_Select->setProperty("prefEntry", QVariant(QByteArray("SelectColor")));
        pcb_Select->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_Select, 2, 2, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_4, 0, 5, 1, 1);

        pcb_Normal = new Gui::PrefColorButton(gb_Colors);
        pcb_Normal->setObjectName(QString::fromUtf8("pcb_Normal"));
        pcb_Normal->setColor(QColor(0, 0, 0));
        pcb_Normal->setProperty("prefEntry", QVariant(QByteArray("NormalColor")));
        pcb_Normal->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_Normal, 0, 2, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 0, 3, 1, 1);

        lbl_PreSelect = new QLabel(gb_Colors);
        lbl_PreSelect->setObjectName(QString::fromUtf8("lbl_PreSelect"));

        gridLayout->addWidget(lbl_PreSelect, 1, 0, 1, 1);

        lbl_Hidden = new QLabel(gb_Colors);
        lbl_Hidden->setObjectName(QString::fromUtf8("lbl_Hidden"));

        gridLayout->addWidget(lbl_Hidden, 0, 4, 1, 1);

        pcb_Surface = new Gui::PrefColorButton(gb_Colors);
        pcb_Surface->setObjectName(QString::fromUtf8("pcb_Surface"));
        pcb_Surface->setColor(QColor(225, 225, 225));
        pcb_Surface->setProperty("prefEntry", QVariant(QByteArray("CutSurfaceColor")));
        pcb_Surface->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_Surface, 1, 6, 1, 1);

        lbl_Select = new QLabel(gb_Colors);
        lbl_Select->setObjectName(QString::fromUtf8("lbl_Select"));

        gridLayout->addWidget(lbl_Select, 2, 0, 1, 1);

        pcb_Hatch = new Gui::PrefColorButton(gb_Colors);
        pcb_Hatch->setObjectName(QString::fromUtf8("pcb_Hatch"));
        pcb_Hatch->setColor(QColor(0, 0, 0));
        pcb_Hatch->setProperty("prefEntry", QVariant(QByteArray("Hatch")));
        pcb_Hatch->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_Hatch, 2, 6, 1, 1);

        label_4 = new QLabel(gb_Colors);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        label_7 = new QLabel(gb_Colors);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 3, 4, 1, 1);

        pcb_GeomHatch = new Gui::PrefColorButton(gb_Colors);
        pcb_GeomHatch->setObjectName(QString::fromUtf8("pcb_GeomHatch"));
        pcb_GeomHatch->setColor(QColor(0, 0, 0));
        pcb_GeomHatch->setProperty("prefEntry", QVariant(QByteArray("GeomHatch")));
        pcb_GeomHatch->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_GeomHatch, 3, 6, 1, 1);

        pcb_Face = new Gui::PrefColorButton(gb_Colors);
        pcb_Face->setObjectName(QString::fromUtf8("pcb_Face"));
        pcb_Face->setColor(QColor(255, 255, 255));
        pcb_Face->setProperty("prefEntry", QVariant(QByteArray("FaceColor")));
        pcb_Face->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_Face, 4, 2, 1, 1);

        pcb_PaintFaces = new Gui::PrefCheckBox(gb_Colors);
        pcb_PaintFaces->setObjectName(QString::fromUtf8("pcb_PaintFaces"));
        pcb_PaintFaces->setProperty("prefEntry", QVariant(QByteArray("ClearFace")));
        pcb_PaintFaces->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Colors")));

        gridLayout->addWidget(pcb_PaintFaces, 4, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);


        gridLayout_5->addWidget(gb_Colors, 1, 0, 1, 1);

        gbMisc = new QGroupBox(TechDrawGui__DlgPrefsTechDrawImp);
        gbMisc->setObjectName(QString::fromUtf8("gbMisc"));
        verticalLayout = new QVBoxLayout(gbMisc);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        lbl_Angle = new QLabel(gbMisc);
        lbl_Angle->setObjectName(QString::fromUtf8("lbl_Angle"));

        formLayout->setWidget(0, QFormLayout::LabelRole, lbl_Angle);

        cb_Angle = new Gui::PrefComboBox(gbMisc);
        cb_Angle->addItem(QString());
        cb_Angle->addItem(QString());
        cb_Angle->setObjectName(QString::fromUtf8("cb_Angle"));
        cb_Angle->setProperty("prefEntry", QVariant(QByteArray("ProjectionAngle")));
        cb_Angle->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/General")));

        formLayout->setWidget(0, QFormLayout::FieldRole, cb_Angle);

        lbl_HidLine = new QLabel(gbMisc);
        lbl_HidLine->setObjectName(QString::fromUtf8("lbl_HidLine"));

        formLayout->setWidget(1, QFormLayout::LabelRole, lbl_HidLine);

        cb_HidLine = new Gui::PrefComboBox(gbMisc);
        cb_HidLine->addItem(QString());
        cb_HidLine->addItem(QString());
        cb_HidLine->addItem(QString());
        cb_HidLine->addItem(QString());
        cb_HidLine->addItem(QString());
        cb_HidLine->addItem(QString());
        cb_HidLine->setObjectName(QString::fromUtf8("cb_HidLine"));
        cb_HidLine->setProperty("prefEntry", QVariant(QByteArray("HiddenLine")));
        cb_HidLine->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/General")));

        formLayout->setWidget(1, QFormLayout::FieldRole, cb_HidLine);

        cb_Faces = new Gui::PrefCheckBox(gbMisc);
        cb_Faces->setObjectName(QString::fromUtf8("cb_Faces"));
        cb_Faces->setChecked(true);
        cb_Faces->setProperty("prefEntry", QVariant(QByteArray("HandleFaces")));
        cb_Faces->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/General")));

        formLayout->setWidget(2, QFormLayout::LabelRole, cb_Faces);

        cb_SectionEdges = new Gui::PrefCheckBox(gbMisc);
        cb_SectionEdges->setObjectName(QString::fromUtf8("cb_SectionEdges"));
        cb_SectionEdges->setProperty("prefEntry", QVariant(QByteArray("ShowSectionEdges")));
        cb_SectionEdges->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/General")));

        formLayout->setWidget(3, QFormLayout::LabelRole, cb_SectionEdges);

        cb_AutoDist = new Gui::PrefCheckBox(gbMisc);
        cb_AutoDist->setObjectName(QString::fromUtf8("cb_AutoDist"));
        cb_AutoDist->setChecked(true);
        cb_AutoDist->setProperty("prefEntry", QVariant(QByteArray("AutoDist")));
        cb_AutoDist->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/General")));

        formLayout->setWidget(4, QFormLayout::LabelRole, cb_AutoDist);

        label_12 = new QLabel(gbMisc);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        label_12->setFont(font1);

        formLayout->setWidget(5, QFormLayout::LabelRole, label_12);

        cb_Global = new Gui::PrefCheckBox(gbMisc);
        cb_Global->setObjectName(QString::fromUtf8("cb_Global"));
        cb_Global->setChecked(true);
        cb_Global->setProperty("prefEntry", QVariant(QByteArray("GlobalUpdateDrawings")));
        cb_Global->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/General")));

        formLayout->setWidget(6, QFormLayout::LabelRole, cb_Global);

        cb_Override = new Gui::PrefCheckBox(gbMisc);
        cb_Override->setObjectName(QString::fromUtf8("cb_Override"));
        cb_Override->setChecked(true);
        cb_Override->setProperty("prefEntry", QVariant(QByteArray("AllowPageOverride")));
        cb_Override->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/General")));

        formLayout->setWidget(7, QFormLayout::LabelRole, cb_Override);

        cb_PageUpdate = new Gui::PrefCheckBox(gbMisc);
        cb_PageUpdate->setObjectName(QString::fromUtf8("cb_PageUpdate"));
        cb_PageUpdate->setChecked(true);
        cb_PageUpdate->setProperty("prefEntry", QVariant(QByteArray("KeepPagesUpToDate")));
        cb_PageUpdate->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/General")));

        formLayout->setWidget(8, QFormLayout::LabelRole, cb_PageUpdate);


        verticalLayout->addLayout(formLayout);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_4);


        gridLayout_5->addWidget(gbMisc, 0, 0, 1, 1);


        retranslateUi(TechDrawGui__DlgPrefsTechDrawImp);

        cb_HidLine->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(TechDrawGui__DlgPrefsTechDrawImp);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__DlgPrefsTechDrawImp)
    {
        TechDrawGui__DlgPrefsTechDrawImp->setWindowTitle(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "TechDraw General", nullptr));
#ifndef QT_NO_TOOLTIP
        TechDrawGui__DlgPrefsTechDrawImp->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        gb_Templates->setTitle(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Files", nullptr));
#ifndef QT_NO_TOOLTIP
        pfc_DefTemp->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Default template file for new pages", nullptr));
#endif // QT_NO_TOOLTIP
        label_11->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Welding Directory", nullptr));
        label_2->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Template Directory", nullptr));
        label->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Default Template", nullptr));
#ifndef QT_NO_TOOLTIP
        pfc_DefDir->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Starting directory for menu 'Insert new page using Template'", nullptr));
#endif // QT_NO_TOOLTIP
        lbl_Hatch->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Hatch Image", nullptr));
        label_10->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Line Group File", nullptr));
#ifndef QT_NO_TOOLTIP
        pfc_HatchFile->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Default SVG or bitmap file for hatching", nullptr));
#endif // QT_NO_TOOLTIP
        label_9->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "PAT File", nullptr));
#ifndef QT_NO_TOOLTIP
        pfc_FilePattern->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Default PAT pattern definition file for hatching", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pfc_LineGroup->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Alternate file for personal LineGroup definition", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pfc_Welding->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Default directory for welding symbols", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        le_NamePattern->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Name of the default PAT pattern", nullptr));
#endif // QT_NO_TOOLTIP
        label_8->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Pattern Name", nullptr));
        gb_Font->setTitle(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Labels", nullptr));
        lbl_LabelFont->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Label Font", nullptr));
        label_6->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Label Size", nullptr));
#ifndef QT_NO_TOOLTIP
        pfb_LabelFont->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Font for labels", nullptr));
#endif // QT_NO_TOOLTIP
        lbl_TemplateDot->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Editable Text Marker Size", nullptr));
#ifndef QT_NO_TOOLTIP
        plsb_LabelSize->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Label size", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        plsb_TemplateDot->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Size of editable text marker in templates (green dots)", nullptr));
#endif // QT_NO_TOOLTIP
        gb_Colors->setTitle(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Colors", nullptr));
        label_5->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Section Hatch", nullptr));
        label_3->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Section Face", nullptr));
#ifndef QT_NO_TOOLTIP
        pcb_Background->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Window background color", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pcb_PreSelect->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Preselection color", nullptr));
#endif // QT_NO_TOOLTIP
        lbl_Normal->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Normal", nullptr));
#ifndef QT_NO_TOOLTIP
        pcb_Hidden->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Hidden line color", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pcb_Select->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Selected item color", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pcb_Normal->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Normal line color", nullptr));
#endif // QT_NO_TOOLTIP
        lbl_PreSelect->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Preselected", nullptr));
        lbl_Hidden->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Hidden Line", nullptr));
#ifndef QT_NO_TOOLTIP
        pcb_Surface->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Section face color", nullptr));
#endif // QT_NO_TOOLTIP
        lbl_Select->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Selected", nullptr));
#ifndef QT_NO_TOOLTIP
        pcb_Hatch->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Section face hatch color", nullptr));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Background", nullptr));
        label_7->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Geometric Hatch", nullptr));
#ifndef QT_NO_TOOLTIP
        pcb_GeomHatch->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Geometric hatch color", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pcb_Face->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Face color", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pcb_PaintFaces->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Transparent faces if checked", nullptr));
#endif // QT_NO_TOOLTIP
        pcb_PaintFaces->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Transparent Faces", nullptr));
        gbMisc->setTitle(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "General", nullptr));
        lbl_Angle->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Projection Angle", nullptr));
        cb_Angle->setItemText(0, QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "First", nullptr));
        cb_Angle->setItemText(1, QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Third", nullptr));

#ifndef QT_NO_TOOLTIP
        cb_Angle->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "First- or third-angle projection", nullptr));
#endif // QT_NO_TOOLTIP
        lbl_HidLine->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Hidden Line", nullptr));
        cb_HidLine->setItemText(0, QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "NeverShow", nullptr));
        cb_HidLine->setItemText(1, QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Solid", nullptr));
        cb_HidLine->setItemText(2, QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Dash", nullptr));
        cb_HidLine->setItemText(3, QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Dot", nullptr));
        cb_HidLine->setItemText(4, QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "DashDot", nullptr));
        cb_HidLine->setItemText(5, QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "DashDotDot", nullptr));

#ifndef QT_NO_TOOLTIP
        cb_HidLine->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Style for hidden lines", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        cb_Faces->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "If checked, TechDraw will attempt to build faces using the\n"
"line segments returned by the hidden line removal algorithm.\n"
"Faces must be detected in order to use hatching, but there\n"
"can be a performance penalty in complex models.", nullptr));
#endif // QT_NO_TOOLTIP
        cb_Faces->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Detect Faces", nullptr));
#ifndef QT_NO_TOOLTIP
        cb_SectionEdges->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Highlights the border of the section cut in section views", nullptr));
#endif // QT_NO_TOOLTIP
        cb_SectionEdges->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Show Section Edges", nullptr));
#ifndef QT_NO_TOOLTIP
        cb_AutoDist->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Automatically distribute secondary views\n"
"for ProjectionGroups", nullptr));
#endif // QT_NO_TOOLTIP
        cb_AutoDist->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Auto-distribute Secondary Views", nullptr));
        label_12->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Drawing Updates", nullptr));
#ifndef QT_NO_TOOLTIP
        cb_Global->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Whether or not pages are updated every time the 3D model is changed", nullptr));
#endif // QT_NO_TOOLTIP
        cb_Global->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Update With 3D (global policy)", nullptr));
#ifndef QT_NO_TOOLTIP
        cb_Override->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Whether or not a page's 'Keep Update' property\n"
"can override the global 'Update With 3D' parameter", nullptr));
#endif // QT_NO_TOOLTIP
        cb_Override->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Allow Page Override (global policy)", nullptr));
#ifndef QT_NO_TOOLTIP
        cb_PageUpdate->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Update pages as scheduled or skip updates.\n"
"Checked is the default for new pages.", nullptr));
#endif // QT_NO_TOOLTIP
        cb_PageUpdate->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDrawImp", "Keep Page Up To Date (default)", nullptr));
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class DlgPrefsTechDrawImp: public Ui_DlgPrefsTechDrawImp {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_DLGPREFSTECHDRAW_H
