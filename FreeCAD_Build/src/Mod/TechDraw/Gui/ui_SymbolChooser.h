/********************************************************************************
** Form generated from reading UI file 'SymbolChooser.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYMBOLCHOOSER_H
#define UI_SYMBOLCHOOSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/FileDialog.h"

QT_BEGIN_NAMESPACE

class Ui_SymbolChooser
{
public:
    QFrame *frame;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QListWidget *lwSymbols;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbCancel;
    QPushButton *pbOK;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    Gui::FileChooser *fcSymbolDir;

    void setupUi(QDialog *SymbolChooser)
    {
        if (SymbolChooser->objectName().isEmpty())
            SymbolChooser->setObjectName(QString::fromUtf8("SymbolChooser"));
        SymbolChooser->setWindowModality(Qt::WindowModal);
        SymbolChooser->resize(400, 394);
        SymbolChooser->setModal(true);
        frame = new QFrame(SymbolChooser);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(19, 19, 361, 341));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        frame->setLineWidth(2);
        verticalLayoutWidget = new QWidget(frame);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(9, 19, 341, 191));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        lwSymbols = new QListWidget(verticalLayoutWidget);
        lwSymbols->setObjectName(QString::fromUtf8("lwSymbols"));

        verticalLayout->addWidget(lwSymbols);

        horizontalLayoutWidget = new QWidget(frame);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 220, 341, 41));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pbCancel = new QPushButton(horizontalLayoutWidget);
        pbCancel->setObjectName(QString::fromUtf8("pbCancel"));

        horizontalLayout->addWidget(pbCancel);

        pbOK = new QPushButton(horizontalLayoutWidget);
        pbOK->setObjectName(QString::fromUtf8("pbOK"));

        horizontalLayout->addWidget(pbOK);

        horizontalLayoutWidget_2 = new QWidget(frame);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 280, 341, 35));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(horizontalLayoutWidget_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        fcSymbolDir = new Gui::FileChooser(horizontalLayoutWidget_2);
        fcSymbolDir->setObjectName(QString::fromUtf8("fcSymbolDir"));
        fcSymbolDir->setMode(Gui::FileChooser::Directory);

        horizontalLayout_2->addWidget(fcSymbolDir);


        retranslateUi(SymbolChooser);

        QMetaObject::connectSlotsByName(SymbolChooser);
    } // setupUi

    void retranslateUi(QDialog *SymbolChooser)
    {
        SymbolChooser->setWindowTitle(QApplication::translate("SymbolChooser", "SymbolChooser", nullptr));
        pbCancel->setText(QApplication::translate("SymbolChooser", "Cancel", nullptr));
        pbOK->setText(QApplication::translate("SymbolChooser", "OK", nullptr));
        label->setText(QApplication::translate("SymbolChooser", "Symbol Dir", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SymbolChooser: public Ui_SymbolChooser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYMBOLCHOOSER_H
