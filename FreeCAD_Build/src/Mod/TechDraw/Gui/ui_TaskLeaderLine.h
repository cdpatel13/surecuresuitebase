/********************************************************************************
** Form generated from reading UI file 'TaskLeaderLine.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKLEADERLINE_H
#define UI_TASKLEADERLINE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/Widgets.h"

namespace TechDrawGui {

class Ui_TaskLeaderLine
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout;
    QLineEdit *leBaseView;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pbCancelEdit;
    QSpacerItem *horizontalSpacer;
    QPushButton *pbTracker;
    QSpacerItem *horizontalSpacer_3;
    QFrame *line;
    QFormLayout *formLayout;
    QLabel *label_2;
    QComboBox *cboxStartSym;
    QLabel *label_3;
    QComboBox *cboxEndSym;
    QLabel *label;
    Gui::ColorButton *cpLineColor;
    QLabel *label_5;
    QDoubleSpinBox *dsbWeight;
    QLabel *label_6;
    QComboBox *cboxStyle;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TechDrawGui__TaskLeaderLine)
    {
        if (TechDrawGui__TaskLeaderLine->objectName().isEmpty())
            TechDrawGui__TaskLeaderLine->setObjectName(QString::fromUtf8("TechDrawGui__TaskLeaderLine"));
        TechDrawGui__TaskLeaderLine->resize(409, 405);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TechDrawGui__TaskLeaderLine->sizePolicy().hasHeightForWidth());
        TechDrawGui__TaskLeaderLine->setSizePolicy(sizePolicy);
        TechDrawGui__TaskLeaderLine->setMinimumSize(QSize(250, 0));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/actions/techdraw-mline.svg"), QSize(), QIcon::Normal, QIcon::Off);
        TechDrawGui__TaskLeaderLine->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(TechDrawGui__TaskLeaderLine);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(TechDrawGui__TaskLeaderLine);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        leBaseView = new QLineEdit(frame);
        leBaseView->setObjectName(QString::fromUtf8("leBaseView"));
        leBaseView->setEnabled(false);
        leBaseView->setMouseTracking(false);
        leBaseView->setFocusPolicy(Qt::NoFocus);
        leBaseView->setAcceptDrops(false);

        gridLayout->addWidget(leBaseView, 0, 1, 1, 1);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 0, 0, 1, 1);


        verticalLayout_4->addLayout(gridLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        pbCancelEdit = new QPushButton(frame);
        pbCancelEdit->setObjectName(QString::fromUtf8("pbCancelEdit"));

        horizontalLayout->addWidget(pbCancelEdit);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pbTracker = new QPushButton(frame);
        pbTracker->setObjectName(QString::fromUtf8("pbTracker"));

        horizontalLayout->addWidget(pbTracker);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);


        verticalLayout_4->addLayout(horizontalLayout);

        line = new QFrame(frame);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_4->addWidget(line);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        cboxStartSym = new QComboBox(frame);
        cboxStartSym->addItem(QString());
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/arrowfilled.svg"), QSize(), QIcon::Normal, QIcon::Off);
        cboxStartSym->addItem(icon1, QString());
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/arrowopen.svg"), QSize(), QIcon::Normal, QIcon::Off);
        cboxStartSym->addItem(icon2, QString());
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/arrowtick.svg"), QSize(), QIcon::Normal, QIcon::Off);
        cboxStartSym->addItem(icon3, QString());
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/arrowdot.svg"), QSize(), QIcon::Normal, QIcon::Off);
        cboxStartSym->addItem(icon4, QString());
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icons/arrowopendot.svg"), QSize(), QIcon::Normal, QIcon::Off);
        cboxStartSym->addItem(icon5, QString());
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/icons/arrowfork.svg"), QSize(), QIcon::Normal, QIcon::Off);
        cboxStartSym->addItem(icon6, QString());
        cboxStartSym->setObjectName(QString::fromUtf8("cboxStartSym"));

        formLayout->setWidget(1, QFormLayout::FieldRole, cboxStartSym);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        cboxEndSym = new QComboBox(frame);
        cboxEndSym->addItem(QString());
        cboxEndSym->addItem(icon1, QString());
        cboxEndSym->addItem(icon2, QString());
        cboxEndSym->addItem(icon3, QString());
        cboxEndSym->addItem(icon4, QString());
        cboxEndSym->addItem(icon5, QString());
        cboxEndSym->addItem(icon6, QString());
        cboxEndSym->setObjectName(QString::fromUtf8("cboxEndSym"));

        formLayout->setWidget(2, QFormLayout::FieldRole, cboxEndSym);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label);

        cpLineColor = new Gui::ColorButton(frame);
        cpLineColor->setObjectName(QString::fromUtf8("cpLineColor"));
        cpLineColor->setColor(QColor(0, 0, 0));

        formLayout->setWidget(3, QFormLayout::FieldRole, cpLineColor);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        dsbWeight = new QDoubleSpinBox(frame);
        dsbWeight->setObjectName(QString::fromUtf8("dsbWeight"));
        dsbWeight->setSingleStep(0.100000000000000);
        dsbWeight->setValue(0.500000000000000);

        formLayout->setWidget(4, QFormLayout::FieldRole, dsbWeight);

        label_6 = new QLabel(frame);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        cboxStyle = new QComboBox(frame);
        cboxStyle->addItem(QString());
        cboxStyle->addItem(QString());
        cboxStyle->addItem(QString());
        cboxStyle->addItem(QString());
        cboxStyle->addItem(QString());
        cboxStyle->addItem(QString());
        cboxStyle->setObjectName(QString::fromUtf8("cboxStyle"));

        formLayout->setWidget(5, QFormLayout::FieldRole, cboxStyle);


        verticalLayout_4->addLayout(formLayout);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));

        verticalLayout_4->addLayout(verticalLayout);


        verticalLayout_3->addLayout(verticalLayout_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        verticalLayout_2->addWidget(frame);


        retranslateUi(TechDrawGui__TaskLeaderLine);

        cboxStartSym->setCurrentIndex(1);
        cboxStyle->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(TechDrawGui__TaskLeaderLine);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskLeaderLine)
    {
        TechDrawGui__TaskLeaderLine->setWindowTitle(QApplication::translate("TechDrawGui::TaskLeaderLine", "Leader Line", nullptr));
        label_4->setText(QApplication::translate("TechDrawGui::TaskLeaderLine", "Base View", nullptr));
        pbCancelEdit->setText(QApplication::translate("TechDrawGui::TaskLeaderLine", "Discard Changes", nullptr));
        pbTracker->setText(QApplication::translate("TechDrawGui::TaskLeaderLine", "Pick Points", nullptr));
        label_2->setText(QApplication::translate("TechDrawGui::TaskLeaderLine", "Start Symbol", nullptr));
        cboxStartSym->setItemText(0, QApplication::translate("TechDrawGui::TaskLeaderLine", "No Symbol", nullptr));
        cboxStartSym->setItemText(1, QApplication::translate("TechDrawGui::TaskLeaderLine", "Filled Triangle", nullptr));
        cboxStartSym->setItemText(2, QApplication::translate("TechDrawGui::TaskLeaderLine", "Open Triangle", nullptr));
        cboxStartSym->setItemText(3, QApplication::translate("TechDrawGui::TaskLeaderLine", "Tick", nullptr));
        cboxStartSym->setItemText(4, QApplication::translate("TechDrawGui::TaskLeaderLine", "Dot", nullptr));
        cboxStartSym->setItemText(5, QApplication::translate("TechDrawGui::TaskLeaderLine", "Open Circle", nullptr));
        cboxStartSym->setItemText(6, QApplication::translate("TechDrawGui::TaskLeaderLine", "Fork", nullptr));

        label_3->setText(QApplication::translate("TechDrawGui::TaskLeaderLine", "End Symbol", nullptr));
        cboxEndSym->setItemText(0, QApplication::translate("TechDrawGui::TaskLeaderLine", "No Symbol", nullptr));
        cboxEndSym->setItemText(1, QApplication::translate("TechDrawGui::TaskLeaderLine", "Filled Triangle", nullptr));
        cboxEndSym->setItemText(2, QApplication::translate("TechDrawGui::TaskLeaderLine", "Open Triangle", nullptr));
        cboxEndSym->setItemText(3, QApplication::translate("TechDrawGui::TaskLeaderLine", "Tick", nullptr));
        cboxEndSym->setItemText(4, QApplication::translate("TechDrawGui::TaskLeaderLine", "Dot", nullptr));
        cboxEndSym->setItemText(5, QApplication::translate("TechDrawGui::TaskLeaderLine", "Open Circle", nullptr));
        cboxEndSym->setItemText(6, QApplication::translate("TechDrawGui::TaskLeaderLine", "Fork", nullptr));

        label->setText(QApplication::translate("TechDrawGui::TaskLeaderLine", "Color", nullptr));
        label_5->setText(QApplication::translate("TechDrawGui::TaskLeaderLine", "Weight", nullptr));
        label_6->setText(QApplication::translate("TechDrawGui::TaskLeaderLine", "Style", nullptr));
        cboxStyle->setItemText(0, QApplication::translate("TechDrawGui::TaskLeaderLine", "NoLine", nullptr));
        cboxStyle->setItemText(1, QApplication::translate("TechDrawGui::TaskLeaderLine", "Solid", nullptr));
        cboxStyle->setItemText(2, QApplication::translate("TechDrawGui::TaskLeaderLine", "Dash", nullptr));
        cboxStyle->setItemText(3, QApplication::translate("TechDrawGui::TaskLeaderLine", "Dot", nullptr));
        cboxStyle->setItemText(4, QApplication::translate("TechDrawGui::TaskLeaderLine", "DashDot", nullptr));
        cboxStyle->setItemText(5, QApplication::translate("TechDrawGui::TaskLeaderLine", "DashDotDot", nullptr));

    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskLeaderLine: public Ui_TaskLeaderLine {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKLEADERLINE_H
