/****************************************************************************
** Meta object code from reading C++ file 'TaskWeldingSymbol.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/TechDraw/Gui/TaskWeldingSymbol.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskWeldingSymbol.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TechDrawGui__TaskWeldingSymbol_t {
    QByteArrayData data[14];
    char stringdata0[199];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TechDrawGui__TaskWeldingSymbol_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TechDrawGui__TaskWeldingSymbol_t qt_meta_stringdata_TechDrawGui__TaskWeldingSymbol = {
    {
QT_MOC_LITERAL(0, 0, 30), // "TechDrawGui::TaskWeldingSymbol"
QT_MOC_LITERAL(1, 31, 20), // "onArrowSymbolClicked"
QT_MOC_LITERAL(2, 52, 0), // ""
QT_MOC_LITERAL(3, 53, 1), // "b"
QT_MOC_LITERAL(4, 55, 20), // "onOtherSymbolClicked"
QT_MOC_LITERAL(5, 76, 19), // "onOtherEraseClicked"
QT_MOC_LITERAL(6, 96, 18), // "onArrowTextChanged"
QT_MOC_LITERAL(7, 115, 2), // "qs"
QT_MOC_LITERAL(8, 118, 18), // "onOtherTextChanged"
QT_MOC_LITERAL(9, 137, 19), // "onDirectorySelected"
QT_MOC_LITERAL(10, 157, 6), // "newDir"
QT_MOC_LITERAL(11, 164, 16), // "onSymbolSelected"
QT_MOC_LITERAL(12, 181, 10), // "symbolPath"
QT_MOC_LITERAL(13, 192, 6) // "source"

    },
    "TechDrawGui::TaskWeldingSymbol\0"
    "onArrowSymbolClicked\0\0b\0onOtherSymbolClicked\0"
    "onOtherEraseClicked\0onArrowTextChanged\0"
    "qs\0onOtherTextChanged\0onDirectorySelected\0"
    "newDir\0onSymbolSelected\0symbolPath\0"
    "source"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TechDrawGui__TaskWeldingSymbol[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x0a /* Public */,
       4,    1,   52,    2, 0x0a /* Public */,
       5,    1,   55,    2, 0x0a /* Public */,
       6,    1,   58,    2, 0x0a /* Public */,
       8,    1,   61,    2, 0x0a /* Public */,
       9,    1,   64,    2, 0x0a /* Public */,
      11,    2,   67,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   12,   13,

       0        // eod
};

void TechDrawGui::TaskWeldingSymbol::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskWeldingSymbol *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onArrowSymbolClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->onOtherSymbolClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->onOtherEraseClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->onArrowTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->onOtherTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->onDirectorySelected((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->onSymbolSelected((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TechDrawGui::TaskWeldingSymbol::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_TechDrawGui__TaskWeldingSymbol.data,
    qt_meta_data_TechDrawGui__TaskWeldingSymbol,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TechDrawGui::TaskWeldingSymbol::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TechDrawGui::TaskWeldingSymbol::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TechDrawGui__TaskWeldingSymbol.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int TechDrawGui::TaskWeldingSymbol::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
struct qt_meta_stringdata_TechDrawGui__TaskDlgWeldingSymbol_t {
    QByteArrayData data[1];
    char stringdata0[34];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TechDrawGui__TaskDlgWeldingSymbol_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TechDrawGui__TaskDlgWeldingSymbol_t qt_meta_stringdata_TechDrawGui__TaskDlgWeldingSymbol = {
    {
QT_MOC_LITERAL(0, 0, 33) // "TechDrawGui::TaskDlgWeldingSy..."

    },
    "TechDrawGui::TaskDlgWeldingSymbol"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TechDrawGui__TaskDlgWeldingSymbol[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void TechDrawGui::TaskDlgWeldingSymbol::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject TechDrawGui::TaskDlgWeldingSymbol::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_TechDrawGui__TaskDlgWeldingSymbol.data,
    qt_meta_data_TechDrawGui__TaskDlgWeldingSymbol,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TechDrawGui::TaskDlgWeldingSymbol::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TechDrawGui::TaskDlgWeldingSymbol::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TechDrawGui__TaskDlgWeldingSymbol.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int TechDrawGui::TaskDlgWeldingSymbol::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
