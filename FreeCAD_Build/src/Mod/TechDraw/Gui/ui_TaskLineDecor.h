/********************************************************************************
** Form generated from reading UI file 'TaskLineDecor.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKLINEDECOR_H
#define UI_TASKLINEDECOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/Widgets.h"

namespace TechDrawGui {

class Ui_TaskLineDecor
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout_5;
    QLineEdit *le_Lines;
    QLabel *label_10;
    QLabel *label_4;
    QLineEdit *le_View;
    QSpacerItem *verticalSpacer_2;
    QGridLayout *gridLayout_4;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_14;
    QLabel *label_6;
    QLabel *label_15;
    Gui::ColorButton *cc_Color;
    QDoubleSpinBox *dsb_Weight;
    QLabel *label;
    QComboBox *cb_Visible;
    QComboBox *cb_Style;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TechDrawGui__TaskLineDecor)
    {
        if (TechDrawGui__TaskLineDecor->objectName().isEmpty())
            TechDrawGui__TaskLineDecor->setObjectName(QString::fromUtf8("TechDrawGui__TaskLineDecor"));
        TechDrawGui__TaskLineDecor->resize(395, 294);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TechDrawGui__TaskLineDecor->sizePolicy().hasHeightForWidth());
        TechDrawGui__TaskLineDecor->setSizePolicy(sizePolicy);
        TechDrawGui__TaskLineDecor->setMinimumSize(QSize(250, 0));
        verticalLayout_2 = new QVBoxLayout(TechDrawGui__TaskLineDecor);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(TechDrawGui__TaskLineDecor);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        le_Lines = new QLineEdit(frame);
        le_Lines->setObjectName(QString::fromUtf8("le_Lines"));
        le_Lines->setEnabled(false);

        gridLayout_5->addWidget(le_Lines, 1, 1, 1, 1);

        label_10 = new QLabel(frame);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_5->addWidget(label_10, 1, 0, 1, 1);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_5->addWidget(label_4, 0, 0, 1, 1);

        le_View = new QLineEdit(frame);
        le_View->setObjectName(QString::fromUtf8("le_View"));
        le_View->setEnabled(false);
        le_View->setMouseTracking(false);
        le_View->setFocusPolicy(Qt::NoFocus);
        le_View->setAcceptDrops(false);

        gridLayout_5->addWidget(le_View, 0, 1, 1, 1);


        verticalLayout_3->addLayout(gridLayout_5);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_4, 0, 1, 1, 1);

        label_14 = new QLabel(frame);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_4->addWidget(label_14, 1, 0, 1, 1);

        label_6 = new QLabel(frame);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_4->addWidget(label_6, 0, 0, 1, 1);

        label_15 = new QLabel(frame);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_4->addWidget(label_15, 2, 0, 1, 1);

        cc_Color = new Gui::ColorButton(frame);
        cc_Color->setObjectName(QString::fromUtf8("cc_Color"));
        cc_Color->setColor(QColor(0, 0, 0));

        gridLayout_4->addWidget(cc_Color, 1, 2, 1, 1);

        dsb_Weight = new QDoubleSpinBox(frame);
        dsb_Weight->setObjectName(QString::fromUtf8("dsb_Weight"));
        dsb_Weight->setValue(0.500000000000000);

        gridLayout_4->addWidget(dsb_Weight, 2, 2, 1, 1);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_4->addWidget(label, 3, 0, 1, 1);

        cb_Visible = new QComboBox(frame);
        cb_Visible->addItem(QString());
        cb_Visible->addItem(QString());
        cb_Visible->setObjectName(QString::fromUtf8("cb_Visible"));
        cb_Visible->setMaxVisibleItems(2);
        cb_Visible->setMaxCount(2);
        cb_Visible->setMinimumContentsLength(2);

        gridLayout_4->addWidget(cb_Visible, 3, 2, 1, 1);

        cb_Style = new QComboBox(frame);
        cb_Style->addItem(QString());
        cb_Style->addItem(QString());
        cb_Style->addItem(QString());
        cb_Style->addItem(QString());
        cb_Style->addItem(QString());
        cb_Style->setObjectName(QString::fromUtf8("cb_Style"));

        gridLayout_4->addWidget(cb_Style, 0, 2, 1, 1);

        gridLayout_4->setColumnStretch(2, 1);

        verticalLayout_3->addLayout(gridLayout_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        verticalLayout_2->addWidget(frame);


        retranslateUi(TechDrawGui__TaskLineDecor);

        cb_Visible->setCurrentIndex(1);
        cb_Style->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(TechDrawGui__TaskLineDecor);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskLineDecor)
    {
        TechDrawGui__TaskLineDecor->setWindowTitle(QApplication::translate("TechDrawGui::TaskLineDecor", "Line Decoration", nullptr));
        label_10->setText(QApplication::translate("TechDrawGui::TaskLineDecor", "Lines", nullptr));
        label_4->setText(QApplication::translate("TechDrawGui::TaskLineDecor", "View", nullptr));
        label_14->setText(QApplication::translate("TechDrawGui::TaskLineDecor", "Color", nullptr));
        label_6->setText(QApplication::translate("TechDrawGui::TaskLineDecor", "Style", nullptr));
        label_15->setText(QApplication::translate("TechDrawGui::TaskLineDecor", "Weight", nullptr));
#ifndef QT_NO_TOOLTIP
        dsb_Weight->setToolTip(QApplication::translate("TechDrawGui::TaskLineDecor", "Thickness of pattern lines.", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("TechDrawGui::TaskLineDecor", "Visible", nullptr));
        cb_Visible->setItemText(0, QApplication::translate("TechDrawGui::TaskLineDecor", "False", nullptr));
        cb_Visible->setItemText(1, QApplication::translate("TechDrawGui::TaskLineDecor", "True", nullptr));

        cb_Style->setItemText(0, QApplication::translate("TechDrawGui::TaskLineDecor", "Solid", nullptr));
        cb_Style->setItemText(1, QApplication::translate("TechDrawGui::TaskLineDecor", "Dash", nullptr));
        cb_Style->setItemText(2, QApplication::translate("TechDrawGui::TaskLineDecor", "Dot", nullptr));
        cb_Style->setItemText(3, QApplication::translate("TechDrawGui::TaskLineDecor", "DashDot", nullptr));
        cb_Style->setItemText(4, QApplication::translate("TechDrawGui::TaskLineDecor", "DashDotDot", nullptr));

    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskLineDecor: public Ui_TaskLineDecor {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKLINEDECOR_H
