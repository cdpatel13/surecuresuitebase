/********************************************************************************
** Form generated from reading UI file 'TaskWeldingSymbol.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKWELDINGSYMBOL_H
#define UI_TASKWELDINGSYMBOL_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/FileDialog.h"

QT_BEGIN_NAMESPACE

class Ui_TaskWeldingSymbol
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *hlArrowSideLayout;
    QGridLayout *gridLayout;
    QLineEdit *leArrowTextL;
    QLineEdit *leArrowTextR;
    QPushButton *pbArrowSymbol;
    QLineEdit *leArrowTextC;
    QFrame *line;
    QHBoxLayout *hlOtherSideLayout;
    QGridLayout *gridLayout_2;
    QLineEdit *leOtherTextR;
    QPushButton *pbOtherSymbol;
    QLineEdit *leOtherTextL;
    QLineEdit *leOtherTextC;
    QPushButton *pbOtherErase;
    QFrame *line_2;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout_3;
    QCheckBox *cbFieldWeld;
    QCheckBox *cbAllAround;
    QCheckBox *cbAltWeld;
    QFormLayout *formLayout_2;
    QLabel *label_5;
    QLineEdit *leTailText;
    QLabel *label;
    Gui::FileChooser *fcSymbolDir;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TaskWeldingSymbol)
    {
        if (TaskWeldingSymbol->objectName().isEmpty())
            TaskWeldingSymbol->setObjectName(QString::fromUtf8("TaskWeldingSymbol"));
        TaskWeldingSymbol->resize(423, 374);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TaskWeldingSymbol->sizePolicy().hasHeightForWidth());
        TaskWeldingSymbol->setSizePolicy(sizePolicy);
        TaskWeldingSymbol->setMinimumSize(QSize(250, 0));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/actions/techdraw-weldsymbol.svg"), QSize(), QIcon::Normal, QIcon::Off);
        TaskWeldingSymbol->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(TaskWeldingSymbol);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(TaskWeldingSymbol);
        frame->setObjectName(QString::fromUtf8("frame"));
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        hlArrowSideLayout = new QHBoxLayout();
        hlArrowSideLayout->setObjectName(QString::fromUtf8("hlArrowSideLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        leArrowTextL = new QLineEdit(frame);
        leArrowTextL->setObjectName(QString::fromUtf8("leArrowTextL"));

        gridLayout->addWidget(leArrowTextL, 2, 0, 1, 1);

        leArrowTextR = new QLineEdit(frame);
        leArrowTextR->setObjectName(QString::fromUtf8("leArrowTextR"));

        gridLayout->addWidget(leArrowTextR, 2, 2, 1, 1);

        pbArrowSymbol = new QPushButton(frame);
        pbArrowSymbol->setObjectName(QString::fromUtf8("pbArrowSymbol"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pbArrowSymbol->sizePolicy().hasHeightForWidth());
        pbArrowSymbol->setSizePolicy(sizePolicy1);
        pbArrowSymbol->setMinimumSize(QSize(0, 32));
        pbArrowSymbol->setBaseSize(QSize(0, 32));
        pbArrowSymbol->setCheckable(false);

        gridLayout->addWidget(pbArrowSymbol, 2, 1, 1, 1);

        leArrowTextC = new QLineEdit(frame);
        leArrowTextC->setObjectName(QString::fromUtf8("leArrowTextC"));

        gridLayout->addWidget(leArrowTextC, 0, 1, 1, 1);


        hlArrowSideLayout->addLayout(gridLayout);


        verticalLayout_4->addLayout(hlArrowSideLayout);

        line = new QFrame(frame);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShadow(QFrame::Plain);
        line->setLineWidth(5);
        line->setFrameShape(QFrame::HLine);

        verticalLayout_4->addWidget(line);

        hlOtherSideLayout = new QHBoxLayout();
        hlOtherSideLayout->setObjectName(QString::fromUtf8("hlOtherSideLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        leOtherTextR = new QLineEdit(frame);
        leOtherTextR->setObjectName(QString::fromUtf8("leOtherTextR"));

        gridLayout_2->addWidget(leOtherTextR, 0, 2, 1, 1);

        pbOtherSymbol = new QPushButton(frame);
        pbOtherSymbol->setObjectName(QString::fromUtf8("pbOtherSymbol"));

        gridLayout_2->addWidget(pbOtherSymbol, 0, 1, 1, 1);

        leOtherTextL = new QLineEdit(frame);
        leOtherTextL->setObjectName(QString::fromUtf8("leOtherTextL"));

        gridLayout_2->addWidget(leOtherTextL, 0, 0, 1, 1);

        leOtherTextC = new QLineEdit(frame);
        leOtherTextC->setObjectName(QString::fromUtf8("leOtherTextC"));

        gridLayout_2->addWidget(leOtherTextC, 1, 1, 1, 1);

        pbOtherErase = new QPushButton(frame);
        pbOtherErase->setObjectName(QString::fromUtf8("pbOtherErase"));
        sizePolicy1.setHeightForWidth(pbOtherErase->sizePolicy().hasHeightForWidth());
        pbOtherErase->setSizePolicy(sizePolicy1);
        pbOtherErase->setMinimumSize(QSize(60, 30));
        pbOtherErase->setMaximumSize(QSize(60, 30));
        pbOtherErase->setBaseSize(QSize(60, 30));

        gridLayout_2->addWidget(pbOtherErase, 1, 0, 1, 1);


        hlOtherSideLayout->addLayout(gridLayout_2);


        verticalLayout_4->addLayout(hlOtherSideLayout);


        verticalLayout_3->addLayout(verticalLayout_4);

        line_2 = new QFrame(frame);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        cbFieldWeld = new QCheckBox(frame);
        cbFieldWeld->setObjectName(QString::fromUtf8("cbFieldWeld"));

        gridLayout_3->addWidget(cbFieldWeld, 0, 0, 1, 1);

        cbAllAround = new QCheckBox(frame);
        cbAllAround->setObjectName(QString::fromUtf8("cbAllAround"));

        gridLayout_3->addWidget(cbAllAround, 0, 1, 1, 1);

        cbAltWeld = new QCheckBox(frame);
        cbAltWeld->setObjectName(QString::fromUtf8("cbAltWeld"));

        gridLayout_3->addWidget(cbAltWeld, 0, 2, 1, 1);


        verticalLayout->addLayout(gridLayout_3);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_5);

        leTailText = new QLineEdit(frame);
        leTailText->setObjectName(QString::fromUtf8("leTailText"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, leTailText);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label);

        fcSymbolDir = new Gui::FileChooser(frame);
        fcSymbolDir->setObjectName(QString::fromUtf8("fcSymbolDir"));
        fcSymbolDir->setMode(Gui::FileChooser::Directory);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, fcSymbolDir);


        verticalLayout->addLayout(formLayout_2);


        verticalLayout_3->addLayout(verticalLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        verticalLayout_2->addWidget(frame);


        retranslateUi(TaskWeldingSymbol);

        QMetaObject::connectSlotsByName(TaskWeldingSymbol);
    } // setupUi

    void retranslateUi(QWidget *TaskWeldingSymbol)
    {
        TaskWeldingSymbol->setWindowTitle(QApplication::translate("TaskWeldingSymbol", "Welding Symbol", nullptr));
#ifndef QT_NO_TOOLTIP
        leArrowTextL->setToolTip(QApplication::translate("TaskWeldingSymbol", "Text before arrow side symbol", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        leArrowTextR->setToolTip(QApplication::translate("TaskWeldingSymbol", "Text after arrow side symbol", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pbArrowSymbol->setToolTip(QApplication::translate("TaskWeldingSymbol", "Pick arrow side symbol", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        pbArrowSymbol->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        pbArrowSymbol->setText(QApplication::translate("TaskWeldingSymbol", "Symbol", nullptr));
#ifndef QT_NO_TOOLTIP
        leArrowTextC->setToolTip(QApplication::translate("TaskWeldingSymbol", "Text above arrow side symbol", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        leOtherTextR->setToolTip(QApplication::translate("TaskWeldingSymbol", "Text after other side symbol", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pbOtherSymbol->setToolTip(QApplication::translate("TaskWeldingSymbol", "Pick other side symbol", nullptr));
#endif // QT_NO_TOOLTIP
        pbOtherSymbol->setText(QApplication::translate("TaskWeldingSymbol", "Symbol", nullptr));
#ifndef QT_NO_TOOLTIP
        leOtherTextL->setToolTip(QApplication::translate("TaskWeldingSymbol", "Text before other side symbol", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        leOtherTextC->setToolTip(QApplication::translate("TaskWeldingSymbol", "Text below other side symbol", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pbOtherErase->setToolTip(QApplication::translate("TaskWeldingSymbol", "Remove other side symbol", nullptr));
#endif // QT_NO_TOOLTIP
        pbOtherErase->setText(QApplication::translate("TaskWeldingSymbol", "Delete", nullptr));
        cbFieldWeld->setText(QApplication::translate("TaskWeldingSymbol", "Field Weld", nullptr));
        cbAllAround->setText(QApplication::translate("TaskWeldingSymbol", "All Around", nullptr));
        cbAltWeld->setText(QApplication::translate("TaskWeldingSymbol", "Alternating", nullptr));
        label_5->setText(QApplication::translate("TaskWeldingSymbol", "Tail Text", nullptr));
#ifndef QT_NO_TOOLTIP
        leTailText->setToolTip(QApplication::translate("TaskWeldingSymbol", "Text at end of symbol", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("TaskWeldingSymbol", "Symbol Directory", nullptr));
#ifndef QT_NO_TOOLTIP
        fcSymbolDir->setToolTip(QApplication::translate("TaskWeldingSymbol", "Pick a directory of welding symbols", nullptr));
#endif // QT_NO_TOOLTIP
        fcSymbolDir->setFilter(QApplication::translate("TaskWeldingSymbol", "*.svg", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskWeldingSymbol: public Ui_TaskWeldingSymbol {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKWELDINGSYMBOL_H
