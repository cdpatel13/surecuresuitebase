/********************************************************************************
** Form generated from reading UI file 'TaskSectionView.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKSECTIONVIEW_H
#define UI_TASKSECTIONVIEW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"

namespace TechDrawGui {

class Ui_TaskSectionView
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout_2;
    QLabel *label_7;
    QLineEdit *leBaseView;
    QLabel *label_2;
    QLineEdit *leSymbol;
    QLabel *label;
    Gui::QuantitySpinBox *sbOrgX;
    QLabel *label_4;
    Gui::QuantitySpinBox *sbOrgY;
    QLabel *label_5;
    Gui::QuantitySpinBox *sbOrgZ;
    QGridLayout *gridLayout_3;
    QPushButton *pbRight;
    QPushButton *pbUp;
    QPushButton *pbLeft;
    QPushButton *pbDown;
    QLabel *label_9;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TechDrawGui__TaskSectionView)
    {
        if (TechDrawGui__TaskSectionView->objectName().isEmpty())
            TechDrawGui__TaskSectionView->setObjectName(QString::fromUtf8("TechDrawGui__TaskSectionView"));
        TechDrawGui__TaskSectionView->resize(434, 368);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TechDrawGui__TaskSectionView->sizePolicy().hasHeightForWidth());
        TechDrawGui__TaskSectionView->setSizePolicy(sizePolicy);
        TechDrawGui__TaskSectionView->setMinimumSize(QSize(250, 300));
        verticalLayout_2 = new QVBoxLayout(TechDrawGui__TaskSectionView);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(TechDrawGui__TaskSectionView);
        frame->setObjectName(QString::fromUtf8("frame"));
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setMinimumSize(QSize(350, 350));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        label_7 = new QLabel(frame);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_7);

        leBaseView = new QLineEdit(frame);
        leBaseView->setObjectName(QString::fromUtf8("leBaseView"));
        leBaseView->setEnabled(false);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, leBaseView);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_2);

        leSymbol = new QLineEdit(frame);
        leSymbol->setObjectName(QString::fromUtf8("leSymbol"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, leSymbol);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label);

        sbOrgX = new Gui::QuantitySpinBox(frame);
        sbOrgX->setObjectName(QString::fromUtf8("sbOrgX"));
        sbOrgX->setProperty("unit", QVariant(QString::fromUtf8("")));

        formLayout_2->setWidget(3, QFormLayout::FieldRole, sbOrgX);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, label_4);

        sbOrgY = new Gui::QuantitySpinBox(frame);
        sbOrgY->setObjectName(QString::fromUtf8("sbOrgY"));
        sbOrgY->setProperty("unit", QVariant(QString::fromUtf8("")));

        formLayout_2->setWidget(4, QFormLayout::FieldRole, sbOrgY);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout_2->setWidget(5, QFormLayout::LabelRole, label_5);

        sbOrgZ = new Gui::QuantitySpinBox(frame);
        sbOrgZ->setObjectName(QString::fromUtf8("sbOrgZ"));
        sbOrgZ->setProperty("unit", QVariant(QString::fromUtf8("")));

        formLayout_2->setWidget(5, QFormLayout::FieldRole, sbOrgZ);


        verticalLayout->addLayout(formLayout_2);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        pbRight = new QPushButton(frame);
        pbRight->setObjectName(QString::fromUtf8("pbRight"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/actions/section-right.svg"), QSize(), QIcon::Normal, QIcon::On);
        pbRight->setIcon(icon);
        pbRight->setIconSize(QSize(48, 48));
        pbRight->setCheckable(true);

        gridLayout_3->addWidget(pbRight, 1, 3, 1, 1);

        pbUp = new QPushButton(frame);
        pbUp->setObjectName(QString::fromUtf8("pbUp"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/actions/section-up.svg"), QSize(), QIcon::Normal, QIcon::On);
        pbUp->setIcon(icon1);
        pbUp->setIconSize(QSize(48, 48));
        pbUp->setCheckable(true);

        gridLayout_3->addWidget(pbUp, 1, 0, 1, 1);

        pbLeft = new QPushButton(frame);
        pbLeft->setObjectName(QString::fromUtf8("pbLeft"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/actions/section-left.svg"), QSize(), QIcon::Normal, QIcon::On);
        pbLeft->setIcon(icon2);
        pbLeft->setIconSize(QSize(48, 48));
        pbLeft->setCheckable(true);

        gridLayout_3->addWidget(pbLeft, 1, 2, 1, 1);

        pbDown = new QPushButton(frame);
        pbDown->setObjectName(QString::fromUtf8("pbDown"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/actions/section-down.svg"), QSize(), QIcon::Normal, QIcon::On);
        pbDown->setIcon(icon3);
        pbDown->setIconSize(QSize(48, 48));
        pbDown->setCheckable(true);

        gridLayout_3->addWidget(pbDown, 1, 1, 1, 1);

        label_9 = new QLabel(frame);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        QFont font;
        font.setBold(false);
        font.setWeight(50);
        label_9->setFont(font);

        gridLayout_3->addWidget(label_9, 0, 0, 1, 1);


        verticalLayout->addLayout(gridLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        verticalLayout_2->addWidget(frame);


        retranslateUi(TechDrawGui__TaskSectionView);

        QMetaObject::connectSlotsByName(TechDrawGui__TaskSectionView);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskSectionView)
    {
        TechDrawGui__TaskSectionView->setWindowTitle(QApplication::translate("TechDrawGui::TaskSectionView", "Section Parameters", nullptr));
        label_7->setText(QApplication::translate("TechDrawGui::TaskSectionView", "BaseView", nullptr));
        label_2->setText(QApplication::translate("TechDrawGui::TaskSectionView", "Symbol", nullptr));
#ifndef QT_NO_TOOLTIP
        leSymbol->setToolTip(QApplication::translate("TechDrawGui::TaskSectionView", "Identifier for this section", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("TechDrawGui::TaskSectionView", "Section Origin X", nullptr));
#ifndef QT_NO_TOOLTIP
        sbOrgX->setToolTip(QApplication::translate("TechDrawGui::TaskSectionView", "<html><head/><body><p>Location of section plane in 3D coordinates</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("TechDrawGui::TaskSectionView", "Section Origin Y", nullptr));
#ifndef QT_NO_TOOLTIP
        sbOrgY->setToolTip(QApplication::translate("TechDrawGui::TaskSectionView", "<html><head/><body><p>Location of section plane in 3D coordinates</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("TechDrawGui::TaskSectionView", "Section Origin Z", nullptr));
#ifndef QT_NO_TOOLTIP
        sbOrgZ->setToolTip(QApplication::translate("TechDrawGui::TaskSectionView", "<html><head/><body><p>Location of section plane in 3D coordinates</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pbRight->setToolTip(QApplication::translate("TechDrawGui::TaskSectionView", "Looking right", nullptr));
#endif // QT_NO_TOOLTIP
        pbRight->setText(QString());
#ifndef QT_NO_TOOLTIP
        pbUp->setToolTip(QApplication::translate("TechDrawGui::TaskSectionView", "Looking up", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        pbUp->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        pbUp->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        pbUp->setText(QString());
#ifndef QT_NO_TOOLTIP
        pbLeft->setToolTip(QApplication::translate("TechDrawGui::TaskSectionView", "Looking left", nullptr));
#endif // QT_NO_TOOLTIP
        pbLeft->setText(QString());
#ifndef QT_NO_TOOLTIP
        pbDown->setToolTip(QApplication::translate("TechDrawGui::TaskSectionView", "Looking down", nullptr));
#endif // QT_NO_TOOLTIP
        pbDown->setText(QString());
        label_9->setText(QApplication::translate("TechDrawGui::TaskSectionView", "Apply", nullptr));
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskSectionView: public Ui_TaskSectionView {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKSECTIONVIEW_H
