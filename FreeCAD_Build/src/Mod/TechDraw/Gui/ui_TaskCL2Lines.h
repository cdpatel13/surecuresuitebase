/********************************************************************************
** Form generated from reading UI file 'TaskCL2Lines.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKCL2LINES_H
#define UI_TASKCL2LINES_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace TechDrawGui {

class Ui_TaskCL2Lines
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QRadioButton *rbFlip;

    void setupUi(QWidget *TechDrawGui__TaskCL2Lines)
    {
        if (TechDrawGui__TaskCL2Lines->objectName().isEmpty())
            TechDrawGui__TaskCL2Lines->setObjectName(QString::fromUtf8("TechDrawGui__TaskCL2Lines"));
        TechDrawGui__TaskCL2Lines->resize(247, 65);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TechDrawGui__TaskCL2Lines->sizePolicy().hasHeightForWidth());
        TechDrawGui__TaskCL2Lines->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(TechDrawGui__TaskCL2Lines);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        rbFlip = new QRadioButton(TechDrawGui__TaskCL2Lines);
        rbFlip->setObjectName(QString::fromUtf8("rbFlip"));

        gridLayout->addWidget(rbFlip, 0, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);


        retranslateUi(TechDrawGui__TaskCL2Lines);

        QMetaObject::connectSlotsByName(TechDrawGui__TaskCL2Lines);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskCL2Lines)
    {
        TechDrawGui__TaskCL2Lines->setWindowTitle(QApplication::translate("TechDrawGui::TaskCL2Lines", "2 Line Parameters", nullptr));
        rbFlip->setText(QApplication::translate("TechDrawGui::TaskCL2Lines", "Flip ends", nullptr));
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskCL2Lines: public Ui_TaskCL2Lines {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKCL2LINES_H
