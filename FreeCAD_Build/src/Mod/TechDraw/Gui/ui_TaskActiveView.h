/********************************************************************************
** Form generated from reading UI file 'TaskActiveView.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKACTIVEVIEW_H
#define UI_TASKACTIVEVIEW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"
#include "Gui/Widgets.h"

QT_BEGIN_NAMESPACE

class Ui_TaskActiveView
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QFormLayout *formLayout;
    QLabel *label_2;
    Gui::QuantitySpinBox *qsbWidth;
    QLabel *label_3;
    QLabel *label_4;
    Gui::QuantitySpinBox *qsbBorder;
    QCheckBox *cbbg;
    Gui::ColorButton *ccBgColor;
    QLabel *label;
    Gui::QuantitySpinBox *qsbWeight;
    QLabel *label_5;
    QComboBox *cbMode;
    Gui::QuantitySpinBox *qsbHeight;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TaskActiveView)
    {
        if (TaskActiveView->objectName().isEmpty())
            TaskActiveView->setObjectName(QString::fromUtf8("TaskActiveView"));
        TaskActiveView->resize(423, 317);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TaskActiveView->sizePolicy().hasHeightForWidth());
        TaskActiveView->setSizePolicy(sizePolicy);
        TaskActiveView->setMinimumSize(QSize(250, 0));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/actions/techdraw-activeview.svg"), QSize(), QIcon::Normal, QIcon::Off);
        TaskActiveView->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(TaskActiveView);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(TaskActiveView);
        frame->setObjectName(QString::fromUtf8("frame"));
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        qsbWidth = new Gui::QuantitySpinBox(frame);
        qsbWidth->setObjectName(QString::fromUtf8("qsbWidth"));
        qsbWidth->setProperty("unit", QVariant(QString::fromUtf8("")));
        qsbWidth->setMinimum(0.000000000000000);
        qsbWidth->setValue(297.000000000000000);

        formLayout->setWidget(0, QFormLayout::FieldRole, qsbWidth);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_4);

        qsbBorder = new Gui::QuantitySpinBox(frame);
        qsbBorder->setObjectName(QString::fromUtf8("qsbBorder"));
        qsbBorder->setProperty("unit", QVariant(QString::fromUtf8("")));
        qsbBorder->setMinimum(0.000000000000000);

        formLayout->setWidget(2, QFormLayout::FieldRole, qsbBorder);

        cbbg = new QCheckBox(frame);
        cbbg->setObjectName(QString::fromUtf8("cbbg"));

        formLayout->setWidget(3, QFormLayout::LabelRole, cbbg);

        ccBgColor = new Gui::ColorButton(frame);
        ccBgColor->setObjectName(QString::fromUtf8("ccBgColor"));

        formLayout->setWidget(3, QFormLayout::FieldRole, ccBgColor);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label);

        qsbWeight = new Gui::QuantitySpinBox(frame);
        qsbWeight->setObjectName(QString::fromUtf8("qsbWeight"));
        qsbWeight->setProperty("unit", QVariant(QString::fromUtf8("")));
        qsbWeight->setMinimum(0.000000000000000);
        qsbWeight->setValue(0.500000000000000);

        formLayout->setWidget(4, QFormLayout::FieldRole, qsbWeight);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_5);

        cbMode = new QComboBox(frame);
        cbMode->addItem(QString());
        cbMode->addItem(QString());
        cbMode->addItem(QString());
        cbMode->addItem(QString());
        cbMode->addItem(QString());
        cbMode->addItem(QString());
        cbMode->setObjectName(QString::fromUtf8("cbMode"));

        formLayout->setWidget(5, QFormLayout::FieldRole, cbMode);

        qsbHeight = new Gui::QuantitySpinBox(frame);
        qsbHeight->setObjectName(QString::fromUtf8("qsbHeight"));
        qsbHeight->setProperty("unit", QVariant(QString::fromUtf8("")));
        qsbHeight->setMinimum(0.000000000000000);
        qsbHeight->setValue(210.000000000000000);

        formLayout->setWidget(1, QFormLayout::FieldRole, qsbHeight);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(6, QFormLayout::FieldRole, verticalSpacer);


        verticalLayout_4->addLayout(formLayout);


        verticalLayout_3->addLayout(verticalLayout_4);


        verticalLayout_2->addWidget(frame);


        retranslateUi(TaskActiveView);

        QMetaObject::connectSlotsByName(TaskActiveView);
    } // setupUi

    void retranslateUi(QWidget *TaskActiveView)
    {
        TaskActiveView->setWindowTitle(QApplication::translate("TaskActiveView", "ActiveView to TD View", nullptr));
        label_2->setText(QApplication::translate("TaskActiveView", "Width", nullptr));
#ifndef QT_NO_TOOLTIP
        qsbWidth->setToolTip(QApplication::translate("TaskActiveView", "Width of generated view", nullptr));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("TaskActiveView", "Height", nullptr));
        label_4->setText(QApplication::translate("TaskActiveView", "Border", nullptr));
#ifndef QT_NO_TOOLTIP
        qsbBorder->setToolTip(QApplication::translate("TaskActiveView", "Unused area around view", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        cbbg->setToolTip(QApplication::translate("TaskActiveView", "Paint background yes/no", nullptr));
#endif // QT_NO_TOOLTIP
        cbbg->setText(QApplication::translate("TaskActiveView", "Background", nullptr));
#ifndef QT_NO_TOOLTIP
        ccBgColor->setToolTip(QApplication::translate("TaskActiveView", "Background color", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("TaskActiveView", "Line Width", nullptr));
#ifndef QT_NO_TOOLTIP
        qsbWeight->setToolTip(QApplication::translate("TaskActiveView", "Width of lines in generated view.", nullptr));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("TaskActiveView", "Render Mode", nullptr));
        cbMode->setItemText(0, QApplication::translate("TaskActiveView", "AS_IS", nullptr));
        cbMode->setItemText(1, QApplication::translate("TaskActiveView", "WIREFRAME", nullptr));
        cbMode->setItemText(2, QApplication::translate("TaskActiveView", "POINTS", nullptr));
        cbMode->setItemText(3, QApplication::translate("TaskActiveView", "WIREFRAME_OVERLAY", nullptr));
        cbMode->setItemText(4, QApplication::translate("TaskActiveView", "HIDDEN_LINE", nullptr));
        cbMode->setItemText(5, QApplication::translate("TaskActiveView", "BOUNDING_BOX", nullptr));

#ifndef QT_NO_TOOLTIP
        cbMode->setToolTip(QApplication::translate("TaskActiveView", "Drawing style - see SoRenderManager", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        qsbHeight->setToolTip(QApplication::translate("TaskActiveView", "Height of generated view", nullptr));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

namespace Ui {
    class TaskActiveView: public Ui_TaskActiveView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKACTIVEVIEW_H
