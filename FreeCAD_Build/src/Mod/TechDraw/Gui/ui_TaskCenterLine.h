/********************************************************************************
** Form generated from reading UI file 'TaskCenterLine.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKCENTERLINE_H
#define UI_TASKCENTERLINE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Gui/QuantitySpinBox.h"
#include "Gui/Widgets.h"

namespace TechDrawGui {

class Ui_TaskCenterLine
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout;
    QLineEdit *leBaseView;
    QLabel *label_4;
    QLabel *label_2;
    QListWidget *lstSubList;
    QGridLayout *gridLayout_2;
    QRadioButton *rbVertical;
    QRadioButton *rbHorizontal;
    QRadioButton *rbAligned;
    QGridLayout *gridLayout_3;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QDoubleSpinBox *dsbRotate;
    Gui::QuantitySpinBox *qsbVertShift;
    Gui::QuantitySpinBox *qsbHorizShift;
    QFrame *line;
    QFormLayout *formLayout;
    QLabel *label;
    Gui::ColorButton *cpLineColor;
    QLabel *label_5;
    QDoubleSpinBox *dsbWeight;
    QLabel *label_6;
    QComboBox *cboxStyle;
    QLabel *label_3;
    Gui::QuantitySpinBox *qsbExtend;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TechDrawGui__TaskCenterLine)
    {
        if (TechDrawGui__TaskCenterLine->objectName().isEmpty())
            TechDrawGui__TaskCenterLine->setObjectName(QString::fromUtf8("TechDrawGui__TaskCenterLine"));
        TechDrawGui__TaskCenterLine->resize(409, 460);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TechDrawGui__TaskCenterLine->sizePolicy().hasHeightForWidth());
        TechDrawGui__TaskCenterLine->setSizePolicy(sizePolicy);
        TechDrawGui__TaskCenterLine->setMinimumSize(QSize(250, 0));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/actions/techdraw-facecenterline.svg"), QSize(), QIcon::Normal, QIcon::Off);
        TechDrawGui__TaskCenterLine->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(TechDrawGui__TaskCenterLine);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(TechDrawGui__TaskCenterLine);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        leBaseView = new QLineEdit(frame);
        leBaseView->setObjectName(QString::fromUtf8("leBaseView"));
        leBaseView->setEnabled(false);
        leBaseView->setMouseTracking(false);
        leBaseView->setFocusPolicy(Qt::NoFocus);
        leBaseView->setAcceptDrops(false);

        gridLayout->addWidget(leBaseView, 0, 1, 1, 1);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 0, 0, 1, 1);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        lstSubList = new QListWidget(frame);
        lstSubList->setObjectName(QString::fromUtf8("lstSubList"));
        lstSubList->setEnabled(false);
        lstSubList->setMaximumSize(QSize(16777215, 100));

        gridLayout->addWidget(lstSubList, 1, 1, 1, 1);


        verticalLayout_4->addLayout(gridLayout);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        rbVertical = new QRadioButton(frame);
        rbVertical->setObjectName(QString::fromUtf8("rbVertical"));
        rbVertical->setChecked(true);

        gridLayout_2->addWidget(rbVertical, 0, 0, 1, 1);

        rbHorizontal = new QRadioButton(frame);
        rbHorizontal->setObjectName(QString::fromUtf8("rbHorizontal"));
        rbHorizontal->setEnabled(true);

        gridLayout_2->addWidget(rbHorizontal, 0, 1, 1, 1);

        rbAligned = new QRadioButton(frame);
        rbAligned->setObjectName(QString::fromUtf8("rbAligned"));
        rbAligned->setEnabled(true);

        gridLayout_2->addWidget(rbAligned, 0, 2, 1, 1);


        verticalLayout_4->addLayout(gridLayout_2);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_7 = new QLabel(frame);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_3->addWidget(label_7, 0, 0, 1, 1);

        label_8 = new QLabel(frame);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_3->addWidget(label_8, 0, 2, 1, 1);

        label_9 = new QLabel(frame);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_3->addWidget(label_9, 2, 0, 1, 1);

        dsbRotate = new QDoubleSpinBox(frame);
        dsbRotate->setObjectName(QString::fromUtf8("dsbRotate"));
        dsbRotate->setMinimum(-360.000000000000000);
        dsbRotate->setMaximum(360.000000000000000);

        gridLayout_3->addWidget(dsbRotate, 2, 1, 1, 1);

        qsbVertShift = new Gui::QuantitySpinBox(frame);
        qsbVertShift->setObjectName(QString::fromUtf8("qsbVertShift"));
        qsbVertShift->setProperty("unit", QVariant(QString::fromUtf8("")));

        gridLayout_3->addWidget(qsbVertShift, 0, 3, 1, 1);

        qsbHorizShift = new Gui::QuantitySpinBox(frame);
        qsbHorizShift->setObjectName(QString::fromUtf8("qsbHorizShift"));
        qsbHorizShift->setProperty("unit", QVariant(QString::fromUtf8("")));

        gridLayout_3->addWidget(qsbHorizShift, 0, 1, 1, 1);

        gridLayout_3->setColumnStretch(1, 3);
        gridLayout_3->setColumnStretch(3, 3);

        verticalLayout_4->addLayout(gridLayout_3);

        line = new QFrame(frame);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_4->addWidget(line);

        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label);

        cpLineColor = new Gui::ColorButton(frame);
        cpLineColor->setObjectName(QString::fromUtf8("cpLineColor"));
        cpLineColor->setColor(QColor(0, 0, 0));

        formLayout->setWidget(2, QFormLayout::FieldRole, cpLineColor);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_5);

        dsbWeight = new QDoubleSpinBox(frame);
        dsbWeight->setObjectName(QString::fromUtf8("dsbWeight"));
        dsbWeight->setSingleStep(0.100000000000000);
        dsbWeight->setValue(0.500000000000000);

        formLayout->setWidget(3, QFormLayout::FieldRole, dsbWeight);

        label_6 = new QLabel(frame);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_6);

        cboxStyle = new QComboBox(frame);
        cboxStyle->addItem(QString());
        cboxStyle->addItem(QString());
        cboxStyle->addItem(QString());
        cboxStyle->addItem(QString());
        cboxStyle->addItem(QString());
        cboxStyle->setObjectName(QString::fromUtf8("cboxStyle"));

        formLayout->setWidget(4, QFormLayout::FieldRole, cboxStyle);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        qsbExtend = new Gui::QuantitySpinBox(frame);
        qsbExtend->setObjectName(QString::fromUtf8("qsbExtend"));
        qsbExtend->setValue(3.000000000000000);

        formLayout->setWidget(1, QFormLayout::FieldRole, qsbExtend);


        verticalLayout_4->addLayout(formLayout);


        verticalLayout_3->addLayout(verticalLayout_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        verticalLayout_2->addWidget(frame);


        retranslateUi(TechDrawGui__TaskCenterLine);

        cboxStyle->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(TechDrawGui__TaskCenterLine);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskCenterLine)
    {
        TechDrawGui__TaskCenterLine->setWindowTitle(QApplication::translate("TechDrawGui::TaskCenterLine", "Center Line", nullptr));
        label_4->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Base View", nullptr));
        label_2->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Elements", nullptr));
#ifndef QT_NO_TOOLTIP
        rbVertical->setToolTip(QApplication::translate("TechDrawGui::TaskCenterLine", "Top to Bottom line", nullptr));
#endif // QT_NO_TOOLTIP
        rbVertical->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Vertical", nullptr));
#ifndef QT_NO_TOOLTIP
        rbHorizontal->setToolTip(QApplication::translate("TechDrawGui::TaskCenterLine", "Left to Right line", nullptr));
#endif // QT_NO_TOOLTIP
        rbHorizontal->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Horizontal", nullptr));
#ifndef QT_NO_TOOLTIP
        rbAligned->setToolTip(QApplication::translate("TechDrawGui::TaskCenterLine", "Option not implemented yet", nullptr));
#endif // QT_NO_TOOLTIP
        rbAligned->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Aligned", nullptr));
        label_7->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Shift Horiz", nullptr));
        label_8->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Shift Vert", nullptr));
        label_9->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Rotate", nullptr));
#ifndef QT_NO_TOOLTIP
        dsbRotate->setToolTip(QApplication::translate("TechDrawGui::TaskCenterLine", "Rotate line +CCW or -CW", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        qsbVertShift->setToolTip(QApplication::translate("TechDrawGui::TaskCenterLine", "Move line +Up or -Down", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        qsbHorizShift->setToolTip(QApplication::translate("TechDrawGui::TaskCenterLine", "Move line -Left or +Right", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Color", nullptr));
        label_5->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Weight", nullptr));
        label_6->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Style", nullptr));
        cboxStyle->setItemText(0, QApplication::translate("TechDrawGui::TaskCenterLine", "Solid", nullptr));
        cboxStyle->setItemText(1, QApplication::translate("TechDrawGui::TaskCenterLine", "Dash", nullptr));
        cboxStyle->setItemText(2, QApplication::translate("TechDrawGui::TaskCenterLine", "Dot", nullptr));
        cboxStyle->setItemText(3, QApplication::translate("TechDrawGui::TaskCenterLine", "DashDot", nullptr));
        cboxStyle->setItemText(4, QApplication::translate("TechDrawGui::TaskCenterLine", "DashDotDot", nullptr));

        label_3->setText(QApplication::translate("TechDrawGui::TaskCenterLine", "Extend By", nullptr));
#ifndef QT_NO_TOOLTIP
        qsbExtend->setToolTip(QApplication::translate("TechDrawGui::TaskCenterLine", "Make the line a little longer.", nullptr));
#endif // QT_NO_TOOLTIP
        qsbExtend->setProperty("unit", QVariant(QApplication::translate("TechDrawGui::TaskCenterLine", "mm", nullptr)));
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskCenterLine: public Ui_TaskCenterLine {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKCENTERLINE_H
