/********************************************************************************
** Form generated from reading UI file 'TaskGeomHatch.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKGEOMHATCH_H
#define UI_TASKGEOMHATCH_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include "Gui/FileDialog.h"
#include "Gui/Widgets.h"

namespace TechDrawGui {

class Ui_TaskGeomHatch
{
public:
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_3;
    Gui::FileChooser *fcFile;
    QLabel *label;
    QGridLayout *gridLayout_2;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;
    QLabel *label_3;
    QComboBox *cbName;
    Gui::ColorButton *ccColor;
    QDoubleSpinBox *sbScale;
    QDoubleSpinBox *sbWeight;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TechDrawGui__TaskGeomHatch)
    {
        if (TechDrawGui__TaskGeomHatch->objectName().isEmpty())
            TechDrawGui__TaskGeomHatch->setObjectName(QString::fromUtf8("TechDrawGui__TaskGeomHatch"));
        TechDrawGui__TaskGeomHatch->resize(385, 265);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TechDrawGui__TaskGeomHatch->sizePolicy().hasHeightForWidth());
        TechDrawGui__TaskGeomHatch->setSizePolicy(sizePolicy);
        TechDrawGui__TaskGeomHatch->setMinimumSize(QSize(250, 0));
        gridLayout = new QGridLayout(TechDrawGui__TaskGeomHatch);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox = new QGroupBox(TechDrawGui__TaskGeomHatch);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        gridLayout_4 = new QGridLayout(groupBox);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        fcFile = new Gui::FileChooser(groupBox);
        fcFile->setObjectName(QString::fromUtf8("fcFile"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(fcFile->sizePolicy().hasHeightForWidth());
        fcFile->setSizePolicy(sizePolicy2);

        gridLayout_3->addWidget(fcFile, 0, 1, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_3->addWidget(label, 0, 0, 1, 1);

        gridLayout_3->setColumnStretch(1, 1);

        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 0, 0, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 2, 0, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 0, 1, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 3, 0, 1, 1);

        cbName = new QComboBox(groupBox);
        cbName->setObjectName(QString::fromUtf8("cbName"));

        gridLayout_2->addWidget(cbName, 0, 2, 1, 1);

        ccColor = new Gui::ColorButton(groupBox);
        ccColor->setObjectName(QString::fromUtf8("ccColor"));

        gridLayout_2->addWidget(ccColor, 3, 2, 1, 1);

        sbScale = new QDoubleSpinBox(groupBox);
        sbScale->setObjectName(QString::fromUtf8("sbScale"));
        sbScale->setValue(1.000000000000000);

        gridLayout_2->addWidget(sbScale, 1, 2, 1, 1);

        sbWeight = new QDoubleSpinBox(groupBox);
        sbWeight->setObjectName(QString::fromUtf8("sbWeight"));
        sbWeight->setValue(1.000000000000000);

        gridLayout_2->addWidget(sbWeight, 2, 2, 1, 1);

        gridLayout_2->setColumnStretch(0, 1);
        gridLayout_2->setColumnStretch(2, 1);

        gridLayout_4->addLayout(gridLayout_2, 1, 0, 1, 1);


        gridLayout->addWidget(groupBox, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 1, 0, 1, 1);


        retranslateUi(TechDrawGui__TaskGeomHatch);

        QMetaObject::connectSlotsByName(TechDrawGui__TaskGeomHatch);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskGeomHatch)
    {
        TechDrawGui__TaskGeomHatch->setWindowTitle(QApplication::translate("TechDrawGui::TaskGeomHatch", "Apply Geometric Hatch to Face", nullptr));
        groupBox->setTitle(QApplication::translate("TechDrawGui::TaskGeomHatch", "Define your pattern", nullptr));
#ifndef QT_NO_TOOLTIP
        fcFile->setToolTip(QApplication::translate("TechDrawGui::TaskGeomHatch", "The PAT file containing your pattern", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("TechDrawGui::TaskGeomHatch", "Pattern File", nullptr));
        label_4->setText(QApplication::translate("TechDrawGui::TaskGeomHatch", "Pattern Name", nullptr));
        label_5->setText(QApplication::translate("TechDrawGui::TaskGeomHatch", "Line Weight", nullptr));
        label_2->setText(QApplication::translate("TechDrawGui::TaskGeomHatch", "Pattern Scale", nullptr));
        label_3->setText(QApplication::translate("TechDrawGui::TaskGeomHatch", "Line Color", nullptr));
#ifndef QT_NO_TOOLTIP
        cbName->setToolTip(QApplication::translate("TechDrawGui::TaskGeomHatch", "Name of pattern within file", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        ccColor->setToolTip(QApplication::translate("TechDrawGui::TaskGeomHatch", "Color of pattern lines", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        sbScale->setToolTip(QApplication::translate("TechDrawGui::TaskGeomHatch", "Enlarges/shrinks the pattern", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        sbWeight->setToolTip(QApplication::translate("TechDrawGui::TaskGeomHatch", "Thickness of lines within the pattern", nullptr));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskGeomHatch: public Ui_TaskGeomHatch {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKGEOMHATCH_H
