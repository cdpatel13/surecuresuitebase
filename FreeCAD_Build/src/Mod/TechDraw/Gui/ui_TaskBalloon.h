/********************************************************************************
** Form generated from reading UI file 'TaskBalloon.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKBALLOON_H
#define UI_TASKBALLOON_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace TechDrawGui {

class Ui_TaskBalloon
{
public:
    QVBoxLayout *verticalLayout_3;
    QFrame *frame;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QComboBox *comboEndType;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label;
    QComboBox *comboSymbol;
    QLabel *label_2;
    QLineEdit *inputValue;
    QLineEdit *inputScale;

    void setupUi(QWidget *TechDrawGui__TaskBalloon)
    {
        if (TechDrawGui__TaskBalloon->objectName().isEmpty())
            TechDrawGui__TaskBalloon->setObjectName(QString::fromUtf8("TechDrawGui__TaskBalloon"));
        TechDrawGui__TaskBalloon->resize(400, 306);
        verticalLayout_3 = new QVBoxLayout(TechDrawGui__TaskBalloon);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        frame = new QFrame(TechDrawGui__TaskBalloon);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        comboEndType = new QComboBox(frame);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/arrowfilled.svg"), QSize(), QIcon::Normal, QIcon::Off);
        comboEndType->addItem(icon, QString());
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/arrowdot.svg"), QSize(), QIcon::Normal, QIcon::Off);
        comboEndType->addItem(icon1, QString());
        comboEndType->setObjectName(QString::fromUtf8("comboEndType"));

        gridLayout->addWidget(comboEndType, 3, 1, 1, 1);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        comboSymbol = new QComboBox(frame);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/circular.svg"), QSize(), QIcon::Normal, QIcon::Off);
        comboSymbol->addItem(icon2, QString());
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/none.svg"), QSize(), QIcon::Normal, QIcon::Off);
        comboSymbol->addItem(icon3, QString());
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/triangle.svg"), QSize(), QIcon::Normal, QIcon::Off);
        comboSymbol->addItem(icon4, QString());
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icons/inspection.svg"), QSize(), QIcon::Normal, QIcon::Off);
        comboSymbol->addItem(icon5, QString());
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/icons/hexagon.svg"), QSize(), QIcon::Normal, QIcon::Off);
        comboSymbol->addItem(icon6, QString());
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/icons/square.svg"), QSize(), QIcon::Normal, QIcon::Off);
        comboSymbol->addItem(icon7, QString());
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/icons/rectangle.svg"), QSize(), QIcon::Normal, QIcon::Off);
        comboSymbol->addItem(icon8, QString());
        comboSymbol->setObjectName(QString::fromUtf8("comboSymbol"));

        gridLayout->addWidget(comboSymbol, 2, 1, 1, 1);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        inputValue = new QLineEdit(frame);
        inputValue->setObjectName(QString::fromUtf8("inputValue"));

        gridLayout->addWidget(inputValue, 0, 1, 1, 1);

        inputScale = new QLineEdit(frame);
        inputScale->setObjectName(QString::fromUtf8("inputScale"));

        gridLayout->addWidget(inputScale, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);


        verticalLayout_2->addLayout(verticalLayout);


        verticalLayout_3->addWidget(frame);


        retranslateUi(TechDrawGui__TaskBalloon);

        QMetaObject::connectSlotsByName(TechDrawGui__TaskBalloon);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskBalloon)
    {
        TechDrawGui__TaskBalloon->setWindowTitle(QApplication::translate("TechDrawGui::TaskBalloon", "Balloon", nullptr));
        comboEndType->setItemText(0, QApplication::translate("TechDrawGui::TaskBalloon", "Arrow", nullptr));
        comboEndType->setItemText(1, QApplication::translate("TechDrawGui::TaskBalloon", "Dot", nullptr));

        label_4->setText(QApplication::translate("TechDrawGui::TaskBalloon", "Start Symbol", nullptr));
        label_3->setText(QApplication::translate("TechDrawGui::TaskBalloon", "Symbol:", nullptr));
        label->setText(QApplication::translate("TechDrawGui::TaskBalloon", "Value:", nullptr));
        comboSymbol->setItemText(0, QApplication::translate("TechDrawGui::TaskBalloon", "Circular", nullptr));
        comboSymbol->setItemText(1, QApplication::translate("TechDrawGui::TaskBalloon", "None", nullptr));
        comboSymbol->setItemText(2, QApplication::translate("TechDrawGui::TaskBalloon", "Triangle", nullptr));
        comboSymbol->setItemText(3, QApplication::translate("TechDrawGui::TaskBalloon", "Inspection", nullptr));
        comboSymbol->setItemText(4, QApplication::translate("TechDrawGui::TaskBalloon", "Hexagon", nullptr));
        comboSymbol->setItemText(5, QApplication::translate("TechDrawGui::TaskBalloon", "Square", nullptr));
        comboSymbol->setItemText(6, QApplication::translate("TechDrawGui::TaskBalloon", "Rectangle", nullptr));

        label_2->setText(QApplication::translate("TechDrawGui::TaskBalloon", "Scale:", nullptr));
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskBalloon: public Ui_TaskBalloon {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKBALLOON_H
