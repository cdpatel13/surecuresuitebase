/********************************************************************************
** Form generated from reading UI file 'DlgTemplateField.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGTEMPLATEFIELD_H
#define UI_DLGTEMPLATEFIELD_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>

namespace TechDrawGui {

class Ui_dlgTemplateField
{
public:
    QGridLayout *gridLayout_2;
    QFrame *frame;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *lblMsg;
    QLabel *lblName;
    QLabel *label;
    QLineEdit *leInput;
    QDialogButtonBox *bbButtons;

    void setupUi(QDialog *TechDrawGui__dlgTemplateField)
    {
        if (TechDrawGui__dlgTemplateField->objectName().isEmpty())
            TechDrawGui__dlgTemplateField->setObjectName(QString::fromUtf8("TechDrawGui__dlgTemplateField"));
        TechDrawGui__dlgTemplateField->setWindowModality(Qt::ApplicationModal);
        TechDrawGui__dlgTemplateField->resize(420, 160);
        TechDrawGui__dlgTemplateField->setModal(true);
        gridLayout_2 = new QGridLayout(TechDrawGui__dlgTemplateField);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(9, 9, 9, 9);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        frame = new QFrame(TechDrawGui__dlgTemplateField);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(9, 9, 9, 9);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        lblMsg = new QLabel(frame);
        lblMsg->setObjectName(QString::fromUtf8("lblMsg"));

        formLayout->setWidget(0, QFormLayout::LabelRole, lblMsg);

        lblName = new QLabel(frame);
        lblName->setObjectName(QString::fromUtf8("lblName"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lblName);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        leInput = new QLineEdit(frame);
        leInput->setObjectName(QString::fromUtf8("leInput"));

        formLayout->setWidget(1, QFormLayout::FieldRole, leInput);


        verticalLayout->addLayout(formLayout);

        bbButtons = new QDialogButtonBox(frame);
        bbButtons->setObjectName(QString::fromUtf8("bbButtons"));
        bbButtons->setOrientation(Qt::Horizontal);
        bbButtons->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        bbButtons->setCenterButtons(false);

        verticalLayout->addWidget(bbButtons);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        gridLayout_2->addWidget(frame, 0, 0, 1, 1);


        retranslateUi(TechDrawGui__dlgTemplateField);
        QObject::connect(bbButtons, SIGNAL(accepted()), TechDrawGui__dlgTemplateField, SLOT(accept()));
        QObject::connect(bbButtons, SIGNAL(rejected()), TechDrawGui__dlgTemplateField, SLOT(reject()));

        QMetaObject::connectSlotsByName(TechDrawGui__dlgTemplateField);
    } // setupUi

    void retranslateUi(QDialog *TechDrawGui__dlgTemplateField)
    {
        TechDrawGui__dlgTemplateField->setWindowTitle(QApplication::translate("TechDrawGui::dlgTemplateField", "Change Editable Field", nullptr));
        lblMsg->setText(QApplication::translate("TechDrawGui::dlgTemplateField", "Text Name:", nullptr));
        lblName->setText(QApplication::translate("TechDrawGui::dlgTemplateField", "TextLabel", nullptr));
        label->setText(QApplication::translate("TechDrawGui::dlgTemplateField", "Value:", nullptr));
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class dlgTemplateField: public Ui_dlgTemplateField {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_DLGTEMPLATEFIELD_H
