/********************************************************************************
** Form generated from reading UI file 'DlgPrefsTechDraw2.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGPREFSTECHDRAW2_H
#define UI_DLGPREFSTECHDRAW2_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
#include "Gui/PrefWidgets.h"
#include "Gui/QuantitySpinBox.h"
#include "Gui/Widgets.h"

namespace TechDrawGui {

class Ui_DlgPrefsTechDraw2Imp
{
public:
    QGridLayout *gridLayout_5;
    QGroupBox *gbDim;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout;
    Gui::PrefCheckBox *cbShowUnits;
    QLabel *label_3;
    Gui::PrefColorButton *colDimColor;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_8;
    Gui::PrefLineEdit *leDiameter;
    Gui::PrefCheckBox *cbGlobalDecimals;
    Gui::PrefComboBox *pcbArrow;
    QLabel *label_9;
    Gui::PrefSpinBox *sbAltDecimals;
    Gui::PrefUnitSpinBox *plsb_FontSize;
    Gui::PrefUnitSpinBox *plsb_ArrowSize;
    QLabel *label_15;
    Gui::PrefLineEdit *leformatSpec;
    QLabel *label_16;
    Gui::PrefComboBox *pcbStandardAndStyle;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    Gui::PrefColorButton *colCenterLine;
    Gui::PrefDoubleSpinBox *pdsb_VertexScale;
    QLabel *label_10;
    Gui::PrefComboBox *pcbMatting;
    QLabel *label_6;
    QLabel *label_4;
    QLabel *label;
    Gui::PrefComboBox *pcbCenterStyle;
    Gui::PrefComboBox *pcbSectionStyle;
    QLabel *label_5;
    QLabel *label_7;
    Gui::PrefColorButton *colSectionLine;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_13;
    Gui::PrefLineEdit *leLineGroup;
    QLabel *label_14;
    Gui::PrefColorButton *pcb_VertexColor;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *TechDrawGui__DlgPrefsTechDraw2Imp)
    {
        if (TechDrawGui__DlgPrefsTechDraw2Imp->objectName().isEmpty())
            TechDrawGui__DlgPrefsTechDraw2Imp->setObjectName(QString::fromUtf8("TechDrawGui__DlgPrefsTechDraw2Imp"));
        TechDrawGui__DlgPrefsTechDraw2Imp->resize(521, 771);
        gridLayout_5 = new QGridLayout(TechDrawGui__DlgPrefsTechDraw2Imp);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gbDim = new QGroupBox(TechDrawGui__DlgPrefsTechDraw2Imp);
        gbDim->setObjectName(QString::fromUtf8("gbDim"));
        gridLayout_4 = new QGridLayout(gbDim);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        cbShowUnits = new Gui::PrefCheckBox(gbDim);
        cbShowUnits->setObjectName(QString::fromUtf8("cbShowUnits"));
        cbShowUnits->setProperty("prefEntry", QVariant(QByteArray("ShowUnits")));
        cbShowUnits->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(cbShowUnits, 0, 0, 1, 1);

        label_3 = new QLabel(gbDim);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 5, 0, 1, 1);

        colDimColor = new Gui::PrefColorButton(gbDim);
        colDimColor->setObjectName(QString::fromUtf8("colDimColor"));
        colDimColor->setColor(QColor(0, 0, 0));
        colDimColor->setProperty("prefEntry", QVariant(QByteArray("Color")));
        colDimColor->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(colDimColor, 5, 2, 1, 1);

        label_2 = new QLabel(gbDim);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 4, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 4, 1, 1, 1);

        label_11 = new QLabel(gbDim);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 2, 0, 1, 1);

        label_12 = new QLabel(gbDim);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 8, 0, 1, 1);

        label_8 = new QLabel(gbDim);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 6, 0, 1, 1);

        leDiameter = new Gui::PrefLineEdit(gbDim);
        leDiameter->setObjectName(QString::fromUtf8("leDiameter"));
        QFont font;
        font.setPointSize(12);
        leDiameter->setFont(font);
        leDiameter->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        leDiameter->setProperty("prefEntry", QVariant(QByteArray("DiameterSymbol")));
        leDiameter->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(leDiameter, 6, 2, 1, 1);

        cbGlobalDecimals = new Gui::PrefCheckBox(gbDim);
        cbGlobalDecimals->setObjectName(QString::fromUtf8("cbGlobalDecimals"));
        cbGlobalDecimals->setChecked(true);
        cbGlobalDecimals->setProperty("prefEntry", QVariant(QByteArray("UseGlobalDecimals")));
        cbGlobalDecimals->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(cbGlobalDecimals, 1, 0, 1, 1);

        pcbArrow = new Gui::PrefComboBox(gbDim);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/arrowfilled.svg"), QSize(), QIcon::Normal, QIcon::On);
        pcbArrow->addItem(icon, QString());
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/arrowopen.svg"), QSize(), QIcon::Normal, QIcon::On);
        pcbArrow->addItem(icon1, QString());
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/arrowtick.svg"), QSize(), QIcon::Normal, QIcon::On);
        pcbArrow->addItem(icon2, QString());
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/arrowdot.svg"), QSize(), QIcon::Normal, QIcon::On);
        pcbArrow->addItem(icon3, QString());
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/arrowopendot.svg"), QSize(), QIcon::Normal, QIcon::On);
        pcbArrow->addItem(icon4, QString());
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icons/arrowfork.svg"), QSize(), QIcon::Normal, QIcon::Off);
        pcbArrow->addItem(icon5, QString());
        pcbArrow->setObjectName(QString::fromUtf8("pcbArrow"));
        pcbArrow->setMaxVisibleItems(5);
        pcbArrow->setProperty("prefEntry", QVariant(QByteArray("ArrowStyle")));
        pcbArrow->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(pcbArrow, 7, 2, 1, 1);

        label_9 = new QLabel(gbDim);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 7, 0, 1, 1);

        sbAltDecimals = new Gui::PrefSpinBox(gbDim);
        sbAltDecimals->setObjectName(QString::fromUtf8("sbAltDecimals"));
        sbAltDecimals->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        sbAltDecimals->setValue(2);
        sbAltDecimals->setProperty("prefEntry", QVariant(QByteArray("AltDecimals")));
        sbAltDecimals->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(sbAltDecimals, 2, 2, 1, 1);

        plsb_FontSize = new Gui::PrefUnitSpinBox(gbDim);
        plsb_FontSize->setObjectName(QString::fromUtf8("plsb_FontSize"));
        plsb_FontSize->setValue(5.000000000000000);
        plsb_FontSize->setProperty("prefEntry", QVariant(QByteArray("FontSize")));
        plsb_FontSize->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(plsb_FontSize, 4, 2, 1, 1);

        plsb_ArrowSize = new Gui::PrefUnitSpinBox(gbDim);
        plsb_ArrowSize->setObjectName(QString::fromUtf8("plsb_ArrowSize"));
        plsb_ArrowSize->setValue(3.500000000000000);
        plsb_ArrowSize->setProperty("prefEntry", QVariant(QByteArray("ArrowSize")));
        plsb_ArrowSize->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(plsb_ArrowSize, 8, 2, 1, 1);

        label_15 = new QLabel(gbDim);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout->addWidget(label_15, 3, 0, 1, 1);

        leformatSpec = new Gui::PrefLineEdit(gbDim);
        leformatSpec->setObjectName(QString::fromUtf8("leformatSpec"));
        leformatSpec->setProperty("prefEntry", QVariant(QByteArray("formatSpec")));
        leformatSpec->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(leformatSpec, 3, 2, 1, 1);

        label_16 = new QLabel(gbDim);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 9, 0, 1, 1);

        pcbStandardAndStyle = new Gui::PrefComboBox(gbDim);
        pcbStandardAndStyle->addItem(QString());
        pcbStandardAndStyle->addItem(QString());
        pcbStandardAndStyle->addItem(QString());
        pcbStandardAndStyle->addItem(QString());
        pcbStandardAndStyle->setObjectName(QString::fromUtf8("pcbStandardAndStyle"));
        pcbStandardAndStyle->setProperty("prefEntry", QVariant(QByteArray("StandardAndStyle")));
        pcbStandardAndStyle->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Dimensions")));

        gridLayout->addWidget(pcbStandardAndStyle, 9, 2, 1, 1);

        gridLayout->setColumnStretch(0, 1);

        gridLayout_4->addLayout(gridLayout, 0, 0, 1, 1);


        gridLayout_5->addWidget(gbDim, 0, 0, 1, 1);

        groupBox = new QGroupBox(TechDrawGui__DlgPrefsTechDraw2Imp);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        colCenterLine = new Gui::PrefColorButton(groupBox);
        colCenterLine->setObjectName(QString::fromUtf8("colCenterLine"));
        colCenterLine->setColor(QColor(175, 175, 175));
        colCenterLine->setProperty("prefEntry", QVariant(QByteArray("CenterColor")));
        colCenterLine->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Decorations")));

        gridLayout_2->addWidget(colCenterLine, 3, 2, 1, 1);

        pdsb_VertexScale = new Gui::PrefDoubleSpinBox(groupBox);
        pdsb_VertexScale->setObjectName(QString::fromUtf8("pdsb_VertexScale"));
        pdsb_VertexScale->setValue(3.000000000000000);
        pdsb_VertexScale->setProperty("prefEntry", QVariant(QByteArray("VertexScale")));
        pdsb_VertexScale->setProperty("prefPath", QVariant(QByteArray("Mod/TechDraw/General")));

        gridLayout_2->addWidget(pdsb_VertexScale, 6, 2, 1, 1);

        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_2->addWidget(label_10, 6, 0, 1, 1);

        pcbMatting = new Gui::PrefComboBox(groupBox);
        pcbMatting->addItem(QString());
        pcbMatting->addItem(QString());
        pcbMatting->setObjectName(QString::fromUtf8("pcbMatting"));
        pcbMatting->setProperty("prefEntry", QVariant(QByteArray("MattingStyle")));
        pcbMatting->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Decorations")));

        gridLayout_2->addWidget(pcbMatting, 1, 2, 1, 1);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_2->addWidget(label_6, 4, 0, 1, 1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 2, 0, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 1, 0, 1, 1);

        pcbCenterStyle = new Gui::PrefComboBox(groupBox);
        pcbCenterStyle->addItem(QString());
        pcbCenterStyle->addItem(QString());
        pcbCenterStyle->addItem(QString());
        pcbCenterStyle->addItem(QString());
        pcbCenterStyle->addItem(QString());
        pcbCenterStyle->addItem(QString());
        pcbCenterStyle->setObjectName(QString::fromUtf8("pcbCenterStyle"));
        pcbCenterStyle->setProperty("prefEntry", QVariant(QByteArray("CenterLine")));
        pcbCenterStyle->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Decorations")));

        gridLayout_2->addWidget(pcbCenterStyle, 2, 2, 1, 1);

        pcbSectionStyle = new Gui::PrefComboBox(groupBox);
        pcbSectionStyle->addItem(QString());
        pcbSectionStyle->addItem(QString());
        pcbSectionStyle->addItem(QString());
        pcbSectionStyle->addItem(QString());
        pcbSectionStyle->addItem(QString());
        pcbSectionStyle->addItem(QString());
        pcbSectionStyle->setObjectName(QString::fromUtf8("pcbSectionStyle"));
        pcbSectionStyle->setProperty("prefEntry", QVariant(QByteArray("SectionLine")));
        pcbSectionStyle->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Decorations")));

        gridLayout_2->addWidget(pcbSectionStyle, 4, 2, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 3, 0, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_2->addWidget(label_7, 5, 0, 1, 1);

        colSectionLine = new Gui::PrefColorButton(groupBox);
        colSectionLine->setObjectName(QString::fromUtf8("colSectionLine"));
        colSectionLine->setColor(QColor(175, 175, 175));
        colSectionLine->setProperty("prefEntry", QVariant(QByteArray("SectionColor")));
        colSectionLine->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Decorations")));

        gridLayout_2->addWidget(colSectionLine, 5, 2, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 2, 1, 1, 1);

        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_2->addWidget(label_13, 0, 0, 1, 1);

        leLineGroup = new Gui::PrefLineEdit(groupBox);
        leLineGroup->setObjectName(QString::fromUtf8("leLineGroup"));
        leLineGroup->setText(QString::fromUtf8("FC 0.70mm"));
        leLineGroup->setProperty("prefEntry", QVariant(QByteArray("LineGroup")));
        leLineGroup->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Decorations")));

        gridLayout_2->addWidget(leLineGroup, 0, 2, 1, 1);

        label_14 = new QLabel(groupBox);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_2->addWidget(label_14, 7, 0, 1, 1);

        pcb_VertexColor = new Gui::PrefColorButton(groupBox);
        pcb_VertexColor->setObjectName(QString::fromUtf8("pcb_VertexColor"));
        pcb_VertexColor->setColor(QColor(150, 147, 145));
        pcb_VertexColor->setProperty("prefEntry", QVariant(QByteArray("VertexColor")));
        pcb_VertexColor->setProperty("prefPath", QVariant(QByteArray("/Mod/TechDraw/Decorations")));

        gridLayout_2->addWidget(pcb_VertexColor, 7, 2, 1, 1);

        gridLayout_2->setColumnStretch(0, 1);

        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_2, 1, 0, 1, 1);


        gridLayout_5->addWidget(groupBox, 1, 0, 1, 1);


        retranslateUi(TechDrawGui__DlgPrefsTechDraw2Imp);

        pcbArrow->setCurrentIndex(0);
        pcbCenterStyle->setCurrentIndex(2);
        pcbSectionStyle->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(TechDrawGui__DlgPrefsTechDraw2Imp);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__DlgPrefsTechDraw2Imp)
    {
        TechDrawGui__DlgPrefsTechDraw2Imp->setWindowTitle(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "TechDraw Dimensions", nullptr));
        gbDim->setTitle(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Dimensions", nullptr));
#ifndef QT_NO_TOOLTIP
        cbShowUnits->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Append unit to Dimension text", nullptr));
#endif // QT_NO_TOOLTIP
        cbShowUnits->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Show Units", nullptr));
        label_3->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Color", nullptr));
#ifndef QT_NO_TOOLTIP
        colDimColor->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Dimension text color", nullptr));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Font Size", nullptr));
        label_11->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Alternate Decimals", nullptr));
        label_12->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Arrow Size", nullptr));
        label_8->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Diameter Symbol", nullptr));
#ifndef QT_NO_TOOLTIP
        leDiameter->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Character to use to indicate Diameter dimension", nullptr));
#endif // QT_NO_TOOLTIP
        leDiameter->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "\342\214\200", nullptr));
#ifndef QT_NO_TOOLTIP
        cbGlobalDecimals->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Use system setting for decimal places.", nullptr));
#endif // QT_NO_TOOLTIP
        cbGlobalDecimals->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Use Global Decimals", nullptr));
        pcbArrow->setItemText(0, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "0 - Filled Triangle", nullptr));
        pcbArrow->setItemText(1, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "1 - Open Arrowhead", nullptr));
        pcbArrow->setItemText(2, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "2 - Tick", nullptr));
        pcbArrow->setItemText(3, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "3 - Dot", nullptr));
        pcbArrow->setItemText(4, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "4 - Open Circle", nullptr));
        pcbArrow->setItemText(5, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "5 - Fork", nullptr));

#ifndef QT_NO_TOOLTIP
        pcbArrow->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Preferred arrowhead style", nullptr));
#endif // QT_NO_TOOLTIP
        label_9->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Arrow Style", nullptr));
#ifndef QT_NO_TOOLTIP
        sbAltDecimals->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Number of decimal places if not using Global Decimals", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        plsb_FontSize->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Dimension font size in units", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        plsb_ArrowSize->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Dimension arrowhead size in units", nullptr));
#endif // QT_NO_TOOLTIP
        label_15->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Default Format", nullptr));
#ifndef QT_NO_TOOLTIP
        leformatSpec->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Custom format for Dimension text", nullptr));
#endif // QT_NO_TOOLTIP
        label_16->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Dimensioning Standard and Style", nullptr));
        pcbStandardAndStyle->setItemText(0, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "ISO Oriented", nullptr));
        pcbStandardAndStyle->setItemText(1, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "ISO Referencing", nullptr));
        pcbStandardAndStyle->setItemText(2, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "ASME Inlined", nullptr));
        pcbStandardAndStyle->setItemText(3, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "ASME Referencing", nullptr));

#ifndef QT_NO_TOOLTIP
        pcbStandardAndStyle->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Preferred standard and style of drawing dimensional values", nullptr));
#endif // QT_NO_TOOLTIP
        groupBox->setTitle(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Decorations", nullptr));
#ifndef QT_NO_TOOLTIP
        colCenterLine->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Color for centerlines", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pdsb_VertexScale->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Adjusts size of vertices in drawing", nullptr));
#endif // QT_NO_TOOLTIP
        label_10->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Vertex Scale", nullptr));
        pcbMatting->setItemText(0, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Round", nullptr));
        pcbMatting->setItemText(1, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Square", nullptr));

#ifndef QT_NO_TOOLTIP
        pcbMatting->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Round or Square outline in Detail view", nullptr));
#endif // QT_NO_TOOLTIP
        label_6->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Section Line Style", nullptr));
        label_4->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Center Line Style", nullptr));
        label->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Matting Style", nullptr));
        pcbCenterStyle->setItemText(0, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "NeverShow", nullptr));
        pcbCenterStyle->setItemText(1, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Solid", nullptr));
        pcbCenterStyle->setItemText(2, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Dash", nullptr));
        pcbCenterStyle->setItemText(3, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Dot", nullptr));
        pcbCenterStyle->setItemText(4, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "DashDot", nullptr));
        pcbCenterStyle->setItemText(5, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "DashDotDot", nullptr));

#ifndef QT_NO_TOOLTIP
        pcbCenterStyle->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Line type for centerlines", nullptr));
#endif // QT_NO_TOOLTIP
        pcbSectionStyle->setItemText(0, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "NeverShow", nullptr));
        pcbSectionStyle->setItemText(1, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Solid", nullptr));
        pcbSectionStyle->setItemText(2, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Dash", nullptr));
        pcbSectionStyle->setItemText(3, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Dot", nullptr));
        pcbSectionStyle->setItemText(4, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "DashDot", nullptr));
        pcbSectionStyle->setItemText(5, QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "DashDotDot", nullptr));

        label_5->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Center Line Color", nullptr));
        label_7->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Section Line Color", nullptr));
#ifndef QT_NO_TOOLTIP
        colSectionLine->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Line color for sectionlines", nullptr));
#endif // QT_NO_TOOLTIP
        label_13->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Line Group Name", nullptr));
#ifndef QT_NO_TOOLTIP
        leLineGroup->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Name of entry in LineGroup CSV file", nullptr));
#endif // QT_NO_TOOLTIP
        label_14->setText(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Vertex Color", nullptr));
#ifndef QT_NO_TOOLTIP
        pcb_VertexColor->setToolTip(QApplication::translate("TechDrawGui::DlgPrefsTechDraw2Imp", "Vertex display color", nullptr));
#endif // QT_NO_TOOLTIP
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class DlgPrefsTechDraw2Imp: public Ui_DlgPrefsTechDraw2Imp {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_DLGPREFSTECHDRAW2_H
