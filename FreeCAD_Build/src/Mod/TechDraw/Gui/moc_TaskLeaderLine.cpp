/****************************************************************************
** Meta object code from reading C++ file 'TaskLeaderLine.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../FreeCAD/src/Mod/TechDraw/Gui/TaskLeaderLine.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TaskLeaderLine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TechDrawGui__TaskLeaderLine_t {
    QByteArrayData data[11];
    char stringdata0[149];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TechDrawGui__TaskLeaderLine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TechDrawGui__TaskLeaderLine_t qt_meta_stringdata_TechDrawGui__TaskLeaderLine = {
    {
QT_MOC_LITERAL(0, 0, 27), // "TechDrawGui::TaskLeaderLine"
QT_MOC_LITERAL(1, 28, 16), // "onTrackerClicked"
QT_MOC_LITERAL(2, 45, 0), // ""
QT_MOC_LITERAL(3, 46, 1), // "b"
QT_MOC_LITERAL(4, 48, 19), // "onCancelEditClicked"
QT_MOC_LITERAL(5, 68, 17), // "onTrackerFinished"
QT_MOC_LITERAL(6, 86, 20), // "std::vector<QPointF>"
QT_MOC_LITERAL(7, 107, 3), // "pts"
QT_MOC_LITERAL(8, 111, 8), // "QGIView*"
QT_MOC_LITERAL(9, 120, 8), // "qgParent"
QT_MOC_LITERAL(10, 129, 19) // "onPointEditComplete"

    },
    "TechDrawGui::TaskLeaderLine\0"
    "onTrackerClicked\0\0b\0onCancelEditClicked\0"
    "onTrackerFinished\0std::vector<QPointF>\0"
    "pts\0QGIView*\0qgParent\0onPointEditComplete"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TechDrawGui__TaskLeaderLine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x0a /* Public */,
       4,    1,   37,    2, 0x0a /* Public */,
       5,    2,   40,    2, 0x0a /* Public */,
      10,    0,   45,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 8,    7,    9,
    QMetaType::Void,

       0        // eod
};

void TechDrawGui::TaskLeaderLine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TaskLeaderLine *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onTrackerClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->onCancelEditClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->onTrackerFinished((*reinterpret_cast< std::vector<QPointF>(*)>(_a[1])),(*reinterpret_cast< QGIView*(*)>(_a[2]))); break;
        case 3: _t->onPointEditComplete(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TechDrawGui::TaskLeaderLine::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_TechDrawGui__TaskLeaderLine.data,
    qt_meta_data_TechDrawGui__TaskLeaderLine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TechDrawGui::TaskLeaderLine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TechDrawGui::TaskLeaderLine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TechDrawGui__TaskLeaderLine.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int TechDrawGui::TaskLeaderLine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_TechDrawGui__TaskDlgLeaderLine_t {
    QByteArrayData data[1];
    char stringdata0[31];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TechDrawGui__TaskDlgLeaderLine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TechDrawGui__TaskDlgLeaderLine_t qt_meta_stringdata_TechDrawGui__TaskDlgLeaderLine = {
    {
QT_MOC_LITERAL(0, 0, 30) // "TechDrawGui::TaskDlgLeaderLine"

    },
    "TechDrawGui::TaskDlgLeaderLine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TechDrawGui__TaskDlgLeaderLine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void TechDrawGui::TaskDlgLeaderLine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject TechDrawGui::TaskDlgLeaderLine::staticMetaObject = { {
    &Gui::TaskView::TaskDialog::staticMetaObject,
    qt_meta_stringdata_TechDrawGui__TaskDlgLeaderLine.data,
    qt_meta_data_TechDrawGui__TaskDlgLeaderLine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TechDrawGui::TaskDlgLeaderLine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TechDrawGui::TaskDlgLeaderLine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TechDrawGui__TaskDlgLeaderLine.stringdata0))
        return static_cast<void*>(this);
    return Gui::TaskView::TaskDialog::qt_metacast(_clname);
}

int TechDrawGui::TaskDlgLeaderLine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Gui::TaskView::TaskDialog::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
