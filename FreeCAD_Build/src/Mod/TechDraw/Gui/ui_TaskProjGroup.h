/********************************************************************************
** Form generated from reading UI file 'TaskProjGroup.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKPROJGROUP_H
#define UI_TASKPROJGROUP_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace TechDrawGui {

class Ui_TaskProjGroup
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_7;
    QComboBox *projection;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label;
    QComboBox *cmbScaleType;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_3;
    QSpinBox *sbScaleNum;
    QLabel *label_4;
    QSpinBox *sbScaleDen;
    QFrame *line;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_5;
    QGridLayout *gridLayout;
    QLineEdit *lePrimary;
    QPushButton *butRightRotate;
    QPushButton *butTopRotate;
    QPushButton *butLeftRotate;
    QPushButton *butDownRotate;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_8;
    QFrame *line_2;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_6;
    QGridLayout *gridLayout_2;
    QCheckBox *chkView8;
    QSpacerItem *horizontalSpacer;
    QCheckBox *chkView4;
    QCheckBox *chkView5;
    QSpacerItem *horizontalSpacer_2;
    QCheckBox *chkView3;
    QCheckBox *chkView7;
    QCheckBox *chkView1;
    QCheckBox *chkView9;
    QCheckBox *chkView2;
    QCheckBox *chkView6;
    QCheckBox *chkView0;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *butCWRotate;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *butCCWRotate;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TechDrawGui__TaskProjGroup)
    {
        if (TechDrawGui__TaskProjGroup->objectName().isEmpty())
            TechDrawGui__TaskProjGroup->setObjectName(QString::fromUtf8("TechDrawGui__TaskProjGroup"));
        TechDrawGui__TaskProjGroup->resize(371, 511);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TechDrawGui__TaskProjGroup->sizePolicy().hasHeightForWidth());
        TechDrawGui__TaskProjGroup->setSizePolicy(sizePolicy);
        TechDrawGui__TaskProjGroup->setMinimumSize(QSize(250, 0));
        verticalLayout_2 = new QVBoxLayout(TechDrawGui__TaskProjGroup);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(TechDrawGui__TaskProjGroup);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_7 = new QLabel(frame);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_3->addWidget(label_7);

        projection = new QComboBox(frame);
        projection->addItem(QString());
        projection->addItem(QString());
        projection->addItem(QString());
        projection->setObjectName(QString::fromUtf8("projection"));
        projection->setEditable(false);

        horizontalLayout_3->addWidget(projection);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_5->addWidget(label);

        cmbScaleType = new QComboBox(frame);
        cmbScaleType->addItem(QString());
        cmbScaleType->addItem(QString());
        cmbScaleType->addItem(QString());
        cmbScaleType->setObjectName(QString::fromUtf8("cmbScaleType"));

        horizontalLayout_5->addWidget(cmbScaleType);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_6->addWidget(label_2);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_3);

        sbScaleNum = new QSpinBox(frame);
        sbScaleNum->setObjectName(QString::fromUtf8("sbScaleNum"));
        sbScaleNum->setMinimum(1);
        sbScaleNum->setValue(1);

        horizontalLayout_6->addWidget(sbScaleNum);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_6->addWidget(label_4);

        sbScaleDen = new QSpinBox(frame);
        sbScaleDen->setObjectName(QString::fromUtf8("sbScaleDen"));
        sbScaleDen->setMinimum(1);
        sbScaleDen->setMaximum(999);
        sbScaleDen->setValue(1);

        horizontalLayout_6->addWidget(sbScaleDen);

        horizontalLayout_6->setStretch(2, 1);
        horizontalLayout_6->setStretch(4, 1);

        verticalLayout->addLayout(horizontalLayout_6);

        line = new QFrame(frame);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy2);
        label_5->setScaledContents(true);
        label_5->setAlignment(Qt::AlignCenter);
        label_5->setIndent(0);

        horizontalLayout_4->addWidget(label_5);


        verticalLayout->addLayout(horizontalLayout_4);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lePrimary = new QLineEdit(frame);
        lePrimary->setObjectName(QString::fromUtf8("lePrimary"));
        lePrimary->setEnabled(false);
        QFont font;
        font.setPointSize(11);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        lePrimary->setFont(font);
        lePrimary->setFocusPolicy(Qt::NoFocus);
        lePrimary->setAlignment(Qt::AlignCenter);
        lePrimary->setReadOnly(true);

        gridLayout->addWidget(lePrimary, 1, 1, 1, 1);

        butRightRotate = new QPushButton(frame);
        butRightRotate->setObjectName(QString::fromUtf8("butRightRotate"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/arrow-right.svg"), QSize(), QIcon::Normal, QIcon::On);
        butRightRotate->setIcon(icon);
        butRightRotate->setIconSize(QSize(24, 24));

        gridLayout->addWidget(butRightRotate, 1, 2, 1, 1);

        butTopRotate = new QPushButton(frame);
        butTopRotate->setObjectName(QString::fromUtf8("butTopRotate"));
        sizePolicy2.setHeightForWidth(butTopRotate->sizePolicy().hasHeightForWidth());
        butTopRotate->setSizePolicy(sizePolicy2);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/arrow-up.svg"), QSize(), QIcon::Normal, QIcon::On);
        butTopRotate->setIcon(icon1);
        butTopRotate->setIconSize(QSize(24, 24));

        gridLayout->addWidget(butTopRotate, 0, 1, 1, 1);

        butLeftRotate = new QPushButton(frame);
        butLeftRotate->setObjectName(QString::fromUtf8("butLeftRotate"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/arrow-left.svg"), QSize(), QIcon::Normal, QIcon::On);
        butLeftRotate->setIcon(icon2);
        butLeftRotate->setIconSize(QSize(24, 24));

        gridLayout->addWidget(butLeftRotate, 1, 0, 1, 1);

        butDownRotate = new QPushButton(frame);
        butDownRotate->setObjectName(QString::fromUtf8("butDownRotate"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/arrow-down.svg"), QSize(), QIcon::Normal, QIcon::On);
        butDownRotate->setIcon(icon3);
        butDownRotate->setIconSize(QSize(24, 24));

        gridLayout->addWidget(butDownRotate, 2, 1, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_7, 2, 2, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_8, 2, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        line_2 = new QFrame(frame);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_6 = new QLabel(frame);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(label_6);


        verticalLayout->addLayout(horizontalLayout_8);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        chkView8 = new QCheckBox(frame);
        chkView8->setObjectName(QString::fromUtf8("chkView8"));
        chkView8->setEnabled(true);
        chkView8->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));

        gridLayout_2->addWidget(chkView8, 5, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 3, 0, 1, 1);

        chkView4 = new QCheckBox(frame);
        chkView4->setObjectName(QString::fromUtf8("chkView4"));
        chkView4->setEnabled(false);
        chkView4->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));
        chkView4->setChecked(true);

        gridLayout_2->addWidget(chkView4, 3, 2, 1, 1);

        chkView5 = new QCheckBox(frame);
        chkView5->setObjectName(QString::fromUtf8("chkView5"));
        chkView5->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));

        gridLayout_2->addWidget(chkView5, 3, 3, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 3, 5, 1, 1);

        chkView3 = new QCheckBox(frame);
        chkView3->setObjectName(QString::fromUtf8("chkView3"));
        chkView3->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));

        gridLayout_2->addWidget(chkView3, 3, 1, 1, 1);

        chkView7 = new QCheckBox(frame);
        chkView7->setObjectName(QString::fromUtf8("chkView7"));
        chkView7->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));

        gridLayout_2->addWidget(chkView7, 5, 1, 1, 1);

        chkView1 = new QCheckBox(frame);
        chkView1->setObjectName(QString::fromUtf8("chkView1"));
        chkView1->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));

        gridLayout_2->addWidget(chkView1, 2, 2, 1, 1);

        chkView9 = new QCheckBox(frame);
        chkView9->setObjectName(QString::fromUtf8("chkView9"));
        chkView9->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));

        gridLayout_2->addWidget(chkView9, 5, 3, 1, 1);

        chkView2 = new QCheckBox(frame);
        chkView2->setObjectName(QString::fromUtf8("chkView2"));
        chkView2->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));

        gridLayout_2->addWidget(chkView2, 2, 3, 1, 1);

        chkView6 = new QCheckBox(frame);
        chkView6->setObjectName(QString::fromUtf8("chkView6"));
        chkView6->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));

        gridLayout_2->addWidget(chkView6, 3, 4, 1, 1);

        chkView0 = new QCheckBox(frame);
        chkView0->setObjectName(QString::fromUtf8("chkView0"));
        chkView0->setStyleSheet(QString::fromUtf8("QCheckBox::indicator {\n"
"width: 24px;\n"
"height: 24px;\n"
"}\n"
""));
        chkView0->setIconSize(QSize(24, 24));

        gridLayout_2->addWidget(chkView0, 2, 1, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        butCWRotate = new QPushButton(frame);
        butCWRotate->setObjectName(QString::fromUtf8("butCWRotate"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/arrow-cw.svg"), QSize(), QIcon::Normal, QIcon::On);
        butCWRotate->setIcon(icon4);

        horizontalLayout->addWidget(butCWRotate);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_6);

        butCCWRotate = new QPushButton(frame);
        butCCWRotate->setObjectName(QString::fromUtf8("butCCWRotate"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icons/arrow-ccw.svg"), QSize(), QIcon::Normal, QIcon::On);
        butCCWRotate->setIcon(icon5);

        horizontalLayout->addWidget(butCCWRotate);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_3->addLayout(verticalLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        verticalLayout_2->addWidget(frame);


        retranslateUi(TechDrawGui__TaskProjGroup);

        QMetaObject::connectSlotsByName(TechDrawGui__TaskProjGroup);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskProjGroup)
    {
        TechDrawGui__TaskProjGroup->setWindowTitle(QApplication::translate("TechDrawGui::TaskProjGroup", "Projection Group", nullptr));
        label_7->setText(QApplication::translate("TechDrawGui::TaskProjGroup", "Projection", nullptr));
        projection->setItemText(0, QApplication::translate("TechDrawGui::TaskProjGroup", "Page", nullptr));
        projection->setItemText(1, QApplication::translate("TechDrawGui::TaskProjGroup", "First Angle", nullptr));
        projection->setItemText(2, QApplication::translate("TechDrawGui::TaskProjGroup", "Third Angle", nullptr));

#ifndef QT_NO_TOOLTIP
        projection->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "First or Third Angle", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("TechDrawGui::TaskProjGroup", "Scale", nullptr));
        cmbScaleType->setItemText(0, QApplication::translate("TechDrawGui::TaskProjGroup", "Page", nullptr));
        cmbScaleType->setItemText(1, QApplication::translate("TechDrawGui::TaskProjGroup", "Automatic", nullptr));
        cmbScaleType->setItemText(2, QApplication::translate("TechDrawGui::TaskProjGroup", "Custom", nullptr));

#ifndef QT_NO_TOOLTIP
        cmbScaleType->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Scale Page/Auto/Custom", nullptr));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("TechDrawGui::TaskProjGroup", "Custom Scale", nullptr));
#ifndef QT_NO_TOOLTIP
        sbScaleNum->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Scale Numerator", nullptr));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("TechDrawGui::TaskProjGroup", ":", nullptr));
#ifndef QT_NO_TOOLTIP
        sbScaleDen->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Scale Denominator", nullptr));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("TechDrawGui::TaskProjGroup", "Adjust Primary Direction", nullptr));
#ifndef QT_NO_TOOLTIP
        lePrimary->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Current primary view direction", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        butRightRotate->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Rotate right", nullptr));
#endif // QT_NO_TOOLTIP
        butRightRotate->setText(QString());
#ifndef QT_NO_TOOLTIP
        butTopRotate->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Rotate up", nullptr));
#endif // QT_NO_TOOLTIP
        butTopRotate->setText(QString());
#ifndef QT_NO_TOOLTIP
        butLeftRotate->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Rotate left", nullptr));
#endif // QT_NO_TOOLTIP
        butLeftRotate->setText(QString());
#ifndef QT_NO_TOOLTIP
        butDownRotate->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Rotate down", nullptr));
#endif // QT_NO_TOOLTIP
        butDownRotate->setText(QString());
        label_6->setText(QApplication::translate("TechDrawGui::TaskProjGroup", "Secondary Projections", nullptr));
#ifndef QT_NO_TOOLTIP
        chkView8->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Bottom", nullptr));
#endif // QT_NO_TOOLTIP
        chkView8->setText(QString());
#ifndef QT_NO_TOOLTIP
        chkView4->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Primary", nullptr));
#endif // QT_NO_TOOLTIP
        chkView4->setText(QString());
#ifndef QT_NO_TOOLTIP
        chkView5->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Right", nullptr));
#endif // QT_NO_TOOLTIP
        chkView5->setText(QString());
#ifndef QT_NO_TOOLTIP
        chkView3->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Left", nullptr));
#endif // QT_NO_TOOLTIP
        chkView3->setText(QString());
#ifndef QT_NO_TOOLTIP
        chkView7->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "LeftFrontBottom", nullptr));
#endif // QT_NO_TOOLTIP
        chkView7->setText(QString());
#ifndef QT_NO_TOOLTIP
        chkView1->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Top", nullptr));
#endif // QT_NO_TOOLTIP
        chkView1->setText(QString());
#ifndef QT_NO_TOOLTIP
        chkView9->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "RightFrontBottom", nullptr));
#endif // QT_NO_TOOLTIP
        chkView9->setText(QString());
#ifndef QT_NO_TOOLTIP
        chkView2->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "RightFrontTop", nullptr));
#endif // QT_NO_TOOLTIP
        chkView2->setText(QString());
#ifndef QT_NO_TOOLTIP
        chkView6->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Rear", nullptr));
#endif // QT_NO_TOOLTIP
        chkView6->setText(QString());
#ifndef QT_NO_TOOLTIP
        chkView0->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "LeftFrontTop", nullptr));
#endif // QT_NO_TOOLTIP
        chkView0->setText(QString());
#ifndef QT_NO_TOOLTIP
        butCWRotate->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Spin CW", nullptr));
#endif // QT_NO_TOOLTIP
        butCWRotate->setText(QString());
#ifndef QT_NO_TOOLTIP
        butCCWRotate->setToolTip(QApplication::translate("TechDrawGui::TaskProjGroup", "Spin CCW", nullptr));
#endif // QT_NO_TOOLTIP
        butCCWRotate->setText(QString());
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskProjGroup: public Ui_TaskProjGroup {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKPROJGROUP_H
