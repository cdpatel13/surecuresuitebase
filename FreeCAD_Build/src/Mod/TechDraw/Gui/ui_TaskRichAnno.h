/********************************************************************************
** Form generated from reading UI file 'TaskRichAnno.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKRICHANNO_H
#define UI_TASKRICHANNO_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace TechDrawGui {

class Ui_TaskRichAnno
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_5;
    QGridLayout *gridLayout;
    QDoubleSpinBox *dsbMaxWidth;
    QLabel *label_2;
    QLabel *label;
    QLineEdit *leBaseView;
    QSpacerItem *horizontalSpacer;
    QCheckBox *cbShowFrame;
    QLabel *label_3;
    QFrame *line;
    QPushButton *pbEditor;
    QTextEdit *teAnnoText;

    void setupUi(QWidget *TechDrawGui__TaskRichAnno)
    {
        if (TechDrawGui__TaskRichAnno->objectName().isEmpty())
            TechDrawGui__TaskRichAnno->setObjectName(QString::fromUtf8("TechDrawGui__TaskRichAnno"));
        TechDrawGui__TaskRichAnno->resize(409, 386);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TechDrawGui__TaskRichAnno->sizePolicy().hasHeightForWidth());
        TechDrawGui__TaskRichAnno->setSizePolicy(sizePolicy);
        TechDrawGui__TaskRichAnno->setMinimumSize(QSize(250, 0));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/actions/techdraw-textleader.svg"), QSize(), QIcon::Normal, QIcon::Off);
        TechDrawGui__TaskRichAnno->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(TechDrawGui__TaskRichAnno);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(TechDrawGui__TaskRichAnno);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setVerticalSpacing(0);
        dsbMaxWidth = new QDoubleSpinBox(frame);
        dsbMaxWidth->setObjectName(QString::fromUtf8("dsbMaxWidth"));
        dsbMaxWidth->setMinimum(-1.000000000000000);
        dsbMaxWidth->setValue(-1.000000000000000);

        gridLayout->addWidget(dsbMaxWidth, 1, 2, 1, 1);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        leBaseView = new QLineEdit(frame);
        leBaseView->setObjectName(QString::fromUtf8("leBaseView"));
        leBaseView->setEnabled(false);

        gridLayout->addWidget(leBaseView, 0, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 2, 1, 1, 1);

        cbShowFrame = new QCheckBox(frame);
        cbShowFrame->setObjectName(QString::fromUtf8("cbShowFrame"));
        cbShowFrame->setLayoutDirection(Qt::LeftToRight);

        gridLayout->addWidget(cbShowFrame, 2, 2, 1, 1);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        gridLayout->setColumnStretch(0, 3);
        gridLayout->setColumnStretch(1, 1);
        gridLayout->setColumnStretch(2, 3);

        verticalLayout_5->addLayout(gridLayout);

        line = new QFrame(frame);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_5->addWidget(line);

        pbEditor = new QPushButton(frame);
        pbEditor->setObjectName(QString::fromUtf8("pbEditor"));
        pbEditor->setEnabled(true);

        verticalLayout_5->addWidget(pbEditor);

        teAnnoText = new QTextEdit(frame);
        teAnnoText->setObjectName(QString::fromUtf8("teAnnoText"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(teAnnoText->sizePolicy().hasHeightForWidth());
        teAnnoText->setSizePolicy(sizePolicy2);

        verticalLayout_5->addWidget(teAnnoText);


        verticalLayout_3->addLayout(verticalLayout_5);


        verticalLayout_2->addWidget(frame);


        retranslateUi(TechDrawGui__TaskRichAnno);

        QMetaObject::connectSlotsByName(TechDrawGui__TaskRichAnno);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskRichAnno)
    {
        TechDrawGui__TaskRichAnno->setWindowTitle(QApplication::translate("TechDrawGui::TaskRichAnno", "Rich Text Annotation Block", nullptr));
        label_2->setText(QApplication::translate("TechDrawGui::TaskRichAnno", "Max Width", nullptr));
        label->setText(QApplication::translate("TechDrawGui::TaskRichAnno", "Base Feature", nullptr));
        cbShowFrame->setText(QString());
        label_3->setText(QApplication::translate("TechDrawGui::TaskRichAnno", "Show Frame", nullptr));
        pbEditor->setText(QApplication::translate("TechDrawGui::TaskRichAnno", "Start Rich Text Editor", nullptr));
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskRichAnno: public Ui_TaskRichAnno {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKRICHANNO_H
