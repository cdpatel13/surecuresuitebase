/********************************************************************************
** Form generated from reading UI file 'TaskCosVertex.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKCOSVERTEX_H
#define UI_TASKCOSVERTEX_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

namespace TechDrawGui {

class Ui_TaskCosVertex
{
public:
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout;
    QLineEdit *leBaseView;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pbTracker;
    QSpacerItem *horizontalSpacer_2;
    QFrame *line;
    QGridLayout *gridLayout_2;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label;
    QLabel *label_3;
    QDoubleSpinBox *dsbX;
    QDoubleSpinBox *dsbY;
    QDoubleSpinBox *dsbZ;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *TechDrawGui__TaskCosVertex)
    {
        if (TechDrawGui__TaskCosVertex->objectName().isEmpty())
            TechDrawGui__TaskCosVertex->setObjectName(QString::fromUtf8("TechDrawGui__TaskCosVertex"));
        TechDrawGui__TaskCosVertex->resize(409, 405);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TechDrawGui__TaskCosVertex->sizePolicy().hasHeightForWidth());
        TechDrawGui__TaskCosVertex->setSizePolicy(sizePolicy);
        TechDrawGui__TaskCosVertex->setMinimumSize(QSize(250, 0));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/actions/techdraw-point.svg"), QSize(), QIcon::Normal, QIcon::Off);
        TechDrawGui__TaskCosVertex->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(TechDrawGui__TaskCosVertex);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(TechDrawGui__TaskCosVertex);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setMinimumSize(QSize(300, 300));
        frame->setBaseSize(QSize(300, 300));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        leBaseView = new QLineEdit(frame);
        leBaseView->setObjectName(QString::fromUtf8("leBaseView"));
        leBaseView->setEnabled(false);
        leBaseView->setMouseTracking(false);
        leBaseView->setFocusPolicy(Qt::NoFocus);
        leBaseView->setAcceptDrops(false);

        gridLayout->addWidget(leBaseView, 0, 1, 1, 1);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 0, 0, 1, 1);


        verticalLayout_4->addLayout(gridLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pbTracker = new QPushButton(frame);
        pbTracker->setObjectName(QString::fromUtf8("pbTracker"));

        horizontalLayout->addWidget(pbTracker);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_4->addLayout(horizontalLayout);

        line = new QFrame(frame);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_4->addWidget(line);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 0, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 0, 1, 1, 1);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 2, 0, 1, 1);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 1, 0, 1, 1);

        dsbX = new QDoubleSpinBox(frame);
        dsbX->setObjectName(QString::fromUtf8("dsbX"));
        dsbX->setDecimals(4);
        dsbX->setMinimum(-2147483647.000000000000000);
        dsbX->setMaximum(2147483647.000000000000000);

        gridLayout_2->addWidget(dsbX, 0, 2, 1, 1);

        dsbY = new QDoubleSpinBox(frame);
        dsbY->setObjectName(QString::fromUtf8("dsbY"));
        dsbY->setDecimals(4);
        dsbY->setMinimum(-2147483647.000000000000000);
        dsbY->setMaximum(2147483647.000000000000000);

        gridLayout_2->addWidget(dsbY, 1, 2, 1, 1);

        dsbZ = new QDoubleSpinBox(frame);
        dsbZ->setObjectName(QString::fromUtf8("dsbZ"));
        dsbZ->setDecimals(4);
        dsbZ->setMinimum(-2147483647.000000000000000);
        dsbZ->setMaximum(2147483647.000000000000000);

        gridLayout_2->addWidget(dsbZ, 2, 2, 1, 1);

        gridLayout_2->setColumnStretch(0, 3);
        gridLayout_2->setColumnStretch(2, 3);

        verticalLayout_4->addLayout(gridLayout_2);


        verticalLayout_3->addLayout(verticalLayout_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        verticalLayout_2->addWidget(frame);


        retranslateUi(TechDrawGui__TaskCosVertex);

        QMetaObject::connectSlotsByName(TechDrawGui__TaskCosVertex);
    } // setupUi

    void retranslateUi(QWidget *TechDrawGui__TaskCosVertex)
    {
        TechDrawGui__TaskCosVertex->setWindowTitle(QApplication::translate("TechDrawGui::TaskCosVertex", "Cosmetic Vertex", nullptr));
        label_4->setText(QApplication::translate("TechDrawGui::TaskCosVertex", "Base View", nullptr));
        pbTracker->setText(QApplication::translate("TechDrawGui::TaskCosVertex", "Point Picker", nullptr));
        label_2->setText(QApplication::translate("TechDrawGui::TaskCosVertex", "X", nullptr));
        label->setText(QApplication::translate("TechDrawGui::TaskCosVertex", "Z", nullptr));
        label_3->setText(QApplication::translate("TechDrawGui::TaskCosVertex", "Y", nullptr));
    } // retranslateUi

};

} // namespace TechDrawGui

namespace TechDrawGui {
namespace Ui {
    class TaskCosVertex: public Ui_TaskCosVertex {};
} // namespace Ui
} // namespace TechDrawGui

#endif // UI_TASKCOSVERTEX_H
