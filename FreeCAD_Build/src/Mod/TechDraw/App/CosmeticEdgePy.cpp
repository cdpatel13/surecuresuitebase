
// This file is generated by src/Tools/generateTemaplates/templateClassPyExport.py out of the .XML file
// Every change you make here gets lost in the next full rebuild!
// This File is normally built as an include in CosmeticEdgePyImp.cpp! It's not intended to be in a project!

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/exception.hpp>
#include <Base/PyObjectBase.h>
#include <Base/Console.h>
#include <Base/Exception.h>
#include <CXX/Objects.hxx>

using Base::streq;
using namespace TechDraw;

/// Type structure of CosmeticEdgePy
PyTypeObject CosmeticEdgePy::Type = {
    PyVarObject_HEAD_INIT(&PyType_Type,0)
    "TechDraw.CosmeticEdge",     /*tp_name*/
    sizeof(CosmeticEdgePy),                       /*tp_basicsize*/
    0,                                                /*tp_itemsize*/
    /* methods */
    PyDestructor,                                     /*tp_dealloc*/
    0,                                                /*tp_print*/
    0,                                                /*tp_getattr*/
    0,                                                /*tp_setattr*/
    0,                                                /*tp_compare*/
    __repr,                                           /*tp_repr*/
    0,                                                /*tp_as_number*/
    0,                                                /*tp_as_sequence*/
    0,                                                /*tp_as_mapping*/
    0,                                                /*tp_hash*/
    0,                                                /*tp_call */
    0,                                                /*tp_str  */
    __getattro,                                       /*tp_getattro*/
    __setattro,                                       /*tp_setattro*/
    /* --- Functions to access object as input/output buffer ---------*/
    0,                                                /* tp_as_buffer */
    /* --- Flags to define presence of optional/expanded features */
#if PY_MAJOR_VERSION >= 3
    Py_TPFLAGS_BASETYPE|Py_TPFLAGS_DEFAULT,        /*tp_flags */
#else
    Py_TPFLAGS_DEFAULT,        /*tp_flags */
#endif
    "CosmeticEdge specifies an extra (cosmetic) edge in Views",           /*tp_doc */
    0,                                                /*tp_traverse */
    0,                                                /*tp_clear */
    0,                                                /*tp_richcompare */
    0,                                                /*tp_weaklistoffset */
    0,                                                /*tp_iter */
    0,                                                /*tp_iternext */
    TechDraw::CosmeticEdgePy::Methods,                     /*tp_methods */
    0,                                                /*tp_members */
    TechDraw::CosmeticEdgePy::GetterSetter,                     /*tp_getset */
    &Base::PyObjectBase::Type,                        /*tp_base */
    0,                                                /*tp_dict */
    0,                                                /*tp_descr_get */
    0,                                                /*tp_descr_set */
    0,                                                /*tp_dictoffset */
    __PyInit,                                         /*tp_init */
    0,                                                /*tp_alloc */
    TechDraw::CosmeticEdgePy::PyMake,/*tp_new */
    0,                                                /*tp_free   Low-level free-memory routine */
    0,                                                /*tp_is_gc  For PyObject_IS_GC */
    0,                                                /*tp_bases */
    0,                                                /*tp_mro    method resolution order */
    0,                                                /*tp_cache */
    0,                                                /*tp_subclasses */
    0,                                                /*tp_weaklist */
    0,                                                /*tp_del */
    0                                                 /*tp_version_tag */
#if PY_MAJOR_VERSION >= 3
    ,0                                                /*tp_finalize */
#endif
};

/// Methods structure of CosmeticEdgePy
PyMethodDef CosmeticEdgePy::Methods[] = {
    {"clone",
        reinterpret_cast<PyCFunction>( staticCallback_clone ),
        METH_VARARGS,
        "Create a clone of this CosmeticEdge"
    },
    {"copy",
        reinterpret_cast<PyCFunction>( staticCallback_copy ),
        METH_VARARGS,
        "Create a copy of this CosmeticEdge"
    },
    {"setFormat",
        reinterpret_cast<PyCFunction>( staticCallback_setFormat ),
        METH_VARARGS,
        "Change the appearance of this CometicEdge. edge.setFormat(style, color, weight, visible)"
    },
    {"getFormat",
        reinterpret_cast<PyCFunction>( staticCallback_getFormat ),
        METH_VARARGS,
        "returns the appearance attributes of this CometicEdge.  returns tuple(style, color, weight, visible)."
    },
    {NULL, NULL, 0, NULL}		/* Sentinel */
};



/// Attribute structure of CosmeticEdgePy
PyGetSetDef CosmeticEdgePy::GetterSetter[] = {
    {"Tag",
        (getter) staticCallback_getTag,
        (setter) staticCallback_setTag, 
        "Gives the tag of the CosmeticEdge as string.",
        NULL
    },
    {NULL, NULL, NULL, NULL, NULL}		/* Sentinel */
};

// clone() callback and implementer
// PyObject*  CosmeticEdgePy::clone(PyObject *args){};
// has to be implemented in CosmeticEdgePyImp.cpp
PyObject * CosmeticEdgePy::staticCallback_clone (PyObject *self, PyObject *args)
{
    // make sure that not a null pointer is passed
    if (!self) {
        PyErr_SetString(PyExc_TypeError, "descriptor 'clone' of 'TechDraw.CosmeticEdge' object needs an argument");
        return NULL;
    }

    // test if twin object isn't already deleted
    if (!static_cast<PyObjectBase*>(self)->isValid()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }


    try { // catches all exceptions coming up from c++ and generate a python exception
        PyObject* ret = static_cast<CosmeticEdgePy*>(self)->clone(args);
        return ret;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif
}

// copy() callback and implementer
// PyObject*  CosmeticEdgePy::copy(PyObject *args){};
// has to be implemented in CosmeticEdgePyImp.cpp
PyObject * CosmeticEdgePy::staticCallback_copy (PyObject *self, PyObject *args)
{
    // make sure that not a null pointer is passed
    if (!self) {
        PyErr_SetString(PyExc_TypeError, "descriptor 'copy' of 'TechDraw.CosmeticEdge' object needs an argument");
        return NULL;
    }

    // test if twin object isn't already deleted
    if (!static_cast<PyObjectBase*>(self)->isValid()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }


    try { // catches all exceptions coming up from c++ and generate a python exception
        PyObject* ret = static_cast<CosmeticEdgePy*>(self)->copy(args);
        return ret;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif
}

// setFormat() callback and implementer
// PyObject*  CosmeticEdgePy::setFormat(PyObject *args){};
// has to be implemented in CosmeticEdgePyImp.cpp
PyObject * CosmeticEdgePy::staticCallback_setFormat (PyObject *self, PyObject *args)
{
    // make sure that not a null pointer is passed
    if (!self) {
        PyErr_SetString(PyExc_TypeError, "descriptor 'setFormat' of 'TechDraw.CosmeticEdge' object needs an argument");
        return NULL;
    }

    // test if twin object isn't already deleted
    if (!static_cast<PyObjectBase*>(self)->isValid()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }

    // test if object is set Const
    if (static_cast<PyObjectBase*>(self)->isConst()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is immutable, you can not set any attribute or call a non const method");
        return NULL;
    }

    try { // catches all exceptions coming up from c++ and generate a python exception
        PyObject* ret = static_cast<CosmeticEdgePy*>(self)->setFormat(args);
        if (ret != 0)
            static_cast<CosmeticEdgePy*>(self)->startNotify();
        return ret;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif
}

// getFormat() callback and implementer
// PyObject*  CosmeticEdgePy::getFormat(PyObject *args){};
// has to be implemented in CosmeticEdgePyImp.cpp
PyObject * CosmeticEdgePy::staticCallback_getFormat (PyObject *self, PyObject *args)
{
    // make sure that not a null pointer is passed
    if (!self) {
        PyErr_SetString(PyExc_TypeError, "descriptor 'getFormat' of 'TechDraw.CosmeticEdge' object needs an argument");
        return NULL;
    }

    // test if twin object isn't already deleted
    if (!static_cast<PyObjectBase*>(self)->isValid()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }

    // test if object is set Const
    if (static_cast<PyObjectBase*>(self)->isConst()) {
        PyErr_SetString(PyExc_ReferenceError, "This object is immutable, you can not set any attribute or call a non const method");
        return NULL;
    }

    try { // catches all exceptions coming up from c++ and generate a python exception
        PyObject* ret = static_cast<CosmeticEdgePy*>(self)->getFormat(args);
        if (ret != 0)
            static_cast<CosmeticEdgePy*>(self)->startNotify();
        return ret;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif
}

// Tag() callback and implementer
// PyObject*  CosmeticEdgePy::Tag(PyObject *args){};
// has to be implemented in CosmeticEdgePyImp.cpp
PyObject * CosmeticEdgePy::staticCallback_getTag (PyObject *self, void * /*closure*/)
{
    if (!static_cast<PyObjectBase*>(self)->isValid()){
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return NULL;
    }

    try {
        return Py::new_reference_to(static_cast<CosmeticEdgePy*>(self)->getTag());
    } catch (const Py::Exception&) {
        // The exception text is already set
        return NULL;
    } catch (...) {
        PyErr_SetString(Base::BaseExceptionFreeCADError, "Unknown exception while reading attribute 'Tag' of object 'CosmeticEdge'");
        return NULL;
    }
}

int CosmeticEdgePy::staticCallback_setTag (PyObject *self, PyObject * /*value*/, void * /*closure*/)
{
    if (!static_cast<PyObjectBase*>(self)->isValid()){
        PyErr_SetString(PyExc_ReferenceError, "This object is already deleted most likely through closing a document. This reference is no longer valid!");
        return -1;
    }

    PyErr_SetString(PyExc_AttributeError, "Attribute 'Tag' of object 'CosmeticEdge' is read-only");
    return -1;
}




//--------------------------------------------------------------------------
// Constructor
//--------------------------------------------------------------------------
CosmeticEdgePy::CosmeticEdgePy(CosmeticEdge *pcObject, PyTypeObject *T)
    : PyObjectBase(static_cast<PyObjectBase::PointerType>(pcObject), T)
{
}


//--------------------------------------------------------------------------
// destructor
//--------------------------------------------------------------------------
CosmeticEdgePy::~CosmeticEdgePy()                                // Everything handled in parent
{
    // delete the handled object when the PyObject dies
    CosmeticEdgePy::PointerType ptr = static_cast<CosmeticEdgePy::PointerType>(_pcTwinPointer);
    delete ptr;
}

//--------------------------------------------------------------------------
// CosmeticEdgePy representation
//--------------------------------------------------------------------------
PyObject *CosmeticEdgePy::_repr(void)
{
    return Py_BuildValue("s", representation().c_str());
}

//--------------------------------------------------------------------------
// CosmeticEdgePy Attributes
//--------------------------------------------------------------------------
PyObject *CosmeticEdgePy::_getattr(const char *attr)			// __getattr__ function: note only need to handle new state
{
    try {
        // getter method for special Attributes (e.g. dynamic ones)
        PyObject *r = getCustomAttributes(attr);
        if(r) return r;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return NULL;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return NULL;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return NULL;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return NULL;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return NULL;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return NULL;
    }
#endif

    PyMethodDef *ml = Methods;
    for (; ml->ml_name != NULL; ml++) {
        if (attr[0] == ml->ml_name[0] &&
            strcmp(attr+1, ml->ml_name+1) == 0)
            return PyCFunction_New(ml, this);
    }

    PyErr_Clear();
    return PyObjectBase::_getattr(attr);
}

int CosmeticEdgePy::_setattr(const char *attr, PyObject *value) // __setattr__ function: note only need to handle new state
{
    try {
        // setter for special Attributes (e.g. dynamic ones)
        int r = setCustomAttributes(attr, value);
        // r = 1: handled
        // r = -1: error
        // r = 0: ignore
        if (r == 1)
            return 0;
        else if (r == -1)
            return -1;
    } // Please sync the following catch implementation with PY_CATCH
    catch(Base::AbortException &e)
    {
        e.ReportException();
        PyErr_SetObject(Base::BaseExceptionFreeCADAbort,e.getPyObject());
        return -1;
    }
    catch(Base::Exception &e)
    {
        e.ReportException();
        auto pye = e.getPyExceptionType();
        if(!pye)
            pye = Base::BaseExceptionFreeCADError;
        PyErr_SetObject(pye,e.getPyObject());
        return -1;
    }
    catch(std::exception &e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e.what());
        return -1;
    }
    catch(const Py::Exception&)
    {
        // The exception text is already set
        return -1;
    }
    catch(const char *e)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,e);
        return -1;
    }
#ifndef DONT_CATCH_CXX_EXCEPTIONS
    catch(...)
    {
        PyErr_SetString(Base::BaseExceptionFreeCADError,"Unknown C++ exception");
        return -1;
    }
#endif

    return PyObjectBase::_setattr(attr, value);
}

CosmeticEdge *CosmeticEdgePy::getCosmeticEdgePtr(void) const
{
    return static_cast<CosmeticEdge *>(_pcTwinPointer);
}

#if 0
/* From here on come the methods you have to implement, but NOT in this module. Implement in CosmeticEdgePyImp.cpp! This prototypes 
 * are just for convenience when you add a new method.
 */

PyObject *CosmeticEdgePy::PyMake(struct _typeobject *, PyObject *, PyObject *)  // Python wrapper
{
    // create a new instance of CosmeticEdgePy and the Twin object 
    return new CosmeticEdgePy(new CosmeticEdge);
}

// constructor method
int CosmeticEdgePy::PyInit(PyObject* /*args*/, PyObject* /*kwd*/)
{
    return 0;
}


// returns a string which represents the object e.g. when printed in python
std::string CosmeticEdgePy::representation(void) const
{
    return std::string("<CosmeticEdge object>");
}

PyObject* CosmeticEdgePy::clone(PyObject *args)
{
    PyErr_SetString(PyExc_NotImplementedError, "Not yet implemented");
    return 0;
}

PyObject* CosmeticEdgePy::copy(PyObject *args)
{
    PyErr_SetString(PyExc_NotImplementedError, "Not yet implemented");
    return 0;
}

PyObject* CosmeticEdgePy::setFormat(PyObject *args)
{
    PyErr_SetString(PyExc_NotImplementedError, "Not yet implemented");
    return 0;
}

PyObject* CosmeticEdgePy::getFormat(PyObject *args)
{
    PyErr_SetString(PyExc_NotImplementedError, "Not yet implemented");
    return 0;
}



Py::String CosmeticEdgePy::getTag(void) const
{
    //return Py::String();
    throw Py::AttributeError("Not yet implemented");
}

PyObject *CosmeticEdgePy::getCustomAttributes(const char* /*attr*/) const
{
    return 0;
}

int CosmeticEdgePy::setCustomAttributes(const char* /*attr*/, PyObject* /*obj*/)
{
    return 0; 
}
#endif



