# Install script for directory: E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/SureCureSuite")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/TechDraw" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/Init.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TestTechDrawApp.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/InitGui.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/TechDraw" TYPE DIRECTORY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/Templates" FILES_MATCHING REGEX "/[^/]*\\.svg[^/]*$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/TechDraw" TYPE DIRECTORY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/PAT" FILES_MATCHING REGEX "/[^/]*\\.pat[^/]*$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/TechDraw" TYPE DIRECTORY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/LineGroup" FILES_MATCHING REGEX "/[^/]*\\.csv[^/]*$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/TechDraw" TYPE DIRECTORY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/Patterns" FILES_MATCHING REGEX "/[^/]*\\.svg[^/]*$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/Mod/TechDraw" TYPE DIRECTORY FILES "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/Symbols" FILES_MATCHING REGEX "/[^/]*\\.svg[^/]*$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/TechDraw/TDTest" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/__init__.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/DHatchTest.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/DProjGroupTest.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/DVAnnoSymImageTest.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/DVDimensionTest.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/DVPartTest.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/DVSectionTest.py"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/DVBalloonTest.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/Mod/TechDraw/TDTest" TYPE FILE FILES
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/TestHatch.svg"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/TestImage.png"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/TestSymbol.svg"
    "E:/SureCureAutomationSuite/SRC/FreeCAD/src/Mod/TechDraw/TDTest/TestTemplate.svg"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/TechDraw/App/cmake_install.cmake")
  include("E:/SureCureAutomationSuite/SRC/FreeCAD_Build/src/Mod/TechDraw/Gui/cmake_install.cmake")

endif()

