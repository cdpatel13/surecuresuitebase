
// Version Number
#define FCVersionMajor "1"
#define FCVersionMinor "0"
#define FCVersionName  "Dan Fischer"
// test: $Format:Hash (%H), Date: %ci$
#define FCRevision      "001"      //Highest committed revision number
#define FCRevisionDate  " "     //Date of highest committed revision
#define FCRepositoryURL " "      //Repository URL of the working copy

// Git relevant stuff
#define FCRepositoryHash   "bc107eacc8e020a779b2bbcb21559283ee440268"
#define FCRepositoryBranch "master"

